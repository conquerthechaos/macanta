<div>
    <h3>Macanta Tag Catgory: </h3>
    <select class="admintagpicker macanta-tag-category-select"  title="Macanta Admin Tag">

        <?php
        $SavedCategory[$CurrentMacantaTagCategoryId] = true;

        foreach($TagCategories as $Category){
            $selected = isset($SavedCategory[$Category->Id]) ? 'selected':'';
            echo "<option value='".$Category->Id."' $selected>".ucfirst($Category->CategoryName)."</option>";
        }

        ?>

    </select>


    <?php
    $DBPermissionTag = array();
    $DBPermissionTag[$DBPermissionTags->administrator] = true;
    ?>
    <div class="macanta-tags">
    <h3>Admin Tag: </h3>
    <select class="admintagpicker macanta-admin-tag"  title="Choose Tag For Macanta Admin">

        <?php
        foreach($OtherTags as $Tag){
            $selected = isset($DBPermissionTag[$Tag->Id]) ? 'selected':'';
            echo "<option value='".$Tag->Id."' $selected>".ucfirst($Tag->GroupName)."</option>";
        }

        ?>

    </select>
    <?php
    $DBPermissionTag = array();
    $DBPermissionTag[$DBPermissionTags->staff] = true;
    ?>
    <h3>User Tag: </h3>
    <select class="admintagpicker macanta-user-tag"  title="Choose Tag For Macanta User">
        <?php

        foreach($OtherTags as $Tag){
            $selected = isset($DBPermissionTag[$Tag->Id]) ? 'selected':'';
            echo "<option value='".$Tag->Id."' $selected>".ucfirst($Tag->GroupName)."</option>";
        }

        ?>
    </select>
    </div>
    <?php

    ?>
    <h3>Contact View Permissions Tag Category: </h3>
    <select class="admintagpicker macanta-contact-view-permission-cat"  title="Choose Category">
        <?php
        $SavedCategory[$this->config->item('contactview_permission_cat')] = true;

        foreach($TagCategories as $Category){
            $selected = isset($SavedCategory[$Category->Id]) ? 'selected':'';
            echo "<option value='".$Category->Id."' $selected>".ucfirst($Category->CategoryName)."</option>";
        }

        ?>
    </select>
    <h3>Saved Search Permissions Tag Category: </h3>
    <select class="admintagpicker  macanta-savedsearch-permission-cat"  title="Choose Category">
        <?php
        $SavedCategory[$this->config->item('savedsearch_permission_cat')] = true;

        foreach($TagCategories as $Category){
            $selected = isset($SavedCategory[$Category->Id]) ? 'selected':'';
            echo "<option value='".$Category->Id."' $selected>".ucfirst($Category->CategoryName)."</option>";
        }

        ?>
    </select>


</div>
<script>
    $('.admintagpicker').selectpicker({ style: 'btn-default', size: 4 });
</script>
<button class="btn btn-primary save-tags-categories"> Save</button>