<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-pad-left no-pad-right">
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-4 no-pad-left no-pad-right select-caller-id">Your Caller ID:</div>
    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-8 no-pad-left no-pad-right">
        <select  id="UserCallerIds" class="UserCallerIds" name="UserCallerIds">

            <?php
            $CallerIdSelected = 0;
            ?>
            <option value="">System Default (<?php echo $DefaultCallerId; ?>)</option>
            <option disabled>────────────────────</option>
            <?php
            $currentSelected = getCurrentUserCallerId($session_data);
            $UserCallerIds = getCurrentUserCallerIds($session_data);
            $first = $currentSelected === false ? true:false;
            foreach ($CompanyCallerIds as $CallerId){
                foreach ($UserCallerIds as $UserCallerId){
                    if($UserCallerId == $CallerId->phoneNumber ){
                        $Selected = $UserCallerId == $currentSelected ? "selected":"";
                        //$Selected = $Selected == "selected" ? "selected": $first==true? "selected":""; // disabled this as resquested
                        echo '<option class="reg-item" value="'.$CallerId->phoneNumber.'" '.$Selected.'>'.$CallerId->friendlyName.' ('.$CallerId->phoneNumber.')</option>';
                        $CallerIdSelected = $Selected === "selected" ? $CallerId->phoneNumber:$CallerIdSelected;
                        $first = false;
                    }
                }
            }
            ?>
        </select>
    </div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-pad-left no-pad-right">
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-4 no-pad-left no-pad-right select-caller-id">Outbound Device:</div>
    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-8 no-pad-left no-pad-right">
        <?php
        $currentSelected = getCurrentUserOutboundDevice($session_data);
        $UserOutboundDevices = getCurrentUserOutboundDevices($session_data);
        if($this->config->item('outbound_devices')){
            $SystemOuboundDevices = json_decode($this->config->item('outbound_devices'), true);
        }else{
            $SystemOuboundDevices = false;
        }
        ?>
        <select  id="UserOutboundDevices" class="UserOutboundDevices" name="UserOutboundDevices">
            <option value="">Browser</option>
            <!--<optgroup label="System Oubound Devices">-->
            <?php
            if($SystemOuboundDevices !== false &&  sizeof($SystemOuboundDevices) != 0){

                foreach ($CompanyCallerIds as $CallerId){
                    foreach ($SystemOuboundDevices as $SystemOuboundDevice){
                        if(!in_array($SystemOuboundDevice,$UserCallerIds)) continue;
                        if($SystemOuboundDevice == $CallerId->phoneNumber ){
                            $Selected = $CallerId->phoneNumber == $currentSelected ? "selected":"";
                            $Selected = $CallerIdSelected != $CallerId->phoneNumber ? $Selected:"";
                            echo '<option  class="reg-item"  value="'.$CallerId->phoneNumber.'" '.$Selected.'>'.$CallerId->friendlyName.' ('.$CallerId->phoneNumber.')</option>';
                        }
                    }
                }

            }else{
                echo '<option disabled>No Device Available</option>';
            }
            ?>
            <!--</optgroup>-->

            <!--<optgroup label="Your Oubound Devices">
                <?php
            //todo: add users the ability to add and manage their devices
            /*                if($UserOutboundDevices !== false && $UserOutboundDevices !== "[]"){
                                $UserOutboundDevices = json_decode($UserOutboundDevices, true);
                                foreach ($UserOutboundDevices as $UserOuboundDevice){
                                    $Selected = $UserOuboundDevice['phone_number'] == $currentSelected ? "selected":"";
                                    echo '<option value="'.$UserOuboundDevice['phone_number'].'" '.$Selected.'>'.$UserOuboundDevice['friendly_name'].' ('.$UserOuboundDevice['phone_number'].')</option>';

                                }
                            }else{
                                echo '<option disabled>You Don\'t Have Device Available</option>';
                            }
                            */?>
                    <option value="manage" class="add-outbound-call-device">Manage Your Outbound Call Devices</option>
                </optgroup>-->

        </select>
    </div>
</div>