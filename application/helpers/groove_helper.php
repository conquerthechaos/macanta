<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 09/03/16
 * Time: 2:28 PM
 */
/*Get groove_get_tickets */
if (!function_exists('groove_get_tickets'))
{
    function groove_get_tickets($Email){
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $action = "groove_get_tickets";
        $action_details = '{"email":"'.$Email.'"}';
        $GrooveTickets = rucksack_request($action, $action_details );
        return $GrooveTickets;
    }
}