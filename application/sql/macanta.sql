
SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ------------   TABLES FOR MACANTA     -----------------
-- ----------------------------
--  Table structure for `ci_sessions`
-- ----------------------------
DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  `timestamp` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `config_data`
-- ----------------------------
DROP TABLE IF EXISTS `config_data`;
CREATE TABLE `config_data` (
  `key` varchar(255) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `users_meta`
-- ----------------------------
DROP TABLE IF EXISTS `users_meta`;
CREATE TABLE `users_meta` (
  `Id` int(15) NOT NULL AUTO_INCREMENT,
  `user_id` int(15) DEFAULT NULL,
  `meta_key` varchar(50) DEFAULT NULL,
  `meta_value` text,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- ------------   TABLES FOR RUCKSACK     -----------------
-- --------------------------------------------------------

--
-- Table structure for table `api_goal`
--

CREATE TABLE IF NOT EXISTS `api_goal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `integration_name` varchar(32) NOT NULL,
  `callname` varchar(32) NOT NULL,
  `name` varchar(64) NOT NULL,
  `products` text NOT NULL,
  `category` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

-- --------------------------------------------------------

--
-- Table structure for table `calls`
--

CREATE TABLE IF NOT EXISTS `calls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `header` text NOT NULL,
  `request` text NOT NULL,
  `at` datetime NOT NULL,
  `server` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=128713 ;

-- --------------------------------------------------------

--
-- Table structure for table `failed_action`
--

CREATE TABLE IF NOT EXISTS `failed_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `queue_id` int(11) NOT NULL,
  `user` varchar(16) NOT NULL,
  `action` varchar(32) NOT NULL,
  `action_params` text NOT NULL,
  `exec_result` text NOT NULL,
  `entered` datetime NOT NULL,
  `last_ran` datetime NOT NULL,
  `mainQueueId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2339 ;

-- --------------------------------------------------------

--
-- Table structure for table `queue`
--

CREATE TABLE IF NOT EXISTS `queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mainQueueId` int(11) NOT NULL,
  `user` varchar(16) NOT NULL,
  `action` varchar(64) NOT NULL,
  `action_params` text NOT NULL,
  `exec_result` text NOT NULL,
  `entered` datetime NOT NULL,
  `last_ran` datetime NOT NULL,
  `status` varchar(16) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21923 ;

-- --------------------------------------------------------

--
-- Table structure for table `success_action`
--

CREATE TABLE IF NOT EXISTS `success_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(16) NOT NULL,
  `action` varchar(32) NOT NULL,
  `action_params` text NOT NULL,
  `exec_result` text NOT NULL,
  `entered` datetime NOT NULL,
  `last_ran` datetime NOT NULL,
  `mainQueueId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=131909 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `permission_level` varchar(32) NOT NULL,
  `meta` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

SET FOREIGN_KEY_CHECKS = 1;
