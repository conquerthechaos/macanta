<div class="col-xs-12 col-sm-12 col-md-10 col-lg-12   contact-call  twillio-phone  no-pad-left no-pad-right">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-pad-left no-pad-right btn-group    twilio_log_container" role="group" aria-label="...">

        <?php
        if(isset($ERROR)){ ?>
            <span id="twilio_log_error">ERROR: <?php echo $ERROR;?></span>
        <?php }else{
            ?>
            <span id="twilio_log">Twilio <?php echo $this->lang->line('text_status');?></span>
        <?php }
        ?>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 no-pad-left  btn-group  call" role="group" aria-label="...">
        <button type="button"
                class="btn btn-default phoneCall"
                id="dropdownMenu1"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="true"
                <?php
                if(isset($ERROR)) echo "disabled";
                ?>
        >
            <span class="fa fa-phone"></span> <?php echo $this->lang->line('text_start_phone_call');?>
        </button>
        <a type="button" class="btn btn-primary DevicephoneCall">
            <span class="fa fa-mobile"></span> Device Activated</a>
        <a type="button" class="btn btn-danger EndphoneCall">
            <span class="fa fa-phone"></span> <?php echo $this->lang->line('text_end_phone_call');?>
        </a>

        <ul class="dropdown-menu phoneCallItems col-sm-12" aria-labelledby="dropdownMenu1">
            <form lpformnum="1">
                <li>
                    <div class="input-group">
                        <span class="input-group-addon JustLabel" > <?php echo $this->lang->line('text_call_phone');?> 1: </span>
                        <input type="text" class="form-control Phone1 basic-url"
                               aria-describedby="basic-addon3"  value="<?php echo @$Contact->Phone1; ?>"  readonly>
                        <span class="input-group-addon phoneLabel CallPhone" id="basic-addon3" data-phone="Phone1">
                            <a id="CallMobile" data-href="#Call" data-phonefield="<?php echo @$Contact->Phone1Twilio; ?>" class="data-toggle-tab Phone1Twilio"><?php echo $this->lang->line('text_call');?></a>
                        </span>
                    </div>
                </li>
                <li>
                    <div class="input-group">
                        <span class="input-group-addon JustLabel"> <?php echo $this->lang->line('text_call_phone');?> 2: </span>
                        <input type="text" class="form-control Phone2 basic-url"
                               aria-describedby="basic-addon3" value="<?php echo @$Contact->Phone2; ?>" readonly>
                        <span class="input-group-addon phoneLabel CallPhone" id="basic-addon3" data-Phone="Phone2">
                             <a  id="CallLandline" data-href="#Call" data-phonefield="<?php echo @$Contact->Phone2Twilio; ?>" class="data-toggle-tab Phone2Twilio"><?php echo $this->lang->line('text_call');?></a>
                        </span>
                    </div>
                </li>
                <li><h4 class="orDial"><?php echo $this->lang->line('text_or');?> <?php echo $this->lang->line('text_dial_number');?>..</h4>

                    <div class="form-group col-sm-12 DialContainer" >
                        <div class="intl-tel-input">

                            <input type="text" id="DialedPhone" class="form-control DialedPhone"
                                   placeholder=" " data-phone="" autocomplete="off"></div>
                        <span id="valid-msg" class="hide">✓ <?php echo $this->lang->line('text_valid');?></span>
                        <span id="error-msg" class="hide"><?php echo $this->lang->line('text_invalid_number');?></span>
                    </div>

                    <button type="button" id="DialedBtn" class="btn btn-default col-sm-12 callbutton CallPhone data-toggle-tab DialContainer" data-href="#Call" data-phonefield="DialedPhone" disabled=""><?php echo $this->lang->line('text_call');?>
                    </button>

                </li>
            </form>
        </ul>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-3 no-pad-left  no-pad-right  btn-group call-ids Lazy-caller-ids" role="group" aria-label="...">
        <script>
            lazy_load("caller-ids","Caller Ids","lazyTwillioCall","core/contact/basic_info");
        </script>

    </div>
    <!--Start of e-commerce panel-->
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 no-pad-left no-pad-right">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  no-pad-left no-pad-right">
            <ul class="user-info Lazy-contact-invoice">
                <script>
                    lazy_load("contact-invoice","foo","ContactInvoice","core/contact");
                </script>

            </ul>
        </div>
    </div>

</div>
<!--Modal For Call Recorded-->
<div id="CallRecording" class="modal fade CallRecording" tabindex="-1" role="dialog" aria-labelledby="CallRecordingLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Call Recording</h4>
            </div>
            <div class="modal-body">
                <audio controls id="callrecord">
                    <source src="" type="audio/ogg" class="crecording_ogg">
                    <source src="" type="audio/mpeg" class="crecording_mp3">
                    Your browser does not support the audio element.
                </audio>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    var CallerIdLoadedValue = $("select#UserCallerIds").val();
    if(CallerIdLoadedValue !== "") $("select#UserOutboundDevices option[value='"+CallerIdLoadedValue+"']").prop( "disabled", true );

    var OutboundDeviceLoadedValue = $("select#UserOutboundDevices").val();
    if(OutboundDeviceLoadedValue !== "") $("select#UserCallerIds option[value='"+OutboundDeviceLoadedValue+"']").prop( "disabled", true );
</script>