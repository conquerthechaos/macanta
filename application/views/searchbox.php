<div class="col-xm-12 col-sm-12 col-lg-12 text ">
    <!--<a  class="btn btn-success logout" type="button" onclick="logout(); return false;">Logout</a>-->

    <div class="col-xs-12 col-sm-10 col-md-10 col-lg-8 main-search-bar input-group">
        <div class="col-xs-12 col-md-7 col-lg-7 no-pad-left no-pad-right">
            <div class="col-xs-12 col-md-5 col-lg-5 no-pad-left no-pad-right"><select id="MainStrSearch" class="search_str_options"  title="Type Your Search:">
                    <optgroup label="Search Options">
                        <option value="default:" selected>by Name</option>
                        <option value="email:" >by Email</option>
                        <option value="company:" >by Company</option>
                        <option value="address:" >by Address</option>
                        <option value="connected:" >by Connected Data</option>
                    </optgroup>
                </select>
            </div>
            <div class="col-xs-12 col-md-7 col-lg-7 no-pad-left no-pad-right">
                <div class="col-xs-10 col-lg-10 no-pad-left no-pad-right">
                    <input type="text" class="searchKey  MainSearch" id="searchKey" placeholder="<?php echo $this->lang->line('text_contact_search');?>..." data-source="MainSearch">
                </div>
                <div class="col-xs-2 col-lg-2 no-pad-left no-pad-right">
                    <button class="btn btn-default addContact " type="button"  ><i class="fa fa-user-plus" aria-hidden="true" title="Add Contact"></i></button>
                </div>
            </div>


        </div>
        <div class="col-xs-12 col-md-5 col-lg-5 no-pad-left no-pad-right">
            <div class="col-xs-12 col-md-10 col-lg-10 no-pad-left no-pad-right">
                <input type="text" class=" searchTag  MainSearch" id="searchTag" placeholder="or search by note #Tag..." data-source="MainSearch">
            </div>
            <div class="col-xs-12 col-md-2 col-lg-2 no-pad-left no-pad-right">
                <button class="btn btn-default search col-lg-12 no-pad-left no-pad-right" type="button"  data-source="MainSearch"><i class="fa fa-search" aria-hidden="true"></i> <?php echo $this->lang->line('text_go');?><!--Go-->!</button>
            </div>
        </div>

    </div>
    <div class="col-xs-12 col-sm-10 col-md-10 col-lg-8 input-group search_filter_bar">
        <select id="MainSavedSearch" class="search_filter"  title="Infusionsoft Saved Search Filter">
            <?php
            function sort_obj($a, $b)
            {
                return strcmp($a->FilterName, $b->FilterName);
            }
            $selected_searched_cache = manual_cache_loader('selected_searched_cache'.$InfusionsoftID);
            $Filters = infusionsoft_get_all_saved_search();
            $Filters = sortMultiArray($Filters,'ReportStoredName');
            $FilterGroups = array();
            $TaskSearch = false;
            if(sizeof($Filters) > 0){
                foreach($Filters as $Filter){
                    $FilterGroups[$Filter->ReportStoredName][] = $Filter;
                }
                foreach($FilterGroups as $SavedFilterGroup => $SavedFilters){
                    echo '<optgroup label="'.$SavedFilterGroup.'">';
                    usort($SavedFilters, "sort_obj");
                    foreach($SavedFilters as $SavedFilter){
                        $UserId = explode(',',$SavedFilter->UserId);
                        //$needle = array('macanta_','macanta');
                        //$FilterDisplay = str_replace($needle,'',$SavedFilter->FilterName);
                        $FilterDisplay = $SavedFilter->FilterName;
                        //if(stristr($SavedFilter->FilterName,'macanta') != false){
                            $Columns = '';
                            //$Columns = infusionsoft_get_saved_search_columns($SavedFilter->Id, $UserId[0]);
                            if(hasSavedSearchTag($SavedFilter->Id,$groups)){
                                $value = $UserId[0].':'.$SavedFilter->Id;
                                $selected = str_replace('"',"", $selected_searched_cache) == $value ? "selected":"";
                                if($selected != "" && $SavedFilterGroup == 'TaskSearch'){
                                    $TaskSearch = true;
                                }
                                echo '<option data-group="'.$SavedFilterGroup.'" data-columns="'.json_encode($Columns).'" value="'.$value.'" '.$selected.'>'.$FilterDisplay.'</option>';
                            }


                        //}
                    }
                    echo '</optgroup>';
                }
            }else{
                echo '<optgroup label="No saved search for macanta"></optgroup>';
            }

            ?>
            <!--<optgroup label="Picnic">
                <option>Mustard</option>
                <option>Ketchup</option>
                <option>Relish</option>
            </optgroup>
            <optgroup label="Camping">
                <option>Tent</option>
                <option>Flashlight</option>
                <option>Toilet Paper</option>
            </optgroup>-->

        </select>
    </div>
</div>

   <?php
   $searched_cache = manual_cache_loader('searched_cache'.$InfusionsoftID);
   $extraField = manual_cache_loader('search_xcolumn_cache'.$InfusionsoftID);
   $extracomlumn = $extraField ? "<th>$extraField<!--Email--></th>":"";
   $searched_column_cache = manual_cache_loader('searched_column_cache'.$InfusionsoftID);
   $connected_searched_cache  = manual_cache_loader('connected_searched_cache'.$InfusionsoftID);
   ?>
<div class="col-xs-12 col-sm-11 col-md-11 col-lg-10  form-box <?php echo !$searched_cache? "hideThis":''?> ">
    <h5 class="login-message"> </h5>
       <div class="search-results">
           <div class="panel panel-default ">
               <div class="panel-heading">
                   <h3 class="panel-title"><?php echo $this->lang->line('text_contact_search_result');?><!--Contact Search Result--></h3>
               </div>
               <div class="panel-body">

                   <table id="ContactsTable" class="table table-striped table-bordered <?php  echo $TaskSearch==true ?  "TaskSearch":"" ?>" cellspacing="0"
                          width="98%">
                       <thead>
                       <tr>
                           <?php
                           if($searched_column_cache && $searched_column_cache != "[]"){
                               $searched_column_cache = json_decode($searched_column_cache);
                               if($TaskSearch == true) echo "<th>Status</th>";
                               foreach($searched_column_cache as $column => $status){
                                   $column = str_replace('opt.out.search.report.field.','',$column);
                                   if($column != 'Id') echo "<th>$column</th>";
                               }
                           }else{
                               ?>
                               <th><?php echo $this->lang->line('text_name');?><!--Name--></th>
                               <th><?php echo $this->lang->line('text_company');?><!--Company--></th>
                               <th><?php echo $this->lang->line('text_email');?><!--Email--></th>
                               <?php echo $extracomlumn; ?>
                               <th><?php echo $this->lang->line('text_town_city');?><!--Email--></th>
                               <th><?php echo $this->lang->line('text_zip_postcode');?><!--Email--></th>
                               <?php
                           }
                           ?>
                       </tr>
                       </thead>
                       <tbody></tbody>
                   </table>
               </div>
           </div>
       </div>
    <!-- /input-group -->
</div>




    <?php

   if($connected_searched_cache){
       $relationships_map = macanta_get_connected_info_relationships_map();
       ?>
       <div class="col-sm-12 form-box-connected-data">
           <h5 class="login-message"> </h5>
       <?php
       $UserConnectedInfo  = json_decode($connected_searched_cache, true);
       $ConnectorTabsEncoded = $this->config->item('connected_info');
       $ConnectorTabs = json_decode($ConnectorTabsEncoded, true);
       function get_connector_group_name_by_id($Id, $ConnectorTabs){
           foreach ($ConnectorTabs as $key => $ConnectorTab){
               if($ConnectorTab['id'] == $Id) {
                    return $ConnectorTab['title'];
               }
           }
           return '';
       }

       $CSVDownLoadData=array();
       foreach($UserConnectedInfo as $GroupKey=>$tmpContactorKey)
       {
           foreach($tmpContactorKey as $Key=>$tampValue)
           {
               $CSVDownLoadData[$GroupKey][]=$Key;
           }
       }

       foreach ($UserConnectedInfo as $GUID => $Items){ ?>

           <div class="search-results search-results-connected-data">
               <div class="col-sm-12 input-group">
                   <div class="panel panel-default ">
                       <div class="panel-heading">
                           <div class="col-sm-8">
                               <h3 class="panel-title">Data Found In <?php echo get_connector_group_name_by_id($GUID, $ConnectorTabs) ?></h3>
                           </div>

                           <div class="col-sm-4 text-right">
                               <a href="javaScript:void(0)" onClick="CSVFieldsSelectionFun('<?php echo $GUID;?>','<?php echo base64_encode(json_encode($CSVDownLoadData[$GUID]));?>')"><i class="fa fa-file-excel-o" title="Download CSV File"></i></a>

                           </div>                       </div>
                       <div class="panel-body">

                           <table id="ConnectedDataTable" class="table table-striped table-bordered conencted-search" cellspacing="0"
                                  width="98%">
                               <thead>
                               <tr>
                               <?php
                               $Order = [];
                               $OrderId = [];
                               $UserValue = [];
                               $FieldIdNamePair = [];
                               $FieldIdNamePairAll = [];
                               foreach ($ConnectorTabs as $key => $ConnectorTab){
                                   if($ConnectorTab['id'] == $GUID) {
                                       $UserValue = isset($UserConnectedInfo[$ConnectorTab['id']]) ? $UserConnectedInfo[$ConnectorTab['id']]:[];
                                       foreach ($ConnectorTab['fields'] as $field) {
                                           if ($field["showInTable"] == "yes") {
                                               $key = (int)$field["showOrder"];
                                               if (isset($Order[$key])) {
                                                   while (isset($order[$key])) {
                                                       $key++;
                                                   }
                                                   $Order[$key] = $field["fieldLabel"];
                                                   $OrderId[$key] = $field["fieldId"];
                                                   $FieldIdNamePair[$field["fieldId"]] = $field["fieldLabel"];
                                               } else {
                                                   $Order[$key] = $field["fieldLabel"];
                                                   $OrderId[$key] = $field["fieldId"];
                                                   $FieldIdNamePair[$field["fieldId"]] = $field["fieldLabel"];
                                               }
                                           }
                                           $FieldIdNamePairAll[$field["fieldId"]] = $field["fieldLabel"];
                                       }
                                       ksort($Order);
                                       ksort($OrderId);

                                       if(sizeof($Order) > 0){
                                           foreach ($Order as $field){
                                               echo "<th>$field</th>";
                                           }
                                       }else{
                                           $count = 0;
                                           foreach ($ConnectorTab['fields'] as $field){
                                               $count++;
                                               $OrderId[] = $field["fieldId"];
                                               $FieldIdNamePair[$field["fieldId"]] = $field["fieldLabel"];
                                               echo "<th>$field[fieldLabel]</th>";
                                               if($count == 3) break;
                                           }
                                       }
                                   }
                               }

                               ?>
                                   <th>Other Details</th>
                                   <th>Connected Contacts</th>
                               </tr>
                               </thead>
                               <tbody>
                               <?php


                               $html = '';
                               $ContactArr = [];
                               foreach ($UserValue as $itemId => $ValuesArr){
                                   $UserField = $ValuesArr['value'];
                                   $ConnectedContacts = $ValuesArr['connected_contact'];
                                   $ContactDisplay = '';
                                   foreach($ConnectedContacts as $index => $ContactInfo){
                                       $theRalation = [];
                                       foreach ($ContactInfo["relationships"] as $relationId){
                                           $theRalation[] = ucfirst($relationships_map[$relationId]);
                                       }
                                       $ContactArr[$ContactInfo['ContactId']] = "$ContactInfo[FirstName] $ContactInfo[LastName]";
                                       $relationships = implode(',',$theRalation);
                                        $ContactDisplay .= "<span class='contacts-connected'><a href='#contact/".$ContactInfo['ContactId']."'>$ContactInfo[FirstName] $ContactInfo[LastName] ($ContactInfo[Email]) <small>Relationship: $relationships</small></a></span>";
                                   }
                                   $OtherDetails ='';
                                   foreach ($UserField as $fieldId => $fieldValue){
                                       if (!array_key_exists($fieldId,$FieldIdNamePair)){
                                           if(is_array($fieldValue)){
                                               $temp = [];
                                               foreach ($fieldValue as $theContactId=>$value){
                                                   if(!$value) continue;
                                                   $theContactId = str_replace('id_','',$theContactId);
                                                   $temp[] = '<span class="other-details"><small><strong>'.$ContactArr[$theContactId].":</strong> ".$value.'</small></span>';
                                               }
                                               $tempStr = implode(' ',$temp);
                                               $OtherDetails .= "<span class='other-details'><strong>$FieldIdNamePairAll[$fieldId]:</strong>$tempStr</span>";
                                           }else{
                                               $OtherDetails .= "<span class='other-details'><strong>$FieldIdNamePairAll[$fieldId]:</strong> $fieldValue</span>";
                                           }
                                       }
                                       //$OtherDetails .= "<span class='other-details'><strong>$FieldIdNamePair[$fieldId]</strong>: $fieldValue</span>";
                                   }
                                   $ConnectedContactsArr =  json_decode($ConnectedContacts, true);
                                   $html .=  "<tr class='connected-search-result-row' data-itemid='$itemId'  data-connectedcontacts ='".str_replace("=","",base64_encode(json_encode($ConnectedContacts)))."' data-raw ='".str_replace("=","",base64_encode(json_encode($UserField)))."'>";
                                   foreach ($OrderId as $fieldName){
                                       $html .=  "<td>$UserField[$fieldName]</td>";
                                   }
                                   $OtherDetails = trim($OtherDetails) == '' ? "- no other details -":$OtherDetails;
                                   $html .=  "<td>".$OtherDetails."</td>";
                                   $html .=  "<td>".$ContactDisplay."</td>";
                                   $html .=  "</tr>";
                               }
                               echo $html;
                               ?>

                               </tbody>
                           </table>
                       </div>
                   </div>

               </div>
           </div>

      <?php  }
       ?>
    <script>
        search_xcolumn_cache = "<?php echo $extraField ? $extraField:""; ?>";
        var ConnectedDataTable = $(".conencted-search").DataTable({
            //var itemFootnote = '<small class="footnote">Contact Id: '+theContact.Id+'</small>'
            "pageLength": 10,
            "paging":   true,
            "lengthChange": false,
            "searching": false,
            "info":     false,
            "createdRow": function ( row, data, index ) {
            }
        });
    </script>
       <!-- /input-group -->
       </div>
   <?php }
   ?>
<script>
    var search_xcolumn_cache = "<?php echo $extraField ? $extraField:""; ?>";
</script>
<input type="hidden" id="CurrentConnectorId"/>
<input type="hidden" id="CurrentConnectorDownLoadType" value=""/>

<!-- Modal -->
<div id="CSVFieldsSelection" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" style="width: 600px;">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="font-size:16px;">Please select fields to download CSV</h4>
            </div>

            <div class="modal-body ConnectedInfoSettingsContainer" style="text-align:left;">
            </div>

        </div>

    </div>
</div>
<style>
    .ConnectedInfoSettingsContainer
    {
        text-align:left;
    }
    .ConnectedInfoSettingsContainer h3
    {
        display:none;
    }
</style>


<?php
/*$MediaPresentation = $this->config->item('MediaPresentation');
if(macanta_check_viewed_media_presentation($InfusionsoftID,$MediaPresentation) == false){
    if($MediaPresentation['Type'] == 'wistia'){
        echo '<script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/E-v1.js" async></script>';
        echo '<div class="wistia_embed wistia_async_'.$MediaPresentation['MediaId'].' popover=true autoPlay=true popoverBorderWidth=2 popoverShowOnLoad=true" style="width:30px;height:30px;position: fixed; opacity: 0;">&nbsp;</div>';
    }else{
        echo '<script>';
        echo '$("#MediaPresentation").modal("show");';
        echo '$("#MediaPresentation").on("hidden.bs.modal",function(){ $("#iframeYoutube").attr("src","#"); })';
        echo '</script>';
    }

}*/
?>