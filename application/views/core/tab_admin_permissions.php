<div class="col-md-12 theNotePanel">
    <div class="row mainbox-top">
        <!--<div class="col-md-6">
            <div class="panel panel-primary left-Permissions">
                <div class="panel-heading">

                    <h3 class="panel-title "><i class="fa fa-key"></i> Access</h3>

                </div>
                <div class="panel-body admin-panelBody admin-panelBody-access">

                    <script>
                        getTagCatSelection("left-Permissions", "admin-panelBody-access");
                    </script>
                </div>
            </div>

        </div>-->
        <?php
        $passwordFile = dirname(APPPATH).DIRECTORY_SEPARATOR."phpliteadmin.txt";
        $passwordArr = json_decode(file_get_contents($passwordFile),true);
        $password = $passwordArr['password'];
        $NoteEditingPermissions = $this->config->item('note_editing_permission');
        ?>
        <div class="col-md-6 tab-panel">
            <div class="panel panel-primary left-Permissions">
                <div class="panel-heading">
                    <h3 class="panel-title "><i class="fa fa-lock" aria-hidden="true"></i> Security</h3>
                </div>
                <div class="panel-body admin-panelBody admin-panelBody-security">
                    <div class="form-group">
                        <label class="control-label securityLabel" for="inputSuccess1">HTTP Post Key</label>
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1"><i class="fa fa-key" aria-hidden="true"></i></span>
                            <input type="text" class="form-control PostKey"  aria-describedby="basic-addon1" readonly value="<?php echo $password; ?>">
                            <span class="btn btn-primary input-group-addon copyPostKey">copy</span>
                        </div>
                        <span class="help-block" id="helpBlock2">
                            Use this key for sending HTTP Post from Infusionsoft.
                        </span>
                    </div>
                    <div class="form-group NoteEditingPermissionsContainer">
                        <label class="control-label securityLabel" for="NoteEditingPermissions">Note Editing Permission</label>
                        <div class="input-group NoteEditingPermissionsBody">
                            <span class="input-group-addon" id="basic-addon1"><i class="fa fa-tag" aria-hidden="true"></i></span>
                            <input id="NoteEditingPermissions"   class="form-control NoteEditingPermissions"  aria-describedby="basic-addon1" value="<?php echo $NoteEditingPermissions; ?>">
                            <span class="btn btn-primary input-group-addon" onclick="saveNoteEditingPermissions();">Save</span>
                        </div>
                        <span class="help-block" id="helpBlock2">
                            Enter the tag id that will give users permission to edit contact notes.
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 tab-panel">
            <div class="panel panel-primary right-Permissions">
                <div class="panel-heading">
                    <h3 class="panel-title "><i class="fa fa-search-plus"></i> Saved Searches</h3>
                    <button type="button" class=" col-xs-1 btn btn-default  refreshSavedSearch"><i class="fa fa-refresh"></i>
                        Refresh
                    </button>
                </div>
                <div class="panel-body admin-panelBody admin-panelBody-savedsearch">
                    <script>
                        getSavedSearchTag("right-Permissions", "admin-panelBody-savedsearch");
                    </script>
                </div>
            </div>
        </div>



    </div>
    <div class="row">
    </div>
</div>