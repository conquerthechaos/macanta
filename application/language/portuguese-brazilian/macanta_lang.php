<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 21/07/16
 * Time: 1:34 PM
 */
$lang['text_welcome_to_macanta'] = 'Bem-vindo ao macanta';
$lang['text_login'] = 'Entrar';
$lang['text_please_fill_in'] = 'Por favor preencha o formulário abaixo';
$lang['text_email'] = 'O email';
$lang['text_password'] = 'Senha';
$lang['text_fed_by'] = 'alimentado por';
$lang['text_powered_by'] = 'distribuído por';
$lang['text_or'] = 'ou';
$lang['text_thanks_for_visiting_google'] = 'Obrigado por visitar macanta.
Infelizmente, o endereço de e-mail, há o registro para você é diferente da que apenas dada a nós pelo Google.
Por favor, indique o seu endereço de e-mail e senha no lado esquerdo e clique em "Ligar-me em ...".
Se você ainda tiver dificuldade em efetuar login, entre em contato com o administrador do sistema e pedir-lhes para verificar o seu registro de contato Infusionsoft. Obrigado.';

$lang['text_oops_looks_like'] = 'Ops! Parece que há um ligeiro problema nosso fim com a forma como a sua conta está configurada.
Entre em contato com o administrador do sistema e pedir-lhes para verificar o seu registro de contato Infusionsoft. Obrigado."';

$lang['text_thanks_for_visiting'] = 'Obrigado por visitar macanta.
Infelizmente, o endereço de e-mail, há o registro para você é diferente do que você acabou de inserir.
Por favor, indique correctamente o seu endereço de e-mail e senha e clique em "Ligar-me em ...".
Se você ainda tiver dificuldade em efetuar login, entre em contato com o administrador do sistema e pedir-lhes para verificar o seu registro de contato Infusionsoft. Obrigado.';
$lang['text_loading'] = 'Carregando';
$lang['text_please_wait'] = 'Por favor, espere';
$lang['text_contact_search'] = 'Contato Buscar';
$lang['text_contact_search_result'] = 'Contato Resultado da pesquisa';
$lang['text_show'] = 'Exposição';
$lang['text_entries'] = 'Entradas';
$lang['text_name'] = 'Nome';
$lang['text_company'] = 'companhia';
$lang['text_phone'] = 'Telefone';
$lang['text_shipping'] = 'Remessa';
$lang['text_billing'] = 'Faturamento';
$lang['text_show_shipping_instead'] = 'mostrar envio vez';
$lang['text_show_billing_instead'] = 'mostrar faturamento vez';
$lang['text_no_data_available'] = 'Sem dados disponíveis na tabela';
$lang['text_previous'] = 'Anterior';
$lang['text_next'] = 'Próximo';
$lang['text_go'] = 'Ir';
$lang['text_address'] = 'Endereço';
$lang['text_search_for'] = 'Procurar por';
$lang['text_view_recent_results'] = 'Ver resultados recentes';
$lang['text_total_invoiced'] = 'total facturado';
$lang['text_amount_owed'] = 'montante em dívida';
$lang['text_call'] = 'Ligar';
$lang['text_note'] = 'Nota';
$lang['text_triggers'] = 'Gatilhos';
$lang['text_ready'] = 'Pronto';
$lang['text_edit_contact_details'] = 'Editar Detalhes de contato';
$lang['text_status'] = 'estado';
$lang['text_error'] = 'Erro';
$lang['text_start_phone_call'] = 'Comece Phone Call';
$lang['text_end_phone_call'] = 'Phone Call End';
$lang['text_previous_calls'] = 'chamadas anteriores';
$lang['text_current_call'] = 'chamada em curso';
$lang['text_edit_note'] = 'Editar Nota';
$lang['text_edit'] = 'Editar';
$lang['text_appointment'] = 'Compromisso';
$lang['text_task'] = 'Tarefa';
$lang['text_no_call_record'] = 'nenhum registro de chamadas';
$lang['text_call_recording'] = 'gravação de chamadas';
$lang['text_no_tags'] = 'não há tags';
$lang['text_add_note_tag'] = 'adicionar tag nota';
$lang['text_create_note'] = 'Criar nota';
$lang['text_call_note'] = 'Nota chamada';
$lang['text_contact_notes'] = 'Contacto Notas';
$lang['text_search_and_filter'] = 'Pesquisar e filtrar Notes';
$lang['text_search_note_content'] = 'Pesquisa Nota conteúdo';
$lang['text_add_tag_filter'] = 'adicionar filtro de tag';
$lang['text_choose_staff_member'] = 'Escolha membro da equipe';
$lang['text_quick_note'] = 'Nota rápida';
$lang['text_by_date'] = 'Por data';
$lang['text_by_note_tag'] = 'Por Nota #tag';
$lang['text_by_staff_member'] = 'Pelo membro do pessoal';
$lang['text_hidden_from_client'] = 'Oculto do cliente?';
$lang['text_triggers'] = 'Gatilhos';
$lang['text_apply_triggers'] = 'aplicar Triggers';
$lang['text_address1'] = 'Endereço 1';
$lang['text_address2'] = 'Endereço 2';
$lang['text_town_city'] = 'Cidade';
$lang['text_county_state'] = 'Estado';
$lang['text_zip_postcode'] = 'Codigo postal';
$lang['text_country'] = 'País';
$lang['text_firstname'] = 'Primeiro nome';
$lang['text_lastname'] = 'Último nome';
$lang['text_streetaddress1'] = 'Endereço 1';
$lang['text_streetaddress2'] = 'Endereço 2';
$lang['text_close'] = 'Fechar';
$lang['text_save'] = 'Salvar';
$lang['text_contact_details'] = 'Detalhes do contato';
$lang['text_add_contact'] = 'Add Contact';
$lang['text_save_notes'] = 'guardar notas';
$lang['text_login_successful'] = 'Login bem-sucedido';
$lang['text_log_me_in'] = 'Logmein';
$lang['text_logout'] = 'Sair';
$lang['text_type_your_note_here'] = 'Digite suas notas aqui';
$lang['text_dial_number'] = 'Clique aqui para inserir manualmente um número de telefone';
$lang['text_valid'] = 'Válido';
$lang['text_invalid_number'] = 'Número inválido';
$lang['text_call_phone'] = 'telefone chamada';
$lang['text_create_note'] = 'Criar nota';
$lang['text_please_wait_loading'] = 'Por favor, espere. Carregando';
$lang['text_words'] = 'Palavras';
$lang['text_legacy_notes'] = 'Notas legados';
$lang['text_legacy'] = 'Legado';
$lang['text_cancel'] = 'Cancelar';
$lang['text_tour_login'] = 'Entrar com um endereço de e-mail do seu aplicativo Infusionsoft que tenha uma senha ea tag de usuário macanta correta';
$lang['text_tour_search'] = 'Digite um primeiro nome, último nome ou o endereço de e-mail e clique em IR';
$lang['text_tour_click_search_result'] = 'Clique em um resultado de pesquisa para abrir o contato em macanta';
$lang['text_tour_edit_contact'] = 'Clique aqui para editar os dados de contacto';
$lang['text_tour_switch_address'] = 'Use esta opção para alternar entre os endereços de cobrança e envio';
$lang['text_tour_linked_contact'] = 'Clique aqui para abrir o contato em Infusionsoft';
$lang['text_tour_start_call'] = 'Clique aqui para iniciar uma chamada de telefone e clique em \'Chamar Telefone 1\' ou \'Chamar Telefone 2\'';
$lang['text_tour_enter_notes'] = 'Inserir notas aqui, tanto para chamadas de entrada e saída';
$lang['text_tour_create_notes'] = 'Você deve clicar em \'Criar Nota\' em chamadas de saída para salvar a gravação de chamadas';
$lang['text_current_action_plan'] = 'Current Action Plan';
$lang['text_sbsm'] = 'SBSM';
$lang['text_no_contact_id'] = "Ops! Esta pesquisa salva não contém um ID de contato, então eu não sei qual o contato para exibir!\\n\\rPor favor, adicionar o ID de contato para as colunas da pesquisa salva em Infusionsoft, guardá-lo e re-selecionar a pesquisa salva nesta página.\\n\\rObrigado.";