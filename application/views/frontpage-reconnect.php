<script>
    var DevicePlatform = "<?php echo $browser['platform']; ?>";
    console.log("Platform: " + DevicePlatform);
</script>
<?php
if ($browser['platform'] == 'iOS' || $browser['platform'] == "Android"){
    ?>
    <style>
        @media (min-width: 768px){
            .container {
                width: inherit;
            }
            .searchCall.user-info-b.col-sm-3 {
                max-width: 200px;
            }
            .user-info-b.col-sm-3.AddressSlider {
                width: 270px;
            }
            .user-info-b.col-sm-4 {
                margin-top: 5px;
                width: 270px;
            }
        }
    </style>
    <?php
}
?>
<div class="container front-page-logo" <?php echo $class_alignleft; ?>>
    <?php
    $SiteLogo =$this->config->item('sitelogo');
    if($SiteLogo){ ?>
        <img class="loginLogo" src="<?php echo $this->config->item('base_url');?>assets/custom_img/logo/<?php echo $SiteLogo;?>" style=" " alt="">
    <?php }else{ ?>
        <img class="loginLogo" src="<?php echo $this->config->item('base_url');?>assets/img/macanta_logo.png" style=" " alt="">
    <?php }
    ?>
    <div class="description">
        <p>
            <!--Welcome to  <strong>macanta</strong>-->
            <button  class="btn btn-default logout logout-from-dashboard text_logout" type="button" onclick="logout(); return false;"><?php echo $this->lang->line('text_logout');?><!--Logout--></button>
        </p>
    </div>
</div>
<div class="front-page">

        <div class="row front-page-body">
                <?php echo $FrontPage; ?>

        </div>
        <div class="footnote "><span class="text_fed_by"><?php echo $this->lang->line('text_fed_by');?></span> <!--fed by--> <strong><img class="isicon" src="<?php echo $this->config->item('base_url');?>assets/img/isicon.png" style=" " alt="">Infusionsoft</strong> | <span class="text_powered_by"><?php echo $this->lang->line('text_powered_by');?></span> <!--powered by--> <img class="macantaicon" src="<?php echo $this->config->item('base_url');?>assets/img/macantaicon.png" style=" " alt="">macanta</div>

    <div class="app_version">
        <?php

            //$FileLocVersion = basename(dirname(dirname(dirname(__FILE__))));
            //echo $FileLocVersion;
            echo $this->config->item('macanta_verson');
        ?>
    </div>
    <div class="reconnectFootnote"><a href="<?php echo $this->config->item('base_url');?>reconnect/token" class="btn btn-danger">Click Here To Securely Connect Your Infusionsoft App</a></div>
</div>