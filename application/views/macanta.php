<?php
//This is the main macanta template wrapper.
ob_start("ob_gzhandler");
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv='cache-control' content='no-cache'>
    <meta http-equiv='expires' content='0'>
    <meta http-equiv='pragma' content='no-cache'>
    <title><?php echo $this->config->item('sitename'); ?></title>
    <meta content="<?php echo $this->config->item('sitename'); ?>" property="og:title">
    <meta content="Infusionsoft&reg; | Simplified" name="description">
    <meta content="Macanta" property="og:site_name">
    <meta content="product" property="og:type">
    <meta content="<?php echo $this->config->item('base_url');?>" property="og:url">
    <meta content="<?php echo $this->config->item('base_url');?>assets/img/macantaicon.png" property="og:image">
    <meta content="<?php echo $this->config->item('base_url');?>assets/img/macantaicon.png" name="twitter:image">
    <link rel='mask-icon' href='<?php echo $this->config->item('base_url');?>assets/img/macantaicon.png' color='#7BBE4A'>
    <?php echo assets_css($stylesheets, array('v'=>$this->config->item('macanta_verson'))); ?>
    <link rel="shortcut icon" href="<?php echo $this->config->item('base_url');?>assets/ico/favicon.png?v=2">
    <script type="application/javascript" >
        var serviceWorkerRegistration;
        var ajax_url = "<?php echo base_url().'index.php/ajax'?>";
        var session_name = localStorage.getItem("session_name") || '';
        var assetsVersion = localStorage.getItem("asset_version");
        var serverAssetsVersion = "<?php echo $this->config->item('macanta_verson');?>";
        if(assetsVersion === null){
            localStorage.setItem("asset_version",serverAssetsVersion);
            assetsVersion = serverAssetsVersion;
        }
        var ajax_requests = {};
        var SearchCache = {};
        var SelectedSearchCache = '';
        var DataTableColumn = {};
        var UserAccess = {};
        var OppTableData=[];
        var OppTable={};
        var ContactsTable;
        var ContactsDataTable;
        var ContactId; // Infustionsoft ContactId
        var ContactInfo = {};
        var UserCallerIds = {};
        /*Javascript Text Language replacement*/
        var MacantaLanguages = {};
        var myDataTable = {};
        var ZuoraDataObj = {};
        var allFilters = [];
        var allFiltersStr = '';
        var CustomEditorInit = false;
        var FilterResults = {};
        var LoqateRequest = '';
        var PhoneValidation = {};
        var FilteredContactId = [];
        var UserConnectorInfoTableFilterResult;
        var EmailValidation = {};
        var BillingAddressValidated = '';
        var ShippingAddressValidated = '';
        MacantaLanguages['please_wait_loading'] = '<?php echo $this->lang->line('text_please_wait_loading'); ?>';
        MacantaLanguages['text_add_note_tag'] = '<?php echo $this->lang->line('text_add_note_tag'); ?>';
        MacantaLanguages['text_add_tag_filter'] = '<?php echo $this->lang->line('text_add_tag_filter'); ?>';
        MacantaLanguages['text_words'] = '<?php echo $this->lang->line('text_words'); ?>';
        MacantaLanguages['text_show'] = '<?php echo $this->lang->line('text_show'); ?>';
        MacantaLanguages['text_entries'] = '<?php echo $this->lang->line('text_entries'); ?>';
        MacantaLanguages['text_previous'] = '<?php echo $this->lang->line('text_previous'); ?>';
        MacantaLanguages['text_next'] = '<?php echo $this->lang->line('text_next'); ?>';
        MacantaLanguages['text_no_contact_id'] = "<?php echo $this->lang->line('text_no_contact_id'); ?>";

    </script>
    <?php
    if($mobile == 'yes') echo '<meta name="viewport" content="width=device-width">';
    ?>

</head>
<body id="macanta-body" class="macanta-body">
<div id="fountainG">
    <div class='fountainText'><?php echo $this->lang->line('text_loading');?>...</div>
    <div id="fountainG_1" class="fountainG"></div>
    <div id="fountainG_2" class="fountainG"></div>
    <div id="fountainG_3" class="fountainG"></div>
    <div id="fountainG_4" class="fountainG"></div>
</div>
<div class="container">
    <div class="row">
        <?php echo $content; ?>
    </div>
</div>
<!--<script type="text/javascript" src="//static.twilio.com/libs/twiliojs/1.2/twilio.min.js"></script>-->
<script type="text/javascript" src="//media.twiliocdn.com/sdk/js/client/v1.3/twilio.min.js"></script>
<script type="text/javascript" src="//media.twiliocdn.com/taskrouter/js/v1.12/taskrouter.min.js"></script>
<script type="text/javascript" src="//media.twiliocdn.com/sdk/js/sync/v0.7/twilio-sync.min.js"></script>
<?php echo assets_js($javascripts, array('v'=>$this->config->item('macanta_verson'))) ?>
<script>
    /* Hopscotch - to be implemented*/
    var calloutMgr = hopscotch.getCalloutManager();

    /*FROM macanta_utilty.js.txt*/
<?php
if (file_exists(FCPATH."assets/custom_js/macanta_utility.js.txt")) {
    $theScript = file_get_contents(FCPATH."assets/custom_js/macanta_utility.js.txt");
    echo $theScript;
}
?>
<?php
    $write_code = is_hosted();
    if ($write_code){  ?>
    <!-- BEGIN zohostatic CODE -->
    var zsFeedbackTabPref = {};
    zsFeedbackTabPref.tabTitle = "Click Here For Help";
    zsFeedbackTabPref.tabColor = "#78bf42";
    zsFeedbackTabPref.tabFontColor = "#FFFFFF";
    zsFeedbackTabPref.tabPosition = "Right";
    zsFeedbackTabPref.tabOffset = "526";
    zsFeedbackTabPref.display = "popout";
    zsFeedbackTabPref.srcDiv = "zsfeedbackwidgetdiv";
    zsFeedbackTabPref.defaultDomain = "https://desk.zoho.com";
    zsFeedbackTabPref.feedbackId = "3c4ce33b05207de385e99c47a0352837afb6719abeecaa48";
    zsFeedbackTabPref.fbURL = "https://desk.zoho.com/support/fbw?formType=AdvancedWebForm&fbwId=3c4ce33b05207de385e99c47a0352837afb6719abeecaa48&xnQsjsdp=bLSN-BymkY1vJE5CPjqIkA$$&mode=showWidget&displayType=popout";
    zsFeedbackTabPref.jsStaticUrl = "https://js.zohostatic.com/support/fbw_v8/js";
    zsFeedbackTabPref.cssStaticUrl = "https://css.zohostatic.com/support/fbw_v8/css";

    var feedbackInitJs = document.createElement("script");
    feedbackInitJs.type = "text/javascript";
    feedbackInitJs.src = "https://js.zohostatic.com/support/fbw_v8/js/zsfeedbackinit.js";
    window.jQueryAndEncoderUrl = "https://js.zohostatic.com/support/app/js/jqueryandencoder.ffa5afd5124fbedceea9.js";
    window.loadingGifUrl = "https://img.zohostatic.com/support/app/images/loading.8f4d3630919d2f98bb85.gif";
    document.head.appendChild(feedbackInitJs);
    var feedbackWidgetCss = document.createElement("link");
    feedbackWidgetCss.setAttribute("rel", "stylesheet");
    feedbackWidgetCss.setAttribute("type", "text/css");
    feedbackWidgetCss.setAttribute("href", "https://css.zohostatic.com/support/app/css/ZSFeedbackPopup.40fce22de60367cedd8b.css");
    document.head.appendChild(feedbackWidgetCss);
    <!-- END zohostatic CODE -->
    <?php }
    ?>
</script>
<div id="ytWidget"></div><script src="https://translate.yandex.net/website-widget/v1/widget.js?widgetId=ytWidget&pageLang=en&widgetTheme=light&trnslKey=trnsl.1.1.20170324T105515Z.cd22b0b89d711c43.893727e9ddd730237b7eed44a266acbf77c5b81c&autoMode=true" type="text/javascript"></script>
<script>
    /* SERVICE WORKER */
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('macanta-sw.js')
            .then(function(registration) {
                console.log('Service Worker registration successful with scope: ', registration.scope);
                serviceWorkerRegistration = registration;
                window.addEventListener("keydown", function (event) {
                    if ((event.shiftKey && event.ctrlKey && event.keyCode === 82) || (event.shiftKey && event.metaKey && event.keyCode === 82)) {
                        // 82 = r
                        event.preventDefault();
                        console.log('Updating Serivce Worker!');
                        registration.unregister();
                        caches.delete('macanta-cache');
                        setTimeout(function(){  window.location.reload(true); }, 2000);

                    } else if ((event.ctrlKey && event.keyCode === 82) || (event.metaKey && event.keyCode === 82)) {
                        // 82 = r
                        window.location.reload(true);
                    }

                }, true);
            })
            .catch(function(err) {
                console.log('Service Worker registration failed: ', err);
            });
    }

</script>
</body>
</html>
<?php
ob_end_flush();
?>