/*
 Navicat Premium Data Transfer

 Source Server         : MacantaSQLIte
 Source Server Type    : SQLite
 Source Server Version : 3008004
 Source Database       : main

 Target Server Type    : SQLite
 Target Server Version : 3008004
 File Encoding         : utf-8

 Date: 07/10/2017 10:28:42 AM
*/

PRAGMA foreign_keys = false;

-- ----------------------------
--  Table structure for connected_data
-- ----------------------------
DROP TABLE IF EXISTS "connected_data";
CREATE TABLE "connected_data" (
	 "id" integer(11,0),
	 "group" text,
	 "item" text,
	 "value" text,
	 "connected_contact" text,
	 "meta" text
);

PRAGMA foreign_keys = true;
