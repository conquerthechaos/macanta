<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';
require_once (APPPATH."libraries/PhoneNumberValidation_Interactive_Validate_v2_20.php");
//require APPPATH . '/libraries/Format.php';
//require BASEPATH . '/libraries/Session/Session.php';
/*if ( ! class_exists('CI_Session'))
{
    load_class('Session', 'libraries/Session');
}*/
class Ajax extends REST_Controller {
    public $OpenCalls = array(
        'login',
        'frontpage',
        'dashboard',
        'draw'
    );
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->config('version');
    }
    public function index_get(){
        $this->response('Forbidden!');
    }
    public function http_get(){
        print_r($this->get());
        $this->response($this->get());
    }
    public function http_post(){
        print_r($this->post());
        $this->response($this->post());
    }
    public function saved_filter_get(){
        $this->response(infusionsoft_get_all_saved_search());
    }
    public function columns_get(){
        //get all the columns in saved search
        //426505:2319
        $this->response(infusionsoft_get_saved_search_columns(2319,426505));
    }
    public function webform_get(){
        $HTML = infusionsoft_get_web_form('exela','dd116c64e31306b08581e35b152b4488');
        $this->response($HTML);
    }
    public function tagcat_get(){
        $TagCategories = infusionsoft_get_tag_categories();
        $this->response($TagCategories);
    }
    public function emailtpl_get(){
        $EmailTpl = infusionsoft_get_email_template_content("150");
        $this->response($EmailTpl);
    }
    public function webform_post(){
        $PostData = $this->post();
        $app = $PostData['app'];
        $formId = $PostData['formId'];
        $HTML = infusionsoft_get_web_form($app,$formId);
        $this->response($HTML);
    }
    public function saved_search_get($savedSearchID, $userID){
        $this->response(infusionsoft_get_all_saved_search_contact($savedSearchID, $userID, 0));
    }
    public function index_post(){
        header('Content-Type: text/html; charset=UTF-8');
        define('AJAX_REQUEST',true);
        $PostData = $this->post();
        $Controller = $PostData['controler'];
        $Action = $PostData['action'];
        $Data = isset($PostData['data']) ? $PostData['data']:[];
        if($Action == 'saveCDFileAttachement') $Data = json_decode($PostData['data'], true);
        $SessionName = $Data['session_name'];
        $AssetsVersion = $Data['assetsVersion'];
        $user_seession_data = empty($SessionName) ? false:macanta_get_user_seession_data($SessionName);


        if (!empty($AssetsVersion) && $AssetsVersion != $this->config->item('macanta_verson')){
            //New Deployed Update: Logout and give App Update Notice
            $Action = "logout";
            $Controller = "core/common";
            $message = "<h4>Macanta has been successfully updated to version ".$this->config->item('macanta_verson')."</h4>";
            $this->db->where('session_name',$SessionName);
            $this->db->delete('user_sessions');
            $asset_version = $this->config->item('macanta_verson');
            $CacheRefreshScript = 'if(typeof serviceWorkerRegistration !== "undefined"){ serviceWorkerRegistration.unregister();}caches.delete("macanta-cache");localStorage.setItem("asset_version", "'.$asset_version.'");';
            $CacheRefreshScript .= "localStorage.setItem('LoginMessage', '$message');";
            $Data['script'] = $CacheRefreshScript;
            $Data['reload'] = true;
        }else{
            if ($Action != 'login'){

                if($user_seession_data == false && !empty($SessionName)){
                    //Login Expired: Logout and give App Login Expired Notice
                    $Action = "logout";
                    $Controller = "core/common";
                    $message = '<h4 class="warning-msg">Your login session has expired. Please login again.</h4>';
                    $CacheRefreshScript = '';
                    $CacheRefreshScript .= "localStorage.setItem('LoginMessage', '$message');";
                    $Data['script'] = $CacheRefreshScript;
                    $Data['reload'] = true;

                }elseif($user_seession_data == false && empty($PostData['session_name'])){
                    //Show Login: No Notice
                    $Action = "logout";
                    $Controller = "core/common";
                    $Data['reload'] = false;
                    $login_disabled =  $this->config->item('login_disabled');
                    if ($login_disabled && trim($login_disabled)!=''){
                        $Data['login_disabled'] = $login_disabled;
                        $Data['script'] = '$(".login-message").fadeIn();';
                    }

                }

            }
        }


        $loadedController =  $this->load->controller($Controller);
        if(method_exists($this->$loadedController, $Action)){
            $return = $this->$loadedController->$Action($Data);
            $this->response($return, REST_Controller::HTTP_OK);
        }else{

            $this->response('POST: No Method Existing-> '.$Action ." in ". $Controller, REST_Controller::HTTP_OK);
        }

    }
    public function data_table_post(){
        header('Content-Type: application/json; charset=UTF-8');
        $PostData = $this->post();
        $PostData = array_merge($PostData,$_GET); // merge get for contact informations
        $Draw = isset($PostData['draw']) ? $PostData['draw']++:1;
        $start = $PostData['start'];
        $length = $PostData['length'];
        $search_value = $PostData['search']['value'];
        $search_regex = $PostData['search']['regex'];
        $request_time = $PostData['_'];
        $order_column = $PostData['order'][0]['column'];
        $order_direction = $PostData['order'][0]['dir'];
        $ContactId = $PostData['ContactId'];
        $FirstName = $PostData['FirstName'];
        $LastName = $PostData['LastName'];
        $Email = $PostData['Email'];
        $GroupId = $PostData['GroupId'];
        $session_name = $PostData['session_name'];


        $FinalRows = [];
        $lengthCount = 0;
        $CacheFile = $GroupId.'-'.$order_column;
        $tempData = manual_cache_loader($CacheFile);
        $tempData = false; // disable cache
        if($tempData == false) {
            $ConnectorRelationship = $this->config->item('ConnectorRelationship') ? json_decode($this->config->item('ConnectorRelationship')):[];
            $availableRelationships = [];
            foreach ($ConnectorRelationship as $RelationshipItem){
                $availableRelationships[$RelationshipItem->Id] = ["RelationshipName"=>$RelationshipItem->RelationshipName,"RelationshipDescription"=>$RelationshipItem->RelationshipDescription];
            }
            /*For connector tab contents*/
            $ConnectorTabsEncoded = $this->config->item('connected_info');
            $ConnectorTabs = json_decode($ConnectorTabsEncoded, true);
            $ConnectorTab = [];
            foreach ($ConnectorTabs as $key => $_ConnectorTab){
                if($_ConnectorTab['id'] === $GroupId){
                    $ConnectorTab = $_ConnectorTab;
                    break;
                }
            }
            //Get FirstColumn
            ob_start();
            ?>
            <select
                    data-contactid="<?php echo $ContactId; ?>"
                    class="multiselect-ui field-relationships form-control ConnectedContactRelationshipsBulkAdd ConnectedContactRelationshipsBulkAdd<?php echo $ContactId; ?> addGroup"
                    multiple="multiple">
                <?php
                $ConnectorFields = $this->config->item('connected_info') ? json_decode($this->config->item('connected_info'), true):[];
                $RelationshipRules = [];
                foreach ($ConnectorFields as $FieldGroups){
                    $RelationshipRules[$FieldGroups['id']] = $FieldGroups['relationships'];
                }
                $Allowed = [];
                foreach ($RelationshipRules[$GroupId] as $theRules){
                    $Allowed[] = $theRules['Id'];
                }
                foreach ($availableRelationships as $Id=>$availableRelationship){
                    if(in_array($Id,$Allowed))
                        echo '<option value="'.$Id.'" >'.$availableRelationship["RelationshipName"].'</option>';
                }
                ?>
            </select>
            <?php
            $ConstantColumn_0 = ob_get_contents();
            ob_end_clean();

            // Get All the Other Columns
            $Order = [];
            $OrderId = [];
            foreach ($ConnectorTab['fields'] as $field){
                if($field["showInTable"] == "yes"){
                    $key = (int) $field["showOrder"];
                    if(isset($Order[$key])){
                        while (isset($order[$key])){
                            $key++;
                        }
                        $Order[$key] = $field["fieldLabel"];
                        $OrderId[$key] = $field["fieldId"];
                    }else{
                        $Order[$key] = $field["fieldLabel"];
                        $OrderId[$key] = $field["fieldId"];
                    }
                }
            }
            ksort($Order);
            ksort($OrderId);
            $ConnectedData = macanta_get_connected_info_by_group_id($GroupId,$ContactId);
            $UserValue = $ConnectedData[$GroupId];
            $AllContact = [];
            $ContainedIds = [];
            $RowIndex = 0;

            foreach ($UserValue as $itemId => $ValuesArr){

                $RowData[$RowIndex][] = $ConstantColumn_0;
                $UserField = $ValuesArr['value'];
                $ConnectedContact = $ValuesArr['connected_contact'];
                foreach ($ConnectedContact as $_ContactId=>$theRalation){
                    if($theRalation['ContactId'] === $ContactId) continue;
                    if(in_array($theRalation['ContactId'],$ContainedIds)) continue;
                    $ContainedIds[] = $theRalation['ContactId'];
                    $AllContact[$theRalation['FirstName']] = $theRalation;
                }
                $ContactIdsArr = array_keys($ConnectedContact);
                $ContactIdsStr = implode(',',$ContactIdsArr);
                $ContactIdsStr = $ContactIdsStr==='' ? '0':$ContactIdsStr;
                $hideThis = array_key_exists($ContactId, $ConnectedContact) ? "hideThis":"";
                foreach ($OrderId as $fieldName){
                    if(is_array($UserField[$fieldName])){
                        $Val = isset($UserField[$fieldName]["id_".$ContactId]) ? $UserField[$fieldName]["id_".$ContactId]:'<small><em>Contact Specific</em></small>';
                    }else{
                        $Val = $UserField[$fieldName];
                    }
                    $RowData[$RowIndex][] = $Val;
                }
                $RowData[$RowIndex]['itemid'] = $itemId;
                $RowData[$RowIndex]['contactids'] = $ContactIdsStr;
                $RowData[$RowIndex]['hideThis'] = $hideThis;
                $RowData[$RowIndex]['data_raw'] = str_replace("=","",base64_encode(json_encode($UserField)));
                $RowData[$RowIndex]['data_connectedcontacts'] = str_replace("=","",base64_encode(json_encode($ConnectedContact)));
                $RowIndex++;

            }
            // Lets Order by request
            foreach ($RowData as $index => $Row){
                if(!isset($tempData['data'][$Row[$order_column]])){
                    $tempData['data'][$Row[$order_column]] = $RowData[$index];
                }else{
                    $tempData['data'][$Row[$order_column].time()] = $RowData[$index];
                }

            }
            $tempData['recordsFiltered'] = sizeof($RowData);
            $tempData['recordsTotal']= sizeof($RowData);
            manual_cache_writer($CacheFile, $tempData, 900);
        }
        $order_direction == 'asc' ? ksort($tempData['data']):krsort($tempData['data']);
        $loopPointer = 0;
        if(isset($PostData['FilteredContactId']) && sizeof($PostData['FilteredContactId']) > 0){
            //Filter By contact Ids
            foreach ($tempData['data'] as $ColValue => $theRow){
                if(!empty($theRow['contactids'])){
                    $contactids = explode(',',$theRow['contactids']);
                    $result = array_diff($PostData['FilteredContactId'], $contactids);
                    if($result == $PostData['FilteredContactId']) unset($tempData['data'][$ColValue]);
                }

            }
            $tempData['recordsTotal'] = $tempData['recordsFiltered'] = sizeof($tempData['data']);
        }
        foreach ($tempData['data'] as $ColValue => $theRow){
            if($loopPointer < $start) {
                $loopPointer++;
                continue;
            }
            $FinalRows[] = $theRow;
            if( $lengthCount == $length ) break;
            $lengthCount++;
        }

        $DataTableResponse = [
            'data' => $FinalRows,
            'draw' => $Draw,
            'recordsFiltered' => $tempData['recordsFiltered'], // lools like the total record also
            'recordsTotal' => $tempData['recordsTotal'] // the total record
        ];

        $this->response($DataTableResponse, REST_Controller::HTTP_OK);

    }
    public function checkAPIkey($Key){
        $passwordFile = dirname(APPPATH).DIRECTORY_SEPARATOR."phpliteadmin.txt";
        $passwordArr = json_decode(file_get_contents($passwordFile),true);
        $password = $passwordArr['password'];
        return  $Key == $password ? true:false;
    }
    public function connecteddata_export_post(){
        /*
        Required Parameters:
            email
            api_key
            connected_group

        Optional Parameters:
            export_fields
         */
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $PostData = $this->post();
        $api_key = isset($PostData['api_key']) ? $PostData['api_key']:0;
        $api_key = isset($PostData['access_key']) ? $PostData['access_key']:$api_key;
        if($this->checkAPIkey($api_key) == false)
            $this->response('Forbidden: Missing or Invalid API KEY', REST_Controller::HTTP_FORBIDDEN);

        if(
            !isset($PostData['email']) &&
            !isset($PostData['connected_group'])
        ) $this->response('Forbidden: Missing Required Field - '. json_encode($PostData), REST_Controller::HTTP_FORBIDDEN);

        $connected_group = strtolower($PostData['connected_group']);
        if(isset($PostData['re-export'])){
            $reexport = trim($PostData['re-export']) === 'yes' ? true:false;
        }else{
            $reexport = false;
        }
        $export_fields = isset($PostData['export_fields']) ? explode(',',strtolower($PostData['export_fields'])) : false;
        if($export_fields != false){
            $export_fields = array_map('trim', $export_fields);
            $export_fields = array_map('strtolower', $export_fields);
        }
        if($reexport == false){
            $QueryField = 'meta';
            $QueryValue = '"exported":"yes"';
        }else{
            $QueryField = '';
            $QueryValue = '';
        }

        $ConnectedData = macanta_get_connected_info('', false, $QueryField, $QueryValue,false);
        $RelationshipMap = macanta_get_connected_info_relationships_map();
        $GroupFieldsMap =  macanta_get_connected_info_group_fields_map();
        //reorganize the array
        $GroupFieldsMapNew = [];
        foreach ($GroupFieldsMap as $Groups){
            $GroupFieldsMapNew[] = $Groups;
        }
        $ToCSV = [];
        $ExportedItem = [];
        foreach ($ConnectedData as $ConnectedDataGroupId=>$Items){
            //get group name
            foreach ($GroupFieldsMapNew as $GroupDetails){
                if($GroupDetails['id'] == $ConnectedDataGroupId){
                    if($connected_group != strtolower($GroupDetails['title'])) continue;
                    $ToCSVTemp = [];
                    foreach ($Items as $ItemId=>$ItemDetails){
                        $ExportedItem[] = $ItemId;
                        foreach ($ItemDetails['value'] as $FieldId=>$FieldValue){
                            if($export_fields != false){
                                if(!in_array($GroupDetails['fields'][$FieldId]['title'],$export_fields)) continue;
                            }
                            if(is_array($FieldValue)) $FieldValue = json_encode($FieldValue);
                            $ToCSVTemp[ucwords($GroupDetails['fields'][$FieldId]['title'])] = $FieldValue;
                        }
                        $ContactCount = 1;
                        foreach ($ItemDetails['connected_contact'] as $ContactId=>$ContactDetails){
                            $Count = $ContactCount == 1 ? '':$ContactCount;
                            $ToCSVTemp[trim('Contact Id ' .$Count)] = $ContactId;
                            $ToCSVTemp[trim('Contact FirstName ' .$Count)] = $ContactDetails['FirstName'];
                            $ToCSVTemp[trim('Contact LastName ' .$Count)] = $ContactDetails['LastName'];
                            $ToCSVTemp[trim('Contact Email ' .$Count)] = $ContactDetails['Email'];

                            $relationships = [];
                            foreach ($ContactDetails['relationships'] as $relationship){
                                $relationships[] = $RelationshipMap[$relationship];
                            }
                            $ToCSVTemp[trim('Contact Relation ' .$Count)] = implode(',',$relationships);
                            $ContactCount++;
                        }
                        $ToCSV[] = $ToCSVTemp;
                    }


                    break;
                }
            }


        }
        $Path = FCPATH."/exported_cd";
        $appname = $this->config->item('IS_App_Name');
        if(!is_dir($Path)) mkdir($Path, 0777, true);
        $Subject = "macanta exported connected data | $appname | $connected_group | ".date('Y-m-d H:i');
        $pathToGenerate = $Path."/".$Subject.".csv";  // your path and file name
        $header=null;
        $createFile = fopen($pathToGenerate,"w+");
        foreach ($ToCSV as $ToCSVItem) {

            if(!$header) {

                fputcsv($createFile,array_keys($ToCSVItem));
                fputcsv($createFile, $ToCSVItem);   // do the first row of data too
                $header = true;
            }
            else {

                fputcsv($createFile, $ToCSVItem);
            }
        }
        fclose($createFile);
        $theEmails = explode(',',$PostData['email']);
        $ContactIds = [];
        foreach ($theEmails as $theEmail){
            $Contacts = infusionsoft_get_contact_by_email($theEmail);
            if(sizeof($Contacts) > 0){
                foreach ($Contacts as $Contact){
                    $ContactIds[] = $Contact->Id;
                }
            }
        }

        $htmlBody = "<a href='https://{$appname}.macanta.org/exported_cd/{$Subject}.csv'>Click here to download exported connected data in CSV</a>";
        $fromAddress = "Macanta Connected Data Export <noreply@".$_SERVER['HTTP_HOST'].">";
        $toAddress = $theEmails[0];
        $bccAddresses = '';
        $contentType = "HTML";
        $ccAddresses = "";
        $textBody = strip_tags($htmlBody);
        $emailSent = infusionsoft_send_email(base64_encode($Subject), base64_encode($htmlBody), implode(',',$ContactIds), $fromAddress, $toAddress, $bccAddresses, $contentType, $ccAddresses, $textBody);
        if($emailSent == 1){
            foreach ($ExportedItem as $CDItemId){
                $this->db->where('id',$CDItemId);
                $query = $this->db->get('connected_data');
                if (sizeof($query->result()) > 0){
                    foreach ($query->result() as $row) {
                        $Old = json_decode($row->meta, true);
                        $modified = false;
                        $modification = [];
                        $meta = ["exported"=>"yes"];
                        $New = macanta_array_update($meta, $Old, $modification, $modified);
                        if($New!=$Old && $modified === true) {
                            $DBData['meta'] = json_encode($New);
                            macanta_cd_record_history($CDItemId, $modification, 'meta');
                            if(sizeof($DBData) > 0){
                                $this->db->where('id',$CDItemId);
                                $Results = $this->db->update('connected_data', $DBData);
                            }
                        }
                    }
                }
            }
        }
        $this->response("Exported, Email Sent:".$emailSent, REST_Controller::HTTP_OK);

    }
    public function connecteddata_edit_post(){
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ALL);

        $RequiredFields = ["contactId","connected_group","connecteddata_id","cd_guid"];
        $PostData = $this->post();
        $api_key = isset($PostData['api_key']) ? $PostData['api_key']:0;
        $api_key = isset($PostData['access_key']) ? $PostData['access_key']:$api_key;
        $Return = [];
        $Debug = [];
        $connected_contacts = [];
        if($this->checkAPIkey($api_key) == false) $this->response('Oops: Missing or Invalid API KEY', REST_Controller::HTTP_FORBIDDEN);

        foreach ($RequiredFields as $RequiredField){
            if(!isset($PostData[$RequiredField])){
                if($RequiredField == "connecteddata_id"){
                    if(!isset($PostData["cd_guid"])) $this->response("Oops: Missing Required Field - connecteddata_id or cd_guid  from ". json_encode($PostData), REST_Controller::HTTP_FORBIDDEN);
                }elseif ($RequiredField == "cd_guid"){
                    if(!isset($PostData["connecteddata_id"])) $this->response("Oops: Missing Required Field - connecteddata_id or cd_guid  from ". json_encode($PostData), REST_Controller::HTTP_FORBIDDEN);
                }else{
                    $this->response('Oops: Missing Required Field - '.$RequiredField." from ". json_encode($PostData), REST_Controller::HTTP_FORBIDDEN);
                }
            }
        }

        $contactId = $PostData['contactId']; //
        $connected_groups = strtolower($PostData['connected_group']);
        $cd_guid = isset($PostData['cd_guid']) ? $PostData['cd_guid'] : "";
        $update_fields = isset($PostData['update_fields']) ?  strtolower($PostData['update_fields']):'';
        $connecteddata_id = isset($PostData['connecteddata_id']) ? trim(strtolower($PostData['connecteddata_id'])) : "";
        $connected_relationship = isset($PostData['connected_relationship']) ?  trim($PostData['connected_relationship']):false;
        if($connected_relationship){
            $connected_contacts[$contactId] = $connected_relationship;
        }

        unset($PostData['cd_guid']);
        unset($PostData['contactId']);
        unset($PostData['connected_group']);
        unset($PostData['update_fields']);
        unset($PostData['connecteddata_id']);
        unset($PostData['api_key']);
        unset($PostData['access_key']);
        unset($PostData['connected_relationship']);
        //get custom field for a given $update_fields and $connecteddata_id
        $UserFields = trim($update_fields) !== '' ? explode(',',$update_fields):[];
        if($connecteddata_id != "")
            $UserFields[] = $connecteddata_id;

        $connected_groupsArr = explode(" or ",$connected_groups);
        foreach ($connected_groupsArr as $connected_group){
            $ConnectedInfos = macanta_get_connected_info_by_groupname($connected_group,$contactId,'',$cd_guid);
            if(sizeof($ConnectedInfos) == 0){
                $Debug['groups'][$connected_group] = $this->connecteddata_add_post(true);
                continue;
            }
            $ISContactFields = [];
            $UserCustomFieldsArr = [];
            foreach ($ConnectedInfos[$connected_group] as $ItemId => $ItemDetails) {
                $ISContactFields = [];
                if(sizeof($UserFields) > 1){
                    //print_r($ItemDetails['fields']);
                    foreach ($UserFields as $UserField){
                        $UserField = trim($UserField);
                        if(isset($ItemDetails['fields'][$UserField])){
                            if($ItemDetails['fields'][$UserField]['custom-field'] != ""){
                                $ISContactFields[] =  "\"_".$ItemDetails['fields'][$UserField]['custom-field'].'"';
                                $UserCustomFieldsArr[$UserField] = "_".$ItemDetails['fields'][$UserField]['custom-field'];
                            }
                        }
                    }
                }else{
                    foreach ($ItemDetails['fields'] as $UserField => $Field){
                        if(trim($Field['custom-field']) !== ""){
                            $ISContactFields[] =  "\"_".$Field['custom-field'].'"';
                            $UserCustomFieldsArr[$UserField] =  "_".$Field['custom-field'];
                        }
                    }
                }

                break; // we only need one item to get custom fields
            }
            $UserCustomFieldValuesArr = sizeof($ISContactFields) > 0 ? infusionsoft_get_contact_by_id_simple($contactId, $ISContactFields)[0]:[];
            if(sizeof($UserCustomFieldValuesArr) == 0)
                $this->response('No Connected Data To Update, Data: '.json_encode($UserCustomFieldValuesArr), REST_Controller::HTTP_OK);
            $Data = [];
            foreach ($UserCustomFieldsArr as $MacantaField => $CustomField){
                if(isset($UserCustomFieldValuesArr->$CustomField)){
                    $Value=$UserCustomFieldValuesArr->$CustomField;
                    if(is_object($Value)){
                        $Value = explode(" ",$Value->date);
                        $Value = $Value[0];
                    }
                    $Data[$MacantaField] =  $Value;
                }else{
                    $Data[$MacantaField] =  "";
                }
            }
            $Parsed = sizeof($PostData) == 0 ? []:infusionsoft_parse_criteria($PostData);
            $Passed = sizeof($Parsed) == 0 ? true : false;
            $Expressions = [];
            if($connecteddata_id == "" && $cd_guid == ""){
                $Debug['groups'][$connected_group] = macanta_add_update_connected_data($contactId, false,$connected_group, $Data,$connected_contacts,false, false, false);
            }else{
                foreach ($ConnectedInfos[$connected_group] as $ItemId => $ItemDetails) {
                    $Passed = sizeof($Parsed) == 0 ? true : false;
                    $Fields = $ItemDetails['fields'];
                    //Validate Fields values
                    foreach ($Parsed as $FieldName => $Expression) {
                        $Expressions[] = $Expression;
                        $Passed = macanta_check_value_by_expression($FieldName,$Fields,$Expression);

                    }
                    if( $Passed == true){
                        $Debug['groups'][$connected_group] = macanta_add_update_connected_data($contactId, false,$connected_group, $Data,$connected_contacts,false, $connecteddata_id, true, $cd_guid);
                    }
                }
            }
        }

        $Debug['body'] = $this->post();
        $Debug['is_cf_key'] = $ISContactFields;
        $Debug['is_cf_value'] = $UserCustomFieldValuesArr;
        $this->response($Debug, REST_Controller::HTTP_OK);
    }

    public function expired_app_post(){
        header("Content-Type: text/plain");
        $PostData = $this->post();
        //file_put_contents(dirname(__FILE__)."/expired_app_post.txt", json_encode($PostData),FILE_APPEND);
        $FirstName = $PostData['FirstName'];
        $LastName = $PostData['LastName'];
        $Email = $PostData['Email'];
        $Company = $PostData['Company'];
        $ContactId = $PostData['ContactId'];
        $coupon_code = $PostData['coupon_code'];
        $hash = $PostData['hash'];
        $MacantaURL = $PostData['MacantaURL'];
        $Country = $PostData['Country'];
        $app_name = $PostData['app_name'];
        $DateTrialEnd = $PostData['DateTrialEnd'];

        if(empty($hash))
            $this->response('Forbidden: Missing or Invalid HASH Code', REST_Controller::HTTP_FORBIDDEN);

        $Link = "https://macanta.org/start-subscription/?FirstName={$FirstName}&LastName={$LastName}&Company={$Company}&ContactId={$ContactId}&coupon_code={$coupon_code}&hash={$hash}&MacantaURL={$MacantaURL}&Country={$Country}&app_name={$app_name}&Email={$Email}";

        $Message = "Oops! Your 30 day free trial of Macanta came to a screeching halt on {$DateTrialEnd} :) 
                    Please <a href='{$Link}'>[click here]</a> to start your Hosted Macanta subscription. 
                    Or contact us via the green 'Click Here For Help' button. 
                    Thank you.";

        $DBData['value'] = $Message;
        if(false == $this->DbExists('key','login_disabled','config_data')){
            $DBData['key'] = 'login_disabled';
          echo  $this->db->insert('config_data', $DBData);
        }else{
            $this->db->where('key','login_disabled');
            echo  $this->db->update('config_data',$DBData);
        }

    }
    public function disable_app_post(){
        header("Content-Type: text/plain");
        $PostData = $this->post();
        $api_key = isset($PostData['api_key']) ? $PostData['api_key']:0;
        $api_key = isset($PostData['access_key']) ? $PostData['access_key']:$api_key;
        if($this->checkAPIkey($api_key) == false)
            $this->response('Forbidden: Missing or Invalid API KEY', REST_Controller::HTTP_FORBIDDEN);

        if(!isset($PostData['messages']))$this->response('Forbidden: Missing Required Field - messages', REST_Controller::HTTP_FORBIDDEN);
        $DBData['messages'] = $PostData['messages'];
        if(false == $this->DbExists('key','login_disabled','config_data')){
            $DBData['key'] = 'login_disabled';
            $this->db->insert('config_data', $DBData);
        }else{
            $this->db->where('key','login_disabled');
            $this->db->update('config_data',$DBData);
        }

    }
    public function enable_app_post(){
        header("Content-Type: text/plain");
        $PostData = $this->post();
        $api_key = isset($PostData['api_key']) ? $PostData['api_key']:0;
        $api_key = isset($PostData['access_key']) ? $PostData['access_key']:$api_key;
        if($this->checkAPIkey($api_key) == false)
            $this->response('Forbidden: Missing or Invalid API KEY', REST_Controller::HTTP_FORBIDDEN);

        $this->db->where('key','login_disabled');
        $this->db->delete('config_data');

    }
    public function connecteddata_add_post($return=false){
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $PostData = $this->post();
        $api_key = isset($PostData['api_key']) ? $PostData['api_key']:0;
        $api_key = isset($PostData['access_key']) ? $PostData['access_key']:$api_key;
        if($this->checkAPIkey($api_key) == false)
            $this->response('Forbidden: Missing or Invalid API KEY', REST_Controller::HTTP_FORBIDDEN);

        if(
            !isset($PostData['contactId']) ||
            !isset($PostData['connected_group']) ||
            //!isset($PostData['connecteddata_id']) || // optional
            !isset($PostData['connected_relationship'])
        ) $this->response('Forbidden: Missing Required Field - '. json_encode($PostData), REST_Controller::HTTP_FORBIDDEN);
        $contactId = $PostData['contactId'];
        $connected_groups = $PostData['connected_group'];
        $connected_relationship = $PostData['connected_relationship'];
        $duplicate_option = isset($PostData['connecteddata_id']) ? trim($PostData['connecteddata_id']) == "" ? false:$PostData['connecteddata_id'] : false;
        $ConnectedContact = [$contactId => $connected_relationship];
        $ConnectorTabsEncoded = $this->config->item('connected_info');
        $ConnectorTabs = json_decode($ConnectorTabsEncoded, true);
        $Debug = [];

        $connected_groupsArr = explode(" or ",$connected_groups);
        foreach ($connected_groupsArr as $connected_group){
            $returnFields = [];
            $DataContainer = [];
            $Data = [];
            foreach ($ConnectorTabs as $index => $ConnectorTab){
                if(strtolower(trim($connected_group)) == strtolower(trim($ConnectorTab['title']))){
                    $Fields = $ConnectorTab['fields'];
                    foreach ($Fields as $sub_index => $Field){
                        if(trim($Field['infusionsoftCustomField']) !== ""){
                            $returnFields[] = '"_'.trim($Field['infusionsoftCustomField']).'"';
                            $DataContainer[$Field['fieldLabel']] = '_'.trim($Field['infusionsoftCustomField']);
                        }
                    }
                    break;
                }
            }
            $Contact = sizeof($returnFields) > 0 ? infusionsoft_get_contact_by_id_simple($contactId, $returnFields)[0]:[];
            foreach ($DataContainer  as $DataFieldName => $CustomFieldName){
                if(isset($Contact->$CustomFieldName)){
                    // Handle Date Object
                    if(is_object($Contact->$CustomFieldName)){
                        if(isset($Contact->$CustomFieldName->date)){
                            $Value = explode(" ",$Contact->$CustomFieldName->date);
                            $Value = $Value[0];
                        }else{
                            $Value = json_encode($Contact->$CustomFieldName);
                        }
                        $Data[$DataFieldName] = $Value;
                    }else{
                        $Data[$DataFieldName] = $Contact->$CustomFieldName;
                    }
                }else{
                    $Data[$DataFieldName] = '';
                }
            }
            $Debug['groups'][$connected_group] = macanta_add_update_connected_data($contactId, false,$connected_group, $Data,$ConnectedContact,false, $duplicate_option, false, "",true);

        }

        if($return) return $Debug;

        $this->response($Debug, REST_Controller::HTTP_OK);
    }
    public function connecteddata_get(){

        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $PostData = $this->get();
        $api_key = isset($PostData['api_key']) ? $PostData['api_key']:0;
        $api_key = isset($PostData['access_key']) ? $PostData['access_key']:$api_key;
        if($this->checkAPIkey($api_key) == false)
            $this->response('Forbidden: Missing or Invalid API KEY', REST_Controller::HTTP_FORBIDDEN);

        if(isset($PostData['ref'])){
            $Results = [];
            $this->db->where('meta_key', $PostData['ref']);
            $query = $this->db->get('users_meta');
            foreach ($query->result() as $row) {
                $value = json_decode($row->meta_value, true);
                $Items = $value['items'];
                $PaidItems = $value['paid_items'];
                $Results['paid_items'] = $PaidItems;
                foreach ($Items as $Item){
                    $CD = macanta_get_connected_info('',false, '', '', true, $Item);
                    $Results['items'][] =  macanta_api_beautify_connected_info($CD)['message'];
                }
            }
            $this->response($Results, REST_Controller::HTTP_OK);
        }else{
            $this->response('Forbidden: Missing Reference Number', REST_Controller::HTTP_FORBIDDEN);
        }
    }
    public function connecteddata_post(){
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $PostData = $this->post();
        $api_key = isset($PostData['api_key']) ? $PostData['api_key']:0;
        $api_key = isset($PostData['access_key']) ? $PostData['access_key']:$api_key;

        if($this->checkAPIkey($api_key) == false)
            $this->response('Forbidden: Missing or Invalid API KEY', REST_Controller::HTTP_FORBIDDEN);

        if(
            !isset($PostData['to_be_mark_paid'])  ||
            trim($PostData['prod_field_title_id']) == '' ||
            trim($PostData['prod_field_date_id']) == ''  ||
            trim($PostData['renewal_time']) == ''
        ) $this->response('Forbidden: Missing or Blank Required Field - '. json_encode($PostData), REST_Controller::HTTP_FORBIDDEN);

        $RenewTime = $PostData['renewal_time'];
        $FieldIdOfTitle = $PostData['prod_field_title_id'];
        $FieldIdOfDateToUpdate = $PostData['prod_field_date_id'];
        $SequnceId = $PostData['sequence_id'];
        $CampaignId = $PostData['campaign_id'];
        $RenewTimeFieldId = $PostData['prod_field_renewal_time_id'];

        //MSC Plugin Feature, Renewing Products
        $Result = $PaidIdArr = [];
        if(isset($PostData['to_be_mark_paid'])) {
            foreach ($PostData['to_be_mark_paid'] as $referrence_id => $Titles) {
                // Update Reference ID Value
                $Titles =  array_map('trim',$Titles);
                $Titles =  array_map('strtolower',$Titles);
                $DBData = [];
                $this->db->where('meta_key', $referrence_id);
                $query = $this->db->get('users_meta');
                foreach ($query->result() as $row) {
                    $value = json_decode($row->meta_value, true);
                    $Items = $value['items'];
                    $PaidItems = $value['paid_items'];
                    $SetupCampaignId = $value['campaign']['id'];
                    $CampaignId = $SetupCampaignId;
                    $SetupSequenceId = $value['campaign']['post_purchase_sequence'];
                    $SequnceId = $SetupSequenceId == false ? $SequnceId:$SetupSequenceId;
                    foreach ($Items as $Item){
                        if(in_array($Item,$PaidItems)) continue;
                        $this->db->where('id', $Item);
                        $query = $this->db->get('connected_data');
                        if (sizeof($query->result()) > 0) {
                            foreach ($query->result() as $row) {
                                $id = $row->id;
                                $values = json_decode($row->value, true);
                                $NewConnectedContacts = $ConnectedContacts = json_decode($row->connected_contact, true);
                                $OldDate = $values[$FieldIdOfDateToUpdate];
                                $Expresion = $OldDate." ".$RenewTime;
                                if(in_array(strtolower(trim($values[$FieldIdOfTitle])),$Titles)){

                                    //Update Connected Data Renewal Date
                                    $DBData = [];
                                    $PaidItems[] = $id;
                                    if(trim($RenewTimeFieldId) !== ''){
                                        if(isset($values[$RenewTimeFieldId]) && trim($values[$RenewTimeFieldId]) !== ''){
                                            $Expresion = $OldDate." ".$values[$RenewTimeFieldId];
                                        }
                                    }
                                    $values[$FieldIdOfDateToUpdate] = isValidTimeString($OldDate) ? date("Y-m-d", strtotime($Expresion)):$OldDate;
                                    $DBData['value'] = json_encode($values);
                                    $this->db->where('id', $id);
                                    $Result['cd_value']['result'] = $this->db->update('connected_data', $DBData);
                                    $Result['cd_value']['expresion'] = $Expresion;
                                    $Result['cd_value']['db_data'] = $DBData;

                                    //Update Reference Information
                                    $DBData = [];
                                    $value['paid_items'] = $PaidItems;
                                    $DBData['meta_value'] = json_encode($value);
                                    $this->db->where('meta_key', $referrence_id);
                                    $Result['users_meta']['result'] = $this->db->update('users_meta', $DBData);
                                    if(!$Result['users_meta']['result']) $Result['users_meta']['error'] = $this->db->error();
                                    $Result['users_meta']['db_data'] = $DBData;

                                    //Update Connected Contact Sequences
                                    $DBData = [];
                                    $ContactIdsForFinalSequence = [];
                                    foreach ($ConnectedContacts as $ContactId => $ContactDetails){
                                        if(isset($ContactDetails['Sequence'])){
                                            foreach ($ContactDetails['Sequence'] as $SequenceKey => $SequenceDetails){
                                                $SequenceKeyArr = explode('.',$SequenceKey);
                                                if($SequenceKeyArr[0] != $CampaignId) continue;
                                                $ContactIdsForFinalSequence[] = $ContactId;
                                                unset($NewConnectedContacts[$ContactId]['Sequence'][$SequenceKey]);
                                            }
                                        }
                                    }
                                    $DBData['connected_contact'] = json_encode($NewConnectedContacts);
                                    $this->db->where('id', $id);
                                    $Result['cd_contacts']['result'] = $this->db->update('connected_data', $DBData);
                                    $Result['cd_contacts']['expresion'] = $Expresion;
                                    $Result['cd_contacts']['db_data'] = $DBData;

                                    //Move Contacts to Final Sequence
                                    $CampaignDetails = infusionsoft_get_campaign_by_id($CampaignId);
                                    if (isset($CampaignDetails->message->sequences)) {
                                        foreach ($CampaignDetails->message->sequences as $sequence) {
                                            if (strpos($sequence->name, $SequnceId) !== false) {
                                                $sequenceId = $sequence->id;
                                                $Result['sequence']['result'] = infusionsoft_multiple_add_to_sequence($CampaignId, $sequenceId, implode(',',$ContactIdsForFinalSequence));
                                                break;
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }
            $this->response($Result, REST_Controller::HTTP_OK);
        }else{
            $this->response('Forbidden: Paid Items Parameters', REST_Controller::HTTP_FORBIDDEN);
        }

    }
    public function genpass_post(){
        $PostData = $this->post();
        $passwordFile = dirname(APPPATH).DIRECTORY_SEPARATOR."phpliteadmin.txt";
        $passwordArr = json_decode(file_get_contents($passwordFile),true);
        $contactId = $PostData['contactId'];
        $passcode = $PostData['passcode'];
        $password = $passwordArr['password'];

        if ($passcode == $password) {
            $result = array();
            $PostData = $this->post();
            $UserLevelTags = json_decode($this->config->item('access_level'));
            $StaffTag = $UserLevelTags->staff;
            $result['AppliedTag'] = $AppliedTag = infusionsoft_apply_tag($contactId, $StaffTag);
            $Contact = infusionsoft_get_contact_by_id($contactId, array('all'));
            if(!isset($Contact->Password)){
                $Password =  generateAdjNounPhrase();
                $data = array(0 => array('name'=>'Password','value'=>$Password));
                $result['UpdatedPassword'] = infusionsoft_update_contact($data,$contactId,'No');
            }
            $this->response($result);
        }else{
            $this->response('Forbidden', REST_Controller::HTTP_FORBIDDEN);
        }

    }
    public function genpass_get(){
        $this->response('GET Request Not Allowed', REST_Controller::HTTP_FORBIDDEN);
    }
    public function read_header_get(){

        $HeaderId = infusionsoft_get_contact_action_header_id();
        echo $HeaderId;
    }
    public function customfieldval_get(){
        $action = "query_is";
        $action_details = '{"table":"Contact","limit":"1000","page":0,"fields":["Id","Email","FirstName","LastName","_ChildAllergies"],"query":{"Id":455}}';
        $Invoice = rucksack_request($action, $action_details, "sherpa", true);
        print_r($Invoice);

        $action = "update_is";
        $action_details = '{"table":"Contact","id":455,"fields":{"_ChildAllergies":""}}';
        $result = rucksack_request($action, $action_details);

        print_r($result);

        $action = "query_is";
        $action_details = '{"table":"Contact","limit":"1000","page":0,"fields":["Id","Email","FirstName","LastName","_ChildAllergies"],"query":{"Id":455}}';
        $Invoice = rucksack_request($action, $action_details, "sherpa", true);
        print_r($Invoice);
    }
    public function customfields_get(){
        print_r(infusionsoft_get_custom_fields("%",false, -5, false,'Macanta Notes'));
    }
    public function json_get(){
        $NoteCustomFields = '["A","B","C","D","E"]';
        $NoteCustomFields = json_decode($NoteCustomFields);
        print_r($NoteCustomFields);
    }
    public function tryget_get(){
        $NoteCustomFields = array(
            array(
                'Label'=>'Call Recording URL',
                'dType'=>'Website'
            ),
            array(
                'Label'=>'JSON',
                'dType'=>'TextArea'
            )

        ) ;
        $CustomFields = infusionsoft_generate_contact_action_custom_fields($NoteCustomFields);
        print_r($CustomFields);
        //$this->response($Notes, REST_Controller::HTTP_OK);
    }
    public function connected_get(){
        header("Content-Type: text/plain");
       $resutls =  macanta_search_connected_info(["andrea","mobile"]);
       print_r(json_encode($resutls));
    }
    public function guid_get(){
       echo  macanta_generate_key('test_');
    }
    public function cf_get(){
        header("Content-Type: text/plain");
        $CF = infusionsoft_contact_custom_fields();
        print_r($CF);
    }
    public function customfield_get(){

        $results = infusionsoft_get_custom_fields("%",false, -4, false,'Assigned To');
        $this->response($results[0]->Name, REST_Controller::HTTP_FORBIDDEN);
    }
    public function parenthesis_get(){
        $in = "(semi-detached or detached) and (freehold)";
        echo preg_match_all('/\(([A-Za-z0-9\-_ ]+?)\)/', $in, $out);
        print_r($out);
    }
    public function strtotime_get(){
        echo time()."\n";
        echo strtotime('2 sec');
    }
    public function map_get(){
        print_r(macanta_get_connected_info_group_fields_map());
    }
    public function token_get(){
        $token = unserialize('O:18:"Infusionsoft\Token":4:{s:11:"accessToken";s:24:"hmfxsnhe4r9vxz2j2dn6yk9h";s:12:"refreshToken";s:24:"btf7hcr78z4cnft9zdczy2r9";s:9:"endOfLife";i:1503547075;s:9:"extraInfo";a:2:{s:10:"token_type";s:6:"bearer";s:5:"scope";s:27:"full|qj311.infusionsoft.com";}}');
        print_r($token->accessToken);
    }
    public function campaign_get(){
        print_r(infusionsoft_get_campaign_by_id(132));
    }
    public function sequence_get(){
        print_r(infusionsoft_multiple_add_to_sequence(11411, 8, '364'));
    }
    public function tags_get(){
        print_r(infusionsoft_get_tags());
    }
    public function phpinfo_get(){
        phpinfo();
        echo "<pre>";
        print_r($_SERVER);
        echo "</pre>";
    }
    public function filebox_get(){
        $PostData = $this->get();
        $fileId = isset($PostData['fileId']) ? $PostData['fileId']:0;
        if(!$fileId)  $this->response('Forbidden', REST_Controller::HTTP_FORBIDDEN);

        $TheFile = infusionsoft_get_file_by_id($fileId)->message;
        $FileDetails = $TheFile->file_descriptor;
        $FileDataEncoded = $TheFile->file_data;
        $FileDataDecoded = base64_decode($FileDataEncoded);
        $FileName = $FileDetails->file_name;
        //file_put_contents($Path, $FileDataDecoded);
        $extension = end(explode(".", $FileName));
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename="'.$FileName.'"');
        switch ($extension){
            case 'jpg':
                header('Content-type: image/jpeg');
                break;
            case 'jpeg':
                header('Content-type: image/jpeg');
                break;
            case 'png':
                header('Content-type: image/png');
                break;
            case 'gif':
                header('Content-type: image/gif');
                break;
            case 'bmp':
                header('Content-type: image/bmp');
                break;
            case 'txt':
                header('Content-type: text/plain');
                break;
            case 'pdf':
                header('Content-type: application/pdf');
                break;
            case 'doc':
                header('Content-type: application/msword');
                break;
            case 'docx':
                header('Content-type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
                break;
            case 'xls':
                header('Content-type: application/vnd.ms-excel');
                break;
            case 'xlsm':
                header('Content-type: application/vnd.ms-excel.sheet.macroenabled.12');
                break;
            case 'xlsx':
                header('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                break;
            case '7z':
                header('application/x-7z-compressed');
                break;
            case 'zip':
                header('application/zip');
                break;
        }
        echo $FileDataDecoded;
    }
    public function DbExists($field,$value,$table)
    {
        $this->db->where($field,$value);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0){

            foreach ($query->result() as $row)
            {
                return true;
            }
            return false;
        }
        else{

            return false;
        }
    }
    public function getAllAccount_get(){
        header('Content-Type: text/plain; charset=UTF-8');
        print_r(zuora_getAllAccount());
    }
    public function getAllContact_get(){
        header('Content-Type: text/plain; charset=UTF-8');
        print_r(zuora_getAllContact());
    }
    public function getContactBy_get(){
        header('Content-Type: text/plain; charset=UTF-8');
        print_r(zuora_getContactBy('WorkEmail', 'geover@gmail.com'));
    }
    public function getAccountBy_get(){
        header('Content-Type: text/plain; charset=UTF-8');
        print_r(zuora_getAccountBy('BillToId', '2c92c0f86140d9b201615208791f66a1'));
    }
    public function getInvoiceBy_get(){
        header('Content-Type: text/plain; charset=UTF-8');
        print_r(zuora_getInvoiceBy('AccountId', '2c92c0f96140e5670161521355315e62'));
    }
    public function getSubscribeResultBy_get(){
        header('Content-Type: text/plain; charset=UTF-8');
        $Subscription = zuora_queryData('Id', $InvoiceItemVar['SubscriptionId'], "Subscription");
        print_r(zuora_getSubscribeResultBy('AccountId', '2c92c0f96140e5670161521355315e62'));
    }
    public function stop_cd_get(){
        // end all the running sequence.
        $this->db->like('connected_contact', '"MoveAfter":');
        $this->db->or_like('connected_contact', '"running"');
        $query = $this->db->get('connected_data');
        echo "Total Items To Reset: ". $query->num_rows()."<br>\n";
        foreach ($query->result() as $row) {
            $DBdata = [];
            $Id = $row->id;
            $UpdatedConnectedContacts = $ConnectedContacts = json_decode($row->connected_contact, true);
            foreach ($ConnectedContacts as $ContactId => $Details){
                $Sequence = [];
                foreach ($Details['Sequence'] as $CampaignStage => $CampaignDetails){
                    unset($CampaignDetails['NextSequence']);
                    unset($CampaignDetails['MoveAfter']);
                    unset($CampaignDetails['Parsed']);
                    $CampaignDetails['Status'] = 'end';
                    $Sequence[$CampaignStage] = $CampaignDetails;
                }
                $UpdatedConnectedContacts[$ContactId]['Sequence'] = $Sequence;
            }
            $DBdata['connected_contact'] = json_encode($UpdatedConnectedContacts);
            $this->db->where('id', $Id);
            $this->db->update('connected_data', $DBdata);

        }
        echo "Updated"."<br>\n";;
    }
    public function uniqueify_cd_get(){
        // end all the running sequence.
        $param = $this->get();
        if(isset($param['field_id'])){
            $field_id = $param['field_id'];
            $this->db->like('value', '"'.$field_id.'"');
            $query = $this->db->get('connected_data');
            echo "Total Items To Check: ". $query->num_rows()."<br>\n";
            $CheckedFieldValueArr = [];
            foreach ($query->result() as $row) {
                $Id = $row->id;
                $ConnectedValues = json_decode($row->value, true);
                if (in_array($ConnectedValues[$field_id],$CheckedFieldValueArr)) continue;
                $CheckedFieldValue = $ConnectedValues[$field_id];
                $CheckedFieldValueArr[] = $CheckedFieldValue;
                $this->db->where('id !=', $Id);
                $this->db->like('value', '"'.$field_id.'":"'.$CheckedFieldValue.'"');
                $this->db->delete('connected_data');
            }
            echo "Updated"."<br>\n";
        }

    }
    public function set_value_cd_get(){
        // end all the running sequence.

        $param = $this->get();
        if(isset($param['field_id']) && isset($param['value'])){
            $field_id = $param['field_id'];
            $value = $param['value'];
            $this->db->like('value', '"'.$field_id.'"');
            $query = $this->db->get('connected_data');
            echo "Total Items To Be Modified: ". $query->num_rows()."<br>\n";
            foreach ($query->result() as $row) {
                $DBdata = [];
                $Id = $row->id;
                $ConnectedValues = json_decode($row->value, true);
                $ConnectedValues[$field_id] = $value;
                $DBdata['value'] = json_encode($ConnectedValues);
                $this->db->where('id', $Id);
                $this->db->update('connected_data', $DBdata);
            }
            echo "Updated"."<br>\n";
        }

    }
    public function remove_campaign_get(){
        // end all the running sequence.
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $param = $this->get();
        echo  $param['campaign_id']."\n";
        if(isset($param['campaign_id'])){
            $campaign_id = $param['campaign_id'];
            $this->db->like('connected_contact', '"'.$campaign_id.'"');
            $query = $this->db->get('connected_data');
            echo $query->num_rows()."\n";
            foreach ($query->result() as $row) {
                $Id = $row->id;
                $DBdata = [];
                $connected_contacts = $row->connected_contact;
                $connected_contacts_new = $connected_contacts = json_decode($connected_contacts, true);
                foreach ($connected_contacts as $ContactId=>$Details){
                    unset($connected_contacts_new[$ContactId]["Sequence"]["$campaign_id"]);
                }
                $connected_contacts_new = json_encode($connected_contacts_new);
                $DBdata['connected_contact'] = $connected_contacts_new;
                $this->db->where('id', $Id);
                $this->db->update('connected_data', $DBdata);
            }
            echo "Updated"."<br>\n";
        }

    }
    public function remove_campaign_before_cd_field_date_get(){
        // end all the running sequence.
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $param = $this->get();
        $param['field_id'] = isset($param['field_id']) ? $param['field_id']:"field_jdlih719";
        $param['date'] = isset($param['date']) ? $param['date']:"2018-05-22";
        echo  "campaign id: ".$param['campaign_id']."\n";
        echo  "field: ".$param['field_id']."\n"; //field_jdlih719
        echo  "date: ".$param['date']."\n";
        if(isset($param['campaign_id'])){
            $campaign_id = $param['campaign_id'];
            $this->db->like('connected_contact', '"'.$campaign_id.'"');
            $query = $this->db->get('connected_data');
            echo $query->num_rows()."\n";
            foreach ($query->result() as $row) {
                $Id = $row->id;
                $connected_contacts = $row->connected_contact;
                $value = json_decode($row->value, true);
                $CD_date_stamp = (int) strtotime($value[$param['field_id']]);
                $Mark_date_stamp = (int) strtotime($param['date']);

                if ($CD_date_stamp >= $Mark_date_stamp) continue;

                echo "To Update "."$Id $CD_date_stamp < $Mark_date_stamp <br>\n";
                $DBdata = [];
                $connected_contacts_new = $connected_contacts = json_decode($connected_contacts, true);
                foreach ($connected_contacts as $ContactId=>$Details){
                    $connected_contacts_new[$ContactId]["Sequence"]["$campaign_id"]['Status'] = 'end';
                    unset($connected_contacts_new[$ContactId]["Sequence"]["$campaign_id"]['NextSequence']);
                    unset($connected_contacts_new[$ContactId]["Sequence"]["$campaign_id"]['MoveAfter']);
                    unset($connected_contacts_new[$ContactId]["Sequence"]["$campaign_id"]['MoveWhen']);
                }
                $connected_contacts_new = json_encode($connected_contacts_new);
                $DBdata['connected_contact'] = $connected_contacts_new;
                $this->db->where('id', $Id);
                $this->db->update('connected_data', $DBdata);
            }
            echo "Updated"."<br>\n";
        }

    }
    public function notetag_get(){
        $NoteIds = [];
        $NoteIdsIndex = 0;
        $query = $this->db->get('note_tags');
        foreach ($query->result() as $row)
        {
            if(!is_numeric($row->note_id)) continue;
            if(sizeof($NoteIds[$NoteIdsIndex]) > 999) $NoteIdsIndex++;
            $NoteIds[$NoteIdsIndex][] = (int) $row->note_id;

        }
        print_r($NoteIds);
        $action = "query_is";
        foreach ($NoteIds as $Index => $Batch){
            $action_details = '{"table":"ContactAction","limit":"1000","page":0,"fields":["Id","ContactId"],"query":{"Id":'.json_encode($Batch).'}}';
            $Notes = applyFn('rucksack_request',$action, $action_details);
            print_r($Notes);

        }
    }
    public function show_contact_by_get(){
        // end all the running sequence.
        header("Content-Type: text/plain");
        error_reporting(E_ALL);
        $param = $this->get();
        $param['campaign_id'] = isset($param['campaign_id']) ? $param['campaign_id']:false;
        if($param['campaign_id'] == false ) {
            echo 'Missing campaign_id';
            return false;
        }
        $ConnectorTabsEncoded = $this->config->item('connected_info');
        $ConnectorTabs = json_decode($ConnectorTabsEncoded, true);
        $ConnectedData = [];
        $ConnectedDataFields = [];
        $ConnectedDataTitle = [];
        $ConnectedDataTitleId = [];
        foreach ($ConnectorTabs as $key => $ConnectorTab){
            $ConnectedData[$ConnectorTab['id']] = ['Name'=>$ConnectorTab['title']];
            $ConnectedDataFields[$ConnectorTab['id']] = $ConnectorTab['fields'];

            $Order = [];
            $OrderId = [];
            foreach ($ConnectorTab['fields'] as $field){
                if($field["showInTable"] == "yes"){
                    $key = (int) $field["showOrder"];
                    if(isset($Order[$key])){
                        while (isset($order[$key])){
                            $key++;
                        }
                        $Order[$key] = $field["fieldLabel"];
                        $OrderId[$key] = $field["fieldId"];
                    }else{
                        $Order[$key] = $field["fieldLabel"];
                        $OrderId[$key] = $field["fieldId"];
                    }
                }
            }
            ksort($Order);
            ksort($OrderId);
            $ConnectedDataTitle[$ConnectorTab['id']] = $Order;
            $ConnectedDataTitleId[$ConnectorTab['id']] = $OrderId;

        }



        $this->db->like('connected_contact', '"'.$param['campaign_id'].".");
        $this->db->like('connected_contact', '"Status":"end"');
        $this->db->like('connected_contact', '"MoveAfter"');
        $query = $this->db->get('connected_data');
        $CDCC = [];
        foreach ($query->result() as $row) {

            $Id = $row->id;
            $Group = $row->group;
            $Value = json_decode($row->value, true);
            $ConnectedContacts = json_decode($row->connected_contact, true);
            foreach ($ConnectedContacts as $ContactId => $Details){
                $ContactInfo = ["Name"=>$Details['FirstName']." ".$Details['LastName'],"Email"=>$Details['Email']];
                foreach ($Details['Sequence'] as $CampaignStage => $CampaignDetails){
                    if (strpos($CampaignStage, $param['campaign_id']) !== false && isset($CampaignDetails['MoveAfter'])) {
                        $CampaignDetails['MoveAfter'] = date('Y-m-d H:i:s',$CampaignDetails['MoveAfter']);
                        $CampaignDetails['StartedOn'] = date('Y-m-d H:i:s',$CampaignDetails['StartedOn']);
                        //unset($CampaignDetails['Parsed']);
                        $ItemDetails = [];
                        $ShowOnly = 2;
                        $Count = 0;
                        foreach ($ConnectedDataTitle[$Group] as $key=>$FieldName){
                            $Count++;
                            $ItemDetails[$FieldName] = $Value[$ConnectedDataTitleId[$Group][$key]];
                            if($Count == $ShowOnly) break;
                        }
                        $CDCC[$ConnectedData[$Group]['Name']]['Items'][$Id]['Details'] = $ItemDetails;
                        $CDCC[$ConnectedData[$Group]['Name']]['Items'][$Id]['Contacts'][$ContactId][$CampaignStage]  = $CampaignDetails;
                        $CDCC[$ConnectedData[$Group]['Name']]['Items'][$Id]['Contacts'][$ContactId]["ContactDetails"]  = $ContactInfo;
                    }
                }
            }

        }
        print_r($CDCC);
    }
    public function date_test_get(){
        /*$date = "13/07/2018";
        $date2 = "07/13/2018";
        echo "$date\n";
        $string = isGBDate($date) ? str_replace("/","-",$date):$date;
        echo date('Y-m-d',strtotime($string))."\n<br>";
        echo date('Y-m-d',strtotime($date2));*/
        echo date('Y-m-d H:i:s');
    }
    public function emails_get(){
        header("Content-Type: text/plain");
        error_reporting(E_ERROR);
        $GetMore = true;
        $TotalHistory = [];
        $Offset = 0;
        $Limit = 1000;
        while($GetMore == true ){
            $EmailHistory = infusionsoft_get_email_history(3354,$Limit,$Offset);
            //print_r($EmailHistory);
            if(sizeof($EmailHistory->emails)<1000) $GetMore = false;
            $TotalHistory = array_merge($TotalHistory,$EmailHistory->emails);
            if(isset($EmailHistory->next)){
                $parts = parse_url($EmailHistory->next);
                parse_str($parts['query'], $query);
                if(isset($query['offset'])){
                    $Offset = $query['offset'];
                }
            }
        }
        print_r($TotalHistory);
    }
    public function validate_phone_get(){
        header("Content-Type: text/plain");
        error_reporting(E_ERROR);
       $results =  macanta_validate_phone_number("205","+63025023034","Philippines");
       print_r($results);
    }
    public function Profile_get(){
        $AppCountryCode = infusionsoft_get_app_account_profile()->message->address->country_code;
        $d_code = getCountryCode($AppCountryCode, false, true);
        echo "$AppCountryCode $d_code";
    }
    public function email_get(){
        header("Content-Type: text/plain");
        error_reporting(E_ERROR);
        $json = macanta_validate_email('peter@conquerthechaos.org');
        print_r($json);

    }
    public function fullcontact_get(){
        header("Content-Type: text/plain");
        error_reporting(E_ALL);
        print_r(macanta_full_contact(364,'geover@gmail.com'));
    }
    public function server_var_get(){
        header("Content-Type: text/plain");
        print_r($_SERVER);
    }
    public function app_features_get(){
        $features = macanta_get_app_features();
        print_r(json_decode($features,true));
    }
    public function checklogin_get(){
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ERROR);
        $return = [
                "status" => false,
                "data" => []
        ];
        $PostData = $this->get();
        $Email = $PostData['Email'];
        $Password = $PostData['Password'];
        $results = infusionsoft_get_contact_by_email_password($Email, $Password);
        $Contact = $results->message[0];
        if (isset($Contact->Email) && isset($Contact->Password))
        {
            $return["status"] = true;
            $return["data"] = $Contact;
            $this->response($return, REST_Controller::HTTP_OK);
        }
        $this->response($return, REST_Controller::HTTP_OK);

    }
    public function contact_get(){
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ERROR);
        $PostData = $this->get();
        $ContactId = $PostData['ContactId'];
        $Contact = infusionsoft_get_contact_by_id($ContactId,  array('all'));
        $this->response($Contact, REST_Controller::HTTP_OK);
    }
    public function upload_get(){
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        /*$b2_upload = macanta_b2_upload("/var/www/macanta/qj311/production/current/public/assets/custom_img/file_preview/thumb-maxresdefault.png", "test", "CD_ATTACHMENT");
        print_r($b2_upload);*/
        $im = new Imagick();
        print_r($im->queryFormats());
    }
    public function file_get(){
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $opts = array(
            'http'=>array(
                'method'=>"GET",
                'header'=>"Accept-language: en\r\n" .
                    "Cookie: foo=bar\r\n"
            )
        );

        $context = stream_context_create($opts);
        $theFileData = parse_url("https://docs.google.com/spreadsheets/d/1RA6qno4lNHt2aqtkveEw8VCHpoVfChOHhxrJCpj0TVw/edit#gid=0");
        print_r($theFileData);
        $theFileData = pathinfo("https://docs.google.com/spreadsheets/d/1RA6qno4lNHt2aqtkveEw8VCHpoVfChOHhxrJCpj0TVw/edit#gid=0");
        print_r($theFileData);
    }
    public function adddbconfig_get(){
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ERROR);
        macanta_migrate_centralised();
    }
    public function puthook_get(){
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ERROR);
        macanta_migrate_centralised();
    }
    public function delhook_get(){
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ERROR);
        macanta_delete_hook_subscription();
    }
    public function refresh_token_get(){
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $TokenKeyName = 'InfusionsoftRSToken';
        $TokenResultKeyName = 'InfusionsoftRSTokenResult';
        $tokenSerialized = $this->config->item($TokenKeyName);
        $existing_token = base64_encode($tokenSerialized);
        $new_token = file_get_contents("http://rucksack.macanta.org/generate/refresh_token.php?token=" . $existing_token);

        $DBdata['value'] = $new_token;
        $this->db->where('key',$TokenResultKeyName);
        $this->db->update('config_data', $DBdata);

        $this->db->where('key',$TokenKeyName);
        $this->db->update('config_data', $DBdata);
        echo $new_token;
    }
}
