/*
 Navicat Premium Data Transfer

 Source Server         : MacantaSQLIte
 Source Server Type    : SQLite
 Source Server Version : 3008004
 Source Database       : main

 Target Server Type    : SQLite
 Target Server Version : 3008004
 File Encoding         : utf-8

 Date: 02/02/2017 15:35:46 PM
*/

PRAGMA foreign_keys = false;

-- ----------------------------
--  Table structure for ci_sessions
-- ----------------------------
DROP TABLE IF EXISTS "ci_sessions";
CREATE TABLE 'ci_sessions' ('ip_address' TEXT, 'user_agent' TEXT, 'last_activity' INTEGER, 'data' TEXT, 'timestamp' DATETIME, 'id'  TEXT PRIMARY KEY  );

-- ----------------------------
--  Table structure for config_data
-- ----------------------------
DROP TABLE IF EXISTS "config_data";
CREATE TABLE 'config_data' ('key' TEXT PRIMARY KEY NOT NULL, 'value' TEXT NOT NULL);

-- ----------------------------
--  Table structure for note_tags
-- ----------------------------
DROP TABLE IF EXISTS "note_tags";
CREATE TABLE "note_tags" (
	 "note_id" INTEGER(8,0),
	 "tag_slugs" TEXT
);

-- ----------------------------
--  Table structure for rucksack_logs
-- ----------------------------
DROP TABLE IF EXISTS "rucksack_logs";
CREATE TABLE "rucksack_logs" (
	 "UserId" int(15,0),
	 "Name" text,
	 "Params" text,
	 "Details" text,
	 "DateTaken" text
);

-- ----------------------------
--  Table structure for tags
-- ----------------------------
DROP TABLE IF EXISTS "tags";
CREATE TABLE "tags" (
	 "tag_slug" text NOT NULL,
	 "tag_name" text,
	 "contact_ids" TEXT,
	PRIMARY KEY("tag_slug")
);

-- ----------------------------
--  Table structure for users_meta
-- ----------------------------
DROP TABLE IF EXISTS "users_meta";
CREATE TABLE 'users_meta' ('user_id' INTEGER, 'meta_key' TEXT, 'meta_value' TEXT);

PRAGMA foreign_keys = true;
