<div class="col-md-12 SBSM">
    <div class="row mainbox-top">
        <div class="col-md-12 tab-panel">
            <div class="panel panel-primary theSBSMPanel">
                <div class="panel-heading otherNotes">
                    <h3 class="panel-title NoteName"><i class="glyphicon glyphicon-th-list"></i> <?php echo $this->lang->line('text_current_action_plan');?></h3>
                </div>
                <div class="panel-body actionPan">
                    <h4>Please click "New Action Plan" to get started </h4>
                    <button type="button" class="btn btn-default newActionPlans"> <span class="glyphicon glyphicon-plus-sign"></span> New Action Plan</button>
                </div>
            </div>

        </div>
    </div>
</div>