<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 30/01/2017
 * Time: 4:35 PM
 */

class InfusionsoftRS
{
    public $infusionsoft;
    public $TokenResultKeyName;
    public $TokenKeyName;
    public $tokenSerialized;
    public $LogFile;
    public $CI;
    public $AdminEmails = "geover@gmail.com,alerts@macanta.org";
    public $results = array(
        'Data' => '{}',
        'Message' => [],
        'Errors' => []
    );
    public $accessToken;
    public $RestApiURL = 'https://api.infusionsoft.com/crm/rest/v1';
    public $executeLog = array();
    public function __construct($CI)
    {
        $this->CI = $CI;
        $this->infusionsoft = new \Infusionsoft\Infusionsoft(array());
        $this->infusionsoft->setHttpClient(new \Infusionsoft\Http\CurlClient());
        $this->TokenResultKeyName = __CLASS__."TokenResult";
        $this->TokenKeyName = __CLASS__."Token";
        $this->LogFile = FCPATH."token_log.txt";
        $this->HoldFile = dirname(__FILE__).DIRECTORY_SEPARATOR."hold.dat";
        while (is_file($this->HoldFile)){
            sleep(5);
            unlink($this->HoldFile);
        }
        //Insert Token config if not existing;
        $this->tokenSerialized = $this->CI->config->item($this->TokenKeyName);
        if($this->tokenSerialized == NULL || $this->tokenSerialized == ""){;
            $Message = "Fetching Token From GetMacanta.";
            $ToLog = "[".gmdate('Y-m-d H:i:s')."] ".$Message."\n";
            $this->tokenSerialized  = trim(getInitialGeneratedToken());
            if(false == $this->DbExists('key',$this->TokenKeyName,'config_data')){
                file_put_contents($this->LogFile, $ToLog, FILE_APPEND);
                $DBdata = ['value'=>$this->tokenSerialized,'key'=>$this->TokenKeyName];
                $this->CI->db->insert('config_data', $DBdata);

                $DBdata = ['value'=>$this->tokenSerialized,'key'=>$this->TokenResultKeyName];
                $this->CI->db->insert('config_data', $DBdata);
            }

        }

    }
    public function doMethod($params=array(), $userFn = '', $tokenRefreshed = false){

        $token = unserialize($this->tokenSerialized);

        if($tokenRefreshed == true){
            $DBdata = [];
            $token = $this->infusionsoft->getToken();
            $DBdata['value'] = serialize($token);
            $this->CI->db->where('key',$this->TokenResultKeyName);
            $this->CI->db->update('config_data', $DBdata);
            $suffix = 'again...';
        }else{
            $this->infusionsoft->setToken($token);
            $suffix = '';
        }
        $this->accessToken = $token->accessToken;
        if ($this->infusionsoft->getToken()) {
            try {
                $this->results['Message'][] = "Trying to call $userFn function ".$suffix;
                $result = call_user_func(array($this, $userFn), $params);
                $this->results['Data'] = $result;
                $this->results['Message'][] = "Success!";
                //If token has just been refreshed
                if($tokenRefreshed == true){
                    unlink($this->HoldFile);
                    $DBdata = [];
                    $DBdata['value'] = serialize($token);
                    $this->CI->db->where('key',$this->TokenKeyName);
                    $this->CI->db->update('config_data', $DBdata);
                    $Message = "Infusionsoft Token Successfully Refreshed.";
                    $ToLog = "[".gmdate('Y-m-d H:i:s')."] ".$Message."\n";
                    file_put_contents($this->LogFile, $ToLog, FILE_APPEND);
                }
            } catch (\Infusionsoft\TokenExpiredException $e) {

                if($tokenRefreshed == true){
                    unlink($this->HoldFile);
                    $Message = "Sorry, Token failed to refresh, Please contact Conquer The Chaos Ltd.";
                    $ToLog = "[".gmdate('Y-m-d H:i:s')."] ".$Message."\n";
                    file_put_contents($this->LogFile, $ToLog, FILE_APPEND);
                    $this->results['Errors'][] = $Message;
                    //Email Admin regarding the issue
                    //$this->MailAdmin();
                    //Restart Connected Data services
                    RestartConnectedDataServices();
                    return $this->results;
                }
                if(get_class($e) == "Infusionsoft\TokenExpiredException"){
                    file_put_contents($this->HoldFile,'1');

                    $Message = "Infusionsoft Token Expired., Trying to refresh token..";
                    $ToLog = "\n[".gmdate('Y-m-d H:i:s')."] ".$Message."\n";
                    file_put_contents($this->LogFile, $ToLog, FILE_APPEND);

                    $this->results['Message'][] = $Message;
                    $existing_token = base64_encode($this->tokenSerialized);
                    $new_token = file_get_contents("http://rucksack.macanta.org/generate/refresh_token.php?token=" . $existing_token);

                    $Message = "Refresh Returned Value: ".trim($new_token);
                    $ToLog = "[".gmdate('Y-m-d H:i:s')."] ".$Message."\n";
                    file_put_contents($this->LogFile, $ToLog, FILE_APPEND);

                    $this->infusionsoft->setToken(unserialize(trim($new_token)));
                    $this->results['Message'][] = "New Token: ". trim($new_token);
                    $this->doMethod($params, $userFn,  true);
                }else{
                    $Message = $e->getMessage();
                    $ToLog = "\n[".gmdate('Y-m-d H:i:s')."] Infusionsoft Unknown Token Error: ".$Message."\n";
                    file_put_contents($this->LogFile, $ToLog, FILE_APPEND);
                    $this->results['Errors'][] = $Message;
                    //Email Admin regarding the issue
                    //$this->MailAdmin();
                    return $this->results;
                }

            }
        } else {
            $Message = "Sorry, App Token is not valid, Please contact Conquer The Chaos Ltd";
            $ToLog = "\n[".gmdate('Y-m-d H:i:s')."] ".$Message."\n";
            file_put_contents($this->LogFile, $ToLog, FILE_APPEND);
            $this->results['Errors'][] = $Message;
            //Email Admin regarding the issue
            //$this->MailAdmin();
        }
        return $this->results;
    }
    //REST ACTIONS/
    public function restListHookSubscriptions($params){
        $Get = $Get =  $this->RestApiURL."/hooks?access_token=".$this->accessToken;
        $res = json_decode(trim(file_get_contents($Get)),true);
        return  $this->encodeOutput($res);
    }
    public function restDeleteHookSubscription($params){
        $params = json_decode($params,true);
        $Get =  $this->RestApiURL."/hooks/".$params['key']."?access_token=".$this->accessToken;
        $opts = array('http' =>
            array(
                'method'  => 'DELETE',
                'header'  => 'Content-type: application/json'
            )
        );
        $context  = stream_context_create($opts);
        $res = file_get_contents($Get, false, $context);
        return  $this->encodeOutput($res);
    }
    public function restCreateHookSubscription($params){
        $Get =  $this->RestApiURL."/hooks?access_token=".$this->accessToken;
        $ckfile = tempnam("/tmp", "infusionsoft");
        $ch = curl_init($Get);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $ckfile);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Connection: Keep-Alive'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        $HTML = curl_exec($ch);
        curl_close($ch);
        return  $this->encodeOutput(json_decode($HTML));
    }
    public function restRetrieveCampanyProfile($params){
        $Get = $this->RestApiURL."/account/profile?access_token=".$this->accessToken;
        $res = json_decode(trim(file_get_contents($Get)),true);
        return  $this->encodeOutput($res);
    }
    public function restRetrieveCampaign($params){
        $params = json_decode($params,true);
        $Get = $this->RestApiURL."/campaigns/".$params['CampaignId']."?optional_properties=goals%2Csequences&access_token=".$this->accessToken;
        $res = json_decode(trim(file_get_contents($Get)),true);
        return  $this->encodeOutput($res);
    }
    public function restAddMultipleToCampaignSequence($params){
        $params = json_decode($params,true);
        $Get =  $this->RestApiURL."/campaigns/".$params['CampaignId']."/sequences/".$params['SequenceId']."/contacts?access_token=".$this->accessToken;
        $opts = array('http' =>
            array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/json',
                'content' => json_encode($params['Contacts'])
            )
        );
        $context  = stream_context_create($opts);
        $res = file_get_contents($Get, false, $context);
        return  $this->encodeOutput($res);
    }
    public function restEmailHistory($params){
        $params = json_decode($params,true);///contacts/{contactId}/emails
        $Get = $this->RestApiURL."/contacts/".$params['ContactId']."/emails?access_token=".$this->accessToken."&limit=".$params['limit']."&offset=".$params['offset'];
        $res = json_decode(trim(file_get_contents($Get)),true);
        return  $this->encodeOutput($res);
    }
    public function restEmailItem($params){
        $params = json_decode($params,true);///contacts/{contactId}/emails
        $Get = $this->RestApiURL."/emails/".$params['Id']."?access_token=".$this->accessToken;
        $res = json_decode(trim(file_get_contents($Get)),true);
        return  $this->encodeOutput($res);
    }
    public function restRetrieveFile($params){
        $params = json_decode($params,true);
        $Get = $this->RestApiURL."/files/".$params['FileId']."?optional_properties=file_data&access_token=".$this->accessToken;
        $res = json_decode(trim(file_get_contents($Get)),true);
        return  $this->encodeOutput($res);
    }
    public function restRetrieveFileLite($params){
        $params = json_decode($params,true);
        $Get = $this->RestApiURL."/files/".$params['FileId']."?access_token=".$this->accessToken;
        $res = json_decode(trim(file_get_contents($Get)),true);
        return  $this->encodeOutput($res);
    }
    public function restDeleteFile($params){
        $params = json_decode($params,true);
        $Get = $this->RestApiURL."/files/".$params['FileId']."?access_token=".$this->accessToken;
        $res = json_decode(trim(file_get_contents($Get)),true);
        return  $this->encodeOutput($res);
    }
    //=============
    /*-----uploadFile----*/
    public function uploadFile($params){
        $params = json_decode($params);
        $res = $this->infusionsoft->files('xml')->uploadFile($params->id, $params->fileName, $params->EncodedData);
        return  $this->encodeOutput($res);
    }
    /*-----addContact----*/
    //previously addcon_is action
    public function addContact($params){
        $data = $this->toDataAssoc($params);

        $res = $this->infusionsoft->contacts('xml')->addWithDupCheck($data,'Email');
        if($this->optIn($params)) $this->infusionsoft->emails()->optIn($data['Email'],"Macanta opt-in");

        return  $this->encodeOutput($res);
    }
    /*-----addCustomField----*/
    //previously addcon_is action
    public function addCustomField($params){
        $params = json_decode($params,true);
        $res = $this->infusionsoft->data()->addCustomField($params['customFieldType'], $params['displayName'], $params['dataType'], $params['headerID']);
        return  $this->encodeOutput($res);
    }
    /*----applyTag-----*/
    //previously apply_tag action
    public function applyTag($params){
        $params = json_decode($params);
        $res = $this->infusionsoft->contacts('xml')->addToGroup($params->id, $params->tag);
        return  $this->encodeOutput($res);
    }

    /*----removeTag-----*/
    //previously apply_tag action
    public function removeTag($params){
        $params = json_decode($params);
        $res = $this->infusionsoft->contacts('xml')->removeFromGroup($params->id, $params->tag);
        return  $this->encodeOutput($res);
    }


    /*----createRecord-----*/
    //previously create_is action
    public function createRecord($params){
        $data = $this->toDataAssoc($params);
        $paramsArr = json_decode($params);
        $res = $this->infusionsoft->data()->add($paramsArr->table, $data);
        if($this->optIn($params) && isset($data['Email'])){
            $this->infusionsoft->emails()->optIn($data['Email'],"Macanta opt-in");
        }
        return  $this->encodeOutput($res);
    }

    /*----updateRecord-----*/
    //previously update_is action
    public function updateRecord($params){
        $data = $this->toDataAssoc($params);
        $paramsArr = json_decode($params);
        $res = $this->infusionsoft->data()->update($paramsArr->table, $paramsArr->id, $data);
        if($this->optIn($params) && isset($data['Email'])){
            $this->infusionsoft->emails()->optIn($data['Email'],"Macanta opt-in");
        }
        return  $this->encodeOutput($res);
    }

    /*----readRecord-----*/
    //previously query_is action
    public function readRecord($params){
        $params = json_decode($params);
        $query_by = array();
        foreach($params->query as $field => $value){
            $query_by[$field] = $value;
        }
        $orderby = isset($params->orderby)? $params->orderby:"";
        $res = $this->infusionsoft->data()->query(
            (string)$params->table,
            (int)$params->limit,
            (int)$params->page,
            $query_by,
            $params->fields,
            $orderby,
            true
        );
        return  $this->encodeOutput($res);
    }

    /*----deleteRecord-----*/
    //previously delete_is action
    public function deleteRecord($params){
        $paramsArr = json_decode($params);
        $res = $this->infusionsoft->data()->delete($paramsArr->table, $paramsArr->id);
        return  $this->encodeOutput($res);
    }

    /*----sendEmail-----*/
    //previously email_is action
    public function sendEmail($params){
        $params = json_decode($params);
        foreach($params as $field => $value){ $$field = $value; }
        $res = $this->infusionsoft->emails()->sendEmail(
            array($contactList),
            (string)$fromAddress,
            (string)$toAddress,
            (string)$ccAddresses,
            (string)$bccAddresses,
            (string)$contentType,
            (string)base64_decode($subject),
            (string)base64_decode($htmlBody),
            (string)$textBody,
            (string)''
        );
        return  $this->encodeOutput($res);
    }

    /*----getEmailTemplate-----*/
    //previously email_template_is action
    public function getEmailTemplate($params){
        $params = json_decode($params);
        $res = $this->infusionsoft->emails()->getEmailTemplate((string)$params->Id);
        return  $this->encodeOutput($res);
    }

    /*----getAllReportColumns-----*/
    //previously saved_search_columns_is action
    public function getAllReportColumns($params){
        $params = json_decode($params);
        $res = $this->infusionsoft->search()->getAllReportColumns((int)$params->savedSearchID, (int)$params->userID);
        return  $this->encodeOutput($res);
    }

    /*----getSavedSearchResultsAllFields-----*/
    //previously saved_search_report_is action
    public function getSavedSearchResultsAllFields($params){
        $params = json_decode($params);
        $res = $this->infusionsoft->search()->getSavedSearchResultsAllFields(
            (int)$params->savedSearchID,
            (int)$params->userID,
            (int)$params->pageNumber
        );
        return  $this->encodeOutput($res);
    }

    /*----getAppSetting-----*/
    //previously setting_is action
    public function getAppSetting($params){
        $params = json_decode($params);
        $res = $this->infusionsoft->data()->getAppSetting((string)$params->module, (string)$params->setting);
        return  $this->encodeOutput($res);
    }

    /*----getFormsHTML-----*/
    //previously webform_html_is action
    public function getFormsHTML($params){
        $params = json_decode($params);
        $res = $this->infusionsoft->webForms()->getHTML((string)$params->formid);
        return  $this->encodeOutput($res);
    }

    /*----getWebFormMap-----*/
    //previously webform_map_is action
    public function getWebFormMap($params){
        $res = $this->infusionsoft->webForms()->getMap();
        return  $this->encodeOutput($res);
    }


    public function toDataAssoc($params){
        $paramsArr = json_decode($params);
        $data = array();
        foreach($paramsArr->fields as $field => $value){
            $data[$field] = $value;
        }
        return $data;
    }
    public function optIn($params){
        $paramsArr = json_decode($params);
        return isset($paramsArr->optin)? 1:0;
    }
    public function encodeOutput($res){
        $return_value = array(
            "message" => $res,
        );
        $return_value = json_encode($return_value);
        return $return_value;
    }
    public function DbExists($field,$value,$table)
    {
        $this->CI->db->where($field,$value);
        $query = $this->CI->db->get($table);
        if ($query->num_rows() > 0){

            foreach ($query->result() as $row)
            {
                return true;
            }
            return false;
        }
        else{

            return false;
        }
    }
    public function MailAdmin(){
        $Logs = file_get_contents($this->LogFile);
        $MacantaApp = $this->CI->config->item('IS_App_Name');
        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: Rucksack <help@conquerthechaos.org>' . "\r\n";
        mail($this->AdminEmails,"Token Alert For $MacantaApp",$Logs,$headers);
    }
}