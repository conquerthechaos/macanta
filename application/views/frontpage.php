<script>
    var DevicePlatform = "<?php echo $browser['platform']; ?>";
    console.log("Platform: " + DevicePlatform+", Welcome to macanta console!");
</script>
<?php
if ($browser['platform'] == 'iOS' || $browser['platform'] == "Android") {
    ?>
    <style>
        .or, .other-login-method{
            display: none;
        }
    </style>
    <?php
}
//$DVE = macanta_validate_feature('DVE');
$DVE = 'enabled';
$appname = $this->config->item('IS_App_Name');
if(empty($DVE) || $DVE != "enabled"){
    $CSS_status = "status-off";
    $DVE_status = "OFF";
    $LeadMsg = '<a href="https://macanta.org/add-data-validation-enrichment/?app_name='.$appname.'" target="_blank">Click here to find out more and enable.</a>';

}else{
    $CSS_status = "status-on";
    $DVE_status = "ON";
    $LeadMsg = '';
}
?>
<div class=" front-page-logo col-xs-12 col-sm-12 col-md-12 col-lg-12  no-pad-left no-pad-right" <?php echo isset($class_alignleft) ? $class_alignleft : ""; ?>>
    <a href="<?php echo $this->config->item('base_url'); ?>" class="col-xs-10 col-sm-4 col-md-3 col-lg-3 loginLogo">
        <?php
        $SRC = $this->config->item('base_url')."assets/img/macanta_logo.png";
        $SiteLogo = $this->config->item('sitelogo');
        if (!$SiteLogo){
            $SiteLogo  = macanta_db_record_exist('key','macanta_custom_logo','config_data',true);
            if($SiteLogo){
                $SiteLogoArr = json_decode($SiteLogo->value);
                $SRC = "data:".$SiteLogoArr->type.";base64,".$SiteLogoArr->imageData;
            }
        }else{
            $SRC = $this->config->item('base_url')."assets/custom_img/logo/".$SiteLogo;
        }
        ?>
        <img class="loginLogo"  src="<?php echo $SRC; ?>" style=" " alt="">
    </a>
    <div class="header-announcement col-xs-12 col-sm-12 col-md-8 col-lg-5  no-pad-left no-pad-right">
            <p class="feature-msg">Data Validation & Enrichment: <span class="<?php echo $CSS_status;?>"><?php echo $DVE_status;?><br><?php echo $LeadMsg;?></span></p>
    </div>
    <div class="description col-xs-12 col-sm-12 col-md-8 col-lg-4  no-pad-left no-pad-right">
        <!--Welcome to  <strong>macanta</strong>-->
        <div class="logoutautologin col-xs-10 col-sm-4 col-md-4 col-lg-3">
            <div class="toggleThisAgentContainer">
                <div class="toggleThisAgentContent">
                    <h4>Agent Status: </h4>
                    <div class="toggleThisAgent toggle-iphone"></div>
                </div>
                <div id="inboundactivity" class="inbound-activity   "></div><div id="inboundlog" class="inbound-log  "></div>
            </div>
            <div class="sec1">
                <button class="btn btn-default logout logout-from-dashboard text_logout" type="button"
                        onclick="logout(); return false;"><i class="glyphicon glyphicon-log-out"></i>
                    <?php echo $this->lang->line('text_logout'); ?><!--Logout--></button>
                <!--<button class="btn btn-default user-settings" type="button"><i class="glyphicon glyphicon-cog"></i>
                </button>-->
                <a class="btn btn-default gotoAdmin "><i class="fa fa-cogs" aria-hidden="true"></i> Goto Admin</a>
            </div>
            <div class="sec2">
                <div class="input-group col-lg-6 AutoLoginCopy">
                </div>
                <div class="input-group  col-sm-12 recent-resultsB">

                </div>
            </div>

        </div>


    </div>
</div>



<div class="front-page col-xs-12 col-sm-12 col-md-12 col-lg-12  no-pad-left no-pad-right">

    <div class="row front-page-body">
        <?php echo $FrontPage; ?>

    </div>
    <div class="footnote"><span class="text_fed_by"><?php echo $this->lang->line('text_fed_by'); ?></span> <!--fed by-->
        <strong><img class="isicon" src="<?php echo $this->config->item('base_url'); ?>assets/img/isicon.png" style=" "
                     alt="">Infusionsoft</strong> | <span
                class="text_powered_by"><?php echo $this->lang->line('text_powered_by'); ?></span> <!--powered by-->
        <img class="macantaicon" src="<?php echo $this->config->item('base_url'); ?>assets/img/macantaicon.png"
             style=" " alt=""><strong>macanta</strong>
    </div>
    <div class="app_version">
        <?php

        //$FileLocVersion = basename(dirname(dirname(dirname(__FILE__))));
        //echo $FileLocVersion;
        echo $this->config->item('macanta_verson');
        ?>
    </div>
</div>



<!--Modal For Adding Call Center Agent-->
<div id="AddCallCenterAgent" class="modal fade AddCallCenterAgent " tabindex="-1" role="dialog" aria-labelledby="AddCallCenterAgent">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Choose Contact to Add as Agent</h4>
            </div>
            <div class="modal-body">
                <h3>Type Contact Name</h3>
                <input type="text" name="search-contact-tobe-agent"  class="search-contact-tobe-agent form-control" value="" title="" required="required">
                <div class="col-lg-12 contact-preloading no-pad-right no-pad-left">
                    <table class="contact-preloading-list table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line('text_close');?></button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>


<!--Modal For Editing ConnectOtherContact-->
<div id="ConnectOtherContact" class="modal fade ConnectOtherContact " tabindex="-1" role="dialog" aria-labelledby="ConnectOtherContact">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Search For Contact</h4>
            </div>
            <div class="modal-body">
                <h3>Type Contact Name</h3>
                <input type="text" name="search-contact-to-connect" class="search-contact-to-connect form-control" value="" title="" required="required">
                <div class="col-lg-12 contact-preloading no-pad-right no-pad-left">
                    <table class="contact-preloading-list table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line('text_close');?></button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>

<!--Modal For Add Conencted Data-->
<div id="ConnectOtherData" class="modal fade ConnectOtherData " tabindex="-1" role="dialog" aria-labelledby="ConnectOtherData">
    <div class="modal-dialog connect-other-data">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Choose your connected data</h4>
            </div>
            <div class="modal-body">
                <h5>*Note: ONLY connected data with a relationship will be added</h5>
                <div class="col-lg-3 connected-data-sidebar-filter">
                    <div class="AllConnectedDataContainer">
                        <div class="radio">
                            <input type="radio" name="AllConnectedData" id="AllConnectedData" value="AllConnectedData" checked=""  class="remove-connected-data-filter">
                            <label for="radio1"   class="remove-connected-data-filter">
                                All Connected Data
                            </label>
                        </div>
                    </div>

                    <div class="ByConnectedContactContainer">
                        <div class="filter-contact-list">

                            <div class="radio">
                                <input type="radio" name="ByConnectedContact" id="ByConnectedContact" value="AllConnectedData" class="apply-connected-data-filter">
                                <label for="ByConnectedContact"  >
                                    Filter By Contacts
                                </label>
                            </div>
                            <ul class="ul-filter-contact-list" >

                            </ul>
                        </div>
                        <!--<div class="filter-relation-list">
                            <div class="checkbox">
                                <input id="ByRelationship" type="checkbox"  class="apply-connected-data-filter">
                                <label for="ByRelationship">
                                    Filter By Relationship
                                </label>
                            </div>
                            <select title="Filter Relation List" multiple>
                                <option class="filter-option-item">
                                    Relation Here
                                </option>
                            </select>
                        </div>-->
                    </div>

                </div>
                <div class="col-lg-9 connected-data-sidebar-results">


                </div>
                <div class="col-lg-12">
                    <button type="button" class="btn btn-default  addBulkConnectedInfo" disabled>
                        <i class="fa fa-plus-square-o"></i> Add
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>
<!--Modal For Permission Info-->
<div id="PermissionDetails" class="modal fade PermissionDetails" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Here's how to know if you have permission to market to an address</h4>
            </div>
            <div class="modal-body">
                <ul>

                    <li> This email address was not purchased, rented, borrowed, or harvested.</li>
                    <li> This is not an old, non-responsive email from another list.</li>
                    <li> I can show proof of consent to receive marketing for this email address.</li>
                    <li> I have recently - as in, the last few months - contacted this person.</li>
                    <li> They are expecting the content I intend to send.</li>
                    <li> I have let them know how frequently I will be emailing them.</li>
                    <li> I trust that they provided me with correct information. (offline sign-up forms that require email addresses are often faked or entered incorrectly)</li>

                </ul>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>
<!--Modal For Address List-->
<div id="ContactAddressList" class="modal fade ContactAddressList" tabindex="-1" role="dialog" aria-labelledby="ContactAddressList">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Found Address</h4>
            </div>
            <div class="modal-body">
                <h4 class="currentAddress hideThis">Current Address: <small></small></h4>
                <h4>Please select correct address and confirm.</h4>
                <div class="AddressContainer">
                    <ul class="AddressMainList">

                    </ul>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">Cancel</button>
                <button type="button"
                        class="btn btn-primary VerifySelectedAddressItem">Confirm</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!--Modal For Connected Data File Attachment-->
<div id="CDFileAttachment" class="modal fade CDFileAttachment" tabindex="-1" role="dialog" aria-labelledby="CDFileAttachment">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">File Attachment</h4>
            </div>
            <div class="modal-body">
                <div class="file-container">
                    
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal -->
<?php
/*$MediaPresentation = $this->config->item('MediaPresentation');
if(isset($MediaPresentation['Type']) && $MediaPresentation['Type'] != 'wistia'){
    */?><!--
    <div class="modal fade MediaPresentation" id="MediaPresentation" tabindex="-1" role="dialog" aria-labelledby="MediaPresentationLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"><?php /*echo $MediaPresentation['Title']; */?></h4>
                </div>
                <div class="modal-body">

                    <iframe id="iframeYoutube" width="560" height="315"  src="<?php /*echo $MediaPresentation['URL']; */?>" frameborder="0" allowfullscreen></iframe>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
--><?php /*}
*/?>


<select class="CountryTeplate hide" id="CountryTeplate" name="CountryTeplate">
    <option value="">Please select a country</option>
    <option value="Afghanistan">Afghanistan</option>
    <option value="Albania">Albania</option>
    <option value="Algeria">Algeria</option>
    <option value="American Samoa">American Samoa</option>
    <option value="Andorra">Andorra</option>
    <option value="Angola">Angola</option>
    <option value="Anguilla">Anguilla</option>
    <option value="Antarctica">Antarctica</option>
    <option value="Antigua and Barbuda">Antigua and Barbuda</option>
    <option value="Argentina">Argentina</option>
    <option value="Armenia">Armenia</option>
    <option value="Aruba">Aruba</option>
    <option value="Australia">Australia</option>
    <option value="Austria">Austria</option>
    <option value="Åland Islands">Åland Islands</option>
    <option value="Azerbaijan">Azerbaijan</option>
    <option value="Bahamas">Bahamas</option>
    <option value="Bahrain">Bahrain</option>
    <option value="Bangladesh">Bangladesh</option>
    <option value="Barbados">Barbados</option>
    <option value="Belarus">Belarus</option>
    <option value="Belgium">Belgium</option>
    <option value="Belize">Belize</option>
    <option value="Benin">Benin</option>
    <option value="Bermuda">Bermuda</option>
    <option value="Bhutan">Bhutan</option>
    <option value="Bolivia">Bolivia</option>
    <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
    <option value="Botswana">Botswana</option>
    <option value="Bouvet Island">Bouvet Island</option>
    <option value="Brazil">Brazil</option>
    <option value="British Indian Ocean Territory">British Indian Ocean
        Territory
    </option>
    <option value="Brunei Darussalam">Brunei Darussalam</option>
    <option value="Bulgaria">Bulgaria</option>
    <option value="Burkina Faso">Burkina Faso</option>
    <option value="Burundi">Burundi</option>
    <option value="Cambodia">Cambodia</option>
    <option value="Cameroon">Cameroon</option>
    <option value="Canada">Canada</option>
    <option value="Cape Verde">Cape Verde</option>
    <option value="Cayman Islands">Cayman Islands</option>
    <option value="Central African Republic">Central African Republic
    </option>
    <option value="Chad">Chad</option>
    <option value="Chile">Chile</option>
    <option value="China">China</option>
    <option value="Christmas Island">Christmas Island</option>
    <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
    <option value="Colombia">Colombia</option>
    <option value="Comoros">Comoros</option>
    <option value="Congo">Congo</option>
    <option value="Democratic Republic Of Congo">Democratic Republic Of
        Congo
    </option>
    <option value="Cook Islands">Cook Islands</option>
    <option value="Costa Rica">Costa Rica</option>
    <option value="Croatia">Croatia</option>
    <option value="Cuba">Cuba</option>
    <option value="Cyprus">Cyprus</option>
    <option value="Czech Republic">Czech Republic</option>
    <option value="Côte D'Ivoire">Côte D'Ivoire</option>
    <option value="Denmark">Denmark</option>
    <option value="Djibouti">Djibouti</option>
    <option value="Dominica">Dominica</option>
    <option value="Dominican Republic">Dominican Republic</option>
    <option value="Ecuador">Ecuador</option>
    <option value="Egypt">Egypt</option>
    <option value="El Salvador">El Salvador</option>
    <option value="Equatorial Guinea">Equatorial Guinea</option>
    <option value="Eritrea">Eritrea</option>
    <option value="Estonia">Estonia</option>
    <option value="Ethiopia">Ethiopia</option>
    <option value="Falkland Islands">Falkland Islands</option>
    <option value="Faroe Islands">Faroe Islands</option>
    <option value="Fiji">Fiji</option>
    <option value="Finland">Finland</option>
    <option value="France">France</option>
    <option value="French Guiana">French Guiana</option>
    <option value="French Polynesia">French Polynesia</option>
    <option value="French Southern Territories">French Southern
        Territories
    </option>
    <option value="Gabon">Gabon</option>
    <option value="Gambia">Gambia</option>
    <option value="Georgia">Georgia</option>
    <option value="Germany">Germany</option>
    <option value="Ghana">Ghana</option>
    <option value="Gibraltar">Gibraltar</option>
    <option value="Greece">Greece</option>
    <option value="Greenland">Greenland</option>
    <option value="Grenada">Grenada</option>
    <option value="Guadeloupe">Guadeloupe</option>
    <option value="Guam">Guam</option>
    <option value="Guatemala">Guatemala</option>
    <option value="Guernsey">Guernsey</option>
    <option value="Guinea">Guinea</option>
    <option value="Guinea-Bissau">Guinea-Bissau</option>
    <option value="Guyana">Guyana</option>
    <option value="Haiti">Haiti</option>
    <option value="Heard and McDonald Islands">Heard and McDonald Islands
    </option>
    <option value="Holy See (Vatican City State)">Holy See (Vatican City
        State)
    </option>
    <option value="Honduras">Honduras</option>
    <option value="Hong Kong">Hong Kong</option>
    <option value="Hungary">Hungary</option>
    <option value="Iceland">Iceland</option>
    <option value="India">India</option>
    <option value="Indonesia">Indonesia</option>
    <option value="Iran">Iran</option>
    <option value="Iraq">Iraq</option>
    <option value="Ireland">Ireland</option>
    <option value="Isle of Man">Isle of Man</option>
    <option value="Israel">Israel</option>
    <option value="Italy">Italy</option>
    <option value="Jamaica">Jamaica</option>
    <option value="Japan">Japan</option>
    <option value="Jersey">Jersey</option>
    <option value="Jordan">Jordan</option>
    <option value="Kazakhstan">Kazakhstan</option>
    <option value="Kenya">Kenya</option>
    <option value="Kiribati">Kiribati</option>
    <option value="North Korea">North Korea</option>
    <option value="South Korea">South Korea</option>
    <option value="Kuwait">Kuwait</option>
    <option value="Kyrgyzstan">Kyrgyzstan</option>
    <option value="Laos">Laos</option>
    <option value="Latvia">Latvia</option>
    <option value="Lebanon">Lebanon</option>
    <option value="Lesotho">Lesotho</option>
    <option value="Liberia">Liberia</option>
    <option value="Libya">Libya</option>
    <option value="Liechtenstein">Liechtenstein</option>
    <option value="Lithuania">Lithuania</option>
    <option value="Luxembourg">Luxembourg</option>
    <option value="Macao">Macao</option>
    <option value="Republic of Macedonia">Republic of Macedonia</option>
    <option value="Madagascar">Madagascar</option>
    <option value="Malawi">Malawi</option>
    <option value="Malaysia">Malaysia</option>
    <option value="Maldives">Maldives</option>
    <option value="Mali">Mali</option>
    <option value="Malta">Malta</option>
    <option value="Marshall Islands">Marshall Islands</option>
    <option value="Martinique">Martinique</option>
    <option value="Mauritania">Mauritania</option>
    <option value="Mauritius">Mauritius</option>
    <option value="Mayotte">Mayotte</option>
    <option value="Mexico">Mexico</option>
    <option value="Federated States of Micronesia">Federated States of
        Micronesia
    </option>
    <option value="Moldova">Moldova</option>
    <option value="Monaco">Monaco</option>
    <option value="Mongolia">Mongolia</option>
    <option value="Montenegro">Montenegro</option>
    <option value="Montserrat">Montserrat</option>
    <option value="Morocco">Morocco</option>
    <option value="Mozambique">Mozambique</option>
    <option value="Myanmar">Myanmar</option>
    <option value="Namibia">Namibia</option>
    <option value="Nauru">Nauru</option>
    <option value="Nepal">Nepal</option>
    <option value="Netherlands">Netherlands</option>
    <option value="Netherlands Antilles">Netherlands Antilles</option>
    <option value="New Caledonia">New Caledonia</option>
    <option value="New Zealand">New Zealand</option>
    <option value="Nicaragua">Nicaragua</option>
    <option value="Niger">Niger</option>
    <option value="Nigeria">Nigeria</option>
    <option value="Niue">Niue</option>
    <option value="Norfolk Island">Norfolk Island</option>
    <option value="Northern Mariana Islands">Northern Mariana Islands
    </option>
    <option value="Norway">Norway</option>
    <option value="Oman">Oman</option>
    <option value="Pakistan">Pakistan</option>
    <option value="Palau">Palau</option>
    <option value="Palestine">Palestine</option>
    <option value="Panama">Panama</option>
    <option value="Papua New Guinea">Papua New Guinea</option>
    <option value="Paraguay">Paraguay</option>
    <option value="Peru">Peru</option>
    <option value="Philippines">Philippines</option>
    <option value="Pitcairn">Pitcairn</option>
    <option value="Poland">Poland</option>
    <option value="Portugal">Portugal</option>
    <option value="Puerto Rico">Puerto Rico</option>
    <option value="Qatar">Qatar</option>
    <option value="Romania">Romania</option>
    <option value="Russian Federation">Russian Federation</option>
    <option value="Rwanda">Rwanda</option>
    <option value="Réunion">Réunion</option>
    <option value="St. Barthélemy">St. Barthélemy</option>
    <option value="St. Helena, Ascension and Tristan Da Cunha">St. Helena,
        Ascension and Tristan Da Cunha
    </option>
    <option value="St. Kitts And Nevis">St. Kitts And Nevis</option>
    <option value="St. Lucia">St. Lucia</option>
    <option value="St. Martin">St. Martin</option>
    <option value="St. Pierre And Miquelon">St. Pierre And Miquelon</option>
    <option value="St. Vincent And The Grenedines">St. Vincent And The
        Grenedines
    </option>
    <option value="Samoa">Samoa</option>
    <option value="San Marino">San Marino</option>
    <option value="Sao Tome and Principe">Sao Tome and Principe</option>
    <option value="Saudi Arabia">Saudi Arabia</option>
    <option value="Senegal">Senegal</option>
    <option value="Serbia">Serbia</option>
    <option value="Seychelles">Seychelles</option>
    <option value="Sierra Leone">Sierra Leone</option>
    <option value="Singapore">Singapore</option>
    <option value="Slovakia">Slovakia</option>
    <option value="Slovenia">Slovenia</option>
    <option value="Solomon Islands">Solomon Islands</option>
    <option value="Somalia">Somalia</option>
    <option value="South Africa">South Africa</option>
    <option value="South Georgia and the South Sandwich Islands">South
        Georgia and the South Sandwich Islands
    </option>
    <option value="Spain">Spain</option>
    <option value="Sri Lanka">Sri Lanka</option>
    <option value="Sudan">Sudan</option>
    <option value="Suriname">Suriname</option>
    <option value="Svalbard And Jan Mayen">Svalbard And Jan Mayen</option>
    <option value="Swaziland">Swaziland</option>
    <option value="Sweden">Sweden</option>
    <option value="Switzerland">Switzerland</option>
    <option value="Syrian Arab Republic">Syrian Arab Republic</option>
    <option value="Taiwan">Taiwan</option>
    <option value="Tajikistan">Tajikistan</option>
    <option value="Tanzania">Tanzania</option>
    <option value="Thailand">Thailand</option>
    <option value="Timor-Leste">Timor-Leste</option>
    <option value="Togo">Togo</option>
    <option value="Tokelau">Tokelau</option>
    <option value="Tonga">Tonga</option>
    <option value="Trinidad and Tobago">Trinidad and Tobago</option>
    <option value="Tunisia">Tunisia</option>
    <option value="Turkey">Turkey</option>
    <option value="Turkmenistan">Turkmenistan</option>
    <option value="Turks and Caicos Islands">Turks and Caicos Islands
    </option>
    <option value="Tuvalu">Tuvalu</option>
    <option value="Uganda">Uganda</option>
    <option value="Ukraine">Ukraine</option>
    <option value="United Arab Emirates">United Arab Emirates</option>
    <option value="United Kingdom">United Kingdom</option>
    <option value="United States">United States</option>
    <option value="US Minor Outlying Islands">US Minor Outlying Islands
    </option>
    <option value="Uruguay">Uruguay</option>
    <option value="Uzbekistan">Uzbekistan</option>
    <option value="Vanuatu">Vanuatu</option>
    <option value="Venezuela">Venezuela</option>
    <option value="Viet Nam">Viet Nam</option>
    <option value="Virgin Islands, British">Virgin Islands, British</option>
    <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
    <option value="Wallis and Futuna">Wallis and Futuna</option>
    <option value="Western Sahara">Western Sahara</option>
    <option value="Yemen">Yemen</option>
    <option value="Zambia">Zambia</option>
    <option value="Zimbabwe">Zimbabwe</option>
</select>