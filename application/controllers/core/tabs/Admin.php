<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 07/03/16
 * Time: 3:00 PM
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');
use Twilio\Rest\Client;
class Admin extends MY_Controller
{
    private $Contact;
    public $ajaxResults = array(
        "status"=> 0, // true or false
        "message"=>"",// any massage
        "html"=>"",// any html view
        "data" => array(), // returned data
        "script" => '' // javascript need to execute // returned data
    );
    public $AdminSections = array(
        1 => array("id" =>"admin_LookFeel", "label"=>"Look & Feel", "view"=>"core/tab_admin_look_feel", "status"=>"active"),
        2 => array("id" =>"admin_Permissions", "label"=>"Security & Saved Searches", "view"=>"core/tab_admin_permissions"),
        //3 => array("id" =>"admin_NoteTags", "label"=>"Note #tags", "view"=>"core/tab_admin_note_tags"),
        4 => array("id" =>"admin_CustomTabs", "label"=>"Custom Tabs", "view"=>"core/tab_admin_custom_tabs"),
        //5 => array("id" =>"admin_Triggers", "label"=>"Triggers", "view"=>"core/tab_admin_triggers"),
        6 => array("id" =>"admin_CallSettings", "label"=>"Call Settings", "view"=>"core/tab_admin_call_settings"),
        7 => array("id" =>"admin_Other", "label"=>"Pipeline & R'ships Mgt.", "view"=>"core/tab_admin_Other"),
        8 => array("id" =>"admin_Connector", "label"=>"Connector", "view"=>"core/tab_admin_connector"),
        //9 => array("id" =>"admin_CallCenter", "label"=>"Call Center", "view"=>"core/tab_admin_callcenter")


    );
    public function __construct($Prams){
        parent::__construct();
        $this->Contact = $Prams;
        //$this->load->database(); this is already loaded in My_Controller
        //$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file')); this is already loaded in My_Controller

    }

    public function index($Data)
    {
        $CurrentDir = dirname(__FILE__) . "/";
        if($this->config->item('CallCenter') && strtolower(trim($this->config->item('CallCenter'))) == 'enabled'){
            $this->AdminSections[] = array("id" =>"admin_CallCenter", "label"=>"Call Center", "view"=>"core/tab_admin_callcenter");
        }
        $ToRecord = '';
        foreach($this->AdminSections as $Sections){
            $TimeStarted = time();
            $TabStatus[$Sections["id"]] = isset($Sections["status"]) ? $Sections["status"]:false;
            $TabLabel[$Sections["id"]] = $Sections["label"];
            $TabView[$Sections["id"]] = sanitize_output($this->load->view($Sections["view"], null, true));
            $TimeEnded = time();
            $TimeLapse = $TimeEnded - $TimeStarted;
            $ToRecord .= $Sections["label"] ." ". $TimeLapse . " | Size: ". strlen($TabView[$Sections["id"]] )/1000 ."\n";

        }
        file_put_contents($CurrentDir . "_AdminTimeLapse_".$this->config->item('IS_App_Name').".txt", $ToRecord . "\n");

        $Prams['menu'] = $TabLabel;
        $Prams['status'] = $TabStatus;
        $Prams['content'] = $TabView;
        $HTML =  $this->load->view('core/tab_admin', $Prams, true);
        return $HTML;
    }
    public function changeMacantaTagCategory($data){
       $catId =  $data['catId'];
        $OtherTags = infusionsoft_get_tags_by_cat_id($catId);
        $DBPermissionTags = json_decode($this->config->item('access_level'));
        $Prams['DBPermissionTags'] = $DBPermissionTags;
        $Prams['OtherTags'] = $OtherTags;
        $HTML =  $this->load->view('core/tab_admin_permissions-selections-admin-user', $Prams, true);
        $this->ajaxResults['message'] = 'changeMacantaTagCategory results';
        $this->ajaxResults['html'] = $HTML;
        $this->ajaxResults['script'] = "$('.admintagpicker').selectpicker('refresh')";
        return $this->ajaxResults;
    }
    public function saveSearchFilterTagPairs($data){
        $Pairs = $data["pairs"];
        $FilterNameArr = array();
        $SavePairsArr = array();
        $SavedSearch = array();
        //Restructure the saved search array
        $Filters = infusionsoft_get_all_saved_search();
        $Filters = sortMultiArray($Filters,'ReportStoredName');
        foreach($Filters as $SavedSearchItem){
            $SavedSearch[$SavedSearchItem->FilterName] = $SavedSearchItem;
        }
        foreach ($Pairs as $Pair){
            if($Pair['value']){
                $FilterNameArr[] = $Pair['value'];
                $TempFilter = $SavedSearch[$Pair['value']];
                $theTag = $this->getcreateTagByName(
                    $Pair['value'],
                    "savedsearch_permission_cat",
                    "macanta saved search permission",
                    "macanta >> saved search >> "
                );
                $SavePairsArr[$TempFilter->Id] = $theTag->Id; // this will be save in the database
            }
        }
        // update Databse
        $DBdata = array();
        $DBdata['value'] = json_encode($SavePairsArr);
        if(false == $this->DbExists('key','saved_search_restriction','config_data')){
            $DBdata['key'] = 'saved_search_restriction';
            $this->db->insert('config_data', $DBdata);
        }else{
            $this->db->where('key','saved_search_restriction');
            $this->db->update('config_data',$DBdata);
        }
        if(sizeof($SavePairsArr) > 0 && isset($data['session_name'])){
            $session_name = $data['session_name'];
            $user_seession_data = macanta_get_user_seession_data($session_name);
            $Session_data = unserialize($user_seession_data->session_data);
            $this->ajaxResults['user_success'] = applyFn('set_macanta_user_success',$Session_data['email']);
        }
        $this->ajaxResults['message'] = '';
        $this->ajaxResults['data'] = $SavePairsArr;
        $this->ajaxResults['html'] = '';
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
    }
    public function saveNoteEditingPermissions($data){
        $Values = $data["Values"];
        // update Databse
        $DBdata = array();
        $DBdata['value'] = trim($Values);
        if(false == $this->DbExists('key','note_editing_permission','config_data')){
            $DBdata['key'] = 'note_editing_permission';
            $this->db->insert('config_data', $DBdata);
        }else{
            $this->db->where('key','note_editing_permission');
            $this->db->update('config_data',$DBdata);
        }
    }
    public function getcreateTagByName($Name, $CatKey ="savedsearch_permission_cat", $TagDesc = "macanta saved search permission" , $prefix = ''){
        $CatId =  $this->config->item($CatKey);
        $theTag = infusionsoft_get_tags_by_groupname_and_catId(trim($Name),$CatId);
        if(isset($theTag[0])){
            return $theTag[0];
        }else{
            $theTag = infusionsoft_get_tags_by_groupname_and_catId($prefix.trim($Name),$CatId);
            if(isset($theTag[0])){
                return $theTag[0];
            }
            $newTag = array();
            $newTag['GroupCategoryId'] = $CatId;
            $newTag['GroupName'] = $prefix.trim($Name);
            $newTag['GroupDescription']  = $TagDesc;
            $result  = infusionsoft_create_tag($newTag);
            $newTag['Id'] = $result->message;

            return json_decode(json_encode($newTag));
        }

    }
    public function saveMacantaAccess($data){
        $MacantaTagCat = $data['MacantaTagCat'];
        $MacantaAdminTag = $data['MacantaAdminTag'];
        $MacantaUserTag = $data['MacantaUserTag'];
        $MacantaContactViewPermissionCat = $data['MacantaContactViewPermissionCat'];
        $MacantaSavedSearchPermissionCat = $data['MacantaSavedSearchPermissionCat'];
        $values = array("administrator"=>"$MacantaAdminTag","staff"=>"$MacantaUserTag","member"=>"0");
        $DBdata = array();
        $DBdata['value'] = json_encode($values);
        $this->db->where('key','access_level');
        $this->db->update('config_data',$DBdata);

        $DBdata['value'] = $MacantaContactViewPermissionCat;
        $this->db->where('key','contactview_permission_cat');
        $this->db->update('config_data',$DBdata);

        $DBdata['value'] = $MacantaSavedSearchPermissionCat;
        $this->db->where('key','savedsearch_permission_cat');
        $this->db->update('config_data',$DBdata);


        $this->ajaxResults['message'] = 'config_data updated';
        $this->ajaxResults['html'] = "";
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
    }
    public function addContactTag($data){
        $tagId =  $data['tagId'];
        $ConId =  $data['ContactId'];
        $user_seession_data = macanta_get_user_seession_data($data['session_name']);
        $session_data = unserialize($user_seession_data->session_data);
        $results = infusionsoft_apply_tag($ConId, $tagId);
        $searched_cache = manual_cache_loader('searched_cache' . $session_data['InfusionsoftID']);
        if ($searched_cache) {
            $searched_cache = json_decode($searched_cache, true);
            foreach ($searched_cache as $key => $Contact) {
                if ($Contact['Id'] == $session_data['InfusionsoftID']) {
                    //$OldContact = $Contact;
                    $TagArr = explode(',',$Contact['Groups']);
                    $TagArr[] = $tagId;
                    $Tags = implode(',',$TagArr);
                    $Contact['Groups'] = $Tags;
                    $searched_cache[$key] = $Contact;
                    break;
                }
            }
            manual_cache_writer('searched_cache' . $session_data['InfusionsoftID'], json_encode($searched_cache), 86400);
        }
        $this->ajaxResults['message'] = $results;
        return $this->ajaxResults;
    }
    public function removeContactTag($data){
        $tagId =  $data['tagId'];
        $ConId =  $data['ContactId'];
        $user_seession_data = macanta_get_user_seession_data($data['session_name']);
        $session_data = unserialize($user_seession_data->session_data);
        $results = infusionsoft_remove_tag($ConId, $tagId);
        $searched_cache = manual_cache_loader('searched_cache' . $session_data['InfusionsoftID']);
        if ($searched_cache) {
            $searched_cache = json_decode($searched_cache, true);
            foreach ($searched_cache as $key => $Contact) {
                if ($Contact['Id'] == $session_data['InfusionsoftID']) {
                    //$OldContact = $Contact;
                    $TagArr = explode(',',$Contact['Groups']);
                    $key = array_search($tagId,$TagArr);
                    unset($TagArr[$key]);
                    $Tags = implode(',',$TagArr);
                    $Contact['Groups'] = $Tags;
                    $searched_cache[$key] = $Contact;
                    break;
                }
            }
            manual_cache_writer('searched_cache' . $session_data['InfusionsoftID'], json_encode($searched_cache), 86400);
        }

        $this->ajaxResults['message'] = $results;
        return $this->ajaxResults;
    }
    public function refreshSavedSearchTag(){
        $ForAutocompleteArr = array();
        $DBFilterTagPair = json_decode($this->config->item('saved_search_restriction'),true);
        $Filters = infusionsoft_get_all_saved_search(true);
        $Filters = sortMultiArray($Filters,'ReportStoredName');
        foreach($Filters as $SavedSearch){
            if(!isset($DBFilterTagPair[$SavedSearch->Id])){
                $ForAutocompleteArr[]='"'.$SavedSearch->FilterName.'"';
            }
        }
        $ForAutocomplete = "[".implode(',',$ForAutocompleteArr)."]";
        $this->ajaxResults['message'] = 'refreshSavedSearchTag results';
        $this->ajaxResults['script'] = "validOptions = $ForAutocomplete; SaveSearchAutoCompleteDestroy(); SaveSearchAutoComplete();";
        return $this->ajaxResults;
    }
    public function getSavedSearchTag($data){
        $PairNames = array();
        $ForAutocompleteArr = array();
        $DBFilterTagPair = json_decode($this->config->item('saved_search_restriction'),true);
        $Filters = infusionsoft_get_all_saved_search();
        $Filters = sortMultiArray($Filters,'ReportStoredName');
        foreach($Filters as $SavedSearch){
            if(isset($DBFilterTagPair[$SavedSearch->Id])){
                //$tempname = str_replace('macanta_','',$SavedSearch->FilterName);
                //$tempname = str_replace('macanta','',$tempname);
                $PairNames[] = array("name"=>trim($SavedSearch->FilterName),"filterid"=>$SavedSearch->Id, "tagid"=>$DBFilterTagPair[$SavedSearch->Id]);

            }else{
                $ForAutocompleteArr[]='"'.$SavedSearch->FilterName.'"';
            }
        }
        $ForAutocomplete = "[".implode(',',$ForAutocompleteArr)."]";
        //$HTML =  $this->load->view('core/tab_admin_permissions-selections', $Prams, true);
        $this->ajaxResults['message'] = 'getSavedSearchTag results';
        $Prams['PairNames'] = $PairNames;
        $Prams['ForAutocomplete'] = $ForAutocomplete;
        $HTML =  $this->load->view('core/tab_admin_permissions-savedsearch', $Prams, true);
        $this->ajaxResults['html'] = $HTML;
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
    }
    public function getTagCatSelection(){

        $DBPermissionTags = json_decode($this->config->item('access_level'));
        $AdminTag = $DBPermissionTags->administrator;
        $AdminTagDetails = infusionsoft_get_tags($AdminTag);
        $CurrentMacantaTagCategoryId = $AdminTagDetails[0]->GroupCategoryId;
        $UserTag = $DBPermissionTags->staff;

        $UserTagDetails = infusionsoft_get_tags($UserTag);
        $OtherTags = infusionsoft_get_tags_by_cat_id($CurrentMacantaTagCategoryId);
        $TagCategories = infusionsoft_get_tags_category();
        $Prams['DBPermissionTags'] = $DBPermissionTags;
        $Prams['CurrentMacantaTagCategoryId'] = $CurrentMacantaTagCategoryId;
        $Prams['TagCategories'] = $TagCategories;
        $Prams['UserTagDetails'] = $UserTagDetails;
        $Prams['OtherTags'] = $OtherTags;
        $HTML =  $this->load->view('core/tab_admin_permissions-selections', $Prams, true);
        $this->ajaxResults['message'] = 'getTagCatSelection results';
        $this->ajaxResults['html'] = $HTML;
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
    }
    public function savePipelines($data){
        $DBdata['value'] = $data['Pipelines'];
        if($this->DbExists("key","OpportunityPipeline","config_data")){
            $this->db->where('key','OpportunityPipeline');
            $this->db->update('config_data',$DBdata);
        }else{
            $DBdata['key'] = 'OpportunityPipeline';
            $this->db->insert('config_data', $DBdata);
        }
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['data'] = $data;
        $this->ajaxResults['message'] = "saveCustomTabs Results Saved";
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
    }
    public function saveRelationships($data){
        $Temp = json_decode($data['Relationships'], true);
        $New = $Temp;
        foreach ($Temp as $index => $Relationships){
            if($Relationships['Id'] == '') $New[$index]['Id'] = macanta_generate_key('re_');
        }
        $DBdata['value'] = json_encode($New);
        if($this->DbExists("key","ConnectorRelationship","config_data")){
            $this->db->where('key','ConnectorRelationship');
            $this->db->update('config_data',$DBdata);
        }else{
            $DBdata['key'] = 'ConnectorRelationship';
            $this->db->insert('config_data', $DBdata);
        }
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['data'] = $data;
        $this->ajaxResults['message'] = "saveRelationships Saved";
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
    }
    public function setLanguage($data){
        $DBdata = array();
        $DBdata['value'] = $data['language'];
        $this->db->where('key','macanta_lang');
        $this->db->update('config_data',$DBdata);
        $this->ajaxResults['message'] = $data['language'];
        $this->lang->load('macanta', $data['language']);
        $all_lang_array = $this->lang->language;
        $JSscript = '';
        foreach($all_lang_array as $key=>$value){
            $value = json_encode(str_replace("'",'',$value));
            $JSscript .= '$("span.'.$key.'").html('.$value.');';
        }
        $this->ajaxResults['script']=$JSscript;
        //print_r($all_lang_array); // shows all the langauage line
        return $this->ajaxResults;
    }
    public function saveCustomTabs($data){

        // will add script tp create tag under access_permission_cat
        $DBdata = array();
        $DBdata['value'] = json_encode($data['CustomTabs']);
        $data['CustomTabsOffTagId'] = trim($data['CustomTabsOffTagId']);
        $CustomTabsOffTagId = $data['CustomTabsOffTagId'] ? $data['CustomTabsOffTagId']:"";
        $CustomTabOnTagId = $data['CustomTabOnTagId'] ? $data['CustomTabOnTagId']:"";
        $DBdata2['value'] = $CustomTabsOffTagId;
        $DBdata3['value'] = $CustomTabOnTagId;
        $oldCustomTabs = $this->config->item('custom_tabs');
        $beforeProc =  false;
        // convert to array with key id
        $oldCustomTabs = json_decode($oldCustomTabs,true);
        $oldCustomTabsArr = array();
        $newCustomTabsArr = array();
        foreach($oldCustomTabs as $oldCustomTabKey => $oldCustomTabParam){
            $oldCustomTabsArr[$oldCustomTabParam['id']] = $oldCustomTabParam;
        }
        $newCustomTabs = json_decode($DBdata['value'],true);
        foreach($newCustomTabs as $newCustomTabKey => $newCustomTabParam){
            $newCustomTabsArr[$newCustomTabParam['id']] = $newCustomTabParam;
        }

        foreach($newCustomTabsArr as $tabId => $tabParam){
            if($newCustomTabsArr[$tabId]['permission']!=="ShowToAll"){
                if(array_key_exists($tabId,$oldCustomTabsArr)){

                        if(isset($oldCustomTabsArr[$tabId]['permission_tag']) && $oldCustomTabsArr[$tabId]['permission_tag'] != ""){
                            $newCustomTabsArr[$tabId]['permission_tag'] = $oldCustomTabsArr[$tabId]['permission_tag'];
                        }else{
                            // retrieve or create permission tag for this custom tab
                            if($beforeProc) sleep(1);
                            $theTag = $this->getcreateTagByName(
                                $newCustomTabsArr[$tabId]['title'],
                                "access_cat_id",
                                "macanta custom tab permission",
                                "macanta >> custom tab >> ");
                            $newCustomTabsArr[$tabId]['permission_tag'] = $theTag->Id;
                            $beforeProc = true;
                        }

                }else{
                    // this is a new custom tab with permission
                    // retrieve or create permission tag for this custom tab
                    if($beforeProc) sleep(1);
                    $theTag = $this->getcreateTagByName(
                        $newCustomTabsArr[$tabId]['title'],
                        "access_cat_id",
                        "macanta custom tab permission",
                        "macanta >> custom tab >> ");
                    $newCustomTabsArr[$tabId]['permission_tag'] = $theTag->Id;
                    $beforeProc = true;

                }
            }else{
                if(array_key_exists($tabId,$oldCustomTabsArr)){

                    if(isset($oldCustomTabsArr[$tabId]['permission_tag']) && $oldCustomTabsArr[$tabId]['permission_tag'] != ""){
                        $newCustomTabsArr[$tabId]['permission_tag'] = $oldCustomTabsArr[$tabId]['permission_tag'];
                    }else{
                        // retrieve or create permission tag for this custom tab
                        $newCustomTabsArr[$tabId]['permission_tag']="";
                    }

                }
            }
        }
        // Update the database
        $DBdata['value'] = json_encode($newCustomTabsArr);
        if($this->DbExists("key","custom_tabs","config_data")){
            $this->db->where('key','custom_tabs');
            $this->db->update('config_data',$DBdata);
        }else{
            $DBdata['key'] = 'custom_tabs';
            $this->db->insert('config_data', $DBdata);
        }
        if($this->DbExists("key","custom_tabs_tag","config_data")){
            $this->db->where('key','custom_tabs_tag');
            $this->db->update('config_data',$DBdata2);
        }else{
            $DBdata2['key'] = 'custom_tabs_tag';
            $this->db->insert('config_data', $DBdata2);
        }
        if($this->DbExists("key","custom_tabs_on_tag","config_data")){
            $this->db->where('key','custom_tabs_on_tag');
            $this->db->update('config_data',$DBdata3);
        }else{
            $DBdata3['key'] = 'custom_tabs_on_tag';
            $this->db->insert('config_data', $DBdata3);
        }
        if(sizeof($newCustomTabsArr) > 0 && isset($data['session_name'])){
            $session_name = $data['session_name'];
            $user_seession_data = macanta_get_user_seession_data($session_name);
            $Session_data = unserialize($user_seession_data->session_data);
            $this->ajaxResults['user_success'] = applyFn('set_macanta_user_success',$Session_data['email']);
        }
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['data'] = $data;
        $this->ajaxResults['message'] = "saveCustomTabs Results: ".json_encode($theTag);
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
    }
    public function saveConnectedInfos($data){

        $SystemConnectedInfos = json_decode($data['ConnectedInfos'], true);



        $oldCustomTabs = $this->config->item('connected_info');
        $beforeProc =  false;
        // convert to array with key id
        $oldCustomTabs = json_decode($oldCustomTabs,true);
        $oldCustomTabsArr = array();
        $newCustomTabsArr = array();
        foreach($oldCustomTabs as $oldCustomTabKey => $oldCustomTabParam){
            $oldCustomTabsArr[$oldCustomTabParam['id']] = $oldCustomTabParam;
        }
        foreach($SystemConnectedInfos as $newCustomTabKey => $newCustomTabParam){
            $newCustomTabsArr[$newCustomTabParam['id']] = $newCustomTabParam;
        }

        foreach($newCustomTabsArr as $tabId => $tabParam){
            if($newCustomTabsArr[$tabId]['visibility']!=="ShowCDToAll"){
                if(array_key_exists($tabId,$oldCustomTabsArr)){

                    if(isset($oldCustomTabsArr[$tabId]['permission_tag']) && $oldCustomTabsArr[$tabId]['permission_tag'] != ""){
                        $newCustomTabsArr[$tabId]['permission_tag'] = $oldCustomTabsArr[$tabId]['permission_tag'];
                    }else{
                        // retrieve or create permission tag for this custom tab
                        if($beforeProc) sleep(1);
                        $theTag = $this->getcreateTagByName(
                            $newCustomTabsArr[$tabId]['title'],
                            "access_cat_id",
                            "macanta custom tab permission",
                            "macanta >> connected info >> ");
                        $newCustomTabsArr[$tabId]['permission_tag'] = $theTag->Id;
                        $beforeProc = true;
                    }

                }else{
                    // this is a new custom tab with permission
                    // retrieve or create permission tag for this custom tab
                    if($beforeProc) sleep(1);
                    $theTag = $this->getcreateTagByName(
                        $newCustomTabsArr[$tabId]['title'],
                        "access_cat_id",
                        "macanta custom tab permission",
                        "macanta >> connected info >> ");
                    $newCustomTabsArr[$tabId]['permission_tag'] = $theTag->Id;
                    $beforeProc = true;

                }
            }else{
                if(array_key_exists($tabId,$oldCustomTabsArr)){

                    if(isset($oldCustomTabsArr[$tabId]['permission_tag']) && $oldCustomTabsArr[$tabId]['permission_tag'] != ""){
                        $newCustomTabsArr[$tabId]['permission_tag'] = $oldCustomTabsArr[$tabId]['permission_tag'];
                    }else{
                        // retrieve or create permission tag for this custom tab
                        $newCustomTabsArr[$tabId]['permission_tag']="";
                    }

                }else{
                    $newCustomTabsArr[$tabId]['permission_tag']="";
                }
            }
        }



        $DBdata = array();
        $DBdata['value'] = json_encode($newCustomTabsArr);
        if($DBdata['value'] != '[]'){
            if($this->DbExists("key","connected_info","config_data")){
                $this->db->where('key','connected_info');
                $this->db->update('config_data',$DBdata);
            }else{
                $DBdata['key'] = 'connected_info';
                $this->db->insert('config_data', $DBdata);
            }
        }








        if(sizeof($data['ConnectedInfos']) > 0 && isset($data['session_name'])){
            $session_name = $data['session_name'];
            $user_seession_data = macanta_get_user_seession_data($session_name);
            $Session_data = unserialize($user_seession_data->session_data);
            $this->ajaxResults['user_success'] = applyFn('set_macanta_user_success',$Session_data['email']);
        }



        //Update Contacts Connected Info
        $GUID = [];
        $ConInfo = [];
        foreach ($SystemConnectedInfos as $index => $SystemConnectedInfo){
            $GUID[] = $SystemConnectedInfo['id'];
            foreach ($SystemConnectedInfo['fields'] as $FieldIndex => $Field){
                    $ConInfo[$SystemConnectedInfo['id']][] = $Field['fieldId'];
            }

        }
        $DBdata = array();
        $this->db->where('meta_key','connected_info');
        $query = $this->db->get('users_meta');
        if ($query->num_rows() > 0){

            foreach ($query->result() as $row)
            {
                $values =  json_decode($row->meta_value, true);
                $user_id = $row->user_id;
                foreach ($values as $theGUID => $Items){
                    if(!in_array($theGUID, $GUID)){
                        unset($values[$theGUID]);
                    }else{
                        foreach ($Items as $ItemId=>$Fields){
                            foreach ($Fields as $FieldId => $FieldValue){
                                if(!in_array($FieldId, $ConInfo[$theGUID])) unset($values[$theGUID][$ItemId][$FieldId]);
                                if(sizeof($values[$theGUID][$ItemId]) == 0) unset($values[$theGUID][$ItemId]);
                                if(sizeof($values[$theGUID]) == 0) unset($values[$theGUID]);
                            }
                        }
                    }
                }
                $NewValue = json_encode($values);
                $this->db->where('user_id',$user_id);
                $this->db->where('meta_key','connected_info');
                $DBdata['meta_value'] = $NewValue;
                $this->db->update('users_meta', $DBdata);
            }
        }


        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['GUID'] = $GUID;
        $this->ajaxResults['ConInfo'] = $ConInfo;
        $this->ajaxResults['data'] = json_decode($data['ConnectedInfos']);
        $this->ajaxResults['message'] = "Saved";
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
    }
    public function saveUsersCallerId($data){
        $UsersCallerIds = $data['UsersCallerIds'];
        $UsersCallerIds = json_decode($UsersCallerIds, true);
        $CallerIds = [];
        $ContactId = 0;
        foreach ($UsersCallerIds as $field){
            if($field['name'] == "UserId"){
                $ContactId = $field['value'];
            }elseif ($field['name'] == "CallerId"){
                $CallerIds[] = $field['value'];
            }
        }
        $this->db->where('user_id',$ContactId);
        $this->db->where('meta_key','caller_id');
        $query = $this->db->get('users_meta');
        $row = $query->row();
        if (isset($row))
        {
            $this->db->where('user_id',$ContactId);
            $this->db->where('meta_key','caller_id');
            $DBdata['meta_value'] = json_encode($CallerIds);
            $this->db->update('users_meta', $DBdata);
        }else{
            $DBdata['user_id'] = $ContactId;
            $DBdata['meta_key'] = 'caller_id';
            $DBdata['meta_value'] = json_encode($CallerIds);
            $this->db->insert('users_meta', $DBdata);
        }
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['data'] = json_encode(getUsersCallerIds());
        $this->ajaxResults['message'] = "saveUsersCallerId Results: ".json_encode($CallerIds);
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
    }
    public function saveCustomJS($data){
        $content = $data['content'];
        $this->db->where('key','CustomJS');
        $query = $this->db->get('config_data');
        $row = $query->row();
        $DBdata = [];
        if (isset($row))
        {
            $this->db->where('key','CustomJS');
            $DBdata['value'] = $content;
            $result = $this->db->update('config_data', $DBdata);
        }else{
            $DBdata['key'] = 'CustomJS';
            $DBdata['value'] = $content;
            $result = $this->db->insert('config_data', $DBdata);
        }
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['message'] = $result;
        return $this->ajaxResults;
    }
    public function saveCustomCSS($data){
        $content = $data['content'];
        $this->db->where('key','CustomCSS');
        $query = $this->db->get('config_data');
        $row = $query->row();
        $DBdata = [];
        if (isset($row))
        {
            $this->db->where('key','CustomCSS');
            $DBdata['value'] = $content;
            $result = $this->db->update('config_data', $DBdata);
        }else{
            $DBdata['key'] = 'CustomCSS';
            $DBdata['value'] = $content;
            $result = $this->db->insert('config_data', $DBdata);
        }
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['message'] = $result;
        return $this->ajaxResults;
    }
    public function insert_update($table,$keyName,$keyValue,$fieldName,$fieldValue, $valueToJson = false){
        //todo:finish this function
        $this->db->where($keyName,$keyValue);
        $query = $this->db->get($table);
        $row = $query->row();
        if (isset($row))
        {
            $existingDevices = json_decode($row->value,true);
            if(!in_array($deviceNumber,$existingDevices)){
                $existingDevices[] = $deviceNumber;
                $this->db->where($keyName,$keyValue);
                $DBdata['value'] = json_encode($existingDevices);
                $this->db->update($table, $DBdata);
            }

        }else{
            $existingDevices[] = $deviceNumber;
            $DBdata['key'] = 'outbound_devices';
            $DBdata['value'] = json_encode($existingDevices);
            $this->db->insert($table, $DBdata);
        }
    }
    public function deleteCallerId($data){
        $Twilio_Account_SID = $this->config->item('Twilio_Account_SID');
        $Twilio_TOKEN = $this->config->item('Twilio_TOKEN');

        try {
            $client = new Twilio\Rest\Client($Twilio_Account_SID, $Twilio_TOKEN);
            $client->outgoingCallerIds($data['SID'])->delete();
            $this->db->where('key','outbound_devices');
            $query = $this->db->get('config_data');
            $row = $query->row();
            if (isset($row))
            {
                if($row->value !== "[]" && $row->value !== ""){
                    $existingDevices = json_decode($row->value,true);
                    $temp = [];
                    foreach ($existingDevices as $existingDevice){
                        if($existingDevice == $data['phone']) continue;
                        $temp[] = $existingDevice;
                    }
                    $existingDevices = $temp;
                    $this->db->where('key','outbound_devices');
                    $DBdata['value'] = json_encode($existingDevices);;
                    $this->db->update('config_data', $DBdata);
                }
            }
            $this->ajaxResults['message'] = "deleted";

        } catch (Twilio\Exceptions\RestException $e) {
            $this->ajaxResults['message'] = $e->getMessage();
        }

        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['data'] = "";
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
    }
    public function saveCallerIdLabel($data){
        $Twilio_Account_SID = $this->config->item('Twilio_Account_SID');
        $Twilio_TOKEN = $this->config->item('Twilio_TOKEN');
        try {
            $client = new Twilio\Rest\Client($Twilio_Account_SID, $Twilio_TOKEN);
            $caller_id = $client
                ->outgoingCallerIds($data['SID'])
                ->update(
                    array("FriendlyName" => $data['Label'])
                );
            //echo $caller_id->phone_number;
            $deviceNumber = $caller_id->phoneNumber;
            $this->db->where('key','outbound_devices');
            $query = $this->db->get('config_data');
            $row = $query->row();
            $existingDevices = [];
            if (isset($row))
            {
                $row->value = $row->value == "" ? "[]":$row->value;
                $existingDevices = json_decode($row->value,true);
                if($data['SetAsDevice'] === 'yes') {
                    if(!in_array($deviceNumber,$existingDevices)){
                        $existingDevices[] = $deviceNumber;
                    }
                }else{
                    $temp = [];
                    foreach ($existingDevices as $existingDevice){
                        if($existingDevice == $deviceNumber) continue;
                        $temp[] = $existingDevice;
                    }
                    $existingDevices = $temp;
                }
                $this->db->where('key','outbound_devices');
                $DBdata['value'] = json_encode($existingDevices);
                $this->db->update('config_data', $DBdata);
            }else{
                if($data['SetAsDevice'] == 'yes') {
                    $existingDevices[] = $deviceNumber;
                    $DBdata['key'] = 'outbound_devices';
                    $DBdata['value'] = json_encode($existingDevices);
                    $this->db->insert('config_data', $DBdata);
                }

            }

        } catch (Twilio\Exceptions\RestException $e) {
            $this->ajaxResults['message'] = $e->getMessage();;
        }
        $this->ajaxResults['existingDevices'] = $existingDevices;
        $this->ajaxResults['deviceNumber'] = $deviceNumber;
        $this->ajaxResults['json_decode'] = json_decode($row->value);
        $this->ajaxResults['script'] = '';
        return $this->ajaxResults;
    }
    public function makeDefaultCallerId($data){
        $PhoneNumber = $data['PhoneNumber'];
        $caller_string = array("callerID" => $PhoneNumber);
        file_put_contents(FCPATH."twilio.txt",json_encode($caller_string));
        ob_start();
        //Get All Available Company Caller ID
        $account_sid = $this->config->item('Twilio_Account_SID');
        $auth_token = $this->config->item('Twilio_TOKEN');
        try {
            $client = new Twilio\Rest\Client($account_sid, $auth_token);
            $CompanyCallerIds = $client->outgoingCallerIds->read();

        } catch (Twilio\Exceptions\RestException $e) {
            $CompanyCallerIds = [];
            echo '<span style="    color: red; font-size: 14px; text-align: left; width: 100%; line-height: 16px !important; display: inherit;">Sorry, Twilio Communication System Down, Details:'.$e->getMessage().'</span>';

        }
        $CompanyCallerIdsTemp = [];
        foreach ($CompanyCallerIds as $CallerId){
            $CompanyCallerIdsTemp[$CallerId->friendlyName] = $CallerId;
        }
        ksort($CompanyCallerIdsTemp);
        $CompanyCallerIds = [];
        foreach ($CompanyCallerIdsTemp as $friendly_name => $CallerId){
            $CompanyCallerIds[] = $CallerId;
        }

        //$ClientTwilioCredentials = json_decode(trim(file_get_contents(FCPATH."twilio.txt")));
        $theURL = $_SERVER['HTTP_HOST'];
        $parsedTheURL = parse_url($theURL);
        $theHost = explode('.', $parsedTheURL['path']);
        $theSubdomain = $theHost[0];
        $ClientTwilioCredentials = json_decode(trim(file_get_contents(FCPATH."twilio.txt")));
        $DefaultCallerId = $ClientTwilioCredentials->callerID ;
        ?>
        <div class="CallerIdItem">
            <div class="form-group default">
                <input class="AllowUserCallerId  "  type="checkbox" id="CallerIdDefault" name="CallerIdDefault"   autocomplete="off" disabled checked />
                <div class="btn-group">
                    <label for="CallerIdDefault" class="btn btn-default ">
                        <span class="glyphicon glyphicon-ok"></span>
                        <span> </span>
                    </label>
                    <label for="CallerIdDefault" class="btn btn-default checkbox-label active ">
                        <div class="MacantaCallerIdTitle">
                            System Default
                        </div>
                        <div class="MacantaCallerIdNumber">
                            <?php echo $DefaultCallerId ?  $DefaultCallerId:"No Default Caller ID"?>
                        </div>
                    </label>
                </div>
            </div>
        </div>
        <form id="MacantaCallerIdList" method="post" class="form-horizontal MacantaCallerIdList dynamic"
              _lpchecked="1">
            <input type="hidden" name="UserId"  class="UserId">
            <ul class="itemContainer">
                <?php
                if($this->config->item('outbound_devices')){
                    $SystemOuboundDevices = json_decode($this->config->item('outbound_devices'), true);
                }else{
                    $SystemOuboundDevices = [];
                }
                foreach ($CompanyCallerIds as $CallerId) {
                    if($DefaultCallerId == $CallerId->phoneNumber) continue;
                    $friendly_name = $CallerId->friendlyName;
                    $friendly_name = $CallerId->phoneNumber == $friendly_name ? "<em>No Label</em>":$friendly_name;
                    $Id = $CallerId->sid;
                    $data =  'data-callerid="'.$CallerId->phoneNumber.'"';
                    $info = "hide";
                    if(in_array($CallerId->phoneNumber, $SystemOuboundDevices)) $info = "";
                    $isdevice = $info == "hide" ? 'no':'yes';
                    $fwidth = $info == "hide" ? 'fwidth':'';
                    ?>
                    <li class="form-group-item" data-calleridtype="user-defined"  data-sid="<?php echo $CallerId->sid; ?>" data-guid="<?php echo $CallerId->phoneNumber; ?>">
                        <div class="CallerIdItem">
                            <div class="form-group success">
                                <input class="AllowUserCallerId UserCallerIdToEnable" value="<?php echo $CallerId->phoneNumber; ?>" data-callerid="<?php echo $CallerId->phoneNumber; ?>" type="checkbox" name="CallerId" id="callerId-<?php echo $Id; ?>" autocomplete="off" disabled   />
                                <div class="btn-group">
                                    <label for="callerId-<?php echo $Id; ?>" class="btn btn-success toggleCallerId">
                                        <span class="glyphicon glyphicon-ok"></span>
                                        <span> </span>
                                    </label>
                                    <label class="btn btn-default checkbox-label active ">
                                        <div class="MacantaCallerIdTitle <?php echo $Id; ?> <?php echo $fwidth; ?>" id="label<?php echo $Id; ?>" title="<?php echo $friendly_name ?>">
                                            <?php echo $friendly_name ?>
                                        </div>
                                        <div class="MacantaCallerIdNumber  <?php echo $Id; ?> <?php echo $fwidth; ?>">
                                            <div class="the-number">
                                                <?php echo $CallerId->phoneNumber ?>
                                            </div>
                                            <div class="the-info">
                                                            <span class="info-as-device-<?php echo $Id; ?>">
                                                                    <a class="info-as-device <?php echo $info; ?>">Can be used as a call device</a>
                                                                </span>
                                            </div>
                                        </div>
                                    </label>
                                    <i class="fa fa-pencil-square-o EditCallerIdLabelIcon CallerIDAction" aria-hidden="true"
                                       data-sid="<?php echo $CallerId->sid ?>"
                                       data-callerid="<?php echo $CallerId->phoneNumber ?>"
                                       data-label="<?php echo $CallerId->friendlyName ?>"
                                       data-isdevice="<?php echo $isdevice; ?>"></i>
                                    <i class="fa fa-trash-o DeleteCallerIdIcon CallerIDAction" aria-hidden="true"
                                       data-sid="<?php echo $CallerId->sid ?>"
                                       data-phone="<?php echo $CallerId->phoneNumber ?>"
                                    ></i>
                                </div>
                            </div>


                        </div>

                    </li>
                    <?php
                }
                ?>

            </ul>

        </form>
        <?php
        $html = ob_get_contents();
        ob_end_clean();
        $this->ajaxResults['html_content'] = $html;
        $this->ajaxResults['script'] = '';
        return $this->ajaxResults;

    }
    public function addCallerId($data)
    {   //"ExtNumber":ExtNumber, "PhoneNumber":PhoneNumber, 'Label':LabelToVerify
        $Twilio_Account_SID = $this->config->item('Twilio_Account_SID');
        $Twilio_TOKEN = $this->config->item('Twilio_TOKEN');
        try {
            $client = new Twilio\Rest\Client($Twilio_Account_SID, $Twilio_TOKEN);
            $file_random_hash = time() . rand(10000, 99990);
            $this->ajaxResults['file_id'] = $file_random_hash;

        } catch (Twilio\Exceptions\RestException $e) {
            $client = false;
            $this->ajaxResults['message'] = $e->getMessage();
        }
        if($client != false){
            try {
                $validationRequest = $client->validationRequests->create(
                    $data['PhoneNumber'],
                    array(
                        "friendlyName" => $data['Label'],
                        "extension" => $data['ExtNumber'],
                        "statusCallback" => "https://macanta.org/twilio/post.php?from=$file_random_hash"
                    )
                );
                $this->ajaxResults['message'] = [
                    "validationCode"=>$validationRequest->__get('validationCode'),
                    'friendlyName' =>$validationRequest->__get('friendlyName')
                ];
            } catch (Exception $e) {
                $this->ajaxResults['message'] = 'already_in';
            }
        }

        return $this->ajaxResults;
    }
    public function checkAddCallerId($data)
    {   //"ExtNumber":ExtNumber, "PhoneNumber":PhoneNumber, "file_id":e.file_id, 'Label':LabelToVerify
        $found = false;
        $file_path = "https://macanta.org/twilio/" . $data['file_id'] . ".txt";
        $count = 0;
        $HTML = '';
        $SetAsDevice = $data['SetAsDevice'];
        if($this->config->item('outbound_devices')){
            $SystemOuboundDevices = json_decode($this->config->item('outbound_devices'), true);
        }else{
            $SystemOuboundDevices = [];
        }
        if($SetAsDevice == "yes") $SystemOuboundDevices[] = $data['PhoneNumber'];
        while (!$found) {
            $fileData = @file_get_contents($file_path);
            if ($fileData) {
                $found = true;
                $existingDevices = [];
                if($SetAsDevice == 'yes') {
                    $deviceNumber = $data['PhoneNumber'];
                    $this->db->where('key','outbound_devices');
                    $query = $this->db->get('config_data');
                    $row = $query->row();
                    if (isset($row))
                    {
                        $existingDevices = json_decode($row->value,true);
                        if(!in_array($deviceNumber,$existingDevices)){
                            $existingDevices[] = $deviceNumber;
                            $this->db->where('key','outbound_devices');
                            $DBdata['value'] = json_encode($existingDevices);
                            $this->db->update('config_data', $DBdata);
                        }

                    }else{
                        $existingDevices[] = $deviceNumber;
                        $DBdata['key'] = 'outbound_devices';
                        $DBdata['value'] = json_encode($existingDevices);
                        $this->db->insert('config_data', $DBdata);
                    }
                }
                $message = json_decode($fileData);
                if($message->VerificationStatus == 'success'){
                    //Get All Available Company Caller ID
                    $account_sid = $this->config->item('Twilio_Account_SID');
                    $auth_token = $this->config->item('Twilio_TOKEN');
                    $client = new Twilio\Rest\Client($account_sid, $auth_token);
                    $CompanyCallerIds = $client->outgoingCallerIds->read();

                    foreach ($CompanyCallerIds as $CallerId){
                        $CompanyCallerIdsTemp[$CallerId->friendlyName] = $CallerId;
                    }
                    ksort($CompanyCallerIdsTemp);
                    $CompanyCallerIds = [];
                    foreach ($CompanyCallerIdsTemp as $friendly_name => $CallerId){
                        $CompanyCallerIds[] = $CallerId;
                    }
                    ob_start();
                    //$ClientTwilioCredentials = json_decode(trim(file_get_contents(FCPATH."twilio.txt")));
                    $ClientTwilioCredentials = json_decode(trim(file_get_contents(FCPATH."twilio.txt")));
                    $DefaultCallerId = $ClientTwilioCredentials->callerID;

                    foreach ($CompanyCallerIds as $CallerId) {
                        if($DefaultCallerId == $CallerId->phoneNumber) continue;
                        $friendly_name = $CallerId->friendlyName;
                        $friendly_name = $CallerId->phoneNumber == $friendly_name ? "<em>No Label</em>":$friendly_name;
                        $Id = $CallerId->sid;
                        $info = "hide";
                        if(in_array($CallerId->phoneNumber, $SystemOuboundDevices)) $info = "";
                        $isdevice = $info == "hide" ? 'no':'yes';
                        $fwidth = $info == "hide" ? 'fwidth':'';
                        echo '<li class="form-group-item" data-calleridtype="user-defined"  data-sid="'.$CallerId->sid.'"  data-guid="'.$CallerId->phoneNumber.'">
                            <div class="CallerIdItem">
                                <div class="form-group success">
                                    <input class="AllowUserCallerId UserCallerIdToEnable" value="'.$CallerId->phoneNumber.'" data-callerid="'.$CallerId->phoneNumber.'"  type="checkbox" name="CallerId" id="callerId-'.$Id.'" autocomplete="off" disabled   />
                                    <div class="btn-group">
                                        <label for="callerId-'.$Id.'" class="btn btn-success toggleCallerId">
                                            <span class="glyphicon glyphicon-ok"></span>
                                            <span> </span>
                                        </label>
                                        <label  class="btn btn-default checkbox-label active">
                                            <div class="MacantaCallerIdTitle '.$Id.' '.$fwidth.'" id="label'.$Id.'"  title="'.$friendly_name.'">
                                                '.$friendly_name.'
                                            </div>
                                            <div class="MacantaCallerIdNumber '.$Id.' '.$fwidth.'">
                                            <div class="the-number">
                                                 '.$CallerId->phoneNumber.'
                                             </div>
                                                <div class="the-info">
                                                     <span class="info-as-device-'.$Id.'">
                                                     <a class="info-as-device '.$info.'">Can be used as a call device`</a>
                                                    </span>
                                                 </div>
                                            </div>
                                        </label>
                                        <i class="fa fa-pencil-square-o EditCallerIdLabelIcon CallerIDAction" aria-hidden="true"
                                            data-sid="'.$CallerId->sid.'"
                                            data-callerid="'.$CallerId->phoneNumber.'"
                                            data-label="'.$CallerId->friendlyName.'" 
                                            data-isdevice="'.$isdevice.'"></i>
                                        <i class="fa fa-trash-o DeleteCallerIdIcon CallerIDAction" aria-hidden="true"
                                           data-sid="'.$CallerId->sid.'"
                                           data-phone="'.$CallerId->phoneNumber.'" ></i>
                                    </div>
                                </div>
                            </div>
                        </li>';
                    }
                    $HTML = ob_get_contents();
                    ob_end_clean();

                }
                return array("message" => $message,"html"=>$HTML);
            } else {
                $count++;
                sleep(5);
            }
            if ($count > 20) {
                $found = true;
            }
        }
        return array("message" => "error");
    }
    public function setLogo(){
        $DBdata = array();
        $this->ajaxResults['data'] = $_FILES['adminlogofile'];
        $tmp_name = $_FILES["adminlogofile"]["tmp_name"];
        $uploads_dir = FCPATH . "assets/custom_img/logo";
        if (!file_exists($uploads_dir)) {
            mkdir($uploads_dir, 0777, true);
        }
        // basename() may prevent filesystem traversal attacks;
        // further validation/sanitation of the filename may be appropriate
        $name = 'macanta-'.time().str_replace(' ','-',basename($_FILES["adminlogofile"]["name"]));
        $DBdata['value'] = $name;
        $status = move_uploaded_file($tmp_name, "$uploads_dir/$name");

        // update Databse
        if(false == $this->DbExists('key','sitelogo','config_data')){
            $DBdata['key'] = 'sitelogo';
            $this->db->insert('config_data', $DBdata);
        }else{
            $this->db->where('key','sitelogo');
            $this->db->update('config_data',$DBdata);
        }
        $this->ajaxResults['status'] = $status;
        $this->ajaxResults['script'] = 'if(typeof serviceWorkerRegistration !== "undefined"){ serviceWorkerRegistration.unregister();}caches.delete(\'macanta-cache\');$("img.loginLogo").attr("src","'.$this->config->item('base_url').'assets/custom_img/logo/'.$name.'");';
        $this->ajaxResults['script'] .= '$("img.adminloginLogo").attr("src","'.$this->config->item('base_url').'assets/custom_img/logo/'.$name.'");';

        return $this->ajaxResults;
    }
    public function DbExists($field,$value,$table)
    {
        $this->db->where($field,$value);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0){

            foreach ($query->result() as $row)
            {
                return true;
            }
            return false;
        }
        else{

            return false;
        }
    }


}
