/**
 * Created by Geover on 06/04/16.
 */
// Macanta Sub Functions
String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}
String.prototype.b64encode = function() {
    return btoa(unescape(encodeURIComponent(this)));
};
String.prototype.b64decode = function() {
    return decodeURIComponent(escape(atob(this)));
};
String.prototype.trunc = String.prototype.trunc ||
    function(n){
        return (this.length > n) ? this.substr(0, n-1) + '&hellip;' : this;
    };
// I use this function to unbind event first before to bind again to prevent multiple instances
$.fn.once = function(a, b) {
    return this.each(function() {
        $(this).off(a).on(a,b);
    });
};
$.fn._once = function(a, c, b) {
    return this.each(function() {
        $(this).off(a, c).on(a, c, b);
    });
};
$.fn.extend({
    // this is to include all uncheck boxes
    serializeArrayB: function() {
        return this.map(function(){
            return this.elements ? jQuery.makeArray( this.elements ) : this;
        })
            .filter(function(){
                return this.name && !this.disabled &&
                    ( this.checked || !this.checked || rselectTextarea.test( this.nodeName ) || rinput.test( this.type ) );
            })

            .map(function( i, elem ){
                var val = jQuery( this ).val();
                    if ((jQuery(this).prop('checked') != undefined) && (!jQuery(this).prop('checked'))) {
                        val = "false";
                    }
                return val == null ?
                    null :
                    jQuery.isArray( val ) ?
                        jQuery.map( val, function( val, i ){
                            return { name: elem.name, value: val.replace( /\r?\n/g, "\r\n" ) };
                        }) :
                    { name: elem.name, value: val.replace( /\r?\n/g, "\r\n" ) };
            }).get();
    }
});
/*
 'Notification title', {
 icon: 'http://cdn.sstatic.net/stackexchange/img/logos/so/so-icon.png',
 body: "Hey there! You've been notified!",
 }
 */
function MacantaDesktopNotification(Title,Options) {
    if (!("Notification" in window)) {
        alert("This browser does not support desktop notification");
    }
    else if (Notification.permission === "granted") {
        var notification = new Notification(Title,Options);
    }
    else if (Notification.permission !== "denied") {
        Notification.requestPermission(function (permission) {
            if (permission === "granted") {
                var notification = new Notification(Title,Options);
            }
        });
    }
}
function Timer(callback, delay) {
    var timerId, start, remaining = delay, orig =  delay;

    this.pause = function() {
        window.clearTimeout(timerId);
        remaining -= new Date() - start;
    };

    this.resume = function() {
        start = new Date();
        window.clearTimeout(timerId);
        timerId = window.setTimeout(callback, remaining);
    };

    this.reset = function() {
        start = new Date();
        window.clearTimeout(timerId);
        remaining = orig;
        timerId = window.setTimeout(callback, remaining);
    };

    this.resume();
}

function randomString(len, charSet) {
    charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
    }
    return randomString;
}
function unique(array) {
    var newarray = [];
    $.each(array, function(key, value){
        var ret = $.inArray( value, newarray );
        if(ret == -1){
            newarray.push(value);
        }
    });

    return newarray;
}
function escapeRegExp(str) {
    return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}
function realTimeNoteFilter(staffSelected,tagInserted,dateRagne,searchStr){
    var y = (x == 2 ? "yes" : "no");
    var staffSelectedArr = staffSelected.split(',');
    var tagInsertedArr = staffSelected.split(',')
}
function savedSearchReport(StrIds,source, Group){
    var IdArr = StrIds.split(":");
    var UserId = IdArr[0];
    var SearchId = IdArr[1];
    console.log("UserId: "+UserId);
    console.log("SearchId: "+SearchId);
    console.log("Source: "+source);
    var jsonData;
    var pageBody = $(".front-page-body");
    // if the search came from dashboard bring the search box first
    if(source == 'InAppSavedSearch'){
        removeHash ();

        jsonData = {"controler":"core/common","action":"frontpage","session_name":session_name,"data":{"session_name":session_name,"Platform":DevicePlatform,"assetsVersion":assetsVersion}};
        $("#fountainG").fadeIn();
        $.ajax({
            url: ajax_url,
            type: "POST",
            data: jsonData,
            success: function(e){
                console.log(e);
                pageBody.fadeOut(100, function(){
                    $(this).html(e.data);
                    $(this).fadeIn(300,function(){
                        removeAddedClasses();
                    });
                    eval(e.script);
                    savedSearchReport(StrIds, "MainSavedSearch", Group);
                });
            }
        });
    }else{

        jsonData = {"controler":"core/common","action":"savesearchreport","session_name":session_name,"data":{"UserId":UserId,"SearchId":SearchId,"Group":Group,"session_name":session_name,"assetsVersion":assetsVersion}};
        var theTable = $("#ContactsTable tbody");
        console.log(jsonData)
        $("#fountainG").fadeIn();
        $(".btn.search").attr("disabled", true);
        pageBody.css("opacity",0);

        $.ajax({
            url: ajax_url,
            type: "POST",
            data: jsonData,
            beforeSend: function(){
                if(source == 'InAppSavedSearch'){
                    getFrontPage();
                }
            },
            success: function(e){
                localStorage.setItem( 'DataTablesDisplayStart', 0 );
                localStorage.setItem( 'DataTablesDisplayPage', 0 );
                console.log(e)
                eval(e.script);

            }
        });
    }


}
function filterNotes(staffSelected,tagInserted,dateRagne,searchStr){
    console.log("searchStr: "+searchStr);
    console.log("dateRagne: "+dateRagne);
    console.log("tagInserted: "+tagInserted);
    staffSelected = staffSelected || '';
    console.log("staffSelected: "+staffSelected);
    var FiltersPanel = $(".CurrentPersonalNotes");
    var NotesPanel = $(".PreviousQuickNotes");
    var controller = "core/tabs/note"
    var jsonData = {"controler":controller,"action":"filterNote","session_name":session_name,"data":{"conId":ContactId, "searchStr":searchStr, "dateRagne":dateRagne,"tagInserted":tagInserted, "staffSelected":staffSelected,"session_name":session_name,"assetsVersion":assetsVersion}};
    $(NotesPanel).addClass("loading-overlay loading-center");
    $(".panel-body", NotesPanel).css("opacity", 0);
    $(".panel-body", FiltersPanel).css("opacity", 0.3);
    $("button.filterNotes").attr('disabled', true);
    console.log(jsonData);
    $.ajax({
        url: ajax_url,
        type: "POST",
        data: jsonData,
        success: function(e){
            console.log(e)
            $("button.filterNotes").removeAttr('disabled');
            $(NotesPanel).removeClass("loading-overlay");
            $(NotesPanel).removeClass("loading-center");
            $(".panel-body", NotesPanel).css("opacity", 1);
            $(".panel-body", FiltersPanel).css("opacity", 1);
            var theData = e.data || e;
            $( ".prev-notes", NotesPanel).replaceWith(theData);
            var script = e.script || '';
            eval(script);
        }
    });



}
function getConnectedContactNotes(ConnectedContacts,GUID) {
    var theContactIds = [];
    var ContactsNotesTableTarget = $("table#ConnectedContactsNote"+GUID);
    $.each(ConnectedContacts,function (theId,theRelationships) {
        theContactIds.push(theId);
    });
    var jsonData = {"controler":"core/tabs/note","action":"getContactsNotes","session_name":session_name,"data":{"ContactIds":theContactIds,"session_name":session_name,"assetsVersion":assetsVersion}};
    var toDataTableContactsNotes = [];
    var successFn = function(e){
        console.log("Data: ");
       console.log(e);
        $.each(e.data, function(key,noteObj){
            var theContactId = noteObj.ContactId;
            var theContact = ConnectedContacts[theContactId].FirstName + " " + ConnectedContacts[theContactId].LastName+'<span class="contact-email">Email: '+ConnectedContacts[theContactId].Email+'</span>';
            var theDate = moment(noteObj.CreationDate.date).format('MMM DD YYYY');
            var ObjectType = noteObj.ObjectType || '';
            var ActionDescription = noteObj.ActionDescription || '';
            var CreationNotes = noteObj.CreationNotes || '';
            var jField = e.jsonNote;

            if(typeof noteObj[jField] !== 'undefined' && noteObj[jField] !== "" && noteObj[jField] !== null){

                try
                {
                    noteObj[jField] = $('<div />').html(noteObj[jField]).text();
                    var jsonNote = $.parseJSON( noteObj[jField]);
                    try {
                        CreationNotes = jsonNote.note.Notes.b64decode();
                    } catch(e) {
                        CreationNotes = jsonNote.note.Notes;
                    }
                }
                catch(err)
                {
                    IS_JSON = false;
                }
            }
            var testStr = ActionDescription.toLowerCase();
            if(testStr.indexOf('changed info for') >= 0) {
                return true;
            }
            CreationNotes = autop(CreationNotes);
            //CreationNotes = CreationNotes.replace(/['"]+/g, '');
            //CreationNotes = CreationNotes.replace(/(?:\r\n|\r|\n)/g, '<br />');
            //CreationNotes = $('<div />').html(CreationNotes).text();
            toDataTableContactsNotes.push([theDate,theContact,ObjectType,ActionDescription,CreationNotes]);
        });

        //console.log(toDataTableContactsNotes);
        var ContactsNotesDataTable = ContactsNotesTableTarget.DataTable( {
            data:toDataTableContactsNotes,
            responsive: true,
            "paging":   true,
            "search": true,
            "info":     false,
            "destroy": true,
            "createdRow": function ( row, data, index ) {
                //$(row).attr('data-contactid',data[3]);
            }
        } );
        ContactsNotesDataTable
            .on( 'draw.dt', function () {
                ContactsNotesDataTable.columns.adjust().responsive.recalc();
                } );
        ContactsNotesDataTable
            .on('page', function () {
                ContactsNotesDataTable.columns.adjust().responsive.recalc();
                })
            .on('sort', function () {
                ContactsNotesDataTable.columns.adjust().responsive.recalc();
                });
        ContactsNotesDataTable.columns.adjust().responsive.recalc();
    };
    ajaxRequester('ConnectedContactsNoteContainer'+GUID, 'ConnectedContactsNote', jsonData, successFn);
}
function autop(Str) {
    if(/<[a-z][\s\S]*>/i.test(Str)) return Str; // no need to autop;
    Str = Str.split("\n");
    Str = $.map( Str, function( n ) {
        return ( '<p class="call-note">'+ n + '</p>' );
    });
    Str = Str.join( "\n");
    return Str;
}
function refreshNote(ContactId, CallType, NotePanel){
    var CurrentCallNotes = $("."+NotePanel);
    if(CallType == 'call_notes'){
        var controller = "core/tabs/call"
    }else{
        var controller = "core/tabs/note"
    }
    var jsonData = {"controler":controller,"action":"refreshNote","session_name":session_name,"data":{"conId":ContactId,"session_name":session_name,"assetsVersion":assetsVersion}};
    $(CurrentCallNotes).addClass("loading-overlay loading-center");
    $(".panel-body", CurrentCallNotes).css("opacity", 0);
    //console.log(jsonData);
    $.ajax({
        url: ajax_url,
        type: "POST",
        data: jsonData,
        success: function(e){
            $(".updateStat", CurrentCallNotes).html('<i class="fa fa-refresh"></i>');
            //console.log(e)
            $(CurrentCallNotes).removeClass("loading-overlay");
            $(CurrentCallNotes).removeClass("loading-center");
            $(".panel-body", CurrentCallNotes).css("opacity", 1);
            var theData = e.data || e;
            $( ".prev-notes", CurrentCallNotes).replaceWith(theData);
            var script = e.script || '';

            eval(script);
        }
    });

}
function newActionPlans(){

        $(".theTriggersPanel").addClass("loading-overlay loading-center");
        $(".goals").css("opacity", 0);
        var jsonData = {"controler":"core/tabs/triggers","action":"applyGoals","session_name":session_name,"data":{"InfusionsoftID":ContactId, "CallIdArr":checkedValues,"session_name":session_name,"assetsVersion":assetsVersion}};
        console.log(jsonData);
        $.ajax({
            url: ajax_url,
            type: "POST",
            data: jsonData,
            success: function(e){
                $(".theTriggersPanel").removeClass("loading-overlay");
                $(".theTriggersPanel").removeClass("loading-center");
                $(".goals").css("opacity", 1);
            }
        });
}
function applyGoals(){
    var checkedValues = $('input[name="callname"]:checked').map(function() {
        return this.value;
    }).get();
    if(checkedValues.length == 0){
        alert("Please Select Appropriate CallName!");
        return false;
    }
    var toApply = confirm('Apply Actions?');
    if(toApply){
        console.log(checkedValues);
        $('.applytags').attr('disabled',true);
        $(".theTriggersPanel").addClass("loading-overlay loading-center");
        $(".goals").css("opacity", 0);
        var jsonData = {"controler":"core/tabs/triggers","action":"applyGoals","session_name":session_name,"data":{"InfusionsoftID":ContactId, "CallIdArr":checkedValues,"session_name":session_name,"assetsVersion":assetsVersion}};
        console.log('Paased: ' + jsonData);
        console.log(jsonData);
        $.ajax({
            url: ajax_url,
            type: "POST",
            data: jsonData,
            success: function(e){
                $(".theTriggersPanel").removeClass("loading-overlay");
                $(".theTriggersPanel").removeClass("loading-center");
                $(".goals").css("opacity", 1);
                console.log('Action Results: ' + e)
                $('.applytags').removeAttr('disabled');
                $('.theTriggersPanel').prepend('<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a><strong>Success!</strong> Action Applied to this contact</div>');
                $('input[name="callname"]').attr('checked',false);
            }
        });
    }
}
function addTask(ContactId, Selector, NotePanel, PreviousNotePanel){
    var Notes = {};
    Notes['Notes'] = '';
    console.log(NotePanel);
    var CurrentCallNotes = $("."+NotePanel);
    var NotesStr = tinyMCE.get(Selector).getContent();
    var NotesStrPlain = tinyMCE.get(Selector).getBody().innerText;
    var TaskActionDescription = $("#TaskActionDescription").val();
    var ActionType = $("#TaskActionType").val();
    var TaskActionPriority = $("#TaskActionPriority").val();
    var TaskActionDate = $("#TaskActionDate").val();
    var MacantaUser = $("#MacantaUser").val();
    if(TaskActionDescription.trim() === ''){
        $("#TaskActionDescription").addClass('error').focus();
        return false;
    }
    if(TaskActionDate.trim() === ''){
        $("#TaskActionDate").addClass('error').focus();
        return false;
    }
    controller = "core/tabs/note";
    $(CurrentCallNotes).addClass("loading-overlay loading-center");
    $(".panel-body", CurrentCallNotes).css("opacity", 0);

    jsonData = {
        "controler":controller,
        "action":"addTask",
        "session_name":session_name,
        "data":{
            "TaskNote":NotesStrPlain,
            "TaskActionDescription":TaskActionDescription,
            "ActionType":ActionType,
            "TaskActionPriority":TaskActionPriority,
            "TaskActionDate":TaskActionDate,
            "MacantaUser":MacantaUser,
            "conId":ContactId,"session_name":session_name,"assetsVersion":assetsVersion
        }
    };
    console.log(jsonData);
    var successFn = function(e){
        console.log(e);
        $("input#TaskActionDescription").val('');
        $("input#TaskActionDate").val('');
        tinyMCE.get(Selector).setContent('');
        $(CurrentCallNotes).removeClass("loading-overlay");
        $(CurrentCallNotes).removeClass("loading-center");
        $('.toggleThisTaskNote').trigger('click');
        $(".panel-body", CurrentCallNotes).css("opacity", 1);
        var PrevPanel =  $( "."+PreviousNotePanel );
        var theNoteData = e.data || e;
        $( ".prev-notes", PrevPanel).replaceWith(theNoteData);
        var script = e.script || '';
        eval(script);
    };
    ajaxRequester('PreviousQuickNotes', 'prev-notes', jsonData, successFn);

}
function addNote(ContactId, CallType, Selector, NotePanel, PreviousNotePanel,theContainer,theClass){
    var Notes = {};
    Notes['Notes'] = '';
    var note_tags = '';
    var controller = '';
    var NotesStr = '';
    var NotesStrPlain = '';
    console.log(NotePanel);
    if(CallType == 'call_notes'){
        controller = "core/contact/basic_info";
        var CallSid = "";
        if(typeof recentCon['parameters'] !== 'undefined'){
            CallSid = recentCon['parameters'].CallSid;
            _addCallNote(controller, CallType, ContactId, CallSid);
        }/*else{ // this is disabled because the note save after the call has been made

            _addCallNote(controller, Notes, CallType, ContactId)
        }*/


    }else{
        var CurrentCallNotes = $("."+NotePanel);
        NotesStr = tinyMCE.get(Selector).getContent();
        NotesStrPlain = tinyMCE.get(Selector).getBody().innerText;
        Notes['Notes'] = NotesStr.b64encode();
        note_tags = $(".quick_notes_tags", CurrentCallNotes).val();
        NoteTitle = $("#NoteTitle", CurrentCallNotes).val();
        NoteType = $("#NoteType", CurrentCallNotes).val();
        if(NoteTitle.trim() === ''){
            $("#NoteTitle").addClass('error').focus();
            return false;
        }
        controller = "core/tabs/note";
        $(CurrentCallNotes).addClass("loading-overlay loading-center");
        $(".panel-body", CurrentCallNotes).css("opacity", 0);
        $('.SaveQuickNote', CurrentCallNotes).attr('disabled',true);
        $("button.SaveQuickNote", CurrentCallNotes).attr('disabled', true);
        _addQuickNote(theContainer, theClass, Selector, controller, Notes, note_tags, CallType, ContactId, CurrentCallNotes,PreviousNotePanel, NotesStrPlain, NoteTitle, NoteType)
    }

}
function _addCallNote(controller, CallType, ContactId, CallSid){
    /*var call_id = TwilioParams['call_id'] || '';
    var acct_id = TwilioParams['acct_id'] || '';
    var rec_id =  TwilioParams['rec_id']  || '';*/
    jsonData = {"controler":controller,"action":"addNote","session_name":session_name,"data":{"DeviceUsed":DeviceUsed, "CallerIdUsed":CallerIdUsed, "NoteType":"Call","NoteTitle":"Outbound call","call_id":CallSid,"tags":"#outbound_call","notesType":CallType,"conId":ContactId,"session_name":session_name,"assetsVersion":assetsVersion}};
    console.log(jsonData);
    var successFn = function(e){
        console.log(e);
        if(typeof e.data !== 'undefined'){
            var theNoteData = e.data.note || e;
            $( ".PreviousQuickNotes .prev-notes").replaceWith(theNoteData);
        }
        var script = e.script || '';
        eval(script);
    };
    ajaxRequester('PreviousQuickNotes', 'prev-notes', jsonData, successFn);
    /*$.ajax({
        url: ajax_url,
        type: "POST",
        data: jsonData,
        success: function(e){
            console.log(e)
            var theNoteData = e.data.note || e;
            $( ".PreviousQuickNotes .prev-notes").replaceWith(theNoteData);
            var script = e.script || '';
            eval(script);
        }
    });*/
}
function createQuickNoteAfterFormSubmission(NotesStrPlain, NoteTitle, Tags, NoteType,assignNoteToContactOwner) {
    var Notes = {};
    Notes['Notes'] = NotesStrPlain.b64encode();
    jsonData = {
        "controler":"core/tabs/note",
        "action":"addNote",
        "session_name":session_name,
        "data":{
            "NoteType":NoteType,
            "NoteTitle":NoteTitle,
            "plainNotes":NotesStrPlain,
            "notes":Notes,
            "tags":Tags,
            "notesType":'quick_notes',
            "OwnerID":ContactInfo.OwnerID,
            "conId":ContactId,
            "assignNoteToContactOwner":assignNoteToContactOwner,"session_name":session_name,"assetsVersion":assetsVersion
        }
    };
    var successFn = function(e){
        console.log(e);
        var PrevPanel =  $( ".PreviousQuickNotes" );
        var theNoteData = e.data || e;
        $( ".prev-notes", PrevPanel).replaceWith(theNoteData);
        var script = e.script || '';
        eval(script);
    };
    ajaxRequester('PreviousQuickNotes', 'prev-notes', jsonData, successFn);
}
function _addQuickNote(theContainer,theClass,Selector, controller, Notes, note_tags, CallType, ContactId, CurrentCallNotes,PreviousNotePanel, NotesStrPlain, NoteTitle, NoteType){
    jsonData = {"controler":controller,"action":"addNote","session_name":session_name,"data":{"NoteType":NoteType,"NoteTitle":NoteTitle,"plainNotes":NotesStrPlain,"notes":Notes,"tags":note_tags,"notesType":CallType,"conId":ContactId,"session_name":session_name,"assetsVersion":assetsVersion}};
    console.log(jsonData);
    var successFn = function(e){
        console.log(e)
        tinyMCE.get(Selector).setContent('');
        $("#current_call_tags").importTags('');
        $("#NoteTitle").val('');
        $("#quick_notes_tags").importTags('');
        $(CurrentCallNotes).removeClass("loading-overlay");
        $(CurrentCallNotes).removeClass("loading-center");
        $(".panel-body", CurrentCallNotes).css("opacity", 1);
        $("button.SaveQuickNote", CurrentCallNotes).removeAttr('disabled');
        var PrevPanel =  $( "."+PreviousNotePanel );
        var theNoteData = e.data || e;
        $( ".prev-notes", PrevPanel).replaceWith(theNoteData);
        var script = e.script || '';
        eval(script);
    };
    ajaxRequester(theContainer, theClass, jsonData, successFn);
    /*$.ajax({
        url: ajax_url,
        type: "POST",
        data: jsonData,
        success: function(e){
            console.log(e)
            tinyMCE.get(Selector).setContent('');
            $("#current_call_tags").importTags('');
            $("#quick_notes_tags").importTags('');
            $(CurrentCallNotes).removeClass("loading-overlay");
            $(CurrentCallNotes).removeClass("loading-center");
            $(".panel-body", CurrentCallNotes).css("opacity", 1);
            $("button.SaveQuickNote", CurrentCallNotes).removeAttr('disabled');
            $("button.SaveQuickNote", CurrentCallNotes).html('Create Note');
            var PrevPanel =  $( "."+PreviousNotePanel );
            var theNoteData = e.data || e;
            $( ".prev-notes", PrevPanel).replaceWith(theNoteData);
            var script = e.script || '';
            eval(script);
        }
    });*/
}
function savePrevNoteRichText(theParent){

    var thId = $(theParent).find(".noteTextRich").attr('Id');
    var theNoteId = theParent.attr("data-noteid");
    var CallType = theParent.attr("data-notetype");
    var Notes = {};

    var NotesStr = tinyMCE.get(thId, theParent).getContent();

    var NotesStrPlain = tinyMCE.get(thId, theParent).getBody().innerText;
    var NoteTitle = $("input.NoteTitleEditor", theParent).val();
    var NoteType= $("select#NoteTypeEditor", theParent).val();
    Notes['Notes'] = NotesStr.b64encode();
    var controller = "core/tabs/note";
    var jsonData = {"controler":controller,"action":"updateNote","session_name":session_name,"data":{"ActionType":NoteType, "NoteTitle":NoteTitle, "macantaNotes":Notes,"notes":NotesStrPlain,"notesType":CallType, "noteid":theNoteId,"conId":ContactId,"session_name":session_name,"assetsVersion":assetsVersion}};
    console.log(jsonData);
    $(theParent).addClass("loading-overlay loading-center");
    $('td.noteDate',theParent).css('opacity',0.1);
    var theNewNote = tinyMCE.get(thId, theParent).getContent();
    $(theParent).find(".noteTextRich").html(theNewNote);
    $(theParent).find(".mce-tinymce").hide();
    $.ajax({
        url: ajax_url,
        type: "POST",
        data: jsonData,
        success: function(e){
            $(theParent).removeClass("loading-overlay");
            $(theParent).removeClass("loading-center");
            $('td.noteDate',theParent).css('opacity',1);
            $(".show-editor", theParent).show();
            $("#noteTitle", theParent).html(NoteTitle);
            $(theParent).find(".noteTextRich").show();
            console.log(e);

        }
    });


}

function startDialer(){
    /*============PHONE FORMAT==========*/
    var telInput = $("#DialedPhone"),
        errorMsg = $("#error-msg"),
        validMsg = $("#valid-msg"),
        callBtn = $("#DialedBtn");
    telInput.intlTelInput({
         allowDropdown: true,
         //autoHideDialCode: true,
        // autoPlaceholder: false,
        // dropdownContainer: "body",
        // excludeCountries: ["us"],
        // geoIpLookup: function(callback) {
        //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
        //     var countryCode = (resp && resp.country) ? resp.country : "";
        //     callback(countryCode);
        //   });
        // },
        // initialCountry: "auto",
        initialCountry: "gb"
        // nationalMode: false,
        // numberType: "MOBILE",
        // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
        // preferredCountries: ['cn', 'jp'],
        // separateDialCode: true,
        //utilsScript: "assets/format-phone/build/js/utils.js"
    });
    var reset = function() {
        telInput.removeClass("error");
        errorMsg.addClass("hide");
        validMsg.addClass("hide");
        callBtn.attr("disabled", true);
    };
    var PhoneInput = function() {
        reset();
        if ($.trim(telInput.val())) {
            if (telInput.intlTelInput("isValidNumber")) {
                validMsg.removeClass("hide");
                callBtn.removeAttr("disabled");

            } else {
                telInput.addClass("error");
                errorMsg.removeClass("hide");
                callBtn.attr("disabled", true);
            }
        }

    };
    // on blur: validate
    telInput.blur(PhoneInput);

    telInput.keyup(function() {
        var validateThis = setTimeout(PhoneInput, 1000);
    });
    telInput
        .unbind("keyup change")
        .on("keyup change", reset);


    /*============PHONE FORMAT END==========*/
}

function initTinymceById(IdName){
    //tinymce.execCommand('mceRemoveControl', true, '#'+IdName); // reload
    tinymce.init({
        selector: '#'+IdName,
        content_css: '/assets/css/content.min.css',
        /*setup : function(ed) {
            ed.on('init', function(e) {
                e.target.hide();
            });
        },*/
        forced_root_block : false,
        menu: {
            view: {title: 'Options', items: 'code'}
        },
        plugins: [
            'advlist autolink link image lists charmap print preview hr anchor pagebreak ',
            'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
            'save table contextmenu directionality emoticons template paste textcolor '
        ]
    });
}

function initTinymceCurrentCall(){

    /* SHUTDOWN EXITING TINYMCE*/
    tinymce.execCommand('mceRemoveEditor', true, '#callNotes'); // SHUTDOWN

    tinymce.init({
        selector: '#callNotes',
        content_css: 'assets/css/content.min.css',
        init_instance_callback : function(editor) {
            //console.log("Editor: " + editor.id + " is now initialized.");
        },
        setup : function(ed) {
            ed.on('keyup', function(e) {
                $content = ed.getContent();
                if($content == ''){
                    $('.SaveNote').attr('disabled',true);
                }else{
                    $('.SaveNote').removeAttr('disabled');
                }

            });
        },
        /*menu: {
            view: {title: 'Options', items: 'code'}
        },*/
        plugins: [
            'advlist autolink link image lists charmap print preview hr anchor pagebreak ',
            'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
            'save table contextmenu directionality emoticons template paste textcolor '
        ]
    });
}
function initTinymceOppNotes(theId){

    /* SHUTDOWN EXITING TINYMCE*/
    var Container = $(".CurrentOpportunityNotesContainer"+theId);
    tinymce.execCommand('mceRemoveEditor', true, '.opp-notes'); // SHUTDOWN
    tinymce.init({
        selector: '.opp-notes',
        menubar: '',
        height : "80",
        content_css: 'assets/css/content.min.css',
        init_instance_callback : function(editor) {
            //console.log("Editor: " + editor.id + " is now initialized.");
        },
        setup : function(ed) {
            ed.on('keyup', function(e) {
                $content = ed.getContent();
                if($content == ''){
                    $('.SaveOppNote',Container).attr('disabled',true);
                }else{
                    $('.SaveOppNote',Container).removeAttr('disabled');
                }

            });
        },
        /*menu: {
            view: {title: 'Options', items: 'code'}
        },*/
        plugins: [
            'advlist autolink link image lists charmap print preview hr anchor pagebreak ',
            'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
            'save table contextmenu directionality emoticons template paste textcolor '
        ]
    });
}
function initTinymcePersonalNotes(){

    /* SHUTDOWN EXITING TINYMCE*/
    tinymce.execCommand('mceRemoveEditor', true, '.personal-notes'); // SHUTDOWN
    tinymce.init({
        selector: '.personal-notes',
        menubar: 'edit insert view format table tools',
        content_css: 'assets/css/content.min.css',
        init_instance_callback : function(editor) {
            //console.log("Editor: " + editor.id + " is now initialized.");
        },
        setup : function(ed) {
            ed.on('keyup', function(e) {
                $content = ed.getContent();
                if($content == ''){
                    //$('.SaveQuickNote').attr('disabled',true);
                }else{
                   // $('.SaveQuickNote').removeAttr('disabled');
                }

            });
        },
        /*menu: {
            view: {title: 'Options', items: 'code'}
        },*/
        plugins: [
            'advlist autolink link image lists charmap print preview hr anchor pagebreak ',
            'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
            'save table contextmenu directionality emoticons template paste textcolor '
        ]
    });
}
function relationshipIt() {


}
function split( val ) {
    return val.split( /,\s*/ );
}
function extractLast( term ) {
    return split( term ).pop();
}
function tagIt(){
    $('.tags_filter').tagsInput({
        width:'auto',
        defaultText: MacantaLanguages['text_add_tag_filter'],
        onChange: function(elem, elem_tags)
        {
            var tagWrapId = $(this).attr('Id')+'_tagsinput';
            var resetField = setTimeout(function(){ $("#"+tagWrapId+" input").val(''); }, 100)

        }
    });
    $('.searchTag').tagsInput({
        width:'auto',
        defaultText: ' or by note #tags',
        onChange: function(elem, elem_tags)
        {
            var tagWrapId = $(this).attr('Id')+'_tagsinput';
            var resetField = setTimeout(function(){ $("#"+tagWrapId+" input").val(''); }, 100)

        }
    });
    $('.current_call_tags').tagsInput({
        width:'auto',
        defaultText: MacantaLanguages['text_add_note_tag'],
        onChange: function(elem, elem_tags)
        {
            var tagWrapId = $(this).attr('Id')+'_tagsinput';
            var resetField = setTimeout(function(){ $("#"+tagWrapId+" input").val(''); }, 100)

        }
    });
    $('.quick_notes_tags').tagsInput({
        width:'auto',
        defaultText: MacantaLanguages['text_add_note_tag'],
        onChange: function(elem, elem_tags)
        {
            var tagWrapId = $(this).attr('Id')+'_tagsinput';
            var resetField = setTimeout(function(){ $("#"+tagWrapId+" input").val(''); }, 100)

        }
    });

    $('.note-tags').tagsInput({
        width:'auto',
        onAddTag: function(obj, tag)
        {
            var noteId = $(this).attr('data-noteid');
            console.log(noteId+' : '+$(this).val());
            updateTagNotes(noteId,$(this).val());

        },
        onRemoveTag: function(obj, tag)
        {
            var noteId = $(this).attr('data-noteid');
            console.log(noteId+' : '+$(this).val());
            updateTagNotes(noteId,$(this).val());
        }

    });
}
function autoCompleteIt(){

    $( ".tagsinput input" ).autocomplete({
        source: availableTags,
        select: function(event, ui) {
            var label = ui.item.label;
            var value = ui.item.value;
            $(this).val(value);
            var e = jQuery.Event("keypress");
            e.which = 13; // # Some key code value
            $(this).trigger(e);
            /*var originalEvent = event;
             while (originalEvent) {
             if (originalEvent.keyCode == 13){
                 originalEvent.stopPropagation();
             }
             if (originalEvent == event.originalEvent)
             break;
             originalEvent = event.originalEvent;
             }*/
        }
    });

    $( "input#searchTag_tag" ).autocomplete({
        source: availableTags,
        select: function(event, ui) {
            var label = ui.item.label;
            var value = ui.item.value;
            $(this).val(value);
            var e = jQuery.Event("keypress");
            e.which = 13; // # Some key code value
            $(this).trigger(e);
            /*var originalEvent = event;
             while (originalEvent) {
             if (originalEvent.keyCode == 13){
             originalEvent.stopPropagation();
             }
             if (originalEvent == event.originalEvent)
             break;
             originalEvent = event.originalEvent;
             }*/
        }
    });
    $( "input#tags_filter_tag" ).autocomplete({
        source: availableTags,
        select: function(event, ui) {
            var label = ui.item.label;
            var value = ui.item.value;
            $(this).val(value);
            var e = jQuery.Event("keypress");
            e.which = 13; // # Some key code value
            $(this).trigger(e);
            /*var originalEvent = event;
            while (originalEvent) {
                if (originalEvent.keyCode == 13){
                    originalEvent.stopPropagation();
                }
                if (originalEvent == event.originalEvent)
                    break;
                originalEvent = event.originalEvent;
            }*/
        }
    });

}
function updateTagNotes(noteId,tagStr){
    var jsonData = {"controler":"core/tabs/note","action":"updateTagNotes","session_name":session_name,"data":{"note_id":noteId,"tags":tagStr,"conId":ContactId,"session_name":session_name,"assetsVersion":assetsVersion}};

    $.ajax({
        url: ajax_url,
        type: "POST",
        data: jsonData,
        success: function(e){
            console.log(e);
            eval(e.script);
            tagIt();
            autoCompleteIt();
        }
    });
}

function addIScontact(theAddForm){
    var form = $(theAddForm);
    var contactDetails = form.serializeArray();
    console.log(contactDetails);
    console.log('Adding Contact...');
    var jsonData = {"controler":"core/contact","action":"AddContact","session_name":session_name,"data":{"conDetails":contactDetails,"session_name":session_name,"assetsVersion":assetsVersion}};
    var theModal = $("#AddContact");
    $("button", theModal).attr("disabled", true);
    $(".modal-content", theModal).addClass("loading-overlay loading-center");
    $(".modal-header, .modal-body", theModal).css("opacity", 0);
    console.log(jsonData);
    $.ajax({
        url: ajax_url,
        type: "POST",
        data: jsonData,
        success: function(e){
            $("button", theModal).removeAttr("disabled");
            $(".modal-content", theModal).removeClass("loading-overlay");
            $(".modal-header, .modal-body", theModal).css("opacity", 1);
            theModal.modal('hide');
            console.log(e);
            eval(e.script);
            theModal.modal('hide');
        }
    });
    return false
}

function updatePhoneNumbersUI() {
    console.log("PhoneValidation Results:");
    console.log(PhoneValidation);
    $.each(PhoneValidation, function (theClass,theValues) {
        var newClass = "IsPhoneValid"+theValues['IsValid'];
        $("div.macanta-notice-info.PhoneStat."+theClass+"Stat").html(theValues['html']);

        $("span.Icon"+theClass+"Stat i.IsPhoneValid")
            .removeClass('IsPhoneValidNo')
            .removeClass('IsPhoneValidUnable')
            .removeClass('IsPhoneValidYes')
            .addClass(newClass);

    })

}
function updateEmailUI() {
    console.log("EmailValidation Results:");
    console.log(EmailValidation);
    $.each(EmailValidation, function (theClass,theValues) {
        var newClass = "IsEmailValid"+theValues['format_valid'];
        $("div.macanta-notice-info."+theClass+"Stat").html(theValues['html']);

        $("span.Icon"+theClass+"Stat i.IsEmailValid")
            .removeClass('IsEmailValidNo')
            .removeClass('IsEmailValidUnable')
            .removeClass('IsEmailValidYes')
            .addClass(newClass);

    })

}
function validateEmailAddress(Email,theClass,theType) {
    var jsonData = {"controler":"core/contact","action":"validateEmailAddress","session_name":session_name,"data":{"Email":Email,"ContactId":ContactId,"session_name":session_name,"assetsVersion":assetsVersion}};
    var theHTML = '';
    var successFn = function(e){
        if(e['data']['format_valid'] === false){
            EmailStat = "No";

        }else{
            EmailStat = e['data']['format_valid'] === true ? "Yes":e['data']['format_valid'];
        }
        EmailStat = e['data']['smtp_check'] === true ? EmailStat:"No";
        if(typeof e['data']['did_you_mean'] !== "undefined" &&  e['data']['did_you_mean'] !=="" ){
            var macantaConfirm = $("."+theClass+"Confirm");
            macantaConfirm.attr('data-value', e['data']['did_you_mean']);
            $("span.message", macantaConfirm).html(e['data']['did_you_mean']);
            macantaConfirm.fadeIn('fast');
        }
        if(EmailStat === 'Yes'){
            $(".Is"+theClass+"Valid"+theType)
                .show()
                .removeClass('IsEmailValidNo')
                .removeClass('IsEmailValidUnable')
                .addClass('IsEmailValidYes');
            $("."+theClass+"Stat"+theType).html(e['html'])
        }
        if(EmailStat === 'No'){
            $(".Is"+theClass+"Valid"+theType)
                .show()
                .removeClass('IsEmailValidYes')
                .removeClass('IsEmailValidUnable')
                .addClass('IsEmailValidNo');
            $("."+theClass+"Stat"+theType).html(e['html'])
        }
        if(EmailStat === 'Unable'){
            $(".Is"+theClass+"Valid"+theType)
                .show()
                .removeClass('IsEmailValidYes')
                .removeClass('IsEmailValidNo')
                .addClass('IsEmailValidUnable');
            $("."+theClass+"Stat"+theType).html(e['html'])
        }
        EmailValidation[theClass] = {"format_valid":EmailStat, "html":e['html']};
    };
    ajaxRequester(theClass+"Container"+theType, theClass+"Container"+theType+" span", jsonData, successFn);

}
function validatePhoneNumber(PhoneNumber,theClass,type) {
    var Country = '';
    if(type !== 'Add'){
        Country = ContactInfo['Country'];
    }else{
        Country = $(".AddressBillingAdd").find('select.Country').val();
    }
    var jsonData = {"controler":"core/contact","action":"validatePhoneNumber","session_name":session_name,"data":{"PhoneNumber":PhoneNumber,"ContactId":ContactId,"Country":Country,"session_name":session_name,"assetsVersion":assetsVersion}};
    var theHTML = '';
    var successFn = function(e){
        if(e['data']['IsValid'] === 'Yes'){
            $(".Is"+theClass+"Valid"+type)
                .show()
                .removeClass('IsPhoneValidNo')
                .removeClass('IsPhoneValidUnable')
                .addClass('IsPhoneValidYes');
            $("."+theClass+"Stat"+type).html(e['html'])
        }
        if(e['data']['IsValid'] === 'No'){
            $(".Is"+theClass+"Valid"+type)
                .show()
                .removeClass('IsPhoneValidYes')
                .removeClass('IsPhoneValidUnable')
                .addClass('IsPhoneValidNo');
            $("."+theClass+"Stat"+type).html(e['html'])
        }
        if(e['data']['IsValid'] === 'Unable'){
            $(".Is"+theClass+"Valid"+type)
                .show()
                .removeClass('IsPhoneValidYes')
                .removeClass('IsPhoneValidNo')
                .addClass('IsPhoneValidUnable');
            $("."+theClass+"Stat"+type).html(e['html'])
        }
        PhoneValidation[theClass] = {"IsValid":e['data']['IsValid'], "html":e['html']};
    };
    ajaxRequester(theClass+"Container", theClass+"Container span", jsonData, successFn);

}
function validateAddress(AddressObj, AddressArr, Container, AddressSection, Country) {
    console.log(AddressObj);
    console.log(AddressArr);
    var AddressStr = AddressArr.join(' ');
    var AddressStrContainer = $("#ContactAddressList h4.currentAddress");
    AddressStrContainer.removeClass('hideThis');
    $("small",AddressStrContainer).html(AddressStr);
    var jsonData = {"controler":"core/contact","action":"validateAddress","session_name":session_name,"data":{"Address":AddressObj,"Country":Country,"Container":Container,"session_name":session_name,"assetsVersion":assetsVersion}};
    $("#ContactAddressList button.VerifySelectedAddressItem").removeClass('AddressShippingEdit').removeClass('AddressBillingEdit');
    var successFn = function(e){

        if(e.data.length > 0){
            console.log('Showing Modals');
            var theLi = '';
            $.each(e.data, function (index,data) {
                if(data['class'] === 'group'){
                    theLi += '<li data-addresstype="'+AddressSection+'" data-searchid="'+data['id']+'" class="address-item '+data['class']+'"><span class="grouplabel"><span><i class="fa fa-list-alt"></i></span>  '+data['text']+' '+data['description']+' - <span class="highlight1">'+data['count']+'</span></span><ul class="AddressSublist" style="display: none;"></ul></li>'
                }else{
                    theLi += '<li data-addresstype="'+AddressSection+'" data-searchid="'+data['id']+'" class="address-item '+data['class']+'"><input class="SelectedAddressOptions" type="radio" name="SelectedAddressItem" value="'+data['id']+'"> '+data['text']+' '+data['description']+'</li>'
                }
            });

            $("#ContactAddressList ul.AddressMainList").html(theLi);
            $("#ContactAddressList button.VerifySelectedAddressItem").addClass(AddressSection);
            $(".ContactAddressList").modal('show');
            $(".ContactDetails").modal('hide');
            $(document)._once('hide.bs.modal', ".ContactAddressList", function(e){
                $("small",AddressStrContainer).html('');
                AddressStrContainer.addClass('hideThis');
                $(".editContact").trigger('click');
                console.log('refreshAddressInfo');
                refreshAddressInfo(AddressSection);
            })
        }else{
            alert("Oops! No Address Match, Please edit the address manually");
        }

    };
    ajaxRequester(AddressSection+" .contactView", AddressSection+" .contactView .info-item", jsonData, successFn);
}
function validateAddressTypeIn(Address, searchId, AddressSection) {
    console.log(Address);
    var Country = '';
    var jsonData = {"controler":"core/contact","action":"validateAddress","session_name":session_name,"data":{"Address":Address,"Country":Country,"Container":searchId,"session_name":session_name,"assetsVersion":assetsVersion}};
    var container = $("."+AddressSection+" ul.AddressMainList");
    var target = $("."+AddressSection+" ul.AddressMainList li");
    var successFn = function(e){
        if(e.data.length > 0){
            console.log('Showing Modals');
            var theLi = '';
            $.each(e.data, function (index,data) {
                if(data['class'] === 'group'){
                    theLi += '<li data-addresstype="'+AddressSection+'"  data-searchid="'+data['id']+'" class="address-item '+data['class']+'"><span class="grouplabel"><span><i class="fa fa-list-alt"></i></span>  '+data['text']+' '+data['description']+' - <span class="highlight1">'+data['count']+'</span></span><ul class="AddressSublist" style="display: none;"></ul></li>'
                }else{
                    theLi += '<li data-addresstype="'+AddressSection+'" data-searchid="'+data['id']+'" class="address-item '+data['class']+'"><input class="SelectedAddressOptions" type="radio" name="SelectedAddressItem" value="'+data['id']+'"> '+data['text']+' '+data['description']+'</li>'
                }
            });

            $("."+AddressSection+"LoqateContainer ul.AddressMainList").html(theLi);
        }

    };

    container.addClass("loading-overlay loading-center");
    target.css("opacity", 0.1);
    if(Address[0].trim() === ''){
        if(LoqateRequest !== '' && LoqateRequest.readyState < 4) {
            LoqateRequest.abort();
        }
        container.removeClass("loading-overlay");
        container.removeClass("loading-center");
        target.css("opacity", 1)
        $("."+AddressSection+"LoqateContainer ul.AddressMainList").html('');
        return;
    }
    LoqateRequest = $.ajax({
        url: ajax_url,
        type: 'POST',
        data: jsonData,
        beforeSend : function() {
            if(LoqateRequest !== '' && LoqateRequest.readyState < 4) {
                LoqateRequest.abort();
            }
        },
        success: function(e) {
            successFn(e);
            container.removeClass("loading-overlay");
            container.removeClass("loading-center");
            target.css("opacity", 1)
        }
    }); //end ajaxReq

}
function getAddresss(Address, Container,thisItem) {
    var addressType = thisItem.attr('data-addresstype');
    thisItem.addClass("loading-overlay loading-center");
    $("span",thisItem).css("opacity", 0.1);

    console.log(Address);
    var jsonData = {"controler":"core/contact","action":"validateAddress","session_name":session_name,"data":{"Address":Address,"Container":Container,"session_name":session_name,"assetsVersion":assetsVersion}};
    var successFn = function(e){
        thisItem.removeClass("loading-overlay loading-center");
        $("span",thisItem).css("opacity", 1);
        var theLi = '';
        $.each(e.data, function (index,data) {
            theLi += '<li  data-addresstype="'+addressType+'" data-searchid="'+data['id']+'" class="address-item '+data['class']+'"><input class="SelectedAddressOptions" type="radio" name="SelectedAddressItem" value="'+data['id']+'"> '+data['text']+' '+data['description']+'</li>'
        });
        $("ul.AddressSublist",thisItem).html(theLi).slideDown().addClass("ulOpened");

    };
    ajaxRequester(null, null, jsonData, successFn);
}
function applyAddresss(Address, Container,EditContainer,ContainerB, Target) {
    console.log(Address);
    var jsonData = {"controler":"core/contact","action":"retrieveFullAddress","session_name":session_name,"data":{"SearchId":Container,"session_name":session_name,"assetsVersion":assetsVersion}};
    var successFn = function(e){
        if(ContainerB === "AddressBillingEdit"){

            $("input.StreetAddress1", EditContainer).val(e.data['StreetAddress1']).attr('data-originalvalue',e.data['StreetAddress1']);
            $("input.StreetAddress2", EditContainer).val(e.data['StreetAddress2']).attr('data-originalvalue',e.data['StreetAddress2']);
            $("input.City", EditContainer).val(e.data['City']).attr('data-originalvalue',e.data['City']);
            $("input.State", EditContainer).val(e.data['State']).attr('data-originalvalue',e.data['State']);
            $("input.PostalCode", EditContainer).val(e.data['PostalCode']).attr('data-originalvalue',e.data['PostalCode']);
            $("select.Country", EditContainer).val(e.data['Country']).attr('data-originalvalue',e.data['Country']);
        }
        if(ContainerB === "AddressShippingEdit"){
            $("input.Address2Street1", EditContainer).val(e.data['StreetAddress1']).attr('data-originalvalue',e.data['StreetAddress1']);
            $("input.Address2Street2", EditContainer).val(e.data['StreetAddress2']).attr('data-originalvalue',e.data['StreetAddress2']);
            $("input.City2", EditContainer).val(e.data['City']).attr('data-originalvalue',e.data['City']);
            $("input.State2", EditContainer).val(e.data['State']).attr('data-originalvalue',e.data['State']);
            $("input.PostalCode2", EditContainer).val(e.data['PostalCode']).attr('data-originalvalue',e.data['PostalCode']);
            $("select.Country2", EditContainer).val(e.data['Country']).attr('data-originalvalue',e.data['Country']);
        }
        if(ContainerB === "AddressBillingAdd"){

            $("input.StreetAddress1", EditContainer).val(e.data['StreetAddress1']).attr('data-originalvalue',e.data['StreetAddress1']);
            $("input.StreetAddress2", EditContainer).val(e.data['StreetAddress2']).attr('data-originalvalue',e.data['StreetAddress2']);
            $("input.City", EditContainer).val(e.data['City']).attr('data-originalvalue',e.data['City']);
            $("input.State", EditContainer).val(e.data['State']).attr('data-originalvalue',e.data['State']);
            $("input.PostalCode", EditContainer).val(e.data['PostalCode']).attr('data-originalvalue',e.data['PostalCode']);
            $("select.Country", EditContainer).val(e.data['Country']).attr('data-originalvalue',e.data['Country']);
        }
        if(ContainerB === "AddressShippingAdd"){
            $("input.Address2Street1", EditContainer).val(e.data['StreetAddress1']).attr('data-originalvalue',e.data['StreetAddress1']);
            $("input.Address2Street2", EditContainer).val(e.data['StreetAddress2']).attr('data-originalvalue',e.data['StreetAddress2']);
            $("input.City2", EditContainer).val(e.data['City']).attr('data-originalvalue',e.data['City']);
            $("input.State2", EditContainer).val(e.data['State']).attr('data-originalvalue',e.data['State']);
            $("input.PostalCode2", EditContainer).val(e.data['PostalCode']).attr('data-originalvalue',e.data['PostalCode']);
            $("select.Country2", EditContainer).val(e.data['Country']).attr('data-originalvalue',e.data['Country']);
        }
        $("#btn"+ContainerB+"NoAddress").addClass('hideThis');
        $("#btn"+ContainerB+"Verify").removeClass('hideThis');
        $("input[name="+ContainerB+"]").val('verified');
        $("."+ContainerB+" i.AddressStatus").removeClass('InValidatedAddress').addClass('ValidatedAddress').attr('title','Address Verified');
        $("."+ContainerB+" span.EditMode").each(function () {
            var theSpan = $(this);
            var input = theSpan.find("input");
            var select = theSpan.find("select");
            if (input.length > 0) {
                if (input.val() === '') {
                    theSpan.parents("li.info-item").addClass('hideThis');
                } else {
                    theSpan.parents("li.info-item").removeClass('hideThis');
                    theSpan.parents("li.info-item").find('span.BlockMode').html(input.val());
                }
            }
            if (select.length > 0) {
                if (select.val() === '') {
                    theSpan.parents("li.info-item").addClass('hideThis');
                } else {
                    theSpan.parents("li.info-item").removeClass('hideThis');
                    theSpan.parents("li.info-item").find('span.BlockMode').html(select.val());
                }
            }
            if(!Target){
                $(".ContactAddressList").modal('hide');
            } else{
                $("."+ContainerB+" input.LoqateField").val('').trigger('change');
            }
            $("."+ContainerB+"LoqateContainer").slideUp('fast');
            $("."+ContainerB+" ul.user-info.contactView").show();
            $("."+ContainerB+" .btn-toolbar.address-edit-btn").show();
            $("."+ContainerB+" span.BlockMode").slideDown(100, function () {
                $("."+ContainerB+" span.EditMode").slideUp(100);
            });
            //$("a.AddressBillingEditManual.CancelManualEdit").trigger('click')
        });
    };
    if(Target){
        ajaxRequester(ContainerB+" .AddressMainList", ContainerB+" .AddressMainList li", jsonData, successFn);
    }else{
        ajaxRequester("AddressContainer", "AddressMainList", jsonData, successFn);
    }

}
function refreshAddressInfo(Section) {
    console.log("a."+Section+"Manual : Clicked!");
    $(this).html("Manual Edit");
    $(this).removeClass('CancelManualEdit');
    $("."+Section+" span.EditMode").each(function () {
        var theSpan = $(this);
        var input = theSpan.find("input");
        var select = theSpan.find("select");
        if (input.length > 0) {
            if (input.val() === '') {
                theSpan.parents("li.info-item").addClass('hideThis');
            } else {
                theSpan.parents("li.info-item").removeClass('hideThis');
                theSpan.parents("li.info-item").find('span.BlockMode').html(input.val());
            }
        }
        if (select.length > 0) {
            if (select.val() === '') {
                theSpan.parents("li.info-item").addClass('hideThis');
            } else {
                theSpan.parents("li.info-item").removeClass('hideThis');
                theSpan.parents("li.info-item").find('span.BlockMode').html(select.val());
            }
        }
        theSpan.fadeOut(300, function () {
            $("."+Section+" span.BlockMode").fadeIn(500);
        });
    });

}
function updateIScontact(contactDetails, Id, optin){
    var jsonData = {"controler":"core/contact","action":"UpdateContact","session_name":session_name,"data":{"conDetails":contactDetails,"conId":Id, "optin":optin,"session_name":session_name,"assetsVersion":assetsVersion}};
    var theModal = $("#ContactDetails");
    $("button", theModal).attr("disabled", true);
    $(".modal-content", theModal).addClass("loading-overlay loading-center");
    $(".modal-header, .modal-body", theModal).css("opacity", 0);
    console.log(jsonData);
    $.ajax({
        url: ajax_url,
        type: "POST",
        data: jsonData,
        success: function(e){
            console.log(e);
            $.each(contactDetails, function(key,field){
                console.log("span."+field.name+ "=>  "+field.value);
                $("span."+field.name).html(field.value);
                $("input."+field.name).val(field.value);
                $("[name=inf_field_"+field.name+"]").val(field.value);
            });

            $("button", theModal).removeAttr("disabled");
            $(".modal-content", theModal).removeClass("loading-overlay");
            $(".modal-header, .modal-body", theModal).css("opacity", 1);
            $('#ContactDetails').modal('hide');
            console.log(e);
            eval(e.script);

            $('#ContactDetails').modal('hide');

        }
    });
}
function showAddresses(ContactInfo,type) {
    var acceptedValues = {
        "AddressBilling":["StreetAddress1","StreetAddress2","City","State","PostalCode","Country"],
        "AddressShipping":["Address2Street1","Address2Street2","City2","State2","PostalCode2","Country2"]
    };
    var html = '';
    var theUL = $("div."+type+" ul.contactView");
    theUL.html("");
    var noAddress = true;
    $.each(acceptedValues[type],function (theIndex,theField) {
        if(typeof ContactInfo[theField] !== 'undefined' && ContactInfo[theField] !== ''){
            noAddress = false;
            html = '<li class="info-item "><span class="'+theField+' basic-info">'+ContactInfo[theField]+'</span></li>';
            theUL.append(html);
        }else {
            html = '<li class="info-item hideThis"><span class="'+theField+' basic-info"></span></li>';
            theUL.append(html);
        }
    });
    if($("input[name="+type+"Edit]").val() === 'verified'){
        $("."+type+" i.AddressStatus").removeClass('InValidatedAddress').addClass('ValidatedAddress').attr('title','Address Verified');
        $("."+type+" li.stat-label").html('Address Verified');

    }else{
        $("."+type+" i.AddressStatus").removeClass('ValidatedAddress').addClass('InValidatedAddress').attr('title','Address Not Verified');
        $("."+type+" li.stat-label").html('Address Not Verified');
    }
    if(noAddress){
        $("#btn"+type+"EditNoAddress").removeClass('hideThis');
        $("#btn"+type+"EditVerify").addClass('hideThis');
    }else{
        $("#btn"+type+"EditNoAddress").addClass('hideThis');
        $("#btn"+type+"EditVerify").removeClass('hideThis');
    }
}
function showAddressesEdit(ContactInfo,type) {
    var acceptedValues = {
        "AddressBillingEdit":["StreetAddress1","StreetAddress2","City","State","PostalCode","Country"],
        "AddressShippingEdit":["Address2Street1","Address2Street2","City2","State2","PostalCode2","Country2"],
        "AddressBillingAdd":["StreetAddress1","StreetAddress2","City","State","PostalCode","Country"],
        "AddressShippingAdd":["Address2Street1","Address2Street2","City2","State2","PostalCode2","Country2"]
    };
    var html = '';
    var countryOptions = $("select.CountryTeplate").html();
    var theUL = $("div."+type+" ul.contactView");
    theUL.html("");
    var noAddress = true;
    $.each(acceptedValues[type],function (theIndex,theField) {
        if(typeof ContactInfo[theField] !== 'undefined' && ContactInfo[theField] !== ''){
            noAddress = false;
            html= '<li class="info-item "> <span class="basic-info BlockMode '+theField+'">'+ContactInfo[theField]+'</span> <span class="label EditMode " style="display: none;"> '+theField+': <input data-originalvalue="'+ContactInfo[theField]+'" name="'+theField+'" class="'+theField+'" value="'+ContactInfo[theField]+'"> </span> </li>';
            if(theField === "Country" || theField === "Country2"){
                html= '<li class="info-item "> <span class="basic-info BlockMode '+theField+'">'+ContactInfo[theField]+'</span> <span class="label EditMode " style="display: none;"> '+theField+': <select  data-originalvalue="'+ContactInfo[theField]+'" name="'+theField+'" class="'+theField+'">'+countryOptions+'</select></span> </li>';
            }
            theUL.append(html);
            if(theField === "Country" || theField === "Country2")
                $("."+type+" select option[value='"+ContactInfo[theField]+"']").attr('selected', true);
        }else {
            html= '<li class="info-item hideThis"> <span class="basic-info BlockMode '+theField+'"> </span> <span class="label EditMode " style="display: none;"> '+theField+': <input data-originalvalue=""  name="'+theField+'" class="'+theField+'" value=""> </span> </li>';
            if(theField === "Country" || theField === "Country2"){
                html= '<li class="info-item hideThis"> <span class="basic-info BlockMode '+theField+'"></span> <span class="label EditMode " style="display: none;"> '+theField+': <select  data-originalvalue="" name="'+theField+'" class="'+theField+'">'+countryOptions+'</select></span> </li>';
            }
            theUL.append(html);
        }
    });
    if(noAddress){
        $("#btn"+type+"NoAddress").removeClass('hideThis');
        $("#btn"+type+"Verify").addClass('hideThis');
    }else{
        $("#btn"+type+"NoAddress").addClass('hideThis');
        $("#btn"+type+"Verify").removeClass('hideThis');
    }


}
function switchAddressType(type){
    var AddressShipping = $(".AddressShipping"+type);
    var AddressBilling = $(".AddressBilling"+type);
    var AddressSliderContainer = $(".AddressSliderContainer"+type);
    if(AddressSliderContainer.hasClass('shipping')){
        AddressBilling.animate({
            opacity: 1
        }, 800, function() {
            // Animation complete.
        });
        AddressShipping.animate({
            opacity: 0
        }, 800, function() {
            // Animation complete.
        });
        AddressSliderContainer.removeClass('shipping');
        AddressSliderContainer.animate({
            left: 0
        }, 100, function() {
            // Animation complete.
        });
    }else{
        AddressBilling.animate({
            opacity: 0
        }, 800, function() {
            // Animation complete.
        });
        AddressShipping.animate({
            opacity: 1
        }, 800, function() {
            // Animation complete.
        });
        AddressSliderContainer.addClass('shipping');
        AddressSliderContainer.animate({
            left: -280
        }, 100, function() {
            // Animation complete.
        });
    }
}
function switchAddress(){
    var AddressSliderContainer = $(".AddressSliderContainer");
    var AddressShipping = $(".AddressShipping");
    var AddressBilling = $(".AddressBilling");
    if(AddressSliderContainer.hasClass('shipping')){
        AddressSliderContainer.removeClass('shipping');
        AddressBilling.animate({
            opacity: 1
        }, 800, function() {
            // Animation complete.
        });
        AddressShipping.animate({
            opacity: 0
        }, 800, function() {
            // Animation complete.
        });
        AddressSliderContainer.animate({
            left: 0
        }, 100, function() {
            // Animation complete.
        });
    }else{
        AddressBilling.animate({
            opacity: 0
        }, 800, function() {
            // Animation complete.
        });
        AddressShipping.animate({
            opacity: 1
        }, 800, function() {
            // Animation complete.
        });
        AddressSliderContainer.addClass('shipping');
        AddressSliderContainer.animate({
            left: -325
        }, 100, function() {
            // Animation complete.
        });
    }
}

function ToAdmin(){
    return UserInfo.Id.toString() === ContactId;
}
function getFrontPage(){
    console.log('Loading Frontpage...');
    var startTime = moment();
    var jsonData = {"controler":"core/common","action":"frontpage","session_name":session_name,"data":{"session_name":session_name,"Platform":DevicePlatform,"assetsVersion":assetsVersion}};
    var pageBody = $(".front-page-body");
    $("#fountainG").fadeIn();
    $.ajax({
        url: ajax_url,
        type: "POST",
        data: jsonData,
        success: function(e){
            var endTime = moment();
            var diffTime = endTime.diff(startTime, 'seconds');
            console.log('Loading Frontpage Done After '+diffTime+' seconds');
            $("#fountainG").fadeOut(300,function(){
                pageBody.fadeOut(100, function(){
                    $(this).html(e.data);
                    $(this).fadeIn(300,function(){
                        removeAddedClasses();
                        eval(e.script);

                    });
                    //
                    //alert(e.script);


                });
            });

        }
    });

}
function processHash(theHash){
    console.log('Loading Dashboard...');
    var startTime = moment();
    var SearchedContactId = theHash[1];
    var Controller = theHash[0];
    //var jsonData = {"controler":"core/"+Controller,"action":"details","data":{"Id":ContactId}};
    var jsonData = {"controler":"core/common","action":"dashboard","session_name":session_name,"data":{"session_name":session_name,"assetsVersion":assetsVersion}};
    var pageBody = $(".front-page-body");
    //console.log(jsonData);
    pageBody.addClass("login-body-effect");
    $("#fountainG").fadeIn();
    $(".btn.search").attr("disabled", true);
    $.ajax({
        url: ajax_url,
        type: "POST",
        data: jsonData,
        success: function(e){
            var endTime = moment();
            var diffTime = endTime.diff(startTime, 'seconds');
            console.log('Loading Dashboard Done After '+diffTime+' seconds');
            $("#fountainG").fadeOut(100,function(){
                pageBody.fadeOut(100, function(){
                    pageBody.removeClass("login-body-effect");
                    $(".SearchPage").slideUp();
                    pageBody.append(e.data);

                    pageBody.fadeIn(100,function(){
                        if(e.status){
                            $('.front-page-logo').addClass('class_alignleft');
                            $('.front-page-logo .description').addClass('class_alignright');
                            $('div.logoutautologin').insertAfter('.header-announcement');
                            eval(e.script);
                            processUserAccess(SearchedContactId);
                        }else{
                            eval(e.script);
                        }

                    });
                });
            });

        }
    });

}
function checkProcesses(objs){
    var theStatus = true;
    $.each(objs, function(processName, status){
       if(status === false){
           theStatus = status;
       }
    });
    return theStatus;
}
function processUserAccess(SearchedContactId){
    var jsonData;
    var pageBody = [];
    var DataSection = [];
    var Processes = {};
    $.each( UserAccess, function(Controller, Methods ) {
            $.each(Methods, function(MethodName, Items){
                var currentProcess = '| '+Controller+' -> '+MethodName+' |';
                var startTime = moment();
                console.log('Calling >>  '+currentProcess+' -> '+Items);
                jsonData = {"controler":Controller,"action":MethodName,"session_name":session_name,"data":{"Items":Items,"Id":SearchedContactId,"session_name":session_name,"assetsVersion":assetsVersion}};

                var theTraget = Controller.replace(/\//g,'-')+'-'+MethodName;
                Processes[theTraget] = false;
                DataSection[theTraget] = $("."+theTraget);
                //console.log(jsonData);
                var ControllerName = Controller.split("/");
                //<span class='loadinginfo'>Loading  "+ControllerName[ControllerName.length - 1].capitalize()+"...</span>
                var progressId = theTraget+'progress';
                DataSection[theTraget].html("<span class='loadinginfo'>"+MacantaLanguages['please_wait_loading']+" "+ControllerName[ControllerName.length - 1].capitalize()+"...</span>");
                DataSection[theTraget].addClass("loading-overlay");

                $.ajax({
                    url: ajax_url+"?"+Math.random().toString(36).substring(7),
                    type: "POST",
                    contentType: "application/x-www-form-urlencoded;charset=UTF-8",
                    data: jsonData,
                    success: function(e){
                        var endTime = moment();
                        var diffTime = endTime.diff(startTime, 'seconds');
                        console.log('Calling Done >>  '+currentProcess+' after '+diffTime+' seconds');
                        Processes[theTraget] = true;
                        if(checkProcesses(Processes) === true){
                            console.log('All Process Completed!')
                        }
                        //console.log(e)
                        DataSection[theTraget].removeClass("loading-overlay");
                        var theData = e.data || e;
                        var script = e.script || '';
                        DataSection[theTraget].html(theData);
                        eval(script);
                        $( 'p:empty' ).remove();
                    },
                    error : function(e){
                        console.log("Error From Calling: "+ JSON.stringify(jsonData));
                        console.log(e);
                        DataSection[theTraget].fadeOut(100, function(){
                            $(this).removeClass("loading-overlay");
                            $(this).html("<span class='error'>"+e.statusText+" , Please check if '"+ControllerName[ControllerName.length - 1].capitalize()+"' is existing.</span><br>");
                            $(this).fadeIn(100,function(){
                                var script = e.script || '';
                                Processes[theTraget] = true;
                                eval(script);

                            });
                        });


            }
                });
            });
    });

}
function removeAddedClasses(){
    $('.front-page-logo').removeClass('class_alignleft');
    $('.front-page-logo .description').removeClass('class_alignright');
    $(".recent-resultsB").html('');
}
function login(obj,class_container){
    var form = $(obj);
    var pageBody = $(".front-page-body");
    var container;
    if(typeof class_container !== "undefined"){
        container = $("."+class_container);
    }else{
        container = form;
    }
    var email = $("#email", form).val();
    var password = $("#password", form).val();
    var keep_me_logged_in = $('#keepmeloggedin').is(':checked') ? 'yes':'no';
    var jsonData = {"controler":"core/common","action":"login","session_name":session_name,"data":{"keep_me_logged_in":keep_me_logged_in,"Email":email,"Password":password,"session_name":session_name,"assetsVersion":assetsVersion}};
    console.log(jsonData)
    //var jsonData = {"controler":"config","action":"get","item":"sitename"};
    container.addClass("login-body-effect");
    $("#fountainG").fadeIn();
    $(".login-message").fadeOut();
    $(".social-login-message").fadeOut();
    $(".submit").attr("disabled", true);
    $(".btn-group a").attr("disabled", true);

    $.ajax({
        url: ajax_url,
        type: "POST",
        data: jsonData,
        success: function(e){
            localStorage.setItem( 'DataTablesDisplayStart', 0 );
            localStorage.setItem( 'DataTablesDisplayPage', 0 );
            $(".submit").attr("disabled", false);
            $(".btn-group a").attr("disabled", false);
            $("#fountainG").fadeOut();
            container.removeClass("login-body-effect");
            console.log(e)
            if(e.status == 1){
                $(".login-message").addClass("successful");
                $(".login-message").html(e.message).fadeIn();
                var hash = window.location.hash.substring(1);
                if (hash) {
                    var thisHash = hash.split('/');
                    console.log(thisHash);
                    processHash(thisHash);
                }else{
                    var fadelogin = setTimeout(function(){
                        pageBody.fadeOut(300, function(){
                            $(this).html(e.data);
                            //reinitialise macanta scripts

                            $(this).fadeIn(150,function(){
                                eval(e.script);
                            });
                        });
                    },2000);
                }



            }else{
                $(".login-message").html(e.message).fadeIn();
            }
        },
        error : function(e){
            console.log(e);
            $(".submit").attr("disabled", false);
            $(".btn-group a").attr("disabled", false);
            $("#fountainG").fadeOut();
            container.removeClass("login-body-effect");
            $(".login-message").html("Oopss! Something went wrong please try again.").fadeIn();
        }
    });
}
function logout(){
    var jsonData = {"controler":"core/common","action":"logout","session_name":session_name,"data":{"session_name":session_name,"assetsVersion":assetsVersion}};
    console.log(jsonData)
    var pageBody = $(".front-page-body");
    pageBody.addClass("login-body-effect");
    $("#fountainG").fadeIn();
    $.ajax({
        url: ajax_url,
        type: "POST",
        data: jsonData,
        success: function(e){
            console.log(e);
            localStorage.setItem( 'DataTablesDisplayStart', 0 );
            localStorage.setItem( 'DataTablesDisplayPage', 0 );
            $("#fountainG").fadeOut(300,function(){
                pageBody.fadeOut(600, function(){
                    //pageBody.removeClass("login-body-effect");
                    //$(this).html(e.data);
                    //$('.logout').fadeOut('fast');
                    //$(this).fadeIn(300,function(){
                        //removeAddedClasses();
                    //});
                    eval(e.script);
                });
            });
        }
    });
}
var searchKey;
function search(source, searchStr){
    console.log("Source: "+source);
    var jsonData;
    var pageBody = $(".front-page-body");
    var searchBy = $(".search_str_options").selectpicker('val') || '';
    localStorage.setItem('last_searchby_value', searchBy);
    // if the search came from dashboard bring the search box first
    if(source == 'InAppSearch'){
        searchKey = $("."+source).val();
        removeHash ();
        var SearchPage = $(".SearchPage");
        if(SearchPage.length === 0){
            jsonData = {"controler":"core/common","action":"frontpage","session_name":session_name,"data":{"session_name":session_name,"Platform":DevicePlatform,"assetsVersion":assetsVersion}};
            var successFn = function(e){
                console.log(e)
                pageBody.fadeOut(100, function(){
                    $(this).html(e.data);
                    $(this).fadeIn(300,function(){
                        removeAddedClasses();
                    });
                    eval(e.script);
                    search("MainSearch", searchKey);
                    $(".searchKey.MainSearch").val(searchKey);
                    $(".gotoAdmin").fadeIn("fast");
                });
            }
            ajaxRequester("DashboardPage", "DashboardPage .container", jsonData, successFn);

        }else{
            $(".DashboardPage").fadeIn(300,function(){
                $(this).remove();
                removeAddedClasses();
            });
            $(".SearchPage").fadeIn("fast");
            search("MainSearch", searchKey);
            $(".searchKey.MainSearch").val(searchKey);
            $(".gotoAdmin").fadeIn("fast");
        }



    }else{

        var notetags = $('#searchTag').val();
        console.log(notetags);
        if(typeof searchStr === "undefined"){
            searchKey = $("."+source).val();
            localStorage.setItem('last_searchstr_value', searchKey);
            searchKey = searchBy+searchKey
        }else{
            searchKey = searchStr;
            localStorage.setItem('last_searchstr_value', searchKey);
        }
        searchKey = searchKey.trim();
        if(!searchKey && !notetags) return false;

        if(searchKey.indexOf('connected:')  > -1){
            $(".SearchPage .form-box").addClass('hideThis');
        }else{
            $(".SearchPage .form-box").removeClass('hideThis');
            $(".SearchPage .form-box-connected-data").addClass('hideThis');
        }


        if(notetags) notetags = notetags.trim();
        jsonData = {"controler":"core/common","action":"search","session_name":session_name,"data":{"session_name":session_name,"searchKey":searchKey,"noteTags":notetags,"source":source,"assetsVersion":assetsVersion}};
        var theTable = $("#ContactsTable tbody");
        $(".btn.search").attr("disabled", true);
        var successFn = function(e){
            $(".btn.search").removeAttr("disabled");
            /*theTable.html('');
                SearchCache = e.data;
                removeAddedClasses();
                $("#fountainG").fadeOut();
                $(".btn.search").attr("disabled", false);
                $('#ContactsTable').trigger("update");
                pageBody.css("opacity", 1);*/
            localStorage.setItem( 'DataTablesDisplayStart', 0 );
            localStorage.setItem( 'DataTablesDisplayPage', 0 );
            eval(e.script);
        };
        ajaxRequester("SearchPage .form-box", "SearchPage .search-results", jsonData, successFn);

    }


}
function showLastSearchKey() {
    var search_str_options  = localStorage.getItem('last_searchby_value') || "";
    $('.search_str_options').selectpicker('val',search_str_options);
    var search_str_value  = localStorage.getItem('last_searchstr_value') || "";
    $('.searchKey').val(search_str_value);
}
function clearLastSearchKey() {
    localStorage.removeItem('last_searchby_value');
    localStorage.removeItem('last_searchstr_value');
}
function removeHash () {
    var scrollV, scrollH, loc = window.location;
    if ("pushState" in history)
        history.pushState("", document.title, loc.pathname + loc.search);
    else {
        // Prevent scrolling by storing the page's current scroll offset
        scrollV = document.body.scrollTop;
        scrollH = document.body.scrollLeft;

        loc.hash = "";

        // Restore the scroll offset, should be flicker free
        document.body.scrollTop = scrollV;
        document.body.scrollLeft = scrollH;
    }
}
function toggleThisTaskInside() {
    var InsideToggles = $("div.toggleThisTaskInside");
    $.each(InsideToggles, function () {
        var TaskItem = $(this);
        var noteId = TaskItem.attr('data-noteid');
        var Id = TaskItem.attr('data-contactid');
        var completed = TaskItem.attr('data-completiondate');
        if(completed !== ''){
            TaskItem.toggles({
                text: {
                    on: 'Done', // text for the ON position
                    off: 'Not Done' // and off
                },
                on: true, // is the toggle ON on init
                animate: 150, // animation time (ms)
                easing: 'easeOutQuint', // animation transition easing function
                width: 80, // width used if not set in css
                height: 25 // height if not set in css
            }).addClass('disabled');
        }
        else{
            var theCounter = [];
            TaskItem.toggles({
                text: {
                    on: 'Done', // text for the ON position
                    off: 'Not Done' // and off
                },
                on: false, // is the toggle ON on init
                animate: 150, // animation time (ms)
                easing: 'easeOutQuint', // animation transition easing function
                width: 80, // width used if not set in css
                height: 25 // height if not set in css
            })
                .on('toggle', function(e, active) {
                    var toggle = $(this);
                    if (active) {
                        console.log('Toggle is now ON!');
                        var blob = $('.toggle-blob', $(this));
                        var sec = 5;
                        theCounter[noteId] = setInterval(function () {
                            blob.html('<span class="counting"><i>'+sec+'</i></span>');
                            $('span', blob).fadeOut(1000, function () {
                                blob.show();
                            });
                            sec--;
                            if(sec < 0){
                                compeleteInsideTask(Id,noteId);
                                clearInterval(theCounter[noteId]);
                                blob.html('');
                                blob.html('<span class="locked"><i class="fa fa-lock" aria-hidden="true"></i></span>');
                                toggle.toggleClass('disabled', true);
                                var out = setTimeout(function () {
                                    $('span', blob).fadeOut('slow');
                                },2000);
                            }
                        },1000)
                    } else {
                        console.log('Toggle is now OFF!');
                        clearInterval(theCounter[noteId]);
                    }
                });
        }
    })
}
function searchToDataTable(){
    var data = [];
    var dataIndex = 0;
    var toggleContainer = '';
    var tableClass = $('select.search_filter option:selected' ).attr('data-group');
    if(SearchCache !== null && typeof SearchCache === 'object'){
        $.each( SearchCache, function( key, contactObject ) {
            if(Object.keys(DataTableColumn).length !== 0){
                data[dataIndex] = [];
                if(tableClass === 'TaskSearch'){
                    toggleContainer = '<div data-contactid="{theId}" data-source="Completion Date" data-guid="'+key+'" data-noteid="'+contactObject['Id']+'" class="toggleThisTask toggle-iphone"></div>';
                }else{
                    toggleContainer = '';
                }
                $.each( DataTableColumn, function( field, status ) {
                    if(field !== 'Id' && field !== 'opt.out.search.report.field.Id'){
                        var theId = contactObject.Id || 0;
                            theId = contactObject.ContactId || theId;
                            theId = contactObject.Contactid || theId;
                            theId = contactObject.ContactID || theId;
                            theId = contactObject['Contact ID'] || theId;
                            theId = contactObject['Contact Id'] || theId;
                            theId = contactObject['opt.out.search.report.field.Id'] || theId;
                        //console.log(contactObject);
                        if(theId !== 0){
                            if(toggleContainer !== '') data[dataIndex].push(toggleContainer.replace(/{theId}/g,theId));
                            toggleContainer = '';
                            data[dataIndex].push('<a href="#contact/'+theId+'" data-field="'+field+'" class="rowContactResults">'+contactObject[field]+'</a>');
                        }else{
                            if(toggleContainer !== '') data[dataIndex].push(toggleContainer.replace(/{theId}/g,theId));
                            toggleContainer = '';
                            data[dataIndex].push('<a  class=" noContactId"  data-field="'+field+'" >'+contactObject[field]+'</a>');
                        }
                    }

                });
                dataIndex++;
            }else{
                var company = contactObject.Company || "-No Company-";
                var LastName = contactObject.LastName || "";
                var FirstName = contactObject.FirstName || "";
                var Email = contactObject.Email || "-No Email-";
                var City = contactObject.City || "-No City-";
                var PostalCode = contactObject.PostalCode || "-No Postal Code-";

                var EmailFix = '<a href="#contact/'+contactObject.Id+'" class="rowContactResults">'+Email+'</a>';
                var CompanyName = '<a href="#contact/'+contactObject.Id+'" class="rowContactResults">'+company+'</a>';
                var FullName = '<a href="#contact/'+contactObject.Id+'" class="rowContactResults">'+FirstName+' '+LastName+'</a>';
                 City = '<a href="#contact/'+contactObject.Id+'" class="rowContactResults">'+City+'</a>';
                 PostalCode = '<a href="#contact/'+contactObject.Id+'" class="rowContactResults">'+PostalCode+'</a>';
                data[dataIndex] = [FullName,CompanyName,EmailFix];
                if(search_xcolumn_cache !== ""){
                    data[dataIndex].push(contactObject[search_xcolumn_cache] || "");
                }
                data[dataIndex].push(City);
                data[dataIndex].push(PostalCode);
                //data[dataIndex].push(contactObject.Id);
                dataIndex++;
            }

        });

    }
    return data;

}
function getCustomFieldValue(CustomFieldsArr,theFormObj){
    $.each(CustomFieldsArr,function (index,customField) {
        customField = customField.replace(/"/g , '');
        if(typeof ContactInfo[customField] !== 'undefined'){
            var customValue = ContactInfo[customField];
            var isCustomFieldTemp =  customField.replace("_", "");
            var result = $.grep(ISCustomFieldsArr, function(e){ return e.Name === isCustomFieldTemp; });
            if(typeof customValue === 'object' && customValue !== null){
                customValue = customValue.date || '';
                if(customValue !== ''){
                    var theDate = customValue.split(' ');
                    customValue = theDate[0];
                }
            }
            $('select[name=inf_custom'+customField+']',theFormObj).val(customValue);
            customValue  = $("<div />").html(customValue).text();
            $("input[type=email][name=inf_custom"+customField+"]",theFormObj).val(customValue);
            $("input[type=password][name=inf_custom"+customField+"]",theFormObj).val(customValue);
            $("input[type=text][name=inf_custom"+customField+"]",theFormObj).val(customValue);
            $("textarea[name=inf_custom"+customField+"]",theFormObj).val(customValue);
            $("input[type=radio][name=inf_custom"+customField+"]",theFormObj).val([customValue]);
            $('select[name=inf_custom'+customField+']',theFormObj).val(customValue);
            //for multiple select
            if(typeof customValue === 'string'){
                var multiSelectVal = customValue.split(",");
                $('select[name=inf_custom'+customField+'][multiple="MULTIPLE"]',theFormObj).val(multiSelectVal);
            }

            if(result[0]){
                if(result[0].DataType === 14){
                    var customValueArr = customValue.split(' ');
                    $('select[name=inf_custom'+customField+']',theFormObj).val(customValueArr[1]);
                    $("input[type=text][name=inf_custom"+customField+"]",theFormObj).val(customValueArr[0]);
                }
            }
        }
    });
    return true;
}
function _getCustomFieldValue(CustomFieldsArr,theFormObj){
    //console.log('CustomFields:');
    CustomFieldsArr = unique(CustomFieldsArr);
    //console.log('CustomFieldsArr: ');
    //console.log(CustomFieldsArr);

    var container = theFormObj.parent("div.panel-body");
    container.addClass("loading-overlay loading-center");
    theFormObj.css("opacity", 0);
    var jsonData = {"controler":"core/common","action":"getCustomFieldValue","session_name":session_name,"data":{"CustomFields":CustomFieldsArr, "ContactId":ContactId,"session_name":session_name,"assetsVersion":assetsVersion}};
    //console.log('Passed: ' + jsonData);
    //console.log(jsonData);
    $.ajax({
        url: ajax_url,
        type: "POST",
        data: jsonData,
        success: function(e)
        {
            container.removeClass("loading-overlay");
            container.removeClass("loading-center");
            theFormObj.css("opacity", 1);
            //console.log('getCustomFieldValue Results: ');
            //console.log(e);
            if(typeof e.data === 'object'){
                $.each(e.data, function( customField, customValue){

                    var isCustomFieldTemp =  customField.replace("_", "");
                    var result = $.grep(ISCustomFieldsArr, function(e){ return e.Name === isCustomFieldTemp; });

                    if(typeof customValue === 'object'  && customValue !== null){
                        customValue = customValue.date || '';
                        if(customValue !== ''){
                            var theDate = customValue.split(' ');
                            customValue = theDate[0];
                        }
                    }
                    //console.log("Field: [name=inf_custom"+customField+"]");
                    customValue  = $("<div />").html(customValue).text();
                    $("input[type=email][name=inf_custom"+customField+"]",theFormObj).val(customValue);
                    $("input[type=password][name=inf_custom"+customField+"]",theFormObj).val(customValue);
                    $("input[type=text][name=inf_custom"+customField+"]",theFormObj).val(customValue);
                    $("textarea[name=inf_custom"+customField+"]",theFormObj).val(customValue);
                    $('input[type="radio"][name="inf_custom' + customField + '"][value="' + customValue + '"]').prop('checked', true);
                    $('select[name=inf_custom'+customField+']',theFormObj).val(customValue);
                    //for multiple select
                    if(typeof customValue === 'string'){
                        var multiSelectVal = customValue.split(",");
                        $('select[name=inf_custom'+customField+'][multiple="MULTIPLE"]',theFormObj).val(multiSelectVal);
                    }

                    if(result[0]){
                        if(result[0].DataType === 14){
                            var customValueArr = customValue.split(' ');
                            $('select[name=inf_custom'+customField+']',theFormObj).val(customValueArr[1]);
                            $("input[type=text][name=inf_custom"+customField+"]",theFormObj).val(customValueArr[0]);
                        }
                    }


                });
            }


        }




    });
    return true;
}
function saveConnectedInfos(){
    var ConnectedInfos = {};
    var GUID = [];
    var saveError = false;
    $("form.ConnectedInfoList").find('li').each(function(){
        var indexSubObj = 0;
        var FieldItem = {};
        var current = $(this);
        var GUIDitem = current.attr('data-guid');

        if(typeof GUIDitem !== 'undefined'){
            GUID.push(GUIDitem);
            var theContent = $('div[data-guid="'+GUIDitem+'"]');
            var connectedDataVisibility = theContent.find("input[name='connectedDataVisibility']:checked").val();
            var title = theContent.find('input.ConnectedInfoTitle').val().trim();
            var item_id_custom_field = theContent.find('select.ItemIdCustomField').val().trim();
            if(title === ""){
                alert("Oops!, Please make sure all the  Title are filled up.");
                saveError = true;
                return false
            }
            var liItems = $('ul#connectorsTable[data-guid="'+GUIDitem+'"]').find('li.field-item');
            if(liItems.length === 0){
                alert("Oops!, Please add at least one field.");
                saveError = true;
                return false
            }
            var theFormRelationship = $('div.ConnectedInfoContent[data-guid="'+GUIDitem+'"]').find("form.FormRelationship");
            var theValuesRelationship = theFormRelationship.serializeArray();
            var currentItem = {};
            var Relationships = [];
            var lastyesno = false;
            $.each(theValuesRelationship, function (key, value) {
                if(typeof value['name'] === "undefined") return true;
                if(lastyesno === true && value['name'] !== 'MultipleLimit'){
                    lastyesno = true;
                    Relationships.push(currentItem);
                    currentItem = {Id:value['name'],exclusive:value['value'],limit:''};
                    return true;
                }
                if(lastyesno === true && value['name'] === 'MultipleLimit'){
                    lastyesno = false;
                    currentItem['limit'] = value['value'];
                    Relationships.push(currentItem);
                    currentItem = {};
                    return true;
                }
                if(lastyesno === false && value['name'] !== 'MultipleLimit'){
                    lastyesno = true;
                    currentItem = {Id:value['name'],exclusive:value['value'],limit:''};
                    return true;
                }


            });
            if( Object.keys(currentItem).length > 0){

                Relationships.push(currentItem);
            }
            liItems.each(function(){
                var theForm = $(this).find("form.FormFieldDetails");

                var theValues = theForm.serializeArray();

                var fieldValue = {};

                $.each(theValues, function (key, value) {
                    fieldValue[value['name']] = value['value']
                })
                if(fieldValue['fieldLabel'].trim() === ""){
                    alert("Oops!, Please fill up all the field label");
                    saveError = true;
                    return false
                }
                if(fieldValue['fieldType'].trim() === ""){
                    alert("Oops!, Please select field type for "+fieldValue['fieldLabel']+" field");
                    saveError = true;
                    return false
                }
                FieldItem[indexSubObj] = fieldValue;
                indexSubObj++;

            });

            ConnectedInfos[GUIDitem] = {"id":GUIDitem,"title":title,"visibility":connectedDataVisibility,"item_id_custom_field":item_id_custom_field,"fields":FieldItem,"relationships":Relationships};
        }

    });
    if(saveError) return false;
    var jsonData = {"controler":"core/tabs/admin","action":"saveConnectedInfos","session_name":session_name,"data":{"ConnectedInfos":JSON.stringify(ConnectedInfos),"session_name":session_name,"assetsVersion":assetsVersion}};
    console.log(jsonData);
    var successFn = function(e){
        if(typeof e === 'object'){
            console.log('Connected Informations Saved!');
            eval(e.script);
        }
    }
   ajaxRequester('left-ConnectedInfos', 'ConnectedInfoContainer', jsonData, successFn);

}
function saveCustomTabs(){
    var CustomTabs = {};
    var GUID = [];
    var indexObj = 0;
    var saveError = false;
    $("form#CustomTabList").find('li').each(function(){
        var current = $(this);
        var GUIDitem = current.attr('data-guid');

        if(typeof GUIDitem !== 'undefined'){
            GUID.push(GUIDitem);
            var theContent = $('div[data-guid="'+GUIDitem+'"]');
            var title = theContent.find('input.customTabTitle').val().trim();
            console.log(title);
            if(title === ""){
                alert("Oops!, Please make sure all the custom tab Title is filled up.");
                saveError = true;
                return false
            }
            var customTabGlobal = 'no';
            if($('input.customTabGlobal',theContent).is(':checked')){
                customTabGlobal = 'yes';
            }
            var tabPermission = $('input.customTabPermission:checked',theContent).val();
            var tabContent = tinyMCE.get('Editor'+GUIDitem).getContent();
            var tabContentEncoded = tabContent.b64encode();
            CustomTabs['key_'+indexObj] = {"id":GUIDitem,"title":title,"content":tabContentEncoded,"permission":tabPermission,"global":customTabGlobal};
            indexObj++;
        }

    });
    var customTabOffTagId = $(".customTabOffTagId").val();
    var customTabOnTagId = $(".customTabOnTagId").val();
    if(saveError) return false;
    //console.log("GUID: ");
    //console.log(GUID);
    //console.log(CustomTabs);
    var jsonData = {"controler":"core/tabs/admin","action":"saveCustomTabs","session_name":session_name,"data":{"CustomTabs":CustomTabs,"CustomTabsOffTagId":customTabOffTagId,"CustomTabOnTagId":customTabOnTagId,"session_name":session_name,"assetsVersion":assetsVersion}};
    var successFn = function(e){
        if(typeof e === 'object'){
            console.log('Custom Tabs Saved!');
            eval(e.script);
        }
    }
    ajaxRequester('left-CustomTabs', 'CustomTabContainer', jsonData, successFn);

}
function bindCTEDIT(GUID){
    $('.customTabContent'+'[data-guid="'+GUID+'"]')
        ._once('keydown','input.customTabTitle',function(event){

            if (event.keyCode == 13){
                tinyMCE.activeEditor.focus();
            }
        })
        ._once('keyup','input.customTabTitle',function(event){
            var theParent = $(this).parents(".customTabContent");
            var theTitle = $(this).val();
            theTitle = theTitle || 'Please enter title..';
            var theList = $("span.CustomTabListTitle[data-guid='"+GUID+"']");
            theList.html(theTitle)
        });
    /*$('input.customTabPermission')._once('click', function () {
        var theGroupId = $(this).attr('data-group');
        var theCheckbox = $("input#CheckboxAllContact"+theGroupId);
        var theLabel = $("label.customTabPermissionAll"+theGroupId);
        if($(this).is(":checked")){
            theCheckbox.attr("checked", true).removeAttr("disabled");
            theLabel.removeAttr("style");
        }else{
            theCheckbox.attr("disabled", true).removeAttr("checked");
            theLabel.css("opacity",0.25);
        }
    });*/
}
function initFieldBehavior(theGUID) {
    var connectorsTable = $( "ul#connectorsTable[data-guid="+theGUID+"]" );
    var connectorsFieldDetails = $("ul.connectorsFieldDetails", connectorsTable);
    connectorsFieldDetails
        ._once('keyup','input[name="fieldLabel"]',function(event){
            var theTitle = $(this).val();
            var spanTitle = $(this).parents("li.field-item").find("span.field-label .label-title");
            theTitle = theTitle || 'Please enter Label';
            spanTitle.html(theTitle.trunc(25));
        })
        ._once("change",'select[name="fieldType"]', function () {
            var theParent = $(this).parents("div.col-lg-7");
            var theType = $(this).val();
            theType = theType === 'Number' ? "Whole Number":theType;
            theType = theType === 'Currency' ? "Decimal Number":theType;
            var spanType = $(this).parents("li.field-item").find("span.field-type");
            spanType.html(theType);
            if(theType === "Select" || theType === "Checkbox" || theType === "Radio"){
                if($("div.choices-container", theParent).length === 0){
                    var choicesContainer = $("div.choices-container.hidenTemplate");
                    choicesContainer
                        .clone()
                        .removeClass('hidenTemplate')
                        .appendTo(theParent)
                        .find("textarea#fieldChoices").focus();
                }else{
                    $("div.choices-container", theParent).find("textarea#fieldChoices").focus();
                }
            }else{
                $("div.choices-container", theParent).remove();
            }

        });
}
function initFieldItem(GUID) {
    var connectorsTable = $("ul#connectorsTable[data-guid=" + GUID + "]");
    connectorsTable
        .sortable({
            axis: "y",
            handle: "div.field-item",
            stop: function (event, ui) {

            }
        });
    connectorsTable
        ._once('click', "div.field-item", function (e) {
            if (typeof e !== "undefined") {
                if ($(e.target).is('i')) {
                    e.preventDefault();
                    return false;
                }
            }

            var theTable = $(this).parents("li.field-item").find("ul.connectorsFieldDetails");
            if (theTable.is(":visible")) {
                theTable.slideToggle();
            } else {
                if ($("ul.connectorsFieldDetails:visible", connectorsTable).length > 0) {
                    $("ul.connectorsFieldDetails:visible", connectorsTable).slideUp();
                    var delay = setTimeout(function () {
                        theTable.slideToggle(400, function (e) {
                            $("input[name=fieldLabel]", theTable).focus();
                        });
                    }, 10);
                } else {
                    var delay = setTimeout(function () {
                        theTable.slideToggle(400, function (e) {
                            $("input[name=fieldLabel]", theTable).focus();
                        });
                    }, 10);
                }
            }


        })
        ._once('click',"i.AddFieldItem",function () {
            var theButton = $(this);
            var theLi = theButton.parents("li.field-item");
            var theFieldSet = $("li.field-item-template");
            var theGUID = $("form.ConnectedInfoList li.active").attr('data-guid');
            var connectorsTable = $( "ul#connectorsTable[data-guid="+theGUID+"]" );
            var fieldId = "field_"+ new Date().getTime().toString(36);
            theFieldSet
                .clone()
                .removeClass("field-item-template")
                .insertAfter(theLi)
                .find("span.cd-field-id").append(fieldId)
                .parents("li.field-item").find("input[name=fieldId]").val(fieldId)
                .parents("li.field-item").find("div.field-item").trigger('click').addClass('field-item-new ' +fieldId)
                .parents("li.field-item").find("select#infusionsoftCustomField").selectpicker('render');

            var delay = setTimeout(function () {
                $([document.documentElement, document.body]).animate({
                    scrollTop: $("div."+fieldId).offset().top
                }, 400);
            }, 400);
            var fieldCount = $("ul#connectorsTable[data-guid="+theGUID+"] li.field-item").length;
            if(fieldCount <= 1){
                $("ul#connectorsTable[data-guid="+theGUID+"] li.field-item").first().find("i.DeleteFieldItem").hide();
            }else{
                $("i.DeleteFieldItem").show();
            }
            if(fieldCount === 0){
                $("span", theButton).html("Add A Field")
            }else{
                $("span", theButton).html("Add Another Field")
            }
            initFieldBehavior(theGUID);
        })
        ._once('click', "i.DeleteFieldItem", function () {
            var theFieldItem = $(this).parents("li.field-item");

            $(FieldDeleteDialog).appendTo("body");
            $("#dialog-confirm").dialog({
                resizable: false,
                height: "auto",
                width: 500,
                modal: true,
                buttons: {
                    "Yes!": function () {
                        $(this).dialog("close");
                        $("#dialog-confirm").remove();
                        theFieldItem.slideUp('fast', function () {
                            theFieldItem.remove();
                            var fieldCount = $("li.field-item", connectorsTable).length;
                            if (fieldCount <= 1) {
                                $("li.field-item", connectorsTable).first().find("i.DeleteFieldItem").hide();
                            }

                        });
                    },
                    Cancel: function () {
                        $(this).dialog("close");
                        $("#dialog-confirm").remove();
                    }
                }
            });


        });
    var connectorsFieldDetails = $("ul.connectorsFieldDetails", connectorsTable);
    connectorsFieldDetails
        ._once('keyup', 'input[name="fieldLabel"]', function (event) {
            var theTitle = $(this).val();
            var spanTitle = $(this).parents("li.field-item").find("span.field-label .label-title");
            theTitle = theTitle || 'Please enter Label';
            spanTitle.html(theTitle);
        })
        ._once("change", 'select[name="fieldType"]', function () {
            var theParent = $(this).parents("div.col-lg-7");
            var theType = $(this).val();
            theType = theType === 'Number' ? "Whole Number":theType;
            theType = theType === 'Currency' ? "Decimal Number":theType;
            var spanType = $(this).parents("li.field-item").find("span.field-type");
            spanType.html(theType);
            if (theType === "Select" || theType === "Checkbox" || theType === "Radio") {
                if ($("div.choices-container", theParent).length === 0) {
                    var choicesContainer = $("div.choices-container.hidenTemplate");
                    choicesContainer
                        .clone()
                        .removeClass('hidenTemplate')
                        .appendTo(theParent)
                        .find("textarea#fieldChoices").focus();
                } else {
                    $("div.choices-container", theParent).find("textarea#fieldChoices").focus();
                }

            } else {
                $("div.choices-container", theParent).remove();
            }
        });
    var fieldCount = $("li.field-item", connectorsTable).length;
    if (fieldCount <= 1) {
        $("li.field-item", connectorsTable).first().find("i.DeleteFieldItem").hide();
    } else {
        $("i.DeleteFieldItem").show();
    }
}
function initButtonAddField() {
    $("div.ConnectedInfoSettingsContainer")
        ._once('click',"button.addConnectorField",function () {
            var theButton = $(this);
        var theFieldSet = $("li.field-item-template");
        var theGUID = $("form.ConnectedInfoList li.active").attr('data-guid');
        var fieldId = "field_"+ new Date().getTime().toString(36);
        theFieldSet
            .clone()
            .removeClass("field-item-template")
            .appendTo("ul#connectorsTable[data-guid="+theGUID+"]")
            .find("span.cd-field-id").append(fieldId)
            .parents("li.field-item").find("input[name=fieldId]").val(fieldId)
            .parents("li.field-item").find("div.field-item").trigger('click').addClass('field-item-new ' +fieldId)
            .parents("li.field-item").find("select#infusionsoftCustomField").selectpicker('render');

            var delay = setTimeout(function () {
                $([document.documentElement, document.body]).animate({
                    scrollTop: $("div."+fieldId).offset().top
                }, 400);
            }, 400);


        var fieldCount = $("ul#connectorsTable[data-guid="+theGUID+"] li.field-item").length;
        if(fieldCount <= 1){
            $("ul#connectorsTable[data-guid="+theGUID+"] li.field-item").first().find("i.DeleteFieldItem").hide();
        }else{
            $("i.DeleteFieldItem").show();
        }
        if(fieldCount === 0){
            $("span", theButton).html("Add A Field")
        }else{
            $("span", theButton).html("Add Another Field")
        }
        initFieldItem(theGUID);
    })
        ._once('change',"input.multiple-options",function (){
            var theRadio = $(this);
            var theParentLi = theRadio.parents("li.field-details");
            var theVal = theRadio.val();
            if(theVal === 'no'){
                $("input.multiple_limit", theParentLi).removeAttr('disabled');
            }else{
                $("input.multiple_limit", theParentLi).attr('disabled', true);
            }

        });
}
function initConnectedInfoList() {
    $('form.ConnectedInfoList.dynamic')
        ._once('click', '.addButton', function () {
            $('.ConnectedInfoSettingsContainerPlaceholder').hide();
            $('.ConnectedInfoList .form-group-item').removeClass('active');
            var GUID = 'ci_' + new Date().getTime().toString(36);
            var template = $(this).parents(".form-group-item"),
                form = $(this).parents('form.ConnectedInfoList'),
                CTcontentTemplate = $("#ConnectedInfoContentTemplate");
            template.find('input').attr('readonly', true);
            template
                .clone()
                .removeClass('remove-button')
                .addClass('active')
                .attr('data-guid', GUID)
                .removeAttr('id')
                .insertBefore($('#dummy', form))
                .find('div.bullet-item').prepend('<span class="ConnectedInfoListTitle" data-guid="' + GUID + '">Information Group title </span>')
                .find('button.addButton').addClass('removeButton').removeClass('addButton')
                .html('<i class="fa fa-trash-o" title="Delete Custom Tab"></i>');
            $('.ConnectedInfoContent').css('display', 'none');
            CTcontentTemplate
                .clone()
                .removeAttr('style')
                .removeAttr('Id')
                .attr('data-guid', GUID)
                .addClass(GUID)
                .insertBefore($('.ConnectedInfoSettingsContainer .ContentHeader'))
                .find('ul#connectorsTable').attr('data-guid', GUID)
                .find("input[name=fieldId]").val("field_" + new Date().getTime().toString(36));

            initConnectedInfoTitle(GUID);
            initFieldItem(GUID);
            initAddRelationship();
            initConnectedInfoListSortable();
            $("button.addConnectorField").removeClass("hideThis");
            $("button.saveConnectedInfosB").removeClass("hideThis");
            $("." + GUID + " input.ConnectedInfoTitle").focus();
        })
        ._once('click', '.removeButton', function () {
            var toDel = false;
            var $row = $(this).parents('.form-group-item');
            var nth = $row.index();
            var nthPrev = $row.index() - 2;
            var theGuid = $row.attr('data-guid');
            var theContent = $('div.ConnectedInfoContent[data-guid=' + theGuid + ']');
            $(ConnectorDeleteDialog).appendTo("body");
            $("#dialog-confirm").dialog({
                resizable: false,
                height: "auto",
                width: 500,
                modal: true,
                buttons: {
                    "Yes!": function () {
                        $(this).dialog("close");
                        $("#dialog-confirm").remove();
                        if (nth === 1 && $("form.ConnectedInfoList ul li:eq( 1 )").hasClass('remove-button')) {
                            $('.ConnectedInfoSettingsContainerPlaceholder').show();
                            $("button.addConnectorField").addClass("hideThis");
                            $("button.saveConnectedInfosB").addClass("hideThis");
                        }
                        $form = $(this).parents('form');
                        if ($row.hasClass('active')) {
                            var nextActive = $("form.ConnectedInfoList ul li:eq( " + nth + " )");
                            var prevActive = $("form.ConnectedInfoList ul li:eq( " + nthPrev + " )");
                            var nextGuid = nextActive.attr('data-guid');
                            var prevGuid = prevActive.attr('data-guid');
                            if (nextActive.hasClass('remove-button')) {
                                prevActive.addClass('active');
                                $('.' + prevGuid).fadeIn('fast')
                            } else {
                                nextActive.addClass('active');
                                $('.' + nextGuid).fadeIn('fast')
                            }
                        }
                        $row.remove();
                        theContent.remove();
                    },
                    Cancel: function () {
                        $(this).dialog("close");
                        $("#dialog-confirm").remove();
                        toDel = false;
                    }
                }
            });


        })
        ._once('click', 'span.ConnectedInfoListTitle', function () {
            $('.ConnectedInfoSettingsContainerPlaceholder').hide();
            $('.ConnectedInfoList .form-group-item').removeClass('active');
            $('.ConnectedInfoContent').css('display', 'none');
            var $row = $(this).parents('.form-group-item'),
                $form = $(this).parents('form'),
                theClassContent = $row.attr('data-guid');
            $row.addClass('active');
            jQuery('#CurrentConnectorId').val($row.attr('data-guid'));
            $('.' + theClassContent).fadeIn('fast').find(".ConnectedInfoTitle").focus();

        });
}
function initConnectedInfoListSortable(){
    $("form.ConnectedInfoList ul").sortable({
        cancel: ".remove-button",
        stop: function (event, ui) {
            if ($(ui.item).prev('.remove-button').length > 0)
                $(this).sortable('cancel');
        }
    });
}
function initConnectedInfoTitle(GUID){
    //console.log("bindCTEDIT_ConnectedInfo: "+ GUID);
    $('.ConnectedInfoContent[data-guid="'+GUID+'"]')
        ._once('keyup','input.ConnectedInfoTitle',function(event){
            var theParent = $(this).parents(".ConnectedInfoContent");
            var theTitle = $(this).val();
            theTitle = theTitle || 'Please enter title..';
            var theListContainer = $(".ConnectedInfoList");
            theListContainer.find("span[data-guid='"+GUID+"']").html(theTitle);
        })
}
function initMultiSelect(ClassName) {
    $('#'+ClassName).multiSelect({
        //selectableHeader: "<div class='custom-header'>Available Tags</div><input type='text' class='search-input' autocomplete='off' placeholder='search tag...'>",
        //selectionHeader: "<div class='custom-header'>Search Tags</div><input type='text' class='search-input' autocomplete='off' placeholder='type tag...'>",
        selectableHeader: "<div class='custom-header'>Not Applied</div>",
        selectionHeader: "<div class='custom-header'>Tags Applied</div>",

        keepOrder: true,
        afterInit: function(ms){
            var that = this,
                $selectableSearch = that.$selectableUl.prev(),
                $selectionSearch = that.$selectionUl.prev(),
                selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                .on('keydown', function(e){
                    if (e.which === 40){
                        that.$selectableUl.focus();
                        return false;
                    }
                });

            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                .on('keydown', function(e){
                    if (e.which == 40){
                        that.$selectionUl.focus();
                        return false;
                    }
                });
        },
        afterSelect: function(values){
            this.qs1.cache();
            this.qs2.cache();
            updateContactTag(values[0], 'addContactTag');
            console.log('Added:'+ values[0]);

        },
        afterDeselect: function(values){
            this.qs1.cache();
            this.qs2.cache();
            updateContactTag(values[0], 'removeContactTag');
            console.log('Removed:'+ values[0]);
        }
    });
}
function setLanguage(theLanguage){
    var jsonData = {"controler":"core/tabs/admin","action":"setLanguage","session_name":session_name,"data":{"language":theLanguage,"session_name":session_name,"assetsVersion":assetsVersion}};
    var successFn = function(e){
        if(typeof e === 'object'){
           console.log('Langage hava been set');
            eval(e.script);
        }
    }
    ajaxRequester('macanta-body', 'front-page', jsonData, successFn);
}
function updateContactTag(tagId, action){
    var jsonData = {"controler":"core/tabs/admin","action":action,"session_name":session_name,"data":{"tagId":tagId,"ContactId":ContactId,"session_name":session_name,"assetsVersion":assetsVersion}};
    var successFn = function(e){

    };
    ajaxRequester(null, null, jsonData, successFn);
}
function changeMacantaTagCategory(catId){
    var jsonData = {"controler":"core/tabs/admin","action":"changeMacantaTagCategory","session_name":session_name,"data":{"catId":catId,"session_name":session_name,"assetsVersion":assetsVersion}};
    var successFn = function(e){
        if(typeof e === 'object'){
            $('.macanta-tags').html(e.html);
            eval(e.script);
        }
    }
    ajaxRequester("admin-panelBody-access", "macanta-tags", jsonData, successFn);
}
function saveSearchFilterTagPairs(){
    var Values = $('form#SaveSearchTagPair').serializeArray();
    var jsonData = {"controler":"core/tabs/admin","action":"saveSearchFilterTagPairs","session_name":session_name,"data":{"pairs":Values,"session_name":session_name,"assetsVersion":assetsVersion}};
    var successFn = function(e){
        $('.admin-panelBody-savedsearch').prepend('<div class="alert alert-success savedAlert" role="alert">Success! <small>Tag(s) now exist in Infusionsoft for all these saved searches. Simply apply the relevant tag to any user who requires access to a saved search in macanta.</small></div>');
        var slider = $('.savedAlert');
        var timer = new Timer(function() {
            slider.slideUp()
        }, 15000);
        slider
            .mouseenter(function() {
                timer.pause();
            })
            .mouseleave(function() {
                timer.resume();
            });
    }
    ajaxRequester("right-Permissions", "admin-panelBody-savedsearch", jsonData, successFn);
}
function saveNoteEditingPermissions(){
    var Values = $('input#NoteEditingPermissions').val();
    var jsonData = {"controler":"core/tabs/admin","action":"saveNoteEditingPermissions","session_name":session_name,"data":{"Values":Values,"session_name":session_name,"assetsVersion":assetsVersion}};
    var successFn = function(e){};
    ajaxRequester("NoteEditingPermissionsContainer", "NoteEditingPermissionsBody", jsonData, successFn);
}
function saveMacantaAccess(){
    var MacantaTagCat = $('.macanta-tag-category-select').selectpicker('val');
    var MacantaAdminTag = $('.macanta-admin-tag').selectpicker('val');
    var MacantaUserTag = $('.macanta-user-tag').selectpicker('val');
    var MacantaContactViewPermissionCat = $('.macanta-contact-view-permission-cat').selectpicker('val');
    var MacantaSavedSearchPermissionCat = $('.macanta-savedsearch-permission-cat').selectpicker('val');
    console.log('MacantaTagCat '+MacantaTagCat);
    console.log('MacantaUserTag '+MacantaUserTag);
    console.log('MacantaAdminTag '+MacantaAdminTag);
    console.log('MacantaContactViewPermissionCat '+MacantaContactViewPermissionCat);
    console.log('MacantaSavedSearchPermissionCat '+MacantaSavedSearchPermissionCat);
    var jsonData = {"controler":"core/tabs/admin","action":"saveMacantaAccess","session_name":session_name,"data":{"MacantaTagCat":MacantaTagCat, "MacantaAdminTag":MacantaAdminTag, "MacantaUserTag":MacantaUserTag, "MacantaContactViewPermissionCat":MacantaContactViewPermissionCat, "MacantaSavedSearchPermissionCat":MacantaSavedSearchPermissionCat,"session_name":session_name,"assetsVersion":assetsVersion}};
    var successFn = function(e){


    }
    ajaxRequester("left-Permissions", "admin-panelBody-access", jsonData, successFn);
}
function getSavedSearchTag(container, target){
    var jsonData = {"controler":"core/tabs/admin","action":"getSavedSearchTag","session_name":session_name,"assetsVersion":assetsVersion};
    var successFn = function(e){
        if(typeof e === 'object'){
            $('.'+ target).html(e.html);
            eval(e.script);
        }
    }
    ajaxRequester(container, target, jsonData, successFn);
}
function getTagCatSelection(container, target){
    var jsonData = {"controler":"core/tabs/admin","action":"getTagCatSelection","session_name":session_name,"assetsVersion":assetsVersion};
    var successFn = function(e){
        if(typeof e === 'object'){
            $('.'+ target).html(e.html);
            eval(e.script);
        }
    }
    ajaxRequester(container, target, jsonData, successFn);
}
function refreshCustomFields (){

    var jsonData = {"controler":"core/common","action":"refreshCustomFields","session_name":session_name,"data":{"session_name":session_name,"assetsVersion":assetsVersion}};
    var successFn = function(e){

    };
    ajaxRequester('refreshCustomFields', 'refreshCustomFieldsButton', jsonData, successFn);

}
function refreshWebform (){

   // var jsonData = {"controler":"core/common","action":"refreshwebform"};
    //var successFn = function(e){

   // };
   // ajaxRequester('refreshWebform', 'refreshWebformButton', jsonData, successFn);

}
function refreshISsettings(){

    var jsonData = {"controler":"core/common","action":"refreshISsettings","session_name":session_name,"data":{"session_name":session_name,"assetsVersion":assetsVersion}};
    var successFn = function(e){

    };
    ajaxRequester('refreshISsettings', 'refreshISsettingsButton', jsonData, successFn);

}
function getWebform (theClass, app, formid){

    var jsonData = {"controler":"core/common","action":"getWebform","session_name":session_name,"data":{"app":app, "formid":formid,"session_name":session_name,"assetsVersion":assetsVersion}};
    var successFn = function(e){
        if(typeof e === 'object'){
            $('.panel-body.'+ theClass).html(e.data);
            var theWebForm = $("."+theClass + " form.infusion-form");
            parseWebForm(theWebForm);
        }
    }
    ajaxRequester(theClass, null, jsonData, successFn);

}
function verifyEmail(theObj){
    //alert(theObj.val());
    var email=theObj.val();
    console.log(email);
    var jsonData = {"controler":"core/common","action":"verifyEmail","session_name":session_name,"data":{"Email":email,"session_name":session_name,"assetsVersion":assetsVersion}};
    var successFn = function(e){
        if(typeof e === 'object'){
            console.log(e.message);
            if(e.message !== 'Email Not Existing'){
                var choose = confirm("A contact with this email address already exists. Do you want to open it?");
                if(choose){
                    eval(e.script)
                    location.reload();
                }else{
                    theObj.val('').focus();
                }
            }

        }
    }
    ajaxRequester(null, null, jsonData, successFn);
}
function _getWebform (theClass, app, formid, readonly, createnote, assign_note_to_contact_owner,submitted_by_customfield){
    //app is not in use for getWebformAPI unlike getWebform
    var jsonData = {"controler":"core/common","action":"getWebformAPI","session_name":session_name,"data":{"app":app, "formid":formid,"session_name":session_name,"assetsVersion":assetsVersion}};
    var successFn = function(e){
        if(typeof e === 'object'){
            $('.panel-body.'+ theClass).html(e.data);
            var theWebForm = $("."+theClass + " form.infusion-form");
            var formId = theWebForm.attr('Id');
            var buttonId = formId.replace('inf_form_','xid_');
            $("button.infusion-recaptcha", theWebForm).attr('Id',buttonId);
            var theTitle = $('input[name="inf_form_name"]',theWebForm).val();
            //theTitle = 'Testing';
            theTitle =  theTitle.replace(/~br~/g, " ");
            $('h3.panel-title.'+ theClass).html(theTitle);
            parseWebForm(theWebForm, readonly, createnote, assign_note_to_contact_owner,submitted_by_customfield);
        }
    }
    ajaxRequester(theClass, null, jsonData, successFn);

}
function sendISEmailTemplate(theId,theClass){
    var container = $("."+theClass);
    $(".alert",container).remove();
    var jsonData = {"controler":"core/contact","action":"sendISEmailTemplate","session_name":session_name,"data":{"conDetails":ContactInfo,"theId":theId,"session_name":session_name,"assetsVersion":assetsVersion}};
    console.log(jsonData);
    var successFn = function(e){
        if(typeof e === 'object'){
            if(e.status){
                container.prepend('<div class="alert alert-success" role="alert">Email Successfully Sent!</div>');
            }else{
                container.prepend('<div class="alert alert-danger" role="alert">Oops, Sending failed., Please contact your Administrator.</div>');
            }

        }
    }
    ajaxRequester(theClass, "sendTemplateEmail"+theClass, jsonData, successFn);
}
function sendISEmail(theClass,createnote){
    var container = $(".container"+theClass);
    var Subject =  $("input.subject",container).val();
    var BCC =  $("input.bcc",container).val();
    var Message = tinyMCE.get(theClass).getContent();
    if (Subject == "" || Message == "") {
        alert("Oops!, Please Fill Up The Missing Field.");
        return false;
    }
    Subject =  Subject.b64encode();
    Message = Message.b64encode();
    $(".alert",container).remove();
    console.log("Subject: "+Subject);
    console.log("Message: "+Message);
    var jsonData = {"controler":"core/contact","action":"sendISEmail","session_name":session_name,"data":{"conDetails":ContactInfo,"Subject":Subject,"BCC":BCC,"Message":Message,"createnote":createnote,"session_name":session_name,"assetsVersion":assetsVersion}};
    console.log(jsonData);
    var successFn = function(e){
        if(typeof e === 'object'){
            if(e.status){
                $(".EmailFormPanelBody", container).prepend('<div class="alert alert-success" role="alert">Email Successfully Sent!</div>');
                eval(e.script);
            }else{
                $(".EmailFormPanelBody", container).prepend('<div class="alert alert-danger" role="alert">Oops, Sending failed., Please contact your Administrator</div>');
            }

        }
    }
    ajaxRequester("container"+theClass, "EmailFormPanelBody", jsonData, successFn);
}
function compeleteTask(noteContactId,noteId, row) {
    console.log('Applying to contact: '+ noteContactId + ' Note Id: '+noteId);
    var jsonData = {"controler":"core/tabs/note","action":"compeleteTask","session_name":session_name,"data":{"noteContactId":noteContactId,"noteId":noteId,"session_name":session_name,"assetsVersion":assetsVersion}};
    console.log(jsonData);
    var successFn = function(e){
        var CompletionDate = e.data[0]['CompletionDate'];
        $('a[data-field="Completion Date"]', row).html(CompletionDate);
        eval(e.script);
        tagIt();
        autoCompleteIt();
    };
    ajaxRequester(null, null, jsonData, successFn);
}
function compeleteInsideTask(noteContactId,noteId) {
    console.log('Applying to contact: '+ noteContactId + ' Note Id: '+noteId);
    var jsonData = {"controler":"core/tabs/note","action":"compeleteTask","session_name":session_name,"data":{"noteContactId":noteContactId,"noteId":noteId,"session_name":session_name,"assetsVersion":assetsVersion}};
    console.log(jsonData);
    var successFn = function(e){
        eval(e.script);
        tagIt();
        autoCompleteIt();
    };
    ajaxRequester(null, null, jsonData, successFn);
}
function verifyCallerId(){
    var theWindow = $("#CallerIdWindow");
    var PhoneNumber = $("input.NumberToVerify", theWindow).val();
    var ExtNumber = $("input.ExtToVerify", theWindow).val();
    var LabelToVerify = $("input.LabelToVerify", theWindow).val();
    var SetAsDevice = $("input#LabelToCallDevice", theWindow).is(":checked") ? 'yes':"no";
    if(PhoneNumber === '' || LabelToVerify === ''){
        alert("Oppss.., Please fill up the required field!..");
        return false;
    }

    $(".validate-phone-div").fadeOut('fast', function () {
        $(".phone-progress").fadeIn();
    });
    var jsonData = {"controler":"core/tabs/admin","action":"addCallerId","session_name":session_name,"data":{"ExtNumber":ExtNumber, "PhoneNumber":PhoneNumber, 'Label':LabelToVerify,"session_name":session_name,"assetsVersion":assetsVersion}};
    console.log(jsonData);
    var successFn_B = function(e){
        console.log(e);
        if (e.message === "error") {
            theMessage = "<p>An error ocurred during the validation...please try again later.</p>";
            $(".phone-progress").html(theMessage);
            var timer = setTimeout(function () {
                $("#CallerIdWindow").modal('hide');
                $(".phone-progress").html('Initializing, Please wait..').fadeOut('fast',function () {
                    $(".validate-phone-div").fadeIn('fast');
                    $("input.NumberToVerify").val('');
                    $("input.ExtToVerify").val('');
                    $("input.LabelToVerify").val('');
                });
            },3000);
        } else if (e.message.VerificationStatus !== "success") {
            theMessage = "<p>We weren't able to verify your phone...please try again later.</p>";
            $(".phone-progress").html(theMessage);
            var timer = setTimeout(function () {
                $("#CallerIdWindow").modal('hide');
                $(".phone-progress").html('Initializing, Please wait..').fadeOut('fast',function () {
                    $(".validate-phone-div").fadeIn('fast');
                    $("input.NumberToVerify").val('');
                    $("input.ExtToVerify").val('');
                    $("input.LabelToVerify").val('');
                });
            },3000);
        } else {
            $(".phone-progress").html(theMessage);
            $("form#MacantaCallerIdList ul").html(e.html);
            $("form#MacantaCallerIdList input.UserId").val('');
            $('#MacantaUsersList ul.itemContainer li').removeClass('active');
            var firstListed = $("#MacantaUsersList ul.itemContainer li");
            firstListed.first().trigger('click');
            var timer = setTimeout(function () {
                $("#CallerIdWindow").modal('hide');
                $(".phone-progress").html('Initializing, Please wait..').fadeOut('fast',function () {
                    $(".validate-phone-div").fadeIn('fast');
                    $("input.NumberToVerify").val('');
                    $("input.ExtToVerify").val('');
                    $("input.LabelToVerify").val('');
                });
            },3000);
        }

    };
    var successFn = function(e){
        if (e.message === "error") {
            theMessage = "<p style='text-align:center'>Invalid phone number!</p>";
        } else if (e.message === "already_in") {
            theMessage = "<p style='text-align:center'>Number already verified.!</p>";
        } else {
            console.log(e);
            theMessage = "<p style='text-align:center'>Please enter this verification code when you get the call:</p><br><h3 style='text-align:center;margin-top:-20px'>" + e.message.validationCode + "</h3>";
            jsonData = {"controler":"core/tabs/admin","action":"checkAddCallerId","session_name":session_name,"data":{"SetAsDevice":SetAsDevice, "ExtNumber":ExtNumber, "PhoneNumber":PhoneNumber, "file_id":e.file_id, 'Label':LabelToVerify,"session_name":session_name,"assetsVersion":assetsVersion}};
            ajaxRequester(null, null, jsonData, successFn_B);
        }
        $(".phone-progress").html(theMessage);
    };
    ajaxRequester(null, null, jsonData, successFn);
}
function deleteCallerId(sid,phone){
    var jsonData = {"controler":"core/tabs/admin","action":"deleteCallerId","session_name":session_name,"data":{"SID":sid,"phone":phone,"session_name":session_name,"assetsVersion":assetsVersion}};
    var successFn = function(e){
        console.log(sid);
        $("li[data-sid='"+sid+"']").slideUp('fast',function () {
            $("li[data-sid="+sid+"]").remove();
        })
    };
    ajaxRequester("CallSettingsContainer", "MacantaCallerIdList", jsonData, successFn);
}
function deviceConnect(PhoneField,callerId,device,FirstName,LastName) {
    var state="Initializing...";
    var setdelay;
    $("#twilio_log").text("Status: "+state);
    var jsonData = {"controler":"twilio","action":"connect","session_name":session_name,"data":{"PhoneField":PhoneField, "callerId":callerId, "device":device,  "FirstName":FirstName, "LastName":LastName,"session_name":session_name,"assetsVersion":assetsVersion}};
    var successFn = function(e){
        var sid = e.sid;
        var duration = 0;
        var finalStatus = ["canceled","completed","busy","failed","no-answer"];
        var jsonData = {"controler":"twilio","action":"getStatus","session_name":session_name,"data":{"SID":sid,"session_name":session_name,"assetsVersion":assetsVersion}};
        setdelay =  setInterval(function () {
            $.ajax({
                url: ajax_url,
                type: "POST",
                async: false,
                data: jsonData,
                success: function(e)
                {
                    if(typeof e.message === "undefined"){
                        state =  e.status;
                        duration =  e.duration;
                        $("#twilio_log").text("Status: "+state);
                        if($.inArray( state, finalStatus ) >= 0){
                            clearInterval(setdelay);
                            $('.phoneCall').removeAttr('disabled');
                            $('.phoneCall').show();
                            $('.DevicephoneCall').hide();
                            var Notes = {};
                            Notes['Notes'] = 'Outbound Call';
                            _addCallNote("core/contact/basic_info",'call_notes', ContactId, sid)
                        }
                    }else {
                        clearInterval(setdelay);
                        $("#twilio_log").text("Status: "+e.message);
                        $('.phoneCall').removeAttr('disabled');
                        $('.phoneCall').show();
                        $('.DevicephoneCall').hide();
                    }
                }
            });
        }, 2000);
    };
    ajaxRequester(null, null, jsonData, successFn);
}
function makeDefaultCallerId(Modal){
    Modal.modal('hide');
    var theInput = $("input.LabelToVerify", Modal);
    var PhoneNumber = theInput.attr("data-callerid");
    var jsonData = {"controler":"core/tabs/admin","action":"makeDefaultCallerId","session_name":session_name,"data":{"PhoneNumber":PhoneNumber,"session_name":session_name,"assetsVersion":assetsVersion}};
    console.log(jsonData);
    var successFn = function(e){
        $(".CallSettingsContainer .CallerIdItem").remove();
        $(".CallSettingsContainer .MacantaCallerIdList").remove();
        $(".CallSettingsContainer").append(e.html_content);
    };
    ajaxRequester("CallSettingsContainer", "MacantaCallerIdList", jsonData, successFn);
}
function saveCallerIdLabel(Modal){
    var theInput = $("input.LabelToVerify", Modal);
    var theCheckbox = $("input#LabelToCallDeviceEdit", Modal);
    var LabelToVerify = theInput.val();
    var sid = theInput.attr("data-sid");
    var PhoneNumber = theInput.attr("data-callerid");
    var SetAsDevice = theCheckbox.is(":checked") ? 'yes':'no';
    if(LabelToVerify === ''){
        alert("Oppss.., Please fill up the required field!..");
        return false;
    }
    var jsonData = {"controler":"core/tabs/admin","action":"saveCallerIdLabel","session_name":session_name,"data":{"SID":sid, "SetAsDevice":SetAsDevice, "PhoneNumber":PhoneNumber, 'Label':LabelToVerify,"session_name":session_name,"assetsVersion":assetsVersion}};
    console.log(jsonData);
    var successFn = function(e){
        var theId = "label"+sid;

        $("div#"+theId).html(LabelToVerify);
        if(SetAsDevice === 'yes' ){
            $("div."+sid).removeClass('fwidth');
        }else {
            $("div."+sid).addClass('fwidth');
        }

        var editButton = $("i[data-sid="+sid+"]");
        editButton.attr("data-isdevice",SetAsDevice);
        editButton.attr("data-label",LabelToVerify);
        var theInfoContainer = $("span.info-as-device-"+sid);
        if(SetAsDevice === "yes"){
            $("a.info-as-device", theInfoContainer).removeClass('hide')
        }else{
            $("a.info-as-device", theInfoContainer).addClass('hide')
        }
        Modal.modal('hide');
    };
    ajaxRequester("EditCallerIdWindowContainer", "EditCallerIdWindowBody", jsonData, successFn);
}
function ajaxRequester(containerClass, targetClass, jsonData, successFn, customAjaxUrl){
    //var jsonData = {"controler":"core/common","action":"getWebform","data":{"app":app, "formid":formid}};
    var theURL = customAjaxUrl || ajax_url;
   if(containerClass){
       var container = $("."+containerClass);
       container.addClass("loading-overlay loading-center");
   }
    if(targetClass){
        var target = $("."+targetClass);
        target.css("opacity", 0.1);
    }
    $.ajax({
        url: theURL,
        type: "POST",
        data: jsonData,
        success: function(e)
        {
            if(container){
                container.removeClass("loading-overlay");
                container.removeClass("loading-center");
            }
            if(target){target.css("opacity", 1)}
            //console.log('ajaxRequester Results: ');
            //console.log(e);
            if(typeof e.logged_out !== 'undefined' && e.logged_out === 'yes'){
                eval(e.script);
            }else{
                successFn(e);
            }

        },
        error: function (e) {
            if(container){
                container.removeClass("loading-overlay");
                container.removeClass("loading-center");
            }
            if(target){target.css("opacity", 1)}
            //console.log('ajaxRequester Results: ');
            //console.log(e);
        }

    });
}
function parseWebForm(theFormObj, readonly, createNote, assign_note_to_contact_owner,submitted_by_customfield){
    var DateFields = ["Anniversary","Birthday","Validated","DateCreated","LastUpdated"];
    theFormObj.removeAttr('onsubmit');
        var FormFieldsStr = theFormObj.serialize(); // to be use in posting to IS
    var FormFields = theFormObj.serializeArrayB(); // to be use in manipulating form values
    // Lets include radio buttons and check boxes
    $.each($('form input[type=radio]',theFormObj)
        .filter(function(idx){
            return $(this).prop('checked') === false
        }),
        function(idx, el){
            // attach matched element names to the formData with a chosen value.
            var emptyVal = "";
            formData += '&' + $(el).attr('name') + '=' + emptyVal;
        }
    );
    $.each($('select[multiple=MULTIPLE]',theFormObj),
        function(idx, el){
            //console.log('form select[multiple=MULTIPLE]');
            var SelectValue = {"name":$(this).attr('name'),"value":""}
            //console.log(SelectValue);
            FormFields.push(SelectValue);
        }
    );
    var itemsProcessed = 0;
    var CustomFieldsArr = [];

    var FieldObj = {};
    //console.log('Webform:');
    //console.log(FormFieldsStr);
    //console.log(FormFields);
    $.each(FormFields, function(theKey,theField){
        FieldObj[theField['name']] = theField['value'];

        //var theId = $("[name="+theField['name']+"]", theFormObj).attr('id');
        //var uniqueId = theId + '_' +randomString(5);
        //$("[name="+theField['name']+"]", theFormObj).attr('id',uniqueId);

        if(theField['name'].indexOf("inf_field_") > -1){
            var isField = theField['name'].replace("inf_field_", "");
            var tempVal = ContactInfo[isField] || '';

            if(typeof tempVal === 'object'){
                tempValVal = tempVal.date || '';
                if(tempValVal != ''){
                    var tempValVal = tempValVal.split(' ');
                    tempVal = tempValVal[0];
                }
            }

            //console.log('tempVal:' + ContactInfo[isField]);
            //console.log(ContactInfo);

            //decode html entities
            tempVal  = $("<div />").html(tempVal).text();

            $("[name=inf_field_"+isField+"]", theFormObj)
                .val(tempVal);//
            if(DateFields.indexOf(isField) > -1){
                $("[name=inf_field_"+isField+"]", theFormObj).daterangepicker({
                    autoUpdateInput: false,
                    showDropdowns: true,
                    locale: {
                        format: "YYYY-MM-DD"
                    },
                    singleDatePicker:true
                }).on('apply.daterangepicker', function(ev, picker) {
                    $(this).val(picker.startDate.format("YYYY-MM-DD"));
                });
            }
            //if(tempVal != ''){// show blank field
            if(isField == 'Email'){ // hide only email
                if($("input[name=inf_field_"+isField+"]", theFormObj)){
                    $("input[name=inf_field_"+isField+"]", theFormObj).attr('readonly', true);//
                    //$("input[name=inf_field_"+isField+"]", theFormObj).hide();//
                    //$("[for="+theField['name']+"]", theFormObj).hide();
                }

            }


        }else if(theField['name'].indexOf("inf_misc_") > -1){
            var isMisc = theField['name'].replace("inf_misc_", "");
            var isMiscTrimLow = isMisc.toLowerCase();
            var tempVal = ContactInfo[isMisc] || '';
            tempVal  = $("<div />").html(tempVal).text();
            var inf_misc =  $("[name=inf_misc_"+isMisc+"]", theFormObj);
            var container = inf_misc.parents('table.infusion-field-container tbody');
            //console.log('tempVal:' + ContactInfo[isMisc]);
            //console.log(ContactInfo);
            inf_misc.val(tempVal);//
            if(isMiscTrimLow.indexOf("personnote") > -1){
                container.find('td').addClass('fullWidth');
                if(typeof ContactInfo['ContactNotes'] !== "undefined"){
                    var thisContactNotes = ContactInfo['ContactNotes'].replace( /\r?\n/g, "<br>" );
                    $('<tr><td class="infusion-field-label-container fullWidth"><label for="inf_misc_AppendtoPersonNotes">Person Note</label></td> <td class="infusion-field-input-container  fullWidth"><div id="PersonNotes" name="PersonNotes" class="PersonNotes" disabled>'+thisContactNotes+'</div></td></tr>').prependTo(container);
                }
            }
            /*if(isMisc.indexOf("Date") > -1){
                $("[name=inf_field_"+isMisc+"]", theFormObj).daterangepicker({
                    autoUpdateInput: false,
                    locale: {
                        format: 'YYYY-MM-DD'
                    },
                    singleDatePicker:true
                }).on('apply.daterangepicker', function(ev, picker) {
                    $(this).val(picker.startDate.format('YYYY-MM-DD'));
                });
            }*/


        }else if (theField['name'].indexOf("inf_custom_") > -1){
            var isCustomField = '"'+theField['name'].replace("inf_custom", "")+'"';
            var isCustomFieldTemp =  theField['name'].replace("inf_custom_", "");
            CustomFieldsArr.push(isCustomField);
            var result = $.grep(ISCustomFieldsArr, function(e){ return e.Name == isCustomFieldTemp; });
            if(result[0]){
                if(result[0].DataType== 13 || result[0].DataType == 14){
                    $("input[name=inf_custom_"+isCustomFieldTemp+"]", theFormObj).daterangepicker({
                        autoUpdateInput: false,
                        showDropdowns: true,
                        locale: {
                            format: "YYYY-MM-DD"
                        },
                        singleDatePicker:true
                    }).on('apply.daterangepicker', function(ev, picker) {
                        $(this).val(picker.startDate.format("YYYY-MM-DD"));
                    });
                }
                // if  Whole Number
                if(result[0].DataType == 12){
                    $("[name=inf_custom_"+isCustomFieldTemp+"]", theFormObj).attr('data-type','WholeNumber');
                }
                // if Decimal Number
                if(result[0].DataType == 11){
                    $("[name=inf_custom_"+isCustomFieldTemp+"]", theFormObj).attr('data-type','DecimalNumber');
                }
            }


        }else if(theField['name'].indexOf("inf_option_") > -1){

        }else{
            $("[name="+theField['name']+"]", theFormObj).hide();
        }
    });
    $("button[type=submit]", theFormObj)
        .removeAttr('style')
        .addClass('btn btn-default');
    if($("button[type=submit]", theFormObj).html().trim() === ''){
        $("button[type=submit]", theFormObj).html('Submit');
    }
    getCustomFieldValue(CustomFieldsArr,theFormObj);
    theFormObj.on('submit',function(event){
        event.preventDefault();
        $(".form-error", theFormObj).remove();
        $(".alert", theFormObj).remove();
        var container = $(this).parents("div.webformPanelBody");
        var theForm = $(this);
        container.addClass("loading-overlay loading-center");
        theForm.css("opacity", 0.1);
        var queryStr = theForm.serialize();
        var queryArr = theForm.serializeArrayB();
        var queryArrA = theForm.serializeArray();
        var radioMaps = {};
        $.each(queryArrA, function (index, field) {
            var FieldName = field['name'];
            if(FieldName.indexOf('inf_option_') !== -1 ){
                var FieldValue = field['value'];
                var theRadio =  $("input[name="+FieldName+"][value="+FieldValue+"][type=radio]");
                var theTxtValue = '';
                if(theRadio.length > 0){
                    var theId = theRadio.attr('id');
                    theTxtValue =  $("label[for="+theId+"]").html();
                    radioMaps[FieldName] = theTxtValue;
                }
            }
        });
        var url = theForm.attr('action');
        var jsonData = {"controler":"core/common","action":"submitWebForm","session_name":session_name,"data":{"ContactId":ContactId,"postdata":queryStr, "url":url, "createNote":createNote, "assignNoteToContactOwner":assign_note_to_contact_owner,"submitted_by_customfield":submitted_by_customfield,"radioMaps":radioMaps,"session_name":session_name,"assetsVersion":assetsVersion}};
        //console.log('Passed: ' + jsonData);
        //console.log(jsonData);
        $.ajax({
            url: ajax_url,
            type: "POST",
            data: jsonData,
            success: function(e){
                container.removeClass("loading-overlay");
                container.removeClass("loading-center");
                theForm.css("opacity", 1);
                console.log('Submit Results: ');
                console.log(e);
                $(".form-error", theFormObj).remove();
                $(".alert", theFormObj).remove();
                $(".thankyoumsg", theFormObj).remove();
                if(e.RawOutput !== false && typeof e.RawOutput !== null){
                    if(e.WebformError === false && e.SystemError_640 === false){
                        if(e.NeedCaptcha === true){
                            theFormObj.prepend('<div class="form-error"><h3>Bot Detection Enabled</h3>Please disable Bot Detection by selecting <code>Opt out of automatic bot detection processing</code> in Infusionsoft webform setting and try again.</div>');

                        }else {
                            if(typeof e.thankyou !== 'undefined' && e.thankyou !== null ){
                                theFormObj.prepend('<div class="thankyoumsg" role="alert">'+e.thankyou+'</div>');
                            }
                            theFormObj.prepend('<div class="alert alert-success" role="alert">Form Successfully Submitted</div>');
                            eval(e.script);
                        }
                    }
                    if(e.WebformError !== false){
                        theFormObj.prepend('<div class="form-error">' + e.WebformError + '</div>');
                    }
                    if(e.SystemError_640 !== false){
                        theFormObj.prepend('<div class="form-error"><h3>Error 640</h3>Your Infusionsoft app prevent this form to submit successfuly.</div>');
                    }
                }else{
                    theFormObj.prepend('<div class="form-error"><h3><i class="fa fa-bug"></i> No Response </h3>' +
                        'This form has not submitted because of a bug or error with INFUSIONSOFT. <br>You are welcome to report it to us, although the likelihood is we know about it, and are working with Infusionsoft to resolve the issue. <br>Thanks for your patience..' +
                        '</div>');
                }



            },
            complete:function(){
                $(".thankyoumsg a").attr('target','_blank');
            }
        });
        return false;
    })
    //console.log(FieldObj);
    if (!('inf_field_Email' in FieldObj)) {
        $("button[type=submit]", theFormObj).attr('disabled', true);
        $("input", theFormObj).attr('disabled', true);
        theFormObj.append('<div class="webformNotice"><i class="glyphicon glyphicon-warning-sign"></i> Sorry, This Webform is disabled because Email field is not detected.</div>');
    }
    if(readonly !== "false"){
        $("button[type=submit]", theFormObj).remove();
        $("input, textarea, select", theFormObj).attr("disabled", true);
    }
    if (typeof WebformFnCallback !== "undefined") {
        console.log("Calling WebformFnCallback();");
        WebformFnCallback();
    }


}
function jumpToTWlink(ContactId){
    $(".link-error").remove();
    //console.log('CustomFields:');
    theButton = $('.teamwork-link');
    theButton.html("GO TO TEAMWORK PROJECT");
    theButton.removeAttr('disabled');
    var container = theButton.parents("div.TWLink");
    container.addClass("loading-overlay loading-center");
    theButton.css("opacity", 0.1);

    var jsonData = {"controler":"custom_controllers/kickstart","action":"getTWLink","session_name":session_name,"data":{"ContactId":ContactId,"session_name":session_name,"assetsVersion":assetsVersion}};
    //console.log('Passed: ' + jsonData);
    //console.log(jsonData);
    $.ajax({
        url: ajax_url,
        type: "POST",
        data: jsonData,
        success: function(e){
            container.removeClass("loading-overlay");
            container.removeClass("loading-center");
            theButton.css("opacity", 1);
            console.log('jumpToTWlink Results: ');
            console.log(e);
            if(e.data.message != 'error'){
                window.open(e.data.message,'_blank');
            }else{
                theButton.attr('disabled', true)
                theButton.html('<span class="link-error">Sorry, this contact has '+e.data.error+'</span>')
            }

        }
    });
}
function renderUserCallerIds(UserCallerIds,UserContactId) {
    $("form#MacantaCallerIdList input.UserId").val(UserContactId);
    $("form#MacantaCallerIdList input.UserCallerIdToEnable").removeAttr('disabled');
    $("form#MacantaCallerIdList li.form-group-item").not( '[data-calleridtype=system-default]' ).each(function () {
        var theInput;
        var $this = $(this);
        var CallerId = $this.attr('data-guid');
        theInput = $this.find("input.AllowUserCallerId");
        //console.log(CallerId);
        if($.inArray( CallerId, UserCallerIds ) > -1){
            //console.log("Yes");
            theInput.prop( "checked", true );
        }else{
            //console.log("No");
            theInput.removeAttr("checked");
        }
    })
}
function saveUserConnectedInfo(itemId, values, GUID, ConnectedContacts) {
    var jsonData = {"controler":"core/contact/basic_info","action":"saveUserConnectedInfo","session_name":session_name,"data":{"ConnectedContacts":ConnectedContacts,"GUID":GUID,"itemId":itemId,"values":values, "ContactId":ContactId,"session_name":session_name,"assetsVersion":assetsVersion}};
    var successFn = function(e){
        console.log(e);
        var theTable = $("table#UserConnectorInfoTable[data-guid="+GUID+"]");
        $("tbody", theTable).html(e.tbody);
        theTable.parents("div.col-md-7").find("button.addUserConnectedInfo[data-guid="+GUID+"]").removeAttr("disabled");
        var theForm = $("form.FormUserConnectedInfo[data-guid="+GUID+"]");
        //theForm.trigger('reset');
        var itemId = theForm.attr("data-itemid");
        $("tr.infoItem[data-itemid="+itemId+"]").trigger('click');
        makeCDListDataTable(".UserConnectorInfoTable"+GUID);
    };
    ajaxRequester("theConnectedInfoPanel", "theConnectedInfoPanelBody", jsonData, successFn);
}
function deleteUserConnectedInfo(itemId, GUID) {
    var jsonData = {"controler":"core/contact/basic_info","action":"deleteUserConnectedInfo","session_name":session_name,"data":{"GUID":GUID,"itemId":itemId,"ContactId":ContactId,"session_name":session_name,"assetsVersion":assetsVersion}};
    var successFn = function(e){
        console.log(e);
        $("table#UserConnectorInfoTable[data-guid="+GUID+"] tbody").html(e.tbody);
        var theForm = $("form.FormUserConnectedInfo[data-guid="+GUID+"]");
        $("button.cancelUserConnectedInfo",theForm).trigger('click');
    };
    ajaxRequester("theConnectedInfoPanel", "theConnectedInfoPanelBody", jsonData, successFn);
}
function saveDefaultCallerId(theCallerId) {
    var jsonData = {"controler":"core/contact/basic_info","action":"saveDefaultCallerId","session_name":session_name,"data":{"PhoneNumber":theCallerId,"session_name":session_name,"assetsVersion":assetsVersion}};
    var successFn = function(e){
    };
    ajaxRequester(null, null, jsonData, successFn);
}
function saveOutboundDevice(theOutboundDevice) {
    var jsonData = {"controler":"core/contact/basic_info","action":"saveDefaultOutboundDevice","session_name":session_name,"data":{"OutboundDevice":theOutboundDevice,"session_name":session_name,"assetsVersion":assetsVersion}};
    var successFn = function(e){

    };
    ajaxRequester(null, null, jsonData, successFn);
}
function addBulkConnectedInfo(GUID) {
    var Email = ContactInfo.Email;
    var FirstName = ContactInfo.FirstName;
    var LastName = ContactInfo.LastName;
    var ContactId = ContactInfo.Id;
    var toSaved = {};
    $("table#UserConnectorInfoTableFilterResult tbody tr").each(function () {
        var theTR =  $(this);
        var theIdsStr = theTR.attr("data-contactids");
        var itemid = theTR.attr("data-itemid");
        var theIdsArr = theIdsStr.split(',');
        var show = false;
        if(theTR.hasClass('hideThis')) return true;
        if(theTR.hasClass('hideThisSub')) return true;
        var theSelect = theTR.find("select").val();
        if(!theSelect) return true;
        theSelect = JSON.stringify(theSelect);
        var connected_info = '{"'+ContactId+'":{"relationships":'+theSelect+',"ContactId":"'+ContactId+'","FirstName":"'+FirstName+'","LastName":"'+LastName+'","Email":"'+Email+'"}}';
        toSaved[itemid] = JSON.parse(connected_info);
    });

    var jsonData = {"controler":"core/contact/basic_info","action":"addBulkConnectedInfo","session_name":session_name,"data":{"Value":toSaved,"ContactId":ContactId,"session_name":session_name,"assetsVersion":assetsVersion}};
    var successFn = function(e){
        $('#ConnectOtherData').modal('hide');
        $("button.addBulkConnectedInfo").attr('disabled', true);
        console.log(GUID);
        $("table#UserConnectorInfoTable[data-guid="+GUID+"] tbody").html(e.tbody).find("tr.infoItem").last().trigger('click');

    };
    ajaxRequester('connect-other-data .modal-content', 'connect-other-data .modal-bodyt', jsonData, successFn);

}
function lazy_load(container,title,action,controller) {
    console.log('Loading '+title);
    controller = controller || "core/tabs";
    var jsonData = {"controler":controller,"action":action,"session_name":session_name,"data":{"title":title,"ContactId":ContactId,"session_name":session_name,"assetsVersion":assetsVersion}};
    var HTMLContainer = 'Lazy-'+container;
    var successFn = function(e){
        $("."+HTMLContainer).html(e.data);
        if(action === "tab_custom"){
            if (typeof CustomTabFnCallback !== "undefined") {
                console.log("Calling CustomTabFnCallback();");
                CustomTabFnCallback();
            }

        }
    };
    ajaxRequester(HTMLContainer, null, jsonData, successFn);
}
function CustomEditor() {
    console.log("CustomEditor Init");
    if($("#csseditor").length > 0){
        editAreaLoader.init({
            id: "csseditor"	// id of the textarea to transform
            ,start_highlight: true
            ,font_size: "10"
            ,font_family: "verdana, monospace"
            ,allow_toggle: false
            ,language: "en"
            ,syntax: "css"
            ,toolbar: "save,|, search, |, undo, redo, |, select_font, |"
            ,save_callback: "csseditor_save"
            ,plugins: "charmap"
            ,charmap_default: "arrows"
            ,EA_load_callback:"ScrollTop"

        });
    }
    if($("#jseditor").length > 0){
        editAreaLoader.init({
            id: "jseditor"	// id of the textarea to transform
            ,start_highlight: true
            ,font_size: "10"
            ,font_family: "verdana, monospace"
            ,allow_toggle: false
            ,language: "en"
            ,syntax: "js"
            ,toolbar: "save,|, search, |, undo, redo, |, select_font, |"
            ,save_callback: "jseditor_save"
            ,plugins: "charmap"
            ,charmap_default: "arrows"
            ,EA_load_callback:"ScrollTop"

        });
    }
    CustomEditorInit = true;

}
function ScrollTop() {
    window.scrollTo(0, 0);
    var theHeight = $('#MainContent .tab-body').outerHeight();
    $('ul.nav.nav-tabs.tabs-left').css('height',theHeight+'px');
}
function addColumnFilters(theTable) {
    allFilters = localStorage.getItem("allFilters") || "";
    allFilters = allFilters !== "" ? JSON.parse(allFilters):[];
    $('thead tr',theTable).clone(true).appendTo( 'table#ContactsTable thead' );
    $('thead tr:eq(1) th',theTable).each( function (i) {
        var title = $(this).text();
        $(this).html( '<input class="filterField" data-keyname="filterField_'+title+'" type="text" placeholder="Filter '+title+'" />' );

        $( 'input', this ).on( 'keyup', function () {
            var storageKey = $(this).attr("data-keyname");
            var filterValue = $(this).val();
            if ( ContactsDataTable.column(i).search() !== this.value ) {
                ContactsDataTable
                    .column(i)
                    .search( this.value )
                    .draw();
            }
            if(filterValue === ""){
                localStorage.removeItem(storageKey);
                var key = allFilters.indexOf(storageKey);
                if(key > -1) allFilters.splice(key, 1);
            }else{
                localStorage.setItem(storageKey, filterValue);
                allFilters.push(storageKey);
            }
            allFilters = $.unique( allFilters );
            if(allFilters.length === 0){
                localStorage.removeItem("allFilters");
                localStorage.removeItem("FilterResults");
            }else{
                allFiltersStr = JSON.stringify(allFilters);
                localStorage.setItem("allFilters", JSON.stringify(allFilters));
            }

        } );

    } );


}
function applyLastSearchFilter(theTable) {
    $('thead tr:eq(1) th',theTable).each( function (i) {
        var storageKey = $( 'input', this ).attr("data-keyname");
        var storageValue = localStorage.getItem(storageKey) || "";
        if(storageValue !== ""){
            $( 'input', this ).val(storageValue);
            if ( ContactsDataTable.column(i).search() !== storageValue ) {
                ContactsDataTable
                    .column(i)
                    .search( storageValue )
                    .draw();
            }
        }else{
            localStorage.removeItem(storageKey);
        }

    } );
}
function overideNextContactbySearchResultsFilter($CurrentContactId) {
    var FilteredContacts = localStorage.getItem("FilterResults") || "";
    var nexContactId = '';
    if(FilteredContacts !== ""){
        FilteredContactsArr = JSON.parse(FilteredContacts);
        if(FilteredContactsArr.length > 1){
            $.each(FilteredContactsArr, function (index,ContactId) {
                if(ContactId == $CurrentContactId){
                    nexContactId = index+1;
                    if(nexContactId < FilteredContactsArr.length){
                        $("a.showNextResult").attr('href','#contact/'+FilteredContactsArr[nexContactId]);
                    }else{
                        $("span.showNextResultContainer").addClass('hideThis');
                    }
                    return false;
                }
            });

        }
    }

}
function clearLastSearchFilter() {
    var allFilters = localStorage.getItem("allFilters") || "";
    if(allFilters !== ""){
        allFilters = JSON.parse(allFilters);
        $.each(allFilters, function (index,name) {
            localStorage.removeItem(name);
        })
        localStorage.removeItem("allFilters");
        localStorage.removeItem("FilterResults");
    }
}
function renderEmailHistory() {
    var EmailHistoryTable = $("table#EmailHistoryTable");
    if(EmailHistoryTable.length === 0) return false;
    EmailHistoryTableData = EmailHistoryTable.DataTable({
        "autoWidth": false,
        responsive: true,
        "paging":   true,
        "search": true,
        "info":     true,
        "destroy": true,
        "aaSorting": [],
        "dom": '<"top-left" <"displayCount"l> <"displayInfo"i> ><"top-right"p>rt<"bottom"p>',
        "oLanguage": {
            "sInfo": "Showing <span class='search-multiple-page'> <span class='dt_start'>_START_</span> to </span> <span class='dt_end'>_END_</span> of _TOTAL_ ",
            "sInfoEmpty": "No Contacts to show",
            "sLengthMenu": "Display _MENU_ "
        }
    });
    $('thead tr',EmailHistoryTable).clone(true).appendTo( 'table#EmailHistoryTable thead' );
    $('thead tr:eq(1) th',EmailHistoryTable).each( function (i) {
        var title = $(this).text();
        $(this).html( '<input class="filterField" data-keyname="filterField_'+title+'" type="text" placeholder="Filter '+title+'" />' );

        $( 'input', this ).on( 'keyup', function () {
            if ( EmailHistoryTableData.column(i).search() !== this.value ) {
                EmailHistoryTableData
                    .column(i)
                    .search( this.value )
                    .draw();
            }

        } );

    } );
    $("thead tr:eq(1) th",EmailHistoryTable).unbind('click.DT');

    EmailHistoryTableData.on( 'draw.dt', function () {
        $("tbody tr", EmailHistoryTable).on('click', function () {
            var HistoryId = $(this).attr('data-historyid');
            var theTable = $("#EmailHistoryItem");
            theTable.modal('show');
            getEmailHistoryItem(HistoryId,theTable);
        });

    });
    $("tbody tr", EmailHistoryTable).on('click', function () {
        var HistoryId = $(this).attr('data-historyid');
        var theTable = $("#EmailHistoryItem");
        theTable.modal('show');
        getEmailHistoryItem(HistoryId,theTable);
    });
}
function getEmailHistoryItem(Id, Table) {
    var jsonData = {"controler":"core/contact","action":"getEmailHistoryItem","session_name":session_name,"data":{"Id":Id,"session_name":session_name,"assetsVersion":assetsVersion}};
    console.log(jsonData);
    Table.find(".panel-title span").html("");
    Table.find(".panel-body").html("");
    var successFn = function(e){
        var theBody = e.data.html_content;
        Table.find(".panel-title span").html(e.data.subject);
        Table.find(".panel-body").html(theBody.b64decode());
    }
    ajaxRequester("EmailHistoryItem .modal-body", "EmailHistoryItem .modal-body .panel-primary", jsonData, successFn);
}
function renderCDAttachments(db_record, theUL) {
    theUL.html('');
    $.each(db_record, function (itemId, attachments) {
        $.each(attachments, function (attachmentIndex, attachmentDetails) {
            console.log("download_url:");
            console.log(attachmentDetails.download_url);
            var fileInfo = '<div class="file-Info"><span class="cd-attachement-filename file-Info-item">'+attachmentDetails.filename+'</span></div>';
            theUL.append('<li class="col-xs-12 col-sm-12 col-md-6 col-lg-6 no-pad-left no-pad-right"><div class="thumb-container"><a target="_blank" href="'+attachmentDetails.download_url+'"><img src="data:image/png;base64,'+attachmentDetails.thumbnail+'"></a>'+fileInfo+'</div></li>')
        })
    });
}
function putCDURLAttachments(theFileURL,ItemId) {
    var jsonData = {"controler":"core/contact/basic_info","action":"putCDURLAttachments","session_name":session_name,"data":{"theFileURL":theFileURL,"ItemId":ItemId,"session_name":session_name,"assetsVersion":assetsVersion}};
    console.log(jsonData);
    var successFn = function(e){
        console.log(e);
        if(e.status === 'success'){
            var thePanel = $("form.FormUserConnectedInfo[data-itemid="+ItemId+"]");
            var theUL = $("ul.file-attachments-list", thePanel);
            var db_record = e.db_record||{};
            renderCDAttachments(db_record, theUL);
            eval(e.script);
        }else{
            eval(e.script);
        }



    };
    ajaxRequester("FormUserConnectedInfo[data-itemid="+ItemId+"] .container-url-attachment-container", "FormUserConnectedInfo[data-itemid="+ItemId+"] .container-url-attachment-container .input-group", jsonData, successFn);
}
function getCDFileAttachments(ItemId) {
    var jsonData = {"controler":"core/contact/basic_info","action":"getCDFileAttachements","session_name":session_name,"data":{"ItemId":ItemId,"session_name":session_name,"assetsVersion":assetsVersion}};
    var successFn = function(e){
        console.log(e);
        var thePanel = $("form.FormUserConnectedInfo[data-itemid="+ItemId+"]");
        var theUL = $("ul.file-attachments-list", thePanel);
        theUL.html('');
        var db_record = e.db_record||{};
        renderCDAttachments(db_record, theUL);

    };
    ajaxRequester("FormUserConnectedInfo[data-itemid="+ItemId+"] .container-file-attachment-container", "FormUserConnectedInfo[data-itemid="+ItemId+"] .container-file-attachments", jsonData, successFn);
}
function renderContactSearch() {
    ContactsTable = $("table#ContactsTable");
    if(ContactsTable.length === 0) return false;
    var SearchItemType = 'Contacts';
    if(ContactsTable.hasClass('TaskSearch')){
        SearchItemType = 'Tasks';
    }

    if(ContactsTable.length >= 1){
        var DataTableContent = searchToDataTable();
        var DataTableLength = $(DataTableContent).length;
        var DataTablesDisplayStart  = localStorage.getItem('DataTablesDisplayStart') || 0;
        var DataTablesDisplayPage  = parseInt(localStorage.getItem('DataTablesDisplayPage')) || 0;
        ContactsDataTable = ContactsTable.DataTable( {
            data:DataTableContent,
            "autoWidth": false,
            responsive: true,
            "paging":   true,
            "search": true,
            "info":     true,
            "destroy": true,
            "aaSorting": [],
            //"bSort": false,
            "dom": '<"top-left" <"displayCount"l> <"displayInfo"i> ><"top-right"p>rt<"bottom"p>',
            "oLanguage": {
                "sInfo": "Showing <span class='search-multiple-page'> <span class='dt_start'>_START_</span> to </span> <span class='dt_end'>_END_</span> of _TOTAL_ <span class='search-item-type'>"+SearchItemType+"</span>",
                "sInfoEmpty": "No Contacts to show",
                "sLengthMenu": "Display _MENU_ "
            },
            "initComplete": function(settings, json) {
                //console.log(settings._iDisplayLength);
                console.log(settings);
                if(DataTableLength <= settings._iDisplayLength){
                    $('span.search-multiple-page').css('display','none');
                }else{
                    $('span.search-multiple-page').removeAttr('style');
                }

            },
            "createdRow": function ( row, data, index ) {
                //$(row).attr('data-contactid',data[3]);
                //console.log(data);
                //console.log(index);
                //object = $('<div/>').html(string).contents();

                //Execute Only If TaskSearch
                if(ContactsTable.hasClass('TaskSearch')){
                    var tableClass = $('select.search_filter option:selected' ).attr('data-group');
                    console.log("tableClass: " + tableClass);
                    $('a.rowContactResults', row).addClass(tableClass);
                    if(tableClass === 'TaskSearch'){
                        var href = $('a.rowContactResults', row).attr('href');
                        $('a.rowContactResults', row).removeAttr('href');
                        $('a.rowContactResults', row).attr('data-href', href);
                    }

                    $.each(data, function(key, value){
                        var object = $('<div/>').html(value);
                        var field = object.find('a').attr('data-field');
                        var fieldVal = object.find('a').html();

                        if(field ==='Completion Date'){
                            //console.log(fieldVal);
                            var noteId = $('.toggleThisTask', row).attr('data-noteid');
                            var contactid = $('.toggleThisTask', row).attr('data-contactid');
                            console.log('Going to Invalid toggleThisTask');
                            if(noteId === "undefined" || contactid === "undefined" || contactid === "0"){
                                console.log('Invalid toggleThisTask');
                                $('.toggleThisTask', row).toggles({
                                    text: {
                                        on: 'Done', // text for the ON position
                                        off: 'Not Done' // and off
                                    },
                                    on: false, // is the toggle ON on init
                                    animate: 150, // animation time (ms)
                                    easing: 'easeOutQuint', // animation transition easing function
                                    width: 80, // width used if not set in css
                                    height: 25 // height if not set in css
                                }).addClass('disabled');
                                $('.toggleThisTask', row).on('click', function () {
                                    alert("Oops! This saved search doesn't contain a Contact Id and/or Note Id, so I don't know which contact or note to display!\n\rPlease add the Id's to the columns of the saved search in Infusionsoft, save it and re-select the saved search on this page.\n\rThanks.");
                                });
                            }else {
                                if(fieldVal.trim() !== ''){
                                    $('.toggleThisTask', row).toggles({
                                        text: {
                                            on: 'Done', // text for the ON position
                                            off: 'Not Done' // and off
                                        },
                                        on: true, // is the toggle ON on init
                                        animate: 150, // animation time (ms)
                                        easing: 'easeOutQuint', // animation transition easing function
                                        width: 80, // width used if not set in css
                                        height: 25 // height if not set in css
                                    }).addClass('disabled');
                                }else
                                {
                                    var theCounter = [];
                                    $('.toggleThisTask', row).toggles({
                                        text: {
                                            on: 'Done', // text for the ON position
                                            off: 'Not Done' // and off
                                        },
                                        on: false, // is the toggle ON on init
                                        animate: 150, // animation time (ms)
                                        easing: 'easeOutQuint', // animation transition easing function
                                        width: 80, // width used if not set in css
                                        height: 25 // height if not set in css
                                    })
                                        .on('toggle', function(e, active) {
                                            var toggle = $(this);
                                            var guid =  toggle.attr('data-guid');
                                            var noteContactId=  toggle.attr('data-contactid');
                                            var noteId =  toggle.attr('data-noteid');
                                            if (active) {
                                                console.log('Toggle is now ON!');
                                                var blob = $('.toggle-blob', $(this));
                                                var sec = 5;
                                                theCounter[guid] = setInterval(function () {
                                                    blob.html('<span class="counting"><i>'+sec+'</i></span>');
                                                    $('span', blob).fadeOut(1000, function () {
                                                        blob.show();
                                                    });
                                                    sec--;
                                                    if(sec < 0){
                                                        compeleteTask(noteContactId,noteId, row);
                                                        clearInterval(theCounter[guid]);
                                                        blob.html('');
                                                        blob.html('<span class="locked"><i class="fa fa-lock" aria-hidden="true"></i></span>');
                                                        toggle.toggleClass('disabled', true);
                                                        var out = setTimeout(function () {
                                                            $('span', blob).fadeOut('slow');
                                                        },2000);
                                                    }
                                                },1000)
                                            } else {
                                                console.log('Toggle is now OFF!');
                                                clearInterval(theCounter[guid]);
                                            }
                                        });
                                }
                            }

                        }
                    });
                }
            }
        });
        addColumnFilters(ContactsTable);
        ContactsDataTable.page( DataTablesDisplayPage ).draw(false);
        ContactsDataTable.on( 'draw.dt', function () {
            //console.log('Perpage: '+ ContactsDataTable.page.len());
            //console.log('Total: '+ DataTableLength);
            var displayIndexes = ContactsDataTable.page.info();
            localStorage.setItem( 'DataTablesDisplayStart', displayIndexes.start );
            localStorage.setItem( 'DataTablesDisplayPage', displayIndexes.page );
            //console.log(displayIndexes);
            if(DataTableLength <= ContactsDataTable.page.len()){
                $('span.search-multiple-page').css('display','none');
            }else{
                $('span.search-multiple-page').removeAttr('style');
            }

        });
        ContactsDataTable.on('search.dt', function() {
            //filtered rows data as arrays
            FilterResults = ContactsDataTable.rows( { filter : 'applied'} ).data();
            var NewFilterResults = [];
            $.each(FilterResults, function (index,theValue) {
                if(/(#contact\/)([^"]*)/g.test(theValue[1])){
                    NewFilterResults.push(theValue[1].match(/(#contact\/)([^"]*)/g)[0].replace(/#contact\//g,''));
                }
            });
            //console.log(JSON.stringify(NewFilterResults));
            localStorage.setItem("FilterResults", JSON.stringify(NewFilterResults));
        })
        $("thead tr:eq(1) th",ContactsTable).unbind('click.DT');
        applyLastSearchFilter(ContactsTable);
        ContactsDataTable.columns.adjust().responsive.recalc();
    }
}
function onlyShowWithIds(showWithIds) {
    $("table#UserConnectorInfoTableFilterResult tbody tr").each(function () {
        var theTR =  $(this);
        if(theTR.hasClass('hideThis')) return true;
        var theIdsStr = theTR.attr("data-contactids");
        var theIdsArr = theIdsStr.split(',');

        var show = false;
        if(showWithIds.length > 0){
            $.each(showWithIds, function (index, theId) {
                if($.inArray(theId,theIdsArr) !== -1){
                    show = true;
                    return false;
                }
                console.log(theId);
            })
            if(show === false){
                theTR.addClass('hideThisSub');
            }else{
                theTR.removeClass('hideThisSub');
            }
        }else{
            theTR.removeClass('hideThisSub');
        }


    });

}
function getConnectedDataByGroup(GroupId) {
    var jsonData = {"controler":"core/contact/basic_info","action":"getConnectedDataByGroup","session_name":session_name,"data":{"GroupId":GroupId, "ContactId":ContactInfo.Id,"FirstName":ContactInfo.FirstName,"LastName":ContactInfo.LastName,"Email":ContactInfo.Email,"session_name":session_name,"assetsVersion":assetsVersion}};
    var successFn = function(e){
        $(".modal-body .connected-data-sidebar-results").html(e.html);
        var list = '';
        $.each(e.allcontact, function (index, theInfo) {
            var maskedEmail = theInfo.Email;
            if(theInfo.Email.length > 22){
                maskedEmail = (theInfo.Email).substring(0, 22) + '...';
            }
        list += '<li class="filter-option-item " data-contactid="'+theInfo.ContactId+'">'+theInfo.FirstName+ ' ' +theInfo.LastName+' <small class="info-email" title="'+theInfo.Email+'">'+ maskedEmail+ '</small></li>';

        });
        $(".filter-contact-list ul.ul-filter-contact-list").html(list);

        $("table#UserConnectorInfoTableFilterResult tbody tr").each(function () {
            checkItemsToDisableBeforeAdd(GroupId,$(this));
        });
        UserConnectorInfoTableFilterResult = $("#UserConnectorInfoTableFilterResult").DataTable({
            //var itemFootnote = '<small class="footnote">Contact Id: '+theContact.Id+'</small>'
            responsive: true,
            "pageLength": 10,
            "paging":   true,
            "lengthChange": true,
            "searching": true,
            "info":     true,
            "createdRow": function ( row, data, index ) {
                //console.log(data);
                $(row).addClass(data['hideThis']);//'hideThis'
                $(row).attr('data-itemid',data['itemid']);
                $(row).attr('data-contactids',data['contactids']);
                $(row).attr('data-raw',data['data_raw']);
                $(row).attr('data-connectedcontacts',data['data_connectedcontacts']);
            },
            "processing": true,
            "serverSide": true,
            "order": [[ 1, "asc" ]],
            "columnDefs": [
                { "orderable": false, "targets": 0 }
            ],
            "ajax": {
                "url": ajax_url+"/data_table?GroupId="+GroupId+"&ContactId="+ContactInfo.Id+"&FirstName="+ContactInfo.FirstName+"&LastName="+ContactInfo.LastName+"&Email="+ContactInfo.Email+"&session_name="+session_name+"&time_now="+Math.floor(Date.now() / 1000),
                "type": "POST",
                "data": function ( d ) {
                    d.FilteredContactId = FilteredContactId;
                },
                "cache": false
            }
        });
        UserConnectorInfoTableFilterResult
            .on( 'draw.dt', function () {
                $(".ConnectedContactRelationshipsBulkAdd").multiselect({
                    //includeSelectAllOption: true
                    numberDisplayed: 2,
                    maxHeight: 200,
                    allSelectedText:false
                });
                UserConnectorInfoTableFilterResult.columns.adjust().responsive.recalc();
            } );
        UserConnectorInfoTableFilterResult
            .on('page', function () {
                UserConnectorInfoTableFilterResult.columns.adjust().responsive.recalc();
                $(".ConnectOtherData .modal-body").toggleClass( "refresh" ); //needed for chrome bug
            })
            .on('sort', function () {
                UserConnectorInfoTableFilterResult.columns.adjust().responsive.recalc();
                $(".ConnectOtherData .modal-body").toggleClass( "refresh" );//needed for chrome bug
            });
        UserConnectorInfoTableFilterResult.columns.adjust().responsive.recalc();
        $(window).scroll(function() {
            $(".ConnectOtherData .modal-body").toggleClass( "refresh" );//needed for chrome bug
        });
        $("div").scroll(function() {
            $(".ConnectOtherData .modal-body").toggleClass( "refresh" );//needed for chrome bug
        });

    };
    ajaxRequester('connect-other-data .modal-content', 'connect-other-data .modal-body', jsonData, successFn);
}
function renderRealTimeStatisticsWorkspaces(Workspaces) {
    $(".agent-status-total span").html(Workspaces.totalWorkers);
    var td = '';
    td += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12   no-pad-left no-pad-right">';
    td += '<h4 class="agent-status-total">Total Agents: <span>'+Workspaces.totalWorkers+'</span></h4>';
    td += '</div>';
    $(Workspaces.activityStatistics).each(function (index, theActivity) {
        td += '<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3   no-pad-left no-pad-right">';
        td += '<span class="agent-status agent-'+theActivity.friendlyName+'">'+theActivity.friendlyName+': <span>'+theActivity.workers+'</span></span>';
        td += '</div>';
    });
    $("div.callcenter-statuses").html(td);
}

function renderRealTimeStatisticsTaskQueues(TaskQueues) {
    var td = '';
    var tbody = $("table.callcenter-sections tbody");
    $.each(TaskQueues, function (QueueName, QueueDetails) {
        //console.log(QueueName);
        //console.log(QueueDetails);
        td += '<tr data-taskqueuename="'+QueueName+'">';
        td += '<td>'+QueueName+'</td>';
        td += '<td>'+QueueDetails.totalEligibleWorkers+'</td>';
        td += '<td>'+QueueDetails.totalAvailableWorkers+'</td>';
        td += '<td class="activity-statistics">';
        $(QueueDetails.activityStatistics).each(function (index, theActivity) {
            if(theActivity.friendlyName === 'Idle') return true;
            td += '<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4   no-pad-left no-pad-right">';
            td += '<span class="agent-status agent-'+theActivity.friendlyName+'">'+theActivity.friendlyName+': <span>'+theActivity.workers+'</span></span>';
            td += '</div>';
        });
        td += '</td></tr>';
        if($("tr[data-taskqueuename='"+QueueName+"']",tbody).length === 0){
            tbody.append(td);
        }else{
            $("tr[data-taskqueuename='"+QueueName+"']",tbody).replaceWith(td);
        }

    });

}
function renderRealTimeStatisticsWorkers(WorkerDetails) {
    var $Row = '';
    var attributes = WorkerDetails.attributes;
    var WorkerAttributes = {
        "skills": ["sales", "billing", "technical", "support"],
        "languages": ["english", "spanish", "russian"]
    };
    var tbody = $("table.callcenter-agents tbody");
    $Row += '<tr class="agent-tr agent-'+WorkerDetails.activityName+'" data-contactid="'+attributes['infusionsoftId']+'" data-email="'+attributes['email']+'" >';
    $Row += "<td><i class=\"fa fa-tty\"></i> "+WorkerDetails.friendlyName+"<small class='footnote'>"+attributes['email']+"</small></td>";
    $Row += "<td>";
    $.each(WorkerAttributes.skills,function (index, skill) {
        var checked = '';
        if(attributes['skills'].indexOf(skill) !== -1) checked = 'checked';
        $Row += '<div class="skill-check col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pad-left"> <span class="form-check-label"> <input id="contact_skill_'+skill+'_'+attributes['infusionsoftId']+'" type="checkbox"  class="addGroup checkbox form-check-input" name="skills_id_'+attributes['infusionsoftId']+'" value="'+skill+'" '+checked+'> <label for="contact_skill_'+skill+'_'+attributes['infusionsoftId']+'">'+skill+'</label> </span> </div>';
    });
    $Row += "</td>";
    $Row += "<td>";
    $.each(WorkerAttributes.languages,function (index, language) {
        var checked = '';
        if(attributes['languages'].indexOf(language) !== -1) checked = 'checked';
        $Row += '<div class="skill-check col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pad-left"> <span class="form-check-label"> <input id="contact_lang_'+language+'_'+attributes['infusionsoftId']+'" type="checkbox"  class="addGroup checkbox form-check-input" name="languages_id_'+attributes['infusionsoftId']+'" value="'+language+'"  '+checked+'> <label for="contact_lang_'+language+'_'+attributes['infusionsoftId']+'">'+language+'</label> </span> </div>';
    });
    $Row += "</td>";
    $Row += "<td><button type=\"button\" class=\"btn btn-default btn-update-agent\" onclick='processUpdateWorker(this);'> Update </button></td>";
    $Row += '</tr>';
    if($("tr[data-contactid='"+attributes['infusionsoftId']+"']",tbody).length === 0){
        tbody.append($Row);
    }else{
        $("tr[data-contactid='"+attributes['infusionsoftId']+"']",tbody).replaceWith($Row);
    }
}
function renderRealTimeStatistics(e) {
    if(typeof e.RealTimeStatistics.Workspaces !== "undefined"){
        var Workspaces = e.RealTimeStatistics.Workspaces;
        renderRealTimeStatisticsWorkspaces(Workspaces);

    }

    if(typeof e.RealTimeStatistics.TaskQueues !== "undefined"){
        var TaskQueues = e.RealTimeStatistics.TaskQueues;
        renderRealTimeStatisticsTaskQueues(TaskQueues);
    }
    if(typeof e.RealTimeStatistics.Workers !== "undefined"){
        var Workers = e.RealTimeStatistics.Workers;
        var WorkerAttributes = e.WorkerAttributes;
        renderRealTimeStatisticsWorkers(Workers,WorkerAttributes)
    }else{
        $("p.agent-note.note2 span").html("Call Center does not have any Agent, Please click add button to add Agents.");
    }

}
function setupCallcenter() {
    var customAjaxUrl = '/contactcenter/action';
    var jsonData = {"action":"SetupCallcenter","session_name":session_name,"assetsVersion":assetsVersion};
    var successFn = function(e){
        console.log(e);
        //$("div.callcenterContent").html(JSON.stringify(e));
        RealtimeStats();
        //renderRealTimeStatistics(e)

    };
    ajaxRequester('callcenterContainer', 'callcenterContent', jsonData, successFn,customAjaxUrl);
}
function initWorkspace() {
    if(WorkspaceToken == '') return false;
    CallCenterWorkspace = new Twilio.TaskRouter.Workspace(WorkspaceToken);
    CallCenterWorkspace.on("ready", function(workspace) {
        console.log("CallCenter Workspace Ready!");
        console.log(workspace.sid); // 'WSxxx'
        console.log(workspace.friendlyName); // 'Workspace 1'
        console.log(workspace.prioritizeQueueOrder); // 'FIFO'
        console.log(workspace.defaultActivityName); // 'Offline'
    });
}
function WorkspaceRealtimeStats() {
    var queryParams = {};
    CallCenterWorkspace.realtimeStats.fetch(
        queryParams,
        function(error, statistics) {
            if(error) {
                console.log(error.code);
                console.log(error.message);
                return {
                    code:error.code,
                    message:error.message
                };
            }
            console.log("Fetched Workspace statistics: ");
            console.log(statistics);
            renderRealTimeStatisticsWorkspaces(statistics);
            return statistics;
        }
    );
    CallCenterWorkspace.workers.fetch(
        function(error, workerList) {
            if(error) {
                console.log(error.code);
                console.log(error.message);
                return;
            }
            var data = workerList.data;
            console.log("Fetched Workers statistics: ");

            for(i=0; i<data.length; i++) {
                //console.log(data[i].friendlyName);
                console.log(data[i]);
                renderRealTimeStatisticsWorkers(data[i]);
            }
        }
    );
}
function TaskQueuesRealtimeStats() {

    $.each(CallcenterTaskQueues, function (TaskQueueName, TaskQueue) {
        var TaskQueuesRealtimeStatsValues = {};
        TaskQueue.realtimeStats.fetch(
            function(error, statistics) {
                if(error) {
                    console.log(error.code);
                    console.log(error.message);
                    return;
                }
                TaskQueuesRealtimeStatsValues[TaskQueueName] = statistics;
                console.log("Fetched TaskQueue statistics: ");
                console.log(TaskQueuesRealtimeStatsValues);
                renderRealTimeStatisticsTaskQueues(TaskQueuesRealtimeStatsValues);
            }
        );

    });

}
function WorkersRealtimeStats() {

    $.each(CallCenterWorkers, function (WorkerSid, Worker) {
        var WorkersRealtimeStatsValues = {};
        Worker.activities.fetch(
            function(error, activityList) {
                if(error) {
                    console.log(error.code);
                    console.log(error.message);
                    return;
                }
                var data = activityList.data;
                console.log("Fetched Workers statistics: ");
                console.log(activityList);
                for(i=0; i<data.length; i++) {
                    //console.log(data[i].friendlyName);
                }
            }
        );

    });

}
function RealtimeStats() {
    console.log("RealtimeStats fired!");
    WorkspaceRealtimeStats()
    TaskQueuesRealtimeStats();

}
function initTaskQueues() {
    $.each(TaskQueueTokens, function (TaskQueueName, TaskQueueToken) {
        CallcenterTaskQueues[TaskQueueName] = new Twilio.TaskRouter.TaskQueue(TaskQueueToken);
        CallcenterTaskQueues[TaskQueueName].on("ready", function(taskQueue) {
            console.log("CallCenter TaskQueue Ready!");
            console.log(taskQueue.sid);                // 'WQxxx'
            console.log(taskQueue.friendlyName);       // 'Simple FIFO Queue'
            console.log(taskQueue.targetWorkers);      // '1==1'
            console.log(taskQueue.maxReservedWorkers); // 20
        });
    });
}
function initWorkers() {
    // skip copy this agentSid
    $.each(WorkersTokens, function (WorkerSid, WorkerToken) {
        if(WorkerSid !== agentSid){
            CallCenterWorkers[WorkerSid] = new Twilio.TaskRouter.Worker(WorkerToken);
        }else{
            CallCenterWorkers[WorkerSid] = CallCenterWorker;
        }
        CallCenterWorkers[WorkerSid].on("ready", function(worker) {
            console.log(worker.sid);          // 'WKxxx'
            console.log(worker.friendlyName);    // 'Worker 1'
            console.log(worker.activityName);    // 'Reserved'
            console.log(worker.available);       // false
        });
    });
}
function initWorker(sid,activitySid) {
    if(CallCenterWorker == null){
        console.log('Initializing Agent:  '+sid);
        CallCenterWorker = new Twilio.TaskRouter.Worker(WorkersTokens[sid], false, activitySid, WorkerActivities['Offline']['sid'], true);
        registerTaskRouterCallbacks();

    }else{
        var Properties = {"ActivitySid":activitySid};
        updateWorker(Properties);
    }
}
function registerTaskRouterCallbacks() {
    CallCenterWorker.on('ready', function(worker) {
        $('.toggleThisAgent').removeClass('disabled');
        console.log("CallCenter Worker Ready!");
        agentActivityChanged(worker.activityName);
        logger("Successfully registered as: " + worker.friendlyName);
        logger("Current activity is: " + worker.activityName);
    });

    CallCenterWorker.on('activity.update', function(worker) {
        agentActivityChanged(worker.activityName);
        logger("Worker activity changed to: " + worker.activityName);
    });

    CallCenterWorker.on("reservation.created", function(reservation) {
        logger("-----");
        logger("You have been reserved to handle a call!");
        logger("Call from: " + reservation.task.attributes.from);
        logger("Selected language: " + reservation.task.attributes.selected_language);
        logger("-----");
    });

    CallCenterWorker.on("reservation.accepted", function(reservation) {
        logger("Reservation " + reservation.sid + " accepted!");
    });

    CallCenterWorker.on("reservation.rejected", function(reservation) {
        logger("Reservation " + reservation.sid + " rejected!");
    });

    CallCenterWorker.on("reservation.timeout", function(reservation) {
        logger("Reservation " + reservation.sid + " timed out!");
    });

    CallCenterWorker.on("reservation.canceled", function(reservation) {
        logger("Reservation " + reservation.sid + " canceled!");
    });
}
/* Hook up the agent Activity buttons to Worker.js */

function bindAgentActivityButtons() {
    // Fetch the full list of available Activities from TaskRouter. Store each
    // ActivitySid against the matching Friendly Name
    var activitySids = {};
    worker.activities.fetch(function(error, activityList) {
        var activities = activityList.data;
        var i = activities.length;
        while (i--) {
            activitySids[activities[i].friendlyName] = activities[i].sid;
        }
    });

    /* For each button of class 'change-activity' in our Agent UI, look up the
    ActivitySid corresponding to the Friendly Name in the button’s next-activity
    data attribute. Use Worker.js to transition the agent to that ActivitySid
    when the button is clicked.*/
    var elements = document.getElementsByClassName('change-activity');
    var i = elements.length;
    while (i--) {
        elements[i].onclick = function() {
            var nextActivity = this.dataset.nextActivity;
            var nextActivitySid = activitySids[nextActivity];
            worker.update("ActivitySid", nextActivitySid);
        }
    }
}

/* Update the UI to reflect a change in Activity */

function agentActivityChanged(activity) {
    showAgentActivity(activity);
}



function showAgentActivity(activity) {
    $(".inbound-activity").html(activity);
}

/* Other stuff */

function logger(message) {
    $(".inbound-log").html(message);
}
function updateWorker(Properties,theBtn) {
    CallCenterWorker.update(Properties, function(error, worker) {
        $('.toggleThisAgent').removeClass('disabled');
        if(theBtn != null) theBtn.prop("disabled", false);
        if(error) {
            console.log(error.code);
            console.log(error.message);
        } else {
            console.log("Agent Updated To: "+worker.activityName); // "Offline"
            var syngData = {action:"adminUpdate"};
            globalSyncDoc.set(syngData);
        }
    });
}

function updateCallcenterAgentActivity_old(Data) {
    var customAjaxUrl = '/contactcenter/action';
    var jsonData = {"action":"UpdateCallcenterAgentActivity","data":Data,"session_name":session_name,"assetsVersion":assetsVersion};
    var theDelay = [];
    var theInterval;
    var theIntervalKey = 0;
    var successFn = function(e){
        console.log(e);
        e['Activities'] = undefined;
        e['TaskQueues'] = undefined;
        e['UpdatedWorker'] = undefined;
        e['Workers'] = undefined;
        e['Workspaces'] = undefined;
        //run this for now,
        globalSyncDoc.set(e);

        //but this is needed when there are huge users
       /* var Workspaces = {};
        Workspaces['RealTimeStatistics'] = {};
        Workspaces['RealTimeStatistics']['Workspaces'] = e['RealTimeStatistics']['Workspaces'];
        theDelay[0] = Workspaces;

        var Workers = {};
        Workers['RealTimeStatistics'] = {};
        Workers['RealTimeStatistics']['Workers'] = e['RealTimeStatistics']['Workers'];
        Workers['WorkerAttributes'] = e['WorkerAttributes'];
        theDelay[1] = Workers;

        var TaskQueues = {};
        TaskQueues['RealTimeStatistics'] = {};
        TaskQueues['RealTimeStatistics']['TaskQueues'] = e['RealTimeStatistics']['TaskQueues'];
        theDelay[2] = TaskQueues;

        theInterval =  setInterval(function(){
            globalSyncDoc.set(theDelay[theIntervalKey]);
            theIntervalKey++;
            if(theIntervalKey >=3) clearInterval(theInterval);
            }, 1000);*/
    };
    ajaxRequester('toggleThisAgentContainer', 'toggleThisAgentContent', jsonData, successFn,customAjaxUrl);
}
function heartbeatCallcenter() {
    var customAjaxUrl = '/contactcenter/action';
    var jsonData = {"action":"CallCenterHeartbeat","session_name":session_name,"assetsVersion":assetsVersion};
    CallCenterHeartbeat = setInterval(function(){
        var successFn = function(e){
            //console.log('renderRealTimeStatistics');
            //console.log(e);
            renderRealTimeStatistics(e)
        };
        ajaxRequester(null, null, jsonData, successFn,customAjaxUrl);
        }, 10000);
}
function initAgentSwitch() {
    if(CallCenterAgent === false) return false;
    $('.toggleThisAgentContainer').fadeIn('fast');
    var Properties;
    initWorker(agentSid, WorkerActivities['Offline']['sid']);
    $('.toggleThisAgent').toggles({
        text: {
            on: 'Ready', // text for the ON position
            off: 'Not Ready' // and off
        },
        on: false, // is the toggle ON on init
        animate: 150, // animation time (ms)
        easing: 'easeOutQuint', // animation transition easing function
        width: 100, // width used if not set in css
        height: 25 // height if not set in css
    })
    .on('toggle', function(e, active) {
        if (active) {
            $('.toggleThisAgent').addClass('disabled');
            Properties = {"ActivitySid":WorkerActivities['Idle']['sid']};
            updateWorker(Properties);
            $(window).bind('beforeunload',function(e) {
                var dialogText =  "Are you sure you want to leave the page?, You are now set as NOT READY for Inbound Calls. !";
                e.returnValue = dialogText;
                return dialogText;
            });
        } else {
            $(window).unbind('beforeunload');
            $('.toggleThisAgent').addClass('disabled');
            Properties = {"ActivitySid":WorkerActivities['Offline']['sid']};
            updateWorker(Properties);
        }
    });
}

function processCallCenterSyncData(UpdatedData) {
        if(UpdatedData.action === 'adminUpdate' && CallCenterAdmin === true){
            RealtimeStats();
        }
}
function bindPreloadingAgentListTR(){
    var timer = 0;
    var delay = 200;
    var prevent = false;
    var clickedTR;
    var Data = {};
    $("#AddCallCenterAgent table.contact-preloading-list tr.ContactItem")
        .once("click", function() {
            clickedTR = $(this);
            timer = setTimeout(function() {
                if (!prevent) {
                    $('#AddCallCenterAgent').modal('hide');
                    $("input.search-contact-tobe-agent").val('');
                    Data['ContactId'] = clickedTR.attr("data-contactid");
                    Data['Email'] = clickedTR.attr("data-email");
                    Data['FullName'] = clickedTR.attr("data-firstname")+' '+clickedTR.attr("data-lastname");
                    Data['LastName'] = clickedTR.attr("data-lastname");
                    Data['FirstName'] = clickedTR.attr("data-firstname");
                    createCallcenterAgent(Data);
                }
                prevent = false;
            }, delay);
        })
        .on("dblclick", function() {
            clearTimeout(timer);
            prevent = true;
        });
}

function bindSaveAgent() {
    var theTr;
    var theBtn;
    var Data = {};
    $("button.btn-save-agent")
        .once("click", function() {
            theBtn = $(this);
            theTr = $(this).parents("tr.agent-tr");
            Data['ContactId'] = theTr.attr("data-contactid");
            Data['Email'] = theTr.attr("data-email");
            Data['FirstName'] = theTr.attr("data-firstname");
            Data['LastName'] = theTr.attr("data-lastname");
            Data['skills'] = [];
            $('input[name="skills_id_'+Data['ContactId']+'"]', theTr).each(function () {
                if($(this).prop('checked')) Data['skills'].push($(this).val());
            });
            Data['languages'] = [];
            $('input[name="languages_id_'+Data['ContactId']+'"]', theTr).each(function () {
                if($(this).prop('checked')) Data['languages'].push($(this).val());
            });
            if(Data['skills'].length === 0){
                alert("Please choose at least one skill");
                theBtn.prop("disabled", false);
                return false;
            }
            if(Data['languages'].length === 0){
                alert("Please choose at least one language");
                theBtn.prop("disabled", false);
                return false;
            }

            saveCallcenterAgent(Data,theBtn)
    });
}
function processUpdateWorker(Obj) {
    var Data = {};
    var theBtn = $(Obj);
    var theTr = theBtn.parents("tr.agent-tr");
    theBtn.prop("disabled", true);
    Data['infusionsoftId'] = theTr.attr("data-contactid");
    Data['email'] = theTr.attr("data-email");
    Data['skills'] = [];
    $('input[name="skills_id_'+Data['infusionsoftId']+'"]', theTr).each(function () {
        if($(this).prop('checked')) Data['skills'].push($(this).val());
    });
    Data['languages'] = [];
    $('input[name="languages_id_'+Data['infusionsoftId']+'"]', theTr).each(function () {
        if($(this).prop('checked')) Data['languages'].push($(this).val());
    });
    if(Data['skills'].length === 0){
        alert("Please choose at least one skill");
        theBtn.prop("disabled", false);
        return false;
    }
    if(Data['languages'].length === 0){
        alert("Please choose at least one language");
        theBtn.prop("disabled", false);
        return false;
    }
    var Param = {};
    Param['WorkspaceSid'] = workspaceSid;
    Param['WorkerSid'] = agentSid;
    Param['NewValues'] = {};
    Param['NewValues']['attributes'] = JSON.stringify(Data);
    var customAjaxUrl = '/contactcenter/action';
    jsonData = {"action":"UpdateWorker","data":Param,"session_name":session_name,"assetsVersion":assetsVersion};
    var successFn = function(e){
        theBtn.prop("disabled", false);
        console.log('Save Agent:');
        console.log(e);
        RealtimeStats();
    };
    ajaxRequester('agent-tr[data-contactid="'+Data['infusionsoftId']+'"]', 'agent-tr[data-contactid="'+Data['infusionsoftId']+'"] td', jsonData, successFn,customAjaxUrl);

}
function saveCallcenterAgent(Data,theBtn) {
    theBtn.prop("disabled", true);
    var customAjaxUrl = '/contactcenter/action';
    jsonData = {"action":"SaveAgent","data":Data,"session_name":session_name,"assetsVersion":assetsVersion};
    var successFn = function(e){
        theBtn.prop("disabled", false);
        console.log('Save Agent:');
        console.log(e);
        RealtimeStats();
    };
    ajaxRequester('agent-tr[data-contactid="'+Data['ContactId']+'"]', 'agent-tr[data-contactid="'+Data['ContactId']+'"] td', jsonData, successFn,customAjaxUrl);

}
function createCallcenterAgent(Data) {
    var customAjaxUrl = '/contactcenter/action';
    jsonData = {"action":"ChosenAgent","data":Data,"session_name":session_name,"assetsVersion":assetsVersion};
    var successFn = function(e){
        console.log('Chosen Agent:');
        $("table.callcenter-agents tbody").append(e);
        bindSaveAgent();
    };
    ajaxRequester('callcenterContainer', 'callcenterContent', jsonData, successFn,customAjaxUrl);

}
function savePipelines(Pipelines) {
    var jsonData = {"controler":"core/tabs/admin","action":"savePipelines","session_name":session_name,"data":{"Pipelines":Pipelines,"session_name":session_name,"assetsVersion":assetsVersion}};
    var successFn = function(e){
    };
    ajaxRequester('OpportunitySettingsContainer', 'OpportunityContent', jsonData, successFn);
}
function saveRelationships(Relationships) {
    var jsonData = {"controler":"core/tabs/admin","action":"saveRelationships","session_name":session_name,"data":{"Relationships":Relationships,"session_name":session_name,"assetsVersion":assetsVersion}};
    var successFn = function(e){
    };
    ajaxRequester('RelationshipSettingsContainer', 'RelationshipContent', jsonData, successFn);
}
function saveUsersCallerId(_UsersCallerIds) {
    var jsonData = {"controler":"core/tabs/admin","action":"saveUsersCallerId","session_name":session_name,"data":{"UsersCallerIds":_UsersCallerIds,"session_name":session_name,"assetsVersion":assetsVersion}};
    console.log(jsonData);
    var successFn = function(e){
        UsersCallerIds = JSON.parse(e.data);
        console.log(UsersCallerIds);
    };
    ajaxRequester(null, null, jsonData, successFn);
}
// callback functions
function jseditor_save(id, content){
    var jsonData = {"controler":"core/tabs/admin","action":"saveCustomJS","session_name":session_name,"data":{"content":content.b64encode(),"session_name":session_name,"assetsVersion":assetsVersion}};
    console.log(jsonData);
    var successFn = function(e){
    };
    ajaxRequester("jseditor", "jseditor .admin-panelBody", jsonData, successFn);
}
// callback functions
function csseditor_save(id, content){
    var jsonData = {"controler":"core/tabs/admin","action":"saveCustomCSS","session_name":session_name,"data":{"content":content.b64encode(),"session_name":session_name,"assetsVersion":assetsVersion}};
    console.log(jsonData);
    var successFn = function(e){
    };
    ajaxRequester("csseditor", "csseditor .admin-panelBody", jsonData, successFn);
}
function updateOpp(jsonStr,Identifier) {
    console.log('updateOpp called');
    var jsonData = {"controler":"core/tabs/opportunity","action":"update","session_name":session_name,"data":{"values":jsonStr,"ContactId":ContactId,"session_name":session_name,"assetsVersion":assetsVersion}};
    var successFn = function(e){
        var NextActionNote = '';
        var NewNextActionDate = '';
        var EstimatedCloseDate = '';
        if(typeof e.new_data[0].NextActionDate !== "undefined"){
            var dateTime = new Date(e.new_data[0].NextActionDate.date);
            NewNextActionDate = moment(dateTime).format("YYYY-MM-DD HH:mm");
            if(NewNextActionDate === 'Invalid date'){
                NewNextActionDate = e.new_data[0].NextActionDate.date.replace(':00.000000','');
            }
        }
        if(typeof e.new_data[0].EstimatedCloseDate !== "undefined"){
            var dateTimeClose = new Date(e.new_data[0].EstimatedCloseDate.date);
            EstimatedCloseDate = moment(dateTimeClose).format("YYYY-MM-DD");
            if(EstimatedCloseDate === 'Invalid date'){
                EstimatedCloseDate = e.new_data[0].EstimatedCloseDate.date.replace(' 00:00:00.000000','');
            }
        }
        if(typeof e.new_data[0].NextActionNotes !== "undefined"){
            NextActionNote = e.new_data[0].NextActionNotes;
        }
        var NewStage = e.new_data[0].StageID;
        var PipelineName = e.PipelineName;
        var StageName = $("select#OppStage option[value="+NewStage+"]").html();
        OppTableData['Data'][3] = NewNextActionDate;
        OppTableData['Data'][2] = StageName;
        OppTableData['Data'][0] = PipelineName;
        OppTable[Identifier]
            .row( OppTableData['Row'] )
            .data( OppTableData['Data'] )
            .draw();
        $("table#OppTable[data-shortcodeid="+Identifier+"] tr[data-oppid="+e.dataoppid+"]")
            .attr('data-nextactionnote',NextActionNote.b64encode())
            .attr('data-closedate',EstimatedCloseDate)
            .attr('data-nextactiondate',NewNextActionDate)
            .attr('data-allowedmove',e.allowed_move)
            .attr('data-stageid',NewStage)
            .attr('data-customfields',e.data_customfields)
            .trigger('click');
    };
    ajaxRequester('opptabsContainer', 'opptabsContainer #opptabs', jsonData, successFn);
}
function getZuoraAccountDetails(theJson) {
    ZuoraDataObj = {};
    console.log('getZuoraAccountDetails called');
    var jsonData = {"controler":"core/tabs/zuora","action":"getAccounts","session_name":session_name,"data":{"Contacts":theJson,"session_name":session_name,"assetsVersion":assetsVersion}};
    var successFn = function(e){
        $.each(e.ZuoraAccounts, function (ZuoraContactId, Accounts) {
            $.each(Accounts, function (index, Account) {
                console.log("Account:");
                Account.ContactId = ZuoraContactId;
                console.log(Account);
                ZuoraDataObj[Account.Id] = Account;
                $("ul.zuoraAccountList").append("<li class='zuoraAccountName' data-accountid ='"+Account.Id+"'  onclick='showZuoraAccountDetails($(this))'>"+Account.Name+"</li>")

            })
        })
        var firstAccount = $("ul.zuoraAccountList li").first();
        showZuoraAccountDetails(firstAccount);
    };
    ajaxRequester('ZuoraAccountsContainer', 'ZuoraAccountsBody', jsonData, successFn);
}
function showZuoraAccountDetails(obj) {
    $("ul.zuoraAccountList li").removeClass('active');
    $("div.ProductCharges .ProductChargesItem").remove();
    $("div.ZuoraPayments .ZuoraPaymentItem").remove();
    obj.addClass('active');
    var accountId =  obj.attr('data-accountid');
    accountDetails = ZuoraDataObj[accountId];
    var InvoiceCount = accountDetails.Invoices.length;
    var LastInvoiceIndex = InvoiceCount -  1;
    $("ul.zuora-contact_info-list span.zuora-ContactId").html(accountDetails.ContactId);
    $("ul.zuora-contact_info-list span.zuora-AccountNumber").html(accountDetails.AccountNumber);
    $("ul.zuora-contact_info-list span.zuora-AccountBalance").html(accountDetails.Balance + " " + accountDetails.Currency);
    $("ul.zuora-contact_info-list span.zuora-CreditBalance").html(accountDetails.CreditBalance +" "+ accountDetails.Currency);
    $("ul.zuora-contact_info-list span.zuora-TotalInvoiceBalance").html(accountDetails.TotalInvoiceBalance +" "+ accountDetails.Currency);

    $("ul.zuora-contact_info-list span.zuora-LastInvoiced").html(accountDetails.Invoices[LastInvoiceIndex].InvoiceDate);
    $("ul.zuora-contact_info-list span.zuora-ContractedMRR").html(zuora_caculateCMRR(accountDetails.Invoices) +" "+ accountDetails.Currency);
    $("ul.zuora-contact_info-list span.zuora-TodaysMRR").html(zuora_caculateTMRR(accountDetails.Invoices) +" "+ accountDetails.Currency);
    if(typeof accountDetails.Notes !== 'undefined')
    {
        $("ul.zuora-contact_info-list pre.zuora-AccountNotes").html(accountDetails.Notes);
    } else{
        $("ul.zuora-contact_info-list pre.zuora-AccountNotes").html("");
    }
    var InvoiceNumbers = {};
    var RatePlanNames = {};
    $.each(accountDetails.Invoices, function (index,Invoice) {
        InvoiceNumbers[Invoice.Id] = Invoice.InvoiceNumber;
        RatePlanNames[Invoice.Id] = {};
        $.each(Invoice.InvoiceItems, function (indexSub, InvoiceItem ) {
            if(InvoiceItem.Subscription.Status !== "Active") return true;
            RatePlanNames[Invoice.Id] = InvoiceItem.RatePlan.Name;
            var htmlTemplate = $("div.HTML-Template.ProductChargesItem");
            htmlTemplate
                .clone()
                .removeClass("HTML-Template")
                .appendTo("div.ProductCharges")
                .find(".prod-name strong").html(InvoiceItem.ProductName).parents(".ProductChargesItem")
                .find(".rate-plan strong").html(InvoiceItem.RatePlan.Name).parents(".ProductChargesItem")
                .find("table.tbl-product-charges td.ProductChargeName").html(InvoiceItem.RatePlanCharge.Name).parents(".ProductChargesItem")
                .find("table.tbl-product-charges td.ProductChargeDesc").html(InvoiceItem.RatePlanCharge.Description || '').parents(".ProductChargesItem")
                .find("table.tbl-product-charges td.ProductChargeType").html(InvoiceItem.RatePlanCharge.ChargeType).parents(".ProductChargesItem")
                .find("table.tbl-product-charges td.ProductChargeModel").html(InvoiceItem.RatePlanCharge.ChargeModel).parents(".ProductChargesItem")
                .find("table.tbl-product-charges td.ProductChargeUnitPrice").html(InvoiceItem.RatePlanCharge.Price).parents(".ProductChargesItem")
                .find("table.tbl-product-charges td.ProductChargeQty").html(InvoiceItem.RatePlanCharge.Quantity).parents(".ProductChargesItem")
                .find("table.tbl-product-charges td.ProductChargeTotal").html(parseFloat(InvoiceItem.RatePlanCharge.Price) * parseInt(InvoiceItem.RatePlanCharge.Quantity)).parents(".ProductChargesItem")
            ;
        })
    });
    console.log(InvoiceNumbers);
    console.log(RatePlanNames);
    $.each(accountDetails.Payments, function (index,Payment) {
        var htmlTemplate = $("div.HTML-Template.ZuoraPaymentItem");
        htmlTemplate
            .clone()
            .removeClass("HTML-Template")
            .appendTo("div.ZuoraPayments")
            .find("table.tbl-zuora-payments td.PaymentDate").html(moment(Payment.CreatedDate, moment.ISO_8601).format("YYYY-MM-DD")).parents(".ZuoraPaymentItem")
            .find("table.tbl-zuora-payments td.PaymentNumber").html(Payment.PaymentNumber).parents(".ZuoraPaymentItem")
            .find("table.tbl-zuora-payments td.PaymentApplied").html(Payment.Amount).parents(".ZuoraPaymentItem")
            .find("table.tbl-zuora-payments td.PaymentType").html(Payment.Type).parents(".ZuoraPaymentItem")
            .find("table.tbl-zuora-payments td.PaymentAppliedTo").html(InvoiceNumbers[Payment.InvoicePayments.InvoiceId]).parents(".ZuoraPaymentItem")
            .find("table.tbl-zuora-payments td.PaymentRefunds").html(Payment.InvoicePayments.RefundAmount).parents(".ZuoraPaymentItem")
            .find("table.tbl-zuora-payments td.PaymentRateplanName").html(RatePlanNames[Payment.InvoicePayments.InvoiceId]).parents(".ZuoraPaymentItem")
            .find("table.tbl-zuora-payments td.PaymentStatus").html(Payment.Status).parents(".ZuoraPaymentItem")
        ;
    });

    console.log(accountDetails);
}
function zuora_caculateCMRR(Invoices) {
    var CMRR = 0;
    $.each(Invoices,function(index, Invoice) {
       var InvoiceItems = Invoice.InvoiceItems;
       $.each(InvoiceItems, function (itemIndex, InvoiceItem ) {
           if(InvoiceItem.Subscription.Status === "Active" && InvoiceItem.RatePlanCharge.ChargeType === "Recurring"  && InvoiceItem.RatePlanCharge.IsLastSegment === true){
               CMRR += InvoiceItem.ChargeAmount*1;
           }
       })
    })
    return CMRR
}function zuora_caculateTMRR(Invoices) {
    var TMRR = 0;
    $.each(Invoices,function(index, Invoice) {
        var InvoiceItems = Invoice.InvoiceItems;
        $.each(InvoiceItems, function (itemIndex, InvoiceItem ) {
            var today = moment();
            var EffectiveEndDate = moment(InvoiceItem.RatePlanCharge.EffectiveEndDate) || today;
            var EffectiveStartDate = moment(InvoiceItem.RatePlanCharge.EffectiveStartDate);
            if(InvoiceItem.Subscription.Status === "Active" && EffectiveStartDate  <= today  && EffectiveEndDate  >= today){
                TMRR += InvoiceItem.ChargeAmount*1;
            }
        })
    })
    return TMRR
}
function decodeEntities(encodedString) {
    var textArea = document.createElement('textarea');
    textArea.innerHTML = encodedString;
    return textArea.value;
}
function  noteCollapsible() {
    $('article.noteCollapsible').readmore({
        speed: 300,
        embedCSS:false,
        collapsedHeight: 115,
        lessLink: '<a href="#">Show less</a>',
        moreLink: '<a href="#">Read more</a>'
    });
}
function upperCaseF(a){
    setTimeout(function(){
        a.value = a.value.toUpperCase();
    }, 1);
}
function resetCodePrefix(Container,pipelinecode){
    var theNum = 1;
    Container.find("div.adddstages").each(function(){
        stagenumer = theNum < 10 ? '0'+theNum:theNum;
        theNum++;
        $(this).find("div.stagePrefix").html(pipelinecode + stagenumer);
    });
}
function validateStageValue(obj){
    var thelength = $(obj).val().length;
    $(obj).setSelectionRange(thelength.length, thelength.length);
}
function  enableAddStage(obj){
    var parentUL = $(obj).parents("ul.opportunityFieldDetails");
    if(parentUL.find("input.PipelineCode").val() !== '' && parentUL.find("input.PipelineName").val() !== ''){
        parentUL.find("button.addStage").removeAttr('disabled');
    }else{
        parentUL.find("button.addStage").attr('disabled', true);
    }

}
function checkItemsToDisableBeforeAdd(GroupId,row) {
    //enable disable options that meets the limit
    var theTR = $(row);
    var ItemId = theTR.attr("data-itemid");
    var currentConnectedContacts = $(row).attr("data-connectedcontacts");
    var ConnectedContacts = JSON.parse(currentConnectedContacts.b64decode());

    var disabledList = [];
    var usedRelationshipName = {};
    $.each(ConnectedContacts, function (index,meta) {
        $.each(meta.relationships, function (index,Id) {
            if(typeof usedRelationshipName[Id] === "undefined"){
                usedRelationshipName[Id] = 1;
            }else{
                usedRelationshipName[Id]++;
            }
        });
    });
    //console.log("Used:");
    //console.log(usedRelationshipName);
    $.each(usedRelationshipName, function (Id,count) {
        $.each(relationshipRules[GroupId],function (index, rules) {
            if(rules.Id === Id){
                if(rules.exclusive === 'yes'){
                    disabledList.push(Id);
                }
                if(rules.exclusive === 'no'){
                    if(rules.limit !== ""){
                        var limit = parseInt(rules.limit);
                        count = parseInt(count);
                        if(count >= limit) disabledList.push(Id);
                    }
                }
            }
        });
    });
    //console.log("Disabled List:");
    //console.log(disabledList);
    $("ul.multiselect-container", theTR).each(function () {
        var ulContainer = $(this);
        ulContainer.find("input[type=checkbox]").each(function () {
            var theCheckbox = $(this);
            var parentLi = theCheckbox.parents("ul.multiselect-container li");
            theCheckbox.removeAttr('disabled');
            parentLi.removeClass("hideThis");
            if(theCheckbox.is(':checked')){

            }else{
                if($.inArray(theCheckbox.val(), disabledList) !== -1){
                    theCheckbox.attr('disabled', true);
                    parentLi.addClass("hideThis");
                }
            }
        })
    });
}
function checkItemsToDisable(GroupId,ItemId) {
    //enable disable options that meets the limit
    var disabledList = [];
    var usedRelationshipName = {};
    $("form.FormUserConnectedInfo[data-itemid="+ItemId+"] select.field-relationships").each(function () {
        var theSelect = $(this);
        var _theValues = $('option:selected', theSelect).map(function(a, item){return item.value;});
        delete _theValues.context;
        delete _theValues.length;
        delete _theValues.prevObject;
        _theValues = Object.keys(_theValues).map(function (key) { return _theValues[key]; });
        $.each(_theValues, function (index,Id) {
            if(typeof usedRelationshipName[Id] === "undefined"){
                usedRelationshipName[Id] = 1;
            }else{
                usedRelationshipName[Id]++;
            }
        })

    });
    //console.log("Used:");
    //console.log(usedRelationshipName);
    $.each(usedRelationshipName, function (Id,count) {
        $.each(relationshipRules[GroupId],function (index, rules) {
            if(rules.Id === Id){
                if(rules.exclusive === 'yes'){
                    disabledList.push(Id);
                }
                if(rules.exclusive === 'no'){
                    if(rules.limit !== ""){
                        var limit = parseInt(rules.limit);
                        count = parseInt(count);
                        if(count >= limit) disabledList.push(Id);
                    }
                }
            }
        });
    });
    //console.log("Disabled List:");
    //console.log(disabledList);
    $("form.FormUserConnectedInfo[data-itemid="+ItemId+"] ul.multiselect-container").each(function () {
        var ulContainer = $(this);
        ulContainer.find("input[type=checkbox]").each(function () {
            var theCheckbox = $(this);
            var parentLi = theCheckbox.parents("ul.multiselect-container li");
            theCheckbox.removeAttr('disabled');
            parentLi.removeClass("hideThis");
            if(theCheckbox.is(':checked')){

            }else{
                if($.inArray(theCheckbox.val(), disabledList) !== -1){
                    theCheckbox.attr('disabled', true);
                    parentLi.addClass("hideThis");
                }
            }
        })
    });
}
function get_multi_select_values(obj){
    var theMultiSelect = $(obj);
    var ContactId = theMultiSelect.attr('data-contactid');
    var ItemId = theMultiSelect.parents("form.FormUserConnectedInfo").attr('data-itemid');
    var GroupId = theMultiSelect.parents("form.FormUserConnectedInfo").attr('data-guid');
    var theValues = $('option:selected', theMultiSelect).map(function(a, item){return item.value;});
    var theTargetConnectedData = $("tr[data-itemid="+ItemId+"]");
    var currentConnectedContacts = theTargetConnectedData.attr("data-connectedcontacts");
    currentConnectedContacts = JSON.parse(currentConnectedContacts.b64decode());
    delete theValues.context;
    delete theValues.length;
    delete theValues.prevObject;
    theValues = Object.keys(theValues).map(function (key) { return theValues[key]; });
    currentConnectedContacts[ContactId]['relationships'] = theValues;
    newcurrentConnectedContacts = (JSON.stringify(currentConnectedContacts)).b64encode();
    theTargetConnectedData.attr("data-connectedcontacts",newcurrentConnectedContacts);
    checkItemsToDisable(GroupId,ItemId);


}
function renderAccordion(theTarget,theDialog){
    var theTable = $( "ul#"+theTarget+"Table" );
    theTable
        .sortable({
            axis: "y",
            handle: "div.field-item",
            stop: function( event, ui ) {

            }
        });
    theTable
        ._once('click',"div.field-item", function (e) {
            //console.log(e);
            var theParentUL = $(this).parents("ul.ui-sortable");
            $("li.field-item div.field-item", theParentUL).removeClass('active');
            if(typeof e !== "undefined"){
                if($(e.target).is('i')){
                    e.preventDefault();
                    return false;
                }else{
                    $(this).addClass('active')
                }
            }

            var theFieldDetails = $(this).parents("li.field-item").find("ul."+theTarget+"FieldDetails");
            if(theFieldDetails.is(":visible")){
                theFieldDetails.slideToggle();
            }else{
                if($("ul."+theTarget+"FieldDetails:visible", theTable).length > 0){
                    $("ul."+theTarget+"FieldDetails:visible", theTable).slideUp();
                    var delay = setTimeout(function () {
                        theFieldDetails.slideToggle(1000, function (e) {
                            $("input[name=fieldLabel]", theFieldDetails).focus();
                        });
                    },10);
                }else{
                    var delay = setTimeout(function () {
                        theFieldDetails.slideToggle(1000, function (e) {
                            $("input[name=fieldLabel]", theFieldDetails).focus();
                        });
                    },10);
                }
            }



        })
        ._once('click',"i.AddFieldItem",function () {
            var theButton = $(this);
            var theLi = theButton.parents("li.field-item");
            var theFieldSet = $("li.field-item-template");
            var theGUID = $("form.ConnectedInfoList li.active").attr('data-guid');
            var connectorsTable = $( "ul#connectorsTable[data-guid="+theGUID+"]" );
            var fieldId = "field_"+ new Date().getTime().toString(36);
            theFieldSet
                .clone()
                .removeClass("field-item-template")
                .insertAfter(theLi)
                .find("span.cd-field-id").append(fieldId)
                .parents("li.field-item").find("input[name=fieldId]").val(fieldId)
                .parents("li.field-item").find("div.field-item").trigger('click').addClass('field-item-new ' +fieldId)
                .parents("li.field-item").find("select#infusionsoftCustomField").selectpicker('render');
            var delay = setTimeout(function () {
                $([document.documentElement, document.body]).animate({
                    scrollTop: $("div."+fieldId).offset().top
                }, 400);
            }, 1000);
            var fieldCount = $("ul#connectorsTable[data-guid="+theGUID+"] li.field-item").length;
            if(fieldCount <= 1){
                $("ul#connectorsTable[data-guid="+theGUID+"] li.field-item").first().find("i.DeleteFieldItem").hide();
            }else{
                $("i.DeleteFieldItem").show();
            }
            if(fieldCount === 0){
                $("span", theButton).html("Add A Field")
            }else{
                $("span", theButton).html("Add Another Field")
            }
            var connectorsFieldDetails = $("ul.connectorsFieldDetails", connectorsTable);
            connectorsFieldDetails
                ._once('keyup','input[name="fieldLabel"]',function(event){
                    var theTitle = $(this).val();
                    var spanTitle = $(this).parents("li.field-item").find("span.field-label .label-title");
                    theTitle = theTitle || 'Please enter Label';
                    spanTitle.html(theTitle);
                })._once("change",'select[name="fieldType"]', function () {
                var theParent = $(this).parents("div.col-lg-7");
                var theType = $(this).val();
                theType = theType === 'Number' ? "Whole Number":theType;
                theType = theType === 'Currency' ? "Decimal Number":theType;
                var spanType = $(this).parents("li.field-item").find("span.field-type");
                spanType.html(theType);
                if(theType === "Select" || theType === "Checkbox" || theType === "Radio"){
                    if($("div.choices-container", theParent).length === 0){
                        var choicesContainer = $("div.choices-container.hidenTemplate");
                        choicesContainer
                            .clone()
                            .removeClass('hidenTemplate')
                            .appendTo(theParent)
                            .find("textarea#fieldChoices").focus();
                    }else{
                        $("div.choices-container", theParent).find("textarea#fieldChoices").focus();
                    }
                }else{
                    $("div.choices-container", theParent).remove();
                }

            });
        })
        ._once('click',"i.DeleteFieldItem", function () {
            var theFieldItem = $(this).parents("li.field-item");

            $(theDialog).appendTo("body");
            $( "#dialog-confirm" ).dialog({
                resizable: false,
                height: "auto",
                width: 500,
                modal: true,
                buttons: {
                    "Yes!": function() {
                        $( this ).dialog( "close" );
                        $( "#dialog-confirm" ).remove();
                        theFieldItem.slideUp('fast', function () {
                            theFieldItem.remove();
                            var fieldCount = $("li.field-item", theTable).length;
                            console.log(fieldCount);
                            if(fieldCount <= 1){
                                //$("li.field-item", connectorsTable).first().find("i.DeleteFieldItem").hide();
                            }

                        });
                    },
                    Cancel: function() {
                        $( this ).dialog( "close" );
                        $( "#dialog-confirm" ).remove();
                    }
                }
            });


        });
}

function bindPreloadingListTR(){
    $("#ConnectOtherContact table.contact-preloading-list tr.ContactItem").once("click", function(){
        console.log("contact-preloading-list TR clicked");
        var clickedTR = $(this);
        var theItem = $("input.search-contact-to-connect").attr("data-itemid");
        var theContactId = clickedTR.attr("data-contactid");
        var theEmail = clickedTR.attr("data-email");
        var theFullName = clickedTR.attr("data-fullname");
        var theLastName = clickedTR.attr("data-lastname");
        var theFirstName = clickedTR.attr("data-firstname");
        var template = $("li.HTML-Template.relationship-list-item");
        var theForm = $("form.FormUserConnectedInfo[data-itemid="+theItem+"]");
        var theGroup = theForm.attr("data-guid");
        var availableRelationship = relationshipRules[theGroup];
        var theList = $("ul.relationship-list", theForm);
        var theTargetConnectedData = $("tr[data-itemid="+theItem+"]");
        var currentConnectedContacts = theTargetConnectedData.attr("data-connectedcontacts");
        currentConnectedContacts = JSON.parse(currentConnectedContacts.b64decode());
        currentConnectedContacts[theContactId] = {ContactId:theContactId,Email:theEmail,FirstName:theFirstName,LastName:theLastName,relationships:[]};
        newcurrentConnectedContacts = (JSON.stringify(currentConnectedContacts)).b64encode();
        theTargetConnectedData.attr("data-connectedcontacts",newcurrentConnectedContacts);
        console.log("inserting to: "+theItem);
        $('#ConnectOtherContact').modal('hide');
        template
            .clone()
            .removeClass("HTML-Template")
            .attr("data-contactid",theContactId)
            .appendTo(theList)
            .find("h3.connected-contact-name").html(theFullName+' <small>'+theEmail+'</small>')
            .parents("li.relationship-list-item")
            .find("i.DeleteConenctedContactItem")
            .attr("data-contactid",theContactId)
            .attr("data-itemid",theItem)
            .on("click", function(){
                var ContactIdTobeDeleted = $(this).attr("data-contactid");
                var tagetItem = $(this).attr("data-itemid");
                var _theTargetConnectedData = $("tr[data-itemid="+tagetItem+"]");
                var _currentConnectedContacts = _theTargetConnectedData.attr("data-connectedcontacts");
                var theParent = $(this).parents("li.relationship-list-item");
                var theConfirm = confirm("Are you sure to remove this connected contact?");
                if(theConfirm) {
                    theParent.slideUp('fast', function(){
                        theParent.remove();
                        _currentConnectedContacts = JSON.parse(_currentConnectedContacts.b64decode());
                        delete _currentConnectedContacts[ContactIdTobeDeleted];
                        _newcurrentConnectedContacts = (JSON.stringify(_currentConnectedContacts)).b64encode();
                        _theTargetConnectedData.attr("data-connectedcontacts",_newcurrentConnectedContacts);
                    })
                }else{
                    return false;
                }
            })
            .parents("li.relationship-list-item")
            .find("select")
            .attr("data-contactid",theContactId)
            .attr("data-email",theEmail)
            .attr("data-firstname",theFirstName)
            .attr("data-lastname",theLastName)
            .attr("id","ConnectedContactRelationships"+theContactId)
            .addClass("ConnectedContactRelationships"+theContactId)
            .find("option").each(function(){
                    var theOption = $(this);
                    var allowed = [];
                    $.each(availableRelationship, function(index, theRules){
                        allowed.push(theRules.Id)
                    })
                    if($.inArray(theOption.attr("value"),allowed) === -1) theOption.remove();

            });
        theList.find("select").each(function () {
            $(this).multiselect({
                //includeSelectAllOption: true
                numberDisplayed: 5,
                maxHeight: 200,
                allSelectedText:false
            });
        });
        relationshipIt();
        checkItemsToDisable(theGroup,theItem)
    });
}
function renderContactsPreloaderDataTable(data){
    console.log("Render Table With Data: ");
    console.log(data);
    var toDataRender = data;
    var theContainer = $("#ConnectOtherContact");
    ContactsPreloaderDataTable.destroy();
    ContactsPreloaderDataTable = $(".contact-preloading-list",theContainer).DataTable({
        //var itemFootnote = '<small class="footnote">Contact Id: '+theContact.Id+'</small>'
        data : data.Column || [],
        "pageLength": 10,
        "paging":   true,
        "lengthChange": false,
        "searching": false,
        "info":     false,
        "createdRow": function ( row, data, index ) {
            $(row).addClass('ContactItem');
            $(row).attr('data-contactid',toDataRender[data[1]].ContactId);
            $(row).attr('data-firstname',toDataRender[data[1]].FirstName);
            $(row).attr('data-lastname',toDataRender[data[1]].LastName);
            $(row).attr('data-fullname',data[0]);
            $(row).attr('data-email',data[1]);
        }
    });
    ContactsPreloaderDataTable.on( 'draw.dt', function () {
        bindPreloadingListTR();
    });

}
function renderAgentsPreloaderDataTable(data){
    console.log("Render Table With Data: ");
    console.log(data);
    var toDataRender = data;
    var theContainer = $("#AddCallCenterAgent");
    AgentsPreloaderDataTable.destroy();
    AgentsPreloaderDataTable = $(".contact-preloading-list",theContainer).DataTable({
        data : data.Column || [],
        "pageLength": 10,
        "paging":   true,
        "lengthChange": false,
        "searching": false,
        "info":     false,
        "createdRow": function ( row, data, index ) {
            $(row).addClass('ContactItem');
            $(row).attr('data-contactid',toDataRender[data[1]].ContactId);
            $(row).attr('data-firstname',toDataRender[data[1]].FirstName);
            $(row).attr('data-lastname',toDataRender[data[1]].LastName);
            $(row).attr('data-fullname',data[0]);
            $(row).attr('data-email',data[1]);
        }
    });
    AgentsPreloaderDataTable.on( 'draw.dt', function () {
        bindPreloadingAgentListTR();
    });

}
function makeDataTable(target) {
    $(target).DataTable( {
        "paging":   true,
        "search": true,
        "info":     false,
        "destroy": true,
        "createdRow": function ( row, data, index ) {
            //$(row).attr('data-contactid',data[3]);
        }
    } );
}
function makeCDListDataTable(target) {
    $(target).DataTable( {
        "order": [[ 0, "desc" ]],
        "paging":   false,
        "searching":   false,
        "destroy": true,
        "info":     false,
        "createdRow": function ( row, data, index ) {
            //$(row).attr('data-contactid',data[3]);
        }
    } );
}
function PhoneNumberValidation_Interactive_Validate_v2_20(Key, Phone, Country) {
    $.getJSON("https://api.addressy.com/PhoneNumberValidation/Interactive/Validate/v2.20/json3.ws?callback=?",
        {
            Key: Key,
            Phone: Phone,
            Country: Country
        },
        function (data) {
            // Test for an error
            if (data.Items.length == 1 && typeof(data.Items[0].Error) != "undefined") {
                // Show the error message
                alert(data.Items[0].Description);
            }
            else {
                // Check if there were any items found
                if (data.Items.length == 0)
                    alert("Sorry, there were no results");
                else {
                    // PUT YOUR CODE HERE
                    //FYI: The output is a JS object (e.g. data.Items[0].PhoneNumber), the keys being:
                    //PhoneNumber
                    //RequestProcessed
                    //IsValid
                    //NetworkCode
                    //NetworkName
                    //NetworkCountry
                    //NationalFormat
                    //CountryPrefix
                    //NumberType
                }
            }
        });
}
function Capture_Interactive_Find_v1_00(Key, Text, Container, Origin, Countries, Limit, Language) {
    $.getJSON("https://api.addressy.com/Capture/Interactive/Find/v1.00/json3.ws?callback=?",
        {
            Key: Key,
            Text: Text,
            Container: Container,
            Origin: Origin,
            Countries: Countries,
            Limit: Limit,
            Language: Language
        },
        function (data) {
            // Test for an error
            if (data.Items.length == 1 && typeof(data.Items[0].Error) != "undefined") {
                // Show the error message
                alert(data.Items[0].Description);
            }
            else {
                // Check if there were any items found
                if (data.Items.length == 0)
                    alert("Sorry, there were no results");
                else {
                    // PUT YOUR CODE HERE
                    //FYI: The output is a JS object (e.g. data.Items[0].Id), the keys being:
                    //Id
                    //Type
                    //Text
                    //Highlight
                    //Description
                }
            }
        });
}
//Returns the full address details based on the Id.
function Capture_Interactive_Retrieve_v1_10(Key, Id, Field1Format, Field2Format, Field3Format, Field4Format, Field5Format, Field6Format, Field7Format, Field8Format, Field9Format, Field10Format, Field11Format, Field12Format, Field13Format, Field14Format, Field15Format, Field16Format, Field17Format, Field18Format, Field19Format, Field20Format) {
    $.getJSON("https://api.addressy.com/Capture/Interactive/Retrieve/v1.10/json3.ws?callback=?",
        {
            Key: Key,
            Id: Id,
            Field1Format: Field1Format,
            Field2Format: Field2Format,
            Field3Format: Field3Format,
            Field4Format: Field4Format,
            Field5Format: Field5Format,
            Field6Format: Field6Format,
            Field7Format: Field7Format,
            Field8Format: Field8Format,
            Field9Format: Field9Format,
            Field10Format: Field10Format,
            Field11Format: Field11Format,
            Field12Format: Field12Format,
            Field13Format: Field13Format,
            Field14Format: Field14Format,
            Field15Format: Field15Format,
            Field16Format: Field16Format,
            Field17Format: Field17Format,
            Field18Format: Field18Format,
            Field19Format: Field19Format,
            Field20Format: Field20Format
        },
        function (data) {
            // Test for an error
            if (data.Items.length == 1 && typeof(data.Items[0].Error) != "undefined") {
                // Show the error message
                alert(data.Items[0].Description);
            }
            else {
                // Check if there were any items found
                if (data.Items.length == 0)
                    alert("Sorry, there were no results");
                else {
                    // PUT YOUR CODE HERE
                    //FYI: The output is a JS object (e.g. data.Items[0].Id), the keys being:
                    //Id
                    //DomesticId
                    //Language
                    //LanguageAlternatives
                    //Department
                    //Company
                    //SubBuilding
                    //BuildingNumber
                    //BuildingName
                    //SecondaryStreet
                    //Street
                    //Block
                    //Neighbourhood
                    //District
                    //City
                    //Line1
                    //Line2
                    //Line3
                    //Line4
                    //Line5
                    //AdminAreaName
                    //AdminAreaCode
                    //Province
                    //ProvinceName
                    //ProvinceCode
                    //PostalCode
                    //CountryName
                    //CountryIso2
                    //CountryIso3
                    //CountryIsoNumber
                    //SortingNumber1
                    //SortingNumber2
                    //Barcode
                    //POBoxNumber
                    //Label
                    //Type
                    //DataLevel
                    //Field1
                    //Field2
                    //Field3
                    //Field4
                    //Field5
                    //Field6
                    //Field7
                    //Field8
                    //Field9
                    //Field10
                    //Field11
                    //Field12
                    //Field13
                    //Field14
                    //Field15
                    //Field16
                    //Field17
                    //Field18
                    //Field19
                    //Field20
                }
            }
        });
}
//Returns full address details based on search terms.
function Capture_Interactive_RetrieveBatch_v1_00(Key, Text, Container, Origin, Countries, Limit, Language, Field1Format, Field2Format, Field3Format, Field4Format, Field5Format, Field6Format, Field7Format, Field8Format, Field9Format, Field10Format, Field11Format, Field12Format, Field13Format, Field14Format, Field15Format, Field16Format, Field17Format, Field18Format, Field19Format, Field20Format) {
    $.getJSON("https://api.addressy.com/Capture/Interactive/RetrieveBatch/v1.00/json3.ws?callback=?",
        {
            Key: Key,
            Text: Text,
            Container: Container,
            Origin: Origin,
            Countries: Countries,
            Limit: Limit,
            Language: Language,
            Field1Format: Field1Format,
            Field2Format: Field2Format,
            Field3Format: Field3Format,
            Field4Format: Field4Format,
            Field5Format: Field5Format,
            Field6Format: Field6Format,
            Field7Format: Field7Format,
            Field8Format: Field8Format,
            Field9Format: Field9Format,
            Field10Format: Field10Format,
            Field11Format: Field11Format,
            Field12Format: Field12Format,
            Field13Format: Field13Format,
            Field14Format: Field14Format,
            Field15Format: Field15Format,
            Field16Format: Field16Format,
            Field17Format: Field17Format,
            Field18Format: Field18Format,
            Field19Format: Field19Format,
            Field20Format: Field20Format
        },
        function (data) {
            // Test for an error
            if (data.Items.length == 1 && typeof(data.Items[0].Error) != "undefined") {
                // Show the error message
                alert(data.Items[0].Description);
            }
            else {
                // Check if there were any items found
                if (data.Items.length == 0)
                    alert("Sorry, there were no results");
                else {
                    // PUT YOUR CODE HERE
                    //FYI: The output is a JS object (e.g. data.Items[0].Id), the keys being:
                    //Id
                    //DomesticId
                    //Language
                    //LanguageAlternatives
                    //Department
                    //Company
                    //SubBuilding
                    //BuildingNumber
                    //BuildingName
                    //SecondaryStreet
                    //Street
                    //Block
                    //Neighbourhood
                    //District
                    //City
                    //Line1
                    //Line2
                    //Line3
                    //Line4
                    //Line5
                    //AdminAreaName
                    //AdminAreaCode
                    //Province
                    //ProvinceName
                    //ProvinceCode
                    //PostalCode
                    //CountryName
                    //CountryIso2
                    //CountryIso3
                    //CountryIsoNumber
                    //SortingNumber1
                    //SortingNumber2
                    //Barcode
                    //POBoxNumber
                    //Label
                    //Type
                    //DataLevel
                    //Field1
                    //Field2
                    //Field3
                    //Field4
                    //Field5
                    //Field6
                    //Field7
                    //Field8
                    //Field9
                    //Field10
                    //Field11
                    //Field12
                    //Field13
                    //Field14
                    //Field15
                    //Field16
                    //Field17
                    //Field18
                    //Field19
                    //Field20
                }
            }
        });
}

function downloadConnectorCSV(customAjaxUrl)
{
    jQuery('#download-csv').remove();
    var InnerHTML='<div id="download-csv" class="ConnectedInfoContent container-fluid" data-guid="download-csv"><img src="assets/img/screen-loader.gif"/></div>';
    jQuery('.ConnectedInfoSettingsContainer').prepend(InnerHTML);

    var theURL = customAjaxUrl || ajax_url;
    var jsonData = {"controler":"core/tabs","action":"connector_csv_download","session_name":session_name,"assetsVersion":assetsVersion,'Current_Element':jQuery('#CurrentConnectorId').val(),CurrentConnectorDownLoadType:jQuery('#CurrentConnectorDownLoadType').val()};

    $.ajax({
        url: theURL,
        type: "POST",
        data: jsonData,
        success: function(e)
        {
            jQuery('#download-csv').remove();
            var InnerHTML='<div id="download-csv" class="ConnectedInfoContent container-fluid" data-guid="download-csv">'+e+'</div>';
            jQuery('.ConnectedInfoSettingsContainer').prepend(InnerHTML);
        }
    });

}

function CSVFieldsSelectionFun(GUID,GroupIds)
{
    jQuery('#CSVFieldsSelection').modal('toggle');
    jQuery('#CurrentConnectorId').val(GUID);
    jQuery('#CurrentConnectorDownLoadType').val(GroupIds);
    downloadConnectorCSV();
}
function renderConnectedInfoTitle() {
    var HTMLString = '';
    $.each(ConnectedInfoSettings,function (key,theTab) {
        HTMLString += '<li class="form-group-item ui-sortable-handle" data-guid="'+theTab['id']+'">';
        HTMLString += '<div class="bullet-item"><span class="ConnectedInfoListTitle" data-guid="'+theTab['id']+'">'+theTab['title']+'</span>';
        HTMLString += '<button type="button" class="col-xs-1 btn btn-default  removeButton"><i class="fa fa-trash-o" title="Delete Custom Tab"></i></button>';
        HTMLString += '</div></li>';
    });
    $(HTMLString).insertAfter("form.ConnectedInfoList ul.itemContainer div.saved");
    
}
function initAddRelationship() {
    $("input[name=AllRelationship]").once('change', function () {
        var theInput = $(this);
        var theVal = theInput.val();
        var theId = theInput.attr("data-relationid");
        var theParent = theInput.parents("div.RelationshipOptions");
        var template = $(".HTML-Template.relationship-option");
        var theOl = $("ol.connector-relationships", theParent);
        if (theInput.is(":checked")) {
            template
                .clone()
                .appendTo(theOl)
                .removeClass("HTML-Template")
                .attr("data-relationid", theId)
                .find("h3.RelationshipName").html(theVal)
                .parents("li.relationship-option").find("input[type=radio]").attr("name", theId)


        } else {
            $("li.relationship-option[data-relationid=" + theId + "]", theParent).remove();
        }
    });
}
function macanta_generate_key(prefix, length) {
    prefix = prefix || '';
    length = length || 8;
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < length; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return prefix+text;
}
function renderConnectedInfoSettingsContainer() {
    var HTMLString = '';
    var RalationKeyName = {};
    $.each(ConnectorRelationshipArr, function (key,ConnectorRelationshipKeyPairs) {
        RalationKeyName[ConnectorRelationshipKeyPairs['Id']] = ConnectorRelationshipKeyPairs['RelationshipName'];
    });
    $.each(ConnectedInfoSettings,function (key,theTab) {
        console.log(theTab);
        var suffix = theTab['id'].replace(/_/g, "").toUpperCase();
        HTMLString += '<div class="ConnectedInfoContent container-fluid '+theTab['id']+'" data-guid="'+theTab['id']+'">';
        HTMLString += '<div class="row"><div class="col-xs-12 text-right"><a href="javaScript:void(0)" onClick="downloadConnectorCSV()" class="downloadConnectorCSV" title="Download CSV File"></a></div>';
        HTMLString += '<div class="col-md-12 no-pad-left no-pad-right"><div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-pad-left"> <div class="form-group ">';
        HTMLString += '<label class="control-label requiredField" for="ConnectedInfoTitle"> Information Title <span class="asteriskField"> * </span> </label>';
        HTMLString += '<input class="form-control ConnectedInfoTitle" name="ConnectedInfoTitle" placeholder="Type in your Information Title" type="text" value="'+theTab['title']+'"/>';
        HTMLString += '</div></div>';
        HTMLString += '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-pad-left no-pad-right"><div class="form-group ">';
        HTMLString += '<label class="control-label requiredField" for="ConnectedInfoTitle">Item ID Custom Field<span class="asteriskField">  </span> </label>';
        HTMLString += '<select name="ItemIdCustomField" id="'+ macanta_generate_key('infusionsoftCustomField_', 5)+'" class=" selectpicker form-control ItemIdCustomField" data-size="false">';
        HTMLString += '<option value=""> -- Select One --</option>';
        var optgroup = '';
        $.each(CustomFields, function (GroupName , CustomField) {
            optgroup += '<optgroup label="'+GroupName+'">';
            var option = '';
            $.each(CustomField, function(Label, Items){
                var selected = theTab['item_id_custom_field'] === Items['Name'] ? "selected" : ""
                option += '<option data-subtext="Type: '+ Items['DataType']+'" value="'+Items['Name']+'"'+ selected + ' title="<?php echo $Items->Label; ?>">'+ Items['Label']+'</option>';
            });
            optgroup += option;
            optgroup += '</optgroup>';
        });
        HTMLString += optgroup;
        HTMLString += '</select></div></div> <div class="form-group "> <label class="control-label" for="ConnectedInfoContent"> Fields </label>';
        HTMLString += '<ul id="connectorsTable" data-guid="'+theTab['id']+'">';
        var fieldItem = '';
        $.each(theTab['fields'], function (fieldKey,theField) {
            fieldItem += '<li class="field-item"> <div class="field-item"> <i class="fa fa-trash-o DeleteFieldItem" aria-hidden="true"></i> <i class="fa fa-plus-square-o AddFieldItem" aria-hidden="true"></i>';
            fieldItem += '<span class="field-label" title="'+theField['fieldLabel']+'">';
            var fieldLabel = theField['fieldLabel'] !== "" ? theField['fieldLabel'].trunc(25) : "Please Enter Label";
            fieldItem += '<span class="label-title">'+ fieldLabel +'</span>';
            fieldItem += '<span class="cd-field-id">ID: '+theField['fieldId']+'</span>';
            fieldItem += '</span> <span class="field-properties"> Type: <span class="field-type">'+theField['fieldType']+'</span> List order: <span class=" field-list-order">'+theField['showOrder']+'</span> <br>';
            fieldItem += '<span class="align-left"> Custom Field: <span class="is-custom-field" title="'+theField['infusionsoftCustomField']+'">'+theField['infusionsoftCustomField'].trunc(25)+'</span></span></span></div>';
            fieldItem += '<form class="FormFieldDetails"> <input type="hidden" name="fieldId" value="'+theField['fieldId']+'">';
            fieldItem += '<ul class="connectorsFieldDetails"><li class="field-details"> <div class="col-lg-5"> <h3>Label</h3> <small>This is the name which will appear in before the field. </small> </div> <div class="col-lg-7">';
            fieldItem += '<input type="text" id="'+macanta_generate_key('fieldLabel_', 5)+'" name="fieldLabel" class="form-control field-input" value="'+theField['fieldLabel']+'" title="" required="required"> </div></li>';
            fieldItem += '<li class="field-details"> <div class="col-lg-5"> <h3>Type</h3> </div> <div class="col-lg-7">';
            fieldItem += '<select name="fieldType" id="'+macanta_generate_key('fieldType_', 5)+'" class="form-control"> <option value=""> -- Select One --</option>';

            var Text = theField['fieldType'] === "Text" ? "selected" : "";
            var TextArea = theField['fieldType'] === "TextArea" ? "selected" : "";
            var Number = theField['fieldType'] === "Number" ? "selected" : "";
            var Password = theField['fieldType'] === "Password" ? "selected" : "";
            var Email = theField['fieldType'] === "Email" ? "selected" : "";
            var Currency = theField['fieldType'] === "Currency" ? "selected" : "";

            var Select = theField['fieldType'] === "Select" ? "selected" : "";
            var Checkbox = theField['fieldType'] === "Checkbox" ? "selected" : "";
            var Radio = theField['fieldType'] === "Radio" ? "selected" : "";

            var Date = theField['fieldType'] === "Date" ? "selected" : "";
            var URL = theField['fieldType'] === "URL" ? "selected" : "";

            fieldItem += '<optgroup label="Basic"> <option value="Text"'+Text+'>Text</option> <option value="TextArea" '+TextArea+'>Text Area</option> <option value="Number" '+Number+'>Whole Number</option> <option value="Currency" '+Currency+'>Decimal Number</option> <option value="Email" '+Email+'>Email</option><option value="Password" '+Password+'>Password</option></optgroup>';
            fieldItem += '<optgroup label="Choice"><option value="Select" '+Select+'> Select </option> <option value="Checkbox" '+Checkbox+'>Checkbox </option> <option value="Radio" '+Radio+'> Radio Button </option> </optgroup>';
            fieldItem += '<optgroup label="Other"><option value="Date" '+Date+'>Date Picker</option><option value="URL" '+URL+'>URL</option><option value="Repeater" disabled="disabled">Repeater(coming soon)</option></optgroup>';
            var SharedFlag = 0;
            $.each(ConnectedInfoSettings,function (ShKey,SharedConnector) {
                if(key!==ShKey)
                {
                    $.each(SharedConnector['fields'], function(SharedFieldsKey,SharedFields)
                    {
                        if(SharedFields['useAsPrimaryKey']==='yes')
                        {
                            if(SharedFlag === 0) { fieldItem += '<optgroup label="Data Reference">'; }
                            var refFieldType = theField['fieldType'] === SharedFields['fieldId'] ?   "selected":"";
                            fieldItem += '<option  ' + refFieldType + ' value="'+ SharedFields['fieldId']+'">' + SharedConnector['title']+ '('+ SharedFields['fieldLabel']+')</option>';
                            if(SharedFlag===0) { fieldItem += '</optgroup>'; SharedFlag=1; }
                        }
                    });
                }
            });
            fieldItem += '</select>';
            if (theField['fieldType'] === "Select" || theField['fieldType'] === "Checkbox" || theField['fieldType'] === "Radio") {
                fieldItem += '<div class="choices-container"> <h4 class="choices-label">Enter Choices <small> - seperated by new line </small> </h4> <textarea name="fieldChoices" id="'+macanta_generate_key('fieldChoices_', 5)+'" class="form-control">'+theField['fieldChoices']+'</textarea> </div>';
            }
            fieldItem += '</div> </li> <li class="field-details"> <div class="col-lg-5"> <h3>Data Reference?</h3> </div> <div class="col-lg-7">';
            if(typeof theField['useAsPrimaryKey'] !== "undefined"){
                var useAsPrimaryKeyNo =  theField['useAsPrimaryKey'] === 'no' ? "checked":"";
                var useAsPrimaryKeyYes = theField['useAsPrimaryKey'] === 'yes' ? "checked":"";
            }
            fieldItem += '<label> <input type="radio" id="'+macanta_generate_key('useAsPrimaryKey_', 5)+'" name="useAsPrimaryKey" value="no" '+useAsPrimaryKeyNo+'> No </label>';
            fieldItem += '<label> <input type="radio" id="'+macanta_generate_key('useAsPrimaryKey_', 5)+'" name="useAsPrimaryKey" value="yes" '+useAsPrimaryKeyYes+'> Yes </label>';
            fieldItem += '</div> </li> <li class="field-details"> <div class="col-lg-5"> <h3>Required?</h3> </div> <div class="col-lg-7">';
            var requiredFieldNo = theField['requiredField'] === "no" ? "checked" : "";
            var requiredFieldYes = theField['requiredField'] === "yes" ? "checked" : "";
            fieldItem += '<label> <input type="radio" id="'+macanta_generate_key('requiredField_', 5)+'" name="requiredField" value="no" '+requiredFieldNo+'> No </label>';
            fieldItem += '<label> <input type="radio" id="'+macanta_generate_key('requiredField_', 5)+'" name="requiredField" value="yes" '+requiredFieldYes+'> Yes </label>';
            fieldItem += '</div> </li> <li class="field-details"> <div class="col-lg-5"> <h3>Contact Specific?</h3> </div> <div class="col-lg-7">';

            if(typeof theField['contactspecificField'] !== "undefined"){
                var contactspecificFieldyNo =  theField['contactspecificField'] === 'no' ? "checked":"";
                var contactspecificFieldYes = theField['contactspecificField'] === 'yes' ? "checked":"";
            }

            fieldItem += '<label> <input type="radio" id="'+macanta_generate_key('contactspecificField_', 5)+'" name="contactspecificField" value="no" '+contactspecificFieldyNo+'> No </label>';
            fieldItem += '<label> <input type="radio" id="'+macanta_generate_key('contactspecificField_', 5)+'" name="contactspecificField" value="yes" '+contactspecificFieldYes+'> Yes </label>';
            fieldItem += '</div> </li> <li class="field-details"> <div class="col-lg-5"> <h3>Default Value</h3> <small>Predefined value for this field</small> </div> <div class="col-lg-7">';
            fieldItem += '<input type="text" id="'+macanta_generate_key('defaultValue_', 5)+'" name="defaultValue" class="form-control  field-input" value="'+theField['defaultValue']+'" title="" required="required">';
            fieldItem += '</div> </li> <li class="field-details"> <div class="col-lg-5"> <h3>Place Holder Text</h3> <small>Text to show when the field is blank </small> </div> <div class="col-lg-7">';
            fieldItem += '<input type="text" id="'+macanta_generate_key('placeHolder_', 5)+'" name="placeHolder" class="form-control field-input" value="'+theField['placeHolder']+'" title="" required="required">';
            fieldItem += '</div> </li> <li class="field-details"> <div class="col-lg-5"> <h3>Helper Text</h3> <small>Some information regarding this field if needed. </small> </div> <div class="col-lg-7">';
            fieldItem += '<input type="text" id="'+macanta_generate_key('helperText_', 5)+'" name="helperText" class="form-control field-input" value="'+ theField['helperText']+'" title="" required="required">';
            fieldItem += '</div> </li> <li class="field-details"> <div class="col-lg-5"> <h3>Infusionsoft Custom Field</h3> <small>Field to be updated in Infusionsoft </small> </div> <div class="col-lg-7">';
            fieldItem += '<select name="infusionsoftCustomField" id="'+macanta_generate_key('infusionsoftCustomField_', 5)+'" class=" selectpicker form-control" data-size="false"><option value=""> -- Select One --</option>';

            $.each(CustomFields, function (GroupName ,CustomField) {
                fieldItem += '<optgroup label="'+GroupName+'">';
                $.each(CustomField, function (Label ,Items) {
                    var infusionsoftCustomField = theField['infusionsoftCustomField'] === Items['Name'] ? "selected" : "";
                    fieldItem += '<option data-subtext="Type: '+Items['DataType']+'" value="'+Items['Name']+'" '+ infusionsoftCustomField +' title="'+Items['Label']+'">'+Items['Label']+'</option>';
                });
                fieldItem += '</optgroup>';
            });
            var showInTableNo = theField['showInTable'] === "no" ? "checked" : "";
            var showInTableYes = theField['showInTable'] === "yes" ? "checked" : "";
            fieldItem += '</select> </div> </li> <li class="field-details"> <div class="col-lg-5"> <h3>Use As List Header</h3> </div> <div class="col-lg-7">';
            fieldItem += '<label><input type="radio" name="showInTable" value="no" '+showInTableNo+'> No </label>';
            fieldItem += '<label><input type="radio" name="showInTable" value="yes" '+showInTableYes+'> Yes </label>';
            fieldItem += '<label><input type="number" name="showOrder" value="'+theField['showOrder']+'" class="ordernumber"> Order </label>';
            fieldItem += '</div></li> <li class="field-details"> <div class="col-lg-5"> <h3>Section Tag ID</h3> <small>Group Fields by Tag Id.</small> </div> <div class="col-lg-7">';
            var sectionTagIdValue = theField['sectionTagId'] || '';
            fieldItem += '<input type="text" id="'+macanta_generate_key('sectionTagId_', 5)+'" name="sectionTagId" class="form-control field-input" value="'+ sectionTagIdValue + '" title="" > </div>';
            fieldItem += '</li> </ul> </form> </li>';

        });
        HTMLString += fieldItem;
        HTMLString += '</ul> </div> <div class="form-group RelationshipOptions"> <label class="control-label control-label-fullwidth"> Relationship Options </label> <div class="col-md-4 no-pad-left"> <h3 class="col-lg-12 no-pad-left no-pad-right no-margin">All Relationships:</h3> <div class="connector-all-relationships-container"> <ol class="connector-all-relationships">';
        var RelationshipItem = '';
        $.each(ConnectorRelationshipArr, function (key,Relationship) {
            var checked = '';
            $.each(theTab['relationships'], function(relationshipKey,relationship){
                if (relationship['Id'] === Relationship['Id']) {
                    checked = 'checked';
                    return false;
                }
            });
            RelationshipItem += '<li class="field-details" title="'+Relationship['RelationshipDescription']+'">' +
                '<div class="checkbox"> <label> <input type="checkbox" name="AllRelationship" data-relationid="'+Relationship['Id']+'" value="'+Relationship['RelationshipName']+'" '+checked+'><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span> <span class="checkbox-label">'+Relationship['RelationshipName']+'</span> </label> </div> </li>'
        });

        HTMLString += RelationshipItem;
        HTMLString += '</div></div> <div class="col-md-8 no-pad-left"> <h3 class="col-lg-12 no-pad-left no-pad-right no-margin">Available Relationships:</h3> <div class="connector-relationships-container"> <div class="connector-relationships-title col-lg-6 no-pad-left no-pad-right"> Name </div> <div class="connector-relationships-title col-lg-6 no-pad-left no-pad-right"> Can have multiple items? <small>Leave limit blank for unlimited</small> </div> <form class="FormRelationship"> <ol class="connector-relationships">';
        var TheTabRelationshipsItems = '';
        $.each(theTab['relationships'], function (relationshipKey, relationship){
            if (relationship['Id'] === "") return true;
            if (typeof RalationKeyName[relationship['Id']] === "undefined")  return true;
            var exclusiveYes = relationship['exclusive'] === 'yes' ? 'checked' : '';
            var exclusiveNo = relationship['exclusive'] === 'no' ? 'checked' : '';
            var exclusiveLimit = relationship['exclusive'] === 'yes' ? 'disabled' : '';
            TheTabRelationshipsItems += '<li class="relationship-option field-details" data-relationid="'+relationship['Id']+'"> <div class="col-lg-5 no-pad-left"> <h3 class="RelationshipName">'+RalationKeyName[relationship['Id']]+'</h3> </div> <div class="col-lg-7 no-pad-left no-pad-right">';
            TheTabRelationshipsItems += '<label><input class="multiple-options" type="radio" name="'+relationship['Id']+'" value="yes" '+exclusiveYes+'> No </label>';
            TheTabRelationshipsItems += '<label><input class="multiple-options" type="radio" name="'+relationship['Id']+'" value="no" '+exclusiveNo+'> Yes </label>';
            TheTabRelationshipsItems += '<label>Limit <input maxlength="3" max="999" min="0" type="number" name="MultipleLimit" value="'+relationship['limit']+'" class="form-control multiple_limit" '+exclusiveLimit+'> </label>';
            TheTabRelationshipsItems += '</div></li>';
        });

        HTMLString += TheTabRelationshipsItems;
        HTMLString += '</ol></form></div></div></div></div></div><form>';
        var visibility = theTab['visibility'] || "ShowCDToAll";
        var ShowCDToAll = visibility === "ShowCDToAll" ? 'checked':'';
        var ShowCDToAllUserSpecific = visibility === "ShowCDToAllUserSpecific" ? 'checked':'';
        HTMLString += '<div class="form-group checkboxGroup"><label class="control-label" for="CDALL'+suffix+'"> Show this Connected Data Tab all the time, with no restriction by contact or user </label> <input type="radio"  data-group="'+theTab['id']+'" id="CDALL'+suffix+'" name="connectedDataVisibility" class="form-control  connectedDataVisibility" value="ShowCDToAll"  '+ ShowCDToAll + ' /> </div>';
        HTMLString += '<div class="form-group checkboxGroup"><label class="control-label " for="CDSOMEUSER'+suffix+'"> Show this Connected Data Tab for all contacts, but only to specific users </label> <input type="radio"  data-group="'+theTab['id']+'" id="CDSOMEUSER'+suffix+'" name="connectedDataVisibility" class="form-control  connectedDataVisibility"  value="ShowCDToAllUserSpecific"  '+ ShowCDToAllUserSpecific + '/></div>';
        HTMLString += '</form></div>';
        HTMLString += '<script>initFieldItem("'+theTab['id']+'");initFieldBehavior("'+theTab['id']+'");initConnectedInfoTitle("'+theTab['id']+'");</script>';
    });
    $("div.ConnectedInfoSettingsContainer").prepend(HTMLString);
    initConnectedInfoListSortable();
    initConnectedInfoList();
}