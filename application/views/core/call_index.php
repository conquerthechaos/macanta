<div class="col-lg-12 theNotePanel">
    <div class="row mainbox-top">
        <div class="col-md-6 tab-panel">
            <div class="panel panel-primary callNotes PreviousCallNotes">
                <div class="panel-heading otherNotes">
                    <h3 class="panel-title NoteName"><i class="fa fa-sticky-note"></i> <?php echo $this->lang->line('text_previous_calls');?></h3>
                </div>
                <div class="panel-body prev-notes"><?php echo $PrevNotes; ?></div>
            </div>

        </div>
        <div class="col-md-6">
            <div class="panel panel-primary CurrentCallNotes callNotes">
                <div class="panel-heading otherNotes">
                    <h3 class="panel-title NoteName"><i class="fa fa-sticky-note-o"></i> <?php echo $this->lang->line('text_current_call');?></h3>
                        <a href="" target="_self" class="" title="" tabindex=""></a>
                </div>
                <div class="panel-body call-notes-panel">
                    <h3><?php echo $this->lang->line('text_call_note');?>:</h3>
                    <textarea name="" id="callNotes" class="call-notes"
                              placeholder="<?php echo $this->lang->line('text_type_your_note_here');?>"></textarea>
                    <input id='current_call_tags' type='text' class='current_call_tags' value='' />
                    <div class="note-action">
                        <button class="btn btn-default btn-md SaveNote" disabled> <?php echo $this->lang->line('text_create_note');?></button>
                    </div>
                </div>
            </div>



        </div>
    </div>
    <div class="row">
    </div>
</div>