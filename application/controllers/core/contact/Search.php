<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 07/03/16
 * Time: 3:00 PM
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Search extends MY_Controller
{

    private $Contact;
    public function __construct($Prams,$session_data){
        parent::__construct();
        $this->Contact = $Prams;
        //$this->load->database(); this is already loaded in My_Controller
        //$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file')); this is already loaded in My_Controller

    }
    public function index($Items,$session_name,$session_data)
    {
        $this->Contact->session_data = $session_data;
        $HTML = $this->load->view('core/contact_search', $this->Contact, true);
        return $HTML;
    }


}
