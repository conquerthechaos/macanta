**Version Info**

* DIY or Hosted (it's hosted if your macanta URL ends with *.macanta.org*): 
* Version (bottom of the screen): 
* Infusionsoft app name: 

---

### What steps reproduce the issue?

1. 
2. 
3. 

### Expected Result

* 

### Actual Result

* 

***Please click 'Attach a file' (bottom right of this edit window) to add screenshots***
