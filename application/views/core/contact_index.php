<div class="<?php echo $Section['class']?> col-xs-12 col-sm-12 col-md-12 col-lg-12  no-pad-left no-pad-right">
    <div class="form-top header col-xs-12 col-sm-12 col-md-12 col-lg-12">
<?php
foreach($Sections as $Section){
    $HTML = implode("\r\n",$Section['content']);
    $BlockOrder = ['contact_info'];
    foreach($Section['content'] as $Method => $HTML){
        echo "<!--".strtoupper($Method)." BLOCK -->";
        echo $HTML;
    }
}
?>
    </div>
</div>
