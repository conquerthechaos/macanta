<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class InfusionsoftHooks
{
    public $CI;
    public $AvailableHooks = [
        "appointment.add",
        "appointment.delete",
        "appointment.edit",
        "company.add",
        "company.delete",
        "company.edit",
        "contact.add",
        "contact.delete",
        "contact.edit",
        "contact.redact",
        "contactGroup.add",
        "contactGroup.applied",
        "contactGroup.delete",
        "contactGroup.edit",
        "contactGroup.removed",
        "invoice.add",
        "invoice.delete",
        "invoice.edit",
        "invoice.payment.add",
        "invoice.payment.delete",
        "invoice.payment.edit",
        "leadsource.add",
        "leadsource.delete",
        "leadsource.edit",
        "note.add",
        "note.delete",
        "note.edit",
        "opportunity.add",
        "opportunity.delete",
        "opportunity.edit",
        "opportunity.stage_move",
        "order.add",
        "order.delete",
        "order.edit",
        "product.add",
        "product.delete",
        "product.edit",
        "subscription.add",
        "subscription.delete",
        "subscription.edit",
        "task.add",
        "task.delete",
        "task.edit",
        "user.activate",
        "user.add",
        "user.edit"
    ];
    public $TableMap = [
        "Company" => "InfusionsoftCompany",
        "Contact" => "InfusionsoftContact",
        "ContactAction" => "InfusionsoftContactAction",
        "ContactGroupAssign" => "InfusionsoftContactGroupAssign",
        "ContactGroup" => "InfusionsoftContactGroup",
        "ContactGroupCategory" => "InfusionsoftContactGroupCategory",
        "DataFormField" => "InfusionsoftDataFormField",
        "DataFormGroup" => "InfusionsoftDataFormGroup",
        "DataFormTab" => "InfusionsoftDataFormTab",
        "EmailAddStatus" => "InfusionsoftEmailAddStatus",
        "FileBox" => "InfusionsoftFileBox",
        "Invoice" => "InfusionsoftInvoice",
        "Lead" => "InfusionsoftLead",
        "LeadSource" => "InfusionsoftLeadSource",
        "SavedFilter" => "InfusionsoftSavedFilter",
        "Stage" => "InfusionsoftStage",
        "Template" => "InfusionsoftTemplate",
        "User" => "InfusionsoftUser"


    ];
    public $FormIdsArr = [
        'Contact' => -1,
        'Opportunity' => -4,
        'Company' => -6,
        'ContactAction' => -5
    ];
    public function __construct($CI)
    {
        $this->CI = $CI;
    }
    public function contact_delete($data){
        //{"event_key":"contact.delete","object_type":"contact","object_keys":[{"apiUrl":"","id":2611,"timestamp":"2018-11-09T04:34:38Z"}],"api_url":""}
        $ContactId = $data['object_keys']['id'];
    }
    public function contact_edit($data){
        //{"event_key":"contact.edit","object_type":"contact","object_keys":[{"apiUrl":"","id":455,"timestamp":"2018-11-09T04:29:59Z"}],"api_url":""}
        $ContactId = $data['object_keys'][0]['id'];
        $EditedContact = $this->_get_infusionsoft_record($ContactId,'Contact');
        if(isset($EditedContact[0]->Id)){
            $result = $this->_update_create_local_record('Contact',$EditedContact[0]);
        }
        return ['record'=>$EditedContact,'result'=>$result];
    }
    public function contact_add($data){
        //{"event_key":"contact.add","object_type":"contact","object_keys":[{"apiUrl":"","id":4445,"timestamp":"2018-11-09T04:38:49Z"}],"api_url":""}
        $ContactId = $data['object_keys'][0]['id'];
        $AddedContact = $this->_get_infusionsoft_record($ContactId,'Contact');
        if(isset($EditedContact[0]->Id)){
            $result = $this->_update_create_local_record('Contact',$AddedContact[0]);
        }
        return ['record'=>$AddedContact,'result'=>$result];
    }
    public function contact_redact($data){}

    public function company_add($data){}
    public function company_delete($data){}
    public function company_edit($data){}

    public function contactGroup_add($data){}
    public function contactGroup_applied($data){}
    public function contactGroup_delete($data){}
    public function contactGroup_edit($data){}
    public function contactGroup_removed($data){}

    public function note_add($data){}
    public function note_delete($data){}
    public function note_edit($data){}

    public function task_add($data){}
    public function task_delete($data){}
    public function task_edit($data){}

    public function appointment_add($data){}
    public function appointment_delete($data){}
    public function appointment_edit($data){}

    public function opportunity_add($data){}
    public function opportunity_delete($data){}
    public function opportunity_edit($data){}
    public function opportunity_stage_move($data){}

    public function leadsource_add($data){}
    public function leadsource_delete($data){}
    public function leadsource_edit($data){}

    public function invoice_add($data){}
    public function invoice_delete($data){}
    public function invoice_edit($data){}
    public function invoice_payment_add($data){}
    public function invoice_payment_delete($data){}
    public function invoice_payment_edit($data){}

    public function user_activate($data){}
    public function user_add($data){}
    public function user_edit($data){}

    public function _get_infusionsoft_record($Id,$Table, $page = 0)
    {
        $TableFields = $this->CI->db->list_fields($this->TableMap[$Table]);
        // For tables that has data originated from macanta
        $theKey = array_search('Origin', $TableFields);
        if ($theKey !== false) unset($TableFields[$theKey]);
        //For Filebox Table
        $theKey = array_search('FileData', $TableFields);
        if ($theKey !== false) unset($TableFields[$theKey]);

        //For Contact Table
        $theKey = array_search('CustomField', $TableFields);
        if ($theKey !== false) unset($TableFields[$theKey]);

        //For Template Table
        $theKey = array_search('PieceContent', $TableFields);
        if ($theKey !== false) unset($TableFields[$theKey]);

        //For Template Table
        $theKey = array_search('IdLocal', $TableFields);
        if ($theKey !== false) unset($TableFields[$theKey]);

        $action = "query_is";
        $query = '{"Id":"'.$Id.'"}';
        $FormIdsArr = [
            'Contact' => -1,
            'Opportunity' => -4,
            'Company' => -6,
            'ContactAction' => -5
        ];
        if ($Table == 'Template') {
            $query = '{"Id":"'.$Id.'","PieceTitle":"~<>~null","Categories":"%"}';
        }
        if ($Table == 'FileBox') {
            $query = '{"Id":"'.$Id.'","ContactId":"~<>~0"}';
        }
        if ($Table == 'ContactGroupAssign') {
            $query = '{"GroupId":"'.$Id.'"}';
            $this->CI->db->order_by('DateCreated', 'DESC');
            $this->CI->db->limit(1);
            $GroupAssign = $this->CI->db->get($this->TableMap[$Table])->row();
            if (isset($GroupAssign)) {
                $LastDateCreatedJson = $GroupAssign->DateCreated;
                $LastDateCreated = json_decode($LastDateCreatedJson);
                $query = '{"DateCreated":"~>~ '.$LastDateCreated->date.'"}';
            }
        }
        $returnFields = [];
        foreach ($TableFields as $returnField) {
            $returnFields[] = '"' . $returnField . '"';
        }

        if (array_key_exists($Table, $FormIdsArr)) {
            //Lets Add Custom Fields
            $CFObjs = infusionsoft_get_custom_fields("%", true, $FormIdsArr[$Table]);
            foreach ($CFObjs as $CFObj) {
                $returnFields[] = '"_' . $CFObj->Name . '"';
            }
        }
        $returnFieldsStr = implode(', ', $returnFields);

        $action_details = '{"table":"' . $Table . '","limit":"1000","page":' . $page . ',"fields":[' . $returnFieldsStr . '],"query":' . $query . '}';
        file_put_contents(dirname(__FILE__)."/log_".$this->CI->config->item('IS_App_Name').".txt",$action_details);
        $temp = applyFn('rucksack_request', $action, $action_details, false);
        $Records = isset($temp->message) ? $temp->message:[];
        return $Records;
    }
    public function _update_create_local_record($Table,$Record){
        $CustomFields = [];
        foreach ($Record as $FieldName => $FieldValue) {
            if ($FieldName[0] == "_") {
                $CustomFields[$FieldName] = html_entity_decode($FieldValue);
                $CustomFields[$FieldName] = isJson($CustomFields[$FieldName]) ? json_decode($CustomFields[$FieldName]) : $CustomFields[$FieldName];
                unset($Record->$FieldName);
            }
        }
        // make sure table has custom field column
        $TableFields = $this->CI->db->list_fields($this->TableMap[$Table]);
        if (in_array('CustomField', $TableFields)) $Record->CustomField = json_encode($CustomFields);
        if (in_array('IdLocal', $TableFields)) $Record->IdLocal = $Record->Id;
        if(macanta_db_record_exist('Id',$Record->Id,$this->TableMap[$Table]) || macanta_db_record_exist('Email',$Record->Email,$this->TableMap[$Table])){
            $this->CI->db->where('Id', $Record->Id);
            $result = $this->CI->db->update($this->TableMap[$Table], $Record);
            return ['action' => 'update','result' => $result,'sql_log'=>$this->CI->db->last_query()];
        }else{
            $result = $this->CI->db->insert($this->TableMap[$Table], $Record);
            return ['action' => 'insert','status' => $result,'sql_log'=>$this->CI->db->last_query()];
        }

    }
}