<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 09/03/16
 * Time: 2:28 PM
 */
/*GET rucksack request
    $api_url='https://rucksack.entrepreneurscircle.org/post.php'
*/
if (!function_exists('rucksack_request'))
{
    function rucksack_request($action, $action_details){

        switch ($action){
            case "addcon_is":
                $action = "addContact";
                break;
            case "apply_tag":
                $action = "applyTag";
                break;
            case "create_is":
                $action = "createRecord";
                break;
            case "update_is":
                $action = "updateRecord";
                break;
            case "query_is":
                $action = "readRecord";
                break;
            case "delete_is":
                $action = "deleteRecord";
                break;
            case "email_is":
                $action = "sendEmail";
                break;
            case "email_template_is":
                $action = "getEmailTemplate";
                break;
            case "saved_search_columns_is":
                $action = "getAllReportColumns";
                break;
            case "saved_search_report_is":
                $action = "getSavedSearchResultsAllFields";
                break;
            case "setting_is":
                $action = "getAppSetting";
                break;
            case "webform_html_is":
                $action = "getFormsHTML";
                break;
            case "webform_map_is":
                $action = "getWebFormMap";
                break;
            default:
                break;

        }
        //$CI =& get_instance();
        //$CI->load->library('rucksack/rucksack');
        require_once (APPPATH."libraries/rucksack/Rucksack.php");
        $Rucksack = new Rucksack();
        $return = new stdClass();
        try {
            $result = $Rucksack->Call($action,$action_details);
            $return = json_decode(trim($result['Data']));
        } catch (Exception $e) {
            $return->message = "Error: ".$e->getMessage();
        }
        return $return;
    }
}