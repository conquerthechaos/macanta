# What is macanta? 

macanta is an open source (free/libre), multi-lingual 're-skin' for Infusionsoft, and other 'mission critical' systems in your business. 

It's a platform and framework that allows you to create your own unique productivity-boosting interface to all your important contact data, regardless of where the data lives.

# Why has macanta been built? 

Ever since he started using and consulting in Infusionsoft back in 2009, Peter Daly-Dickson, the Chief Instigator of macanta, has been developing custom interfaces for his clients - they wanted the power of Infusionsoft without the complexity of its interface.

For a lot of every day, day-to-day, contact-centric tasks, Infusionsoft's multi-menu, multi-click interface is often more of a hindrance than a help to productivity.

Also, Infusionsoft may be only one of several mission critical applications in your business - and macanta can give you a unified view and user interface for you and your team to get the important things done with your contacts, regardless of the underlying system.

In other words, train your staff on one system, that's been designed for ease-of-use, not several.

# How much does it cost? 

macanta starts at $0 for the Do-It-Yourself version of the open source software, to only $15/£15pm for...

* macanta installed on a dedicated, security-hardended and optimised server
* lightning fast connectivty to your Infusionosft app
* hourly backups retained for three months and tested daily for integrity
* regular security and app updates
* unlimited email support with a guaranteed maximum 24hr response time

# What's in it for you?

## Reduce learning curve 

Reduce the learning curve for new members of staff. Infusionsoft is complex. There's a lot of menus. 

A new member of staff can be put in front of macanta and be instantly productive - doing the right things, to the right contacts. 

## In-browser phone calls & call recording 

Find a contact. Call them. From your browser. Simples!  And all phone calls made to your contacts via macanta are automatically recorded and saved with the contact record. 

Not only that... macanta's improvements to Infusionsoft's 'contact notes' functionality means you can even extend your search into the content of a phone call as all calls can be automatically transcribed! 

## Internal forms on steroids! 

The smart Custom Workflow Tabs (CWT), each containing one or more 'internal' forms, inside macanta are the real power behind enabling you to create your own, unique interface to Infusionsoft. 

Here's the thing... internal forms in Infusionsoft are available to all users, all the time. And, particularly if you're a new user, they can be difficult to find. 

Custom Workflow Tabs in macanta can be quickly and easily configured to display or hide depending on the tags of the contact and the macanta user. e.g. you could setup an 'engagement call' CWT that only appears every 3 months when a campaign sets the appropriate tag, and it only appears for the 3 macanta users who make engagement calls. i.e. when the 'Engagement Call' tab is visible, you gotta make an engagement call! 

## Important saved searches in one place 

This one's better experienced than explained!.

You can save searches and reports in a LOT of places in Infusionsoft. With macanta, you'll save time with quick and easy access to the results of your most important saved searches in one place.

## Multi-lingual & Open Source 

If your and/or your team's native language is not English, macanta's out-of-the-box multi-lingual support is probably a big enough reason on it's own to macanta-ize your business! 

And if you don't want the benefit of a quick and easy, one-click install onto a security-hardened, optimized server that's down the road from Infusionsoft's servers, macanta is open source and will always be free. 

## Reduce Infusionsoft user licenses 

With your staff and all your outsourced business support personnel, such as telemarketers, call handling service etc., accessing and using Infusionsoft via macanta, you can reduce additional costly monthly user licenses. 

# Browser Testing #

![browserstack-header-logo_1](https://macanta.org/wp-HpzCzf/wp-content/uploads/2018/04/browserstack-header-logo-1.png)

Testing stack for mutiple browsers and operating systems generously donated by [www.browserstack.com](https://www.browserstack.com)