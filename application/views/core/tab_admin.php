<div class="col-xs-12 no-pad-left no-pad-right tab-menu-admin"> <!-- required for floating -->
    <!-- Nav tabs -->
    <ul class="nav nav-tabs">
        <?php
        $replaceArr = array(' ','#','!','@','$','%','&','*','(',')','?',',','.','/','\\',':',';','"',"'",'[',']','-','=','+','|','{','}','<','>');
        foreach($menu as $MenuId => $MenuName){ $MenuId = str_replace($replaceArr,'_',$MenuId); ?>

            <li class="tab<?php echo $MenuId;?> <?php echo $status[$MenuId]? $status[$MenuId]:"";?>"><a href="#<?php echo $MenuId;?>" data-toggle="tab"> <?php echo $MenuName;?></a></li>
        <?php }
        ?>

    </ul>
</div>
<div class="col-xs-12 no-pad-left no-pad-right  tab-body">
    <!-- Tab panes -->
    <div class="tab-content">
        <?php
        foreach($menu as $MenuId => $MenuName ){  $MenuId = str_replace($replaceArr,'_',$MenuId); ?>
        <div class="tab-pane   <?php echo $status[$MenuId]? $status[$MenuId]:"";?>" id="<?php echo $MenuId;?>"> <?php echo$content[$MenuId];?></div>        <?php }
    ?>

</div>
</div>