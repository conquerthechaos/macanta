<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 09/03/16
 * Time: 2:28 PM
 */
/*Update Infusionsoft Notes*/
if (!function_exists('infusionsoft_update_note')) {
    function infusionsoft_update_note($data, $UserInfo)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $noteId = $data['noteid'];
        $NoteType = $data['notesType'];
        $Notes = $data['notes'];
        $action = "update_is";
        $NoteTitle = $data['NoteTitle'];
        $ActionType = $data['ActionType'];
        $ActionDescription = $NoteTitle;
        //$ActionDescription = $NoteType == 'call_notes' ? "macanta Call Notes":"macanta Quick Notes";
        //$data['macantaNotes'] =  json_encode(json_decode($data['macantaNotes']));

        if ($NoteType != 'Legacy notes') {
            if ($CI->config->item('NoteCustomFields')) {
                $NoteCustomFields = json_decode($CI->config->item('NoteCustomFields'), true);
                $CF_RecordingURL = '_' . $NoteCustomFields['Call Recording URL'];
                $CF_MacantaNotes = '_' . $NoteCustomFields['JSON'];
                $Notes = json_encode($Notes);
                $MacantaNotes = json_encode($data['macantaNotes']);
                $action_details = '{"table":"ContactAction","id":"' . $noteId . '","fields":{"CreationNotes":' . $Notes . ',"' . $CF_MacantaNotes . '":' . $MacantaNotes . ',"ActionType":"' . $ActionType . '","ActionDescription":"' . $ActionDescription . '"}}';
                if (isset($data['callRecordingUrl']) && $data['callRecordingUrl'] != '') // when updating notes from parse
                    $action_details = '{"table":"ContactAction","id":"' . $noteId . '","fields":{"CreationNotes":' . $Notes . ',"' . $CF_MacantaNotes . '":' . $MacantaNotes . ',"' . $CF_RecordingURL . '":"' . $data['callRecordingUrl'] . '","ActionType":"' . $ActionType . '","ActionDescription":"' . $ActionDescription . '"}}';
            } else {
                $CombinedNotes = json_encode($Notes . "\n\n== macanta data: do not edit below this line ==\n" . $data['macantaNotes']);
                $action_details = '{"table":"ContactAction","id":"' . $noteId . '","fields":{"CreationNotes":' . $CombinedNotes . ',"ActionType":"' . $ActionType . '","ActionDescription":"' . $ActionDescription . '"}}';

            }

        } else {
            $Notes = $data['notes'];
            $action = "update_is";
            $Notes = addslashes(base64_decode($Notes['Notes']));
            $action_details = '{"table":"ContactAction","id":"' . $noteId . '","fields":{"CreationNotes":"' . $Notes . '","LastUpdatedBy":"' . $UserInfo->Id . '","LastUpdated":"' . infuDate(date('Y-m-d H:i:s')) . '"}}';
        }
        $result = applyFn('rucksack_request',$action, $action_details);
        file_put_contents(dirname(__FILE__) . '/result.txt', $action . $action_details);
        return $result;
    }
}

if (!function_exists('infusionsoft_compeleteTask')) {
    function infusionsoft_compeleteTask($noteContactId, $noteId, $session_name)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $user_seession_data = macanta_get_user_seession_data($session_name);
        $session_data = unserialize($user_seession_data->session_data);
        $action = "update_is";

        $defaultTimeZone = 'UTC';
        if (date_default_timezone_get() != $defaultTimeZone) date_default_timezone_set($defaultTimeZone); // server sets to EST already by peter
        $IS_Date = infuDate(_date('Y-m-d H:i:s', false, 'EST'));
        $action_details = '{"table":"ContactAction","id":"' . $noteId . '","fields":{"CompletionDate":"' . $IS_Date . '"}}';
        $result = applyFn('rucksack_request',$action, $action_details);
        $return['result'] = $result;
        //Update Cache
        $searched_cache = manual_cache_loader('searched_cache' . $session_data['InfusionsoftID']);
        $searched_cache_decoded = json_decode($searched_cache, true);
        foreach ($searched_cache_decoded as $key => $note) {
            if ($note['Id'] == $noteId) {
                $note['Completion Date'] = _date('d M Y', false);
                $return['CompletionDate'] = $note['Completion Date'];
                $searched_cache_decoded[$key] = $note;
                manual_cache_writer('searched_cache' . $session_data['InfusionsoftID'], json_encode($searched_cache_decoded), 86400);
                break;
            }
        }
        $return['searched_cache'] = $searched_cache_decoded;
        return $return;
    }
}

function _date($format = "r", $timestamp = false, $timezone = false)
{
    $userTimezone = new DateTimeZone(!empty($timezone) ? $timezone : 'GMT');
    $gmtTimezone = new DateTimeZone('GMT');
    $myDateTime = new DateTime(($timestamp != false ? date("r", (int)$timestamp) : date("r")), $gmtTimezone);
    $offset = $userTimezone->getOffset($myDateTime);
    return date($format, ($timestamp != false ? (int)$timestamp : $myDateTime->format('U')) + $offset);
}

/*Add Infusionsoft Task*/
if (!function_exists('infusionsoft_add_task')) {
    function infusionsoft_add_task($PostData, $UserInfo, $session_data)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $ContactId = $PostData['conId'];
        $TaskNote = $PostData['TaskNote'];
        $TaskCustomFields = json_decode($CI->config->item('TaskCustomFields'), true);
        $TaskAssignedTo = '_' . $TaskCustomFields['Task assigned to'];
        $TaskActionDescription = $PostData['TaskActionDescription'];
        $ActionType = $PostData['ActionType'];
        $TaskActionPriority = $PostData['TaskActionPriority'];
        $MacantaUser = $PostData['MacantaUser'];
        $ISUserId = $session_data['IS_Users'][0]->Id;
        $TaskActionDate = infuDate($PostData['TaskActionDate']);
        $defaultTimeZone = 'UTC';
        if (date_default_timezone_get() != $defaultTimeZone) date_default_timezone_set($defaultTimeZone); // server sets to EST already by peter
        $IS_Date = infuDate(_date('Y-m-d H:i:s', false, 'EST'));
        //$IS_Date = infuDate(date('Y-m-d H:i:s'));
        $action = "create_is";
        $action_details = '{"table":"ContactAction","fields":{"' . $TaskAssignedTo . '":"' . $MacantaUser . '", "ContactId":' . $ContactId . ', "CreatedBy":0, "CreationNotes":"' . $TaskNote . '","ActionType":"' . $ActionType . '","ActionDescription":"' . $TaskActionDescription . '","UserID":' . $ISUserId . ',"Accepted":1, "IsAppointment":0, "Priority":"' . $TaskActionPriority . '", "ActionDate":"' . $TaskActionDate . '"}}';
        /*Save Notes*/
        $result = applyFn('rucksack_request',$action, $action_details);
        $Id = $result->message;


        return ['result' => $Id, "action_details" => $action_details];
    }
}

/*Add Infusionsoft Notes*/
if (!function_exists('infusionsoft_add_note')) {
    function infusionsoft_add_note($PostData, $UserInfo)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $ContactId = $PostData['conId'];
        $call_id = $PostData['call_id'];
        $acct_id = $PostData['acct_id'];
        $rec_id = $PostData['rec_id'];
        $Notes = json_encode($PostData['notes']);
        $assignNoteToContactOwner = trim($PostData['assignNoteToContactOwner']);
        $OwnerID = $PostData['OwnerID'];
        $UserID = $assignNoteToContactOwner == 'yes' ? $OwnerID:$UserInfo->Id;
        $NoteTitle = $PostData['NoteTitle'];
        $NoteActionType = $PostData['NoteType'];
        $PlainNotes = $PostData['plainNotes'];
        $NoteType = $PostData['notesType'];
        $defaultTimeZone = 'UTC';
        if (date_default_timezone_get() != $defaultTimeZone) date_default_timezone_set($defaultTimeZone); // server sets to EST already by peter
        $IS_Date = infuDate(_date('Y-m-d H:i:s', false, 'EST'));
        //$IS_Date = infuDate(date('Y-m-d H:i:s'));
        $action = "create_is";
        $CallRecordingURL = '';
        $MacantaNotes = '{"note_type":"' . $NoteType . '","user":"' . $UserInfo->FirstName . " " . $UserInfo->LastName . '", "note":' . $Notes . '}';
        if (trim($rec_id) != '' && $rec_id != null) {
            $CallRecordingURL = $URL = "https://api.twilio.com/2010-04-01/Accounts/" . $acct_id . "/Recordings/" . $rec_id . ".mp3";
            $MacantaNotes = '{"note_type":"' . $NoteType . '","acct_id":"' . $acct_id . '","call_id":"' . $call_id . '","rec_id":"' . $rec_id . '","user":"' . $UserInfo->FirstName . " " . $UserInfo->LastName . '","timestamp": ' . time() . ', "note":' . $Notes . '}';
        }
        if ($CI->config->item('NoteCustomFields')) {
            $NoteCustomFields = json_decode($CI->config->item('NoteCustomFields'), true);
            $CF_RecordingURL = '_' . $NoteCustomFields['Call Recording URL'];
            $CF_MacantaNotes = '_' . $NoteCustomFields['JSON'];
            $MacantaNotes = json_encode($MacantaNotes);
            $PlainNotes = json_encode($PlainNotes);
            $action_details = '{"table":"ContactAction","fields":{"' . $CF_RecordingURL . '":"' . $CallRecordingURL . '","' . $CF_MacantaNotes . '":' . $MacantaNotes . ',"ContactId":' . $ContactId . ', "CreatedBy":' . $UserInfo->Id . ', "CreationNotes":' . $PlainNotes . ',"ActionType":"' . $NoteActionType . '","ActionDescription":"' . $NoteTitle . '","UserID":' . $UserID . ',"IsAppointment":0,"CreationDate":"' . $IS_Date . '","CompletionDate":"' . $IS_Date . '"}}';

        } else {
            $CombinedNotes = json_encode($PlainNotes . "\n\n== macanta data: do not edit below this line ==\n" . $MacantaNotes);
            $action_details = '{"table":"ContactAction","fields":{"ContactId":' . $ContactId . ', "CreatedBy":' . $UserInfo->Id . ', "CreationNotes":' . $CombinedNotes . ',"ActionType":"' . $NoteActionType . '","ActionDescription":"' . $NoteTitle . '","UserID":' . $UserID . ',"IsAppointment":0,"CreationDate":"' . $IS_Date . '","CompletionDate":"' . $IS_Date . '"}}';
        }
        /*Save Notes*/

        $result = applyFn('rucksack_request',$action, $action_details);
        //file_put_contents(dirname(__FILE__).'/result.txt', $action. $action_details );
        /*Save Notes To DB for back up*/
        $DBdata = array(
            'user_id' => $UserInfo->Id,
            'meta_key' => $NoteType,
            'meta_value' => $action_details
        );
        $CI->db->insert('users_meta', $DBdata);
        return $result;
    }
}
if (!function_exists('infusionsoft_get_contact_notes_by_multiple_contact_id')) {
    function infusionsoft_get_contact_notes_by_multiple_contact_id($ContactIds = [])
    {
        /*GET CONTACT NOTES*/
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $returnFields = array('"Id"', '"ContactId"', '"CreationNotes"', '"ActionDescription"', '"CreatedBy"', '"UserID"', '"IsAppointment"', '"ActionType"', '"CreationDate"', '"CompletionDate"', '"ObjectType"');
        if ($CI->config->item('NoteCustomFields')) {
            $NoteCustomFields = json_decode($CI->config->item('NoteCustomFields'), true);
            $CF_RecordingURL = '_' . $NoteCustomFields['Call Recording URL'];
            $CF_MacantaNotes = '_' . $NoteCustomFields['JSON'];
            $returnFields = array('"' . $CF_RecordingURL . '"', '"' . $CF_MacantaNotes . '"', '"Id"', '"ContactId"', '"CreationNotes"', '"ActionDescription"', '"CreatedBy"', '"UserID"', '"IsAppointment"', '"ActionType"', '"CreationDate"', '"CompletionDate"', '"ObjectType"');
            if ($CI->config->item('TaskCustomFields')) {
                $TaskCustomFields = json_decode($CI->config->item('TaskCustomFields'), true);
                $TaskAssignedTo = '_' . $TaskCustomFields['Task assigned to'];
                $returnFields = array('"' . $TaskAssignedTo . '"', '"' . $CF_RecordingURL . '"', '"' . $CF_MacantaNotes . '"', '"Id"', '"ContactId"', '"CreationNotes"', '"ActionDescription"', '"CreatedBy"', '"UserID"', '"IsAppointment"', '"ActionType"', '"CreationDate"', '"CompletionDate"', '"ObjectType"');
            }
        }
        $TurboDialHeaderId = infusionsoft_get_contact_action_turbo_dial_header_id();
        if ($TurboDialHeaderId !== false) {
            $TurboDialCustomFieldCallURL = infusionsoft_get_custom_fields_by_header_id($TurboDialHeaderId, "Call Recording URL");
            $value = '_' . $TurboDialCustomFieldCallURL[0]->Name;
            $key = 'TurboDial_CallRecordingURL_Field';
            $config_data=array(
                'key'=>$key,
                'value'=>$value
            );
            $CI->db->where('key', $key);
            $query = $CI->db->get('config_data');

            if(sizeof($query->result()) > 0){
                $CI->db->where('key', $key);
                $CI->db->update('config_data',$config_data);
            }else{
                $CI->db->insert('config_data',$config_data);
            }
            $returnFields[] = '"' . $value . '"';
        }
        $action = "query_is";
        $returnFieldsStr = implode(', ', $returnFields);
        $ContactIntArr =  '['.implode(',',$ContactIds)."]";
        $action_details = '{"table":"ContactAction","limit":"1000","page":0,"fields":[' . $returnFieldsStr . '],"query":{"ContactId":' . $ContactIntArr . '}}';
        $Notes = applyFn('rucksack_request',$action, $action_details);
        //file_put_contents(dirname(__FILE__).'/'.__FUNCTION__.'.txt', $Notes );

        return $Notes->message;
    }
}
if (!function_exists('infusionsoft_get_contact_notes_by_contact_id')) {
    function infusionsoft_get_contact_notes_by_contact_id($ContactId)
    {
        /*GET CONTACT NOTES*/
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $returnFields = array(
            '"Id"',
            '"CreationNotes"',
            '"ActionDescription"',
            '"CreatedBy"',
            '"UserID"',
            '"IsAppointment"',
            '"ActionType"',
            '"CreationDate"',
            '"CompletionDate"',
            '"ObjectType"',
            '"ActionDate"');
        if ($CI->config->item('NoteCustomFields')) {
            $NoteCustomFields = json_decode($CI->config->item('NoteCustomFields'), true);
            $CF_RecordingURL = '_' . $NoteCustomFields['Call Recording URL'];
            $CF_MacantaNotes = '_' . $NoteCustomFields['JSON'];
            $returnFields = array('"' . $CF_RecordingURL . '"', '"' . $CF_MacantaNotes . '"', '"Id"', '"CreationNotes"', '"ActionDescription"', '"CreatedBy"', '"UserID"', '"IsAppointment"', '"ActionType"', '"CreationDate"', '"CompletionDate"', '"ObjectType"', '"ActionDate"');
            if ($CI->config->item('TaskCustomFields')) {
                $TaskCustomFields = json_decode($CI->config->item('TaskCustomFields'), true);
                $TaskAssignedTo = '_' . $TaskCustomFields['Task assigned to'];
                $returnFields = array('"' . $TaskAssignedTo . '"', '"' . $CF_RecordingURL . '"', '"' . $CF_MacantaNotes . '"', '"Id"', '"CreationNotes"', '"ActionDescription"', '"CreatedBy"', '"UserID"', '"IsAppointment"', '"ActionType"', '"CreationDate"', '"CompletionDate"', '"ObjectType"', '"ActionDate"');
            }
        }
        $TurboDialHeaderId = infusionsoft_get_contact_action_turbo_dial_header_id();
        if ($TurboDialHeaderId != false) {
            $TurboDialCustomFieldCallURL = infusionsoft_get_custom_fields_by_header_id($TurboDialHeaderId, "Call Recording URL");
            $value = '_' . $TurboDialCustomFieldCallURL[0]->Name;
            $key = 'TurboDial_CallRecordingURL_Field';
            $config_data=array(
                'key'=>$key,
                'value'=>$value
            );
            $CI->db->where('key', $key);
            $query = $CI->db->get('config_data');

            if(sizeof($query->result()) > 0){
                $CI->db->where('key', $key);
                $CI->db->update('config_data',$config_data);
            }else{
                $CI->db->insert('config_data',$config_data);
            }
            $returnFields[] = '"' . $value . '"';
        }
        $action = "query_is";
        $returnFieldsStr = implode(', ', $returnFields);
        $action_details = '{"table":"ContactAction","limit":"1000","page":0,"fields":[' . $returnFieldsStr . '],"query":{"ContactId":' . $ContactId . '}}';
        $Notes = applyFn('rucksack_request',$action, $action_details);
        //file_put_contents(dirname(__FILE__).'/'.__FUNCTION__.'.txt', $Notes );

        return $Notes->message;
    }
}
if (!function_exists('infusionsoft_get_contact_by_email')) {
    function infusionsoft_get_contact_by_email($Email)
    {   $CI =& get_instance();
        $CI->load->helper('macanta_helper');
        /*GET CONTACT*/
        $action = "query_is";
        $action_details = '{"table":"Contact","limit":"1000","page":0,"fields":["Id","Email","FirstName","LastName"],"query":{"Email":"' . $Email . '"}}';
        $Invoice = applyFn('rucksack_request',$action, $action_details, "sherpa", true);
        return $Invoice->message;
    }
}
if (!function_exists('infusionsoft_get_invoice_by_contact_id')) {
    function infusionsoft_get_invoice_by_contact_id($ContactId)
    {
        /*GET CONTACT NOTES*/
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $action = "query_is";
        $returnFields = array('"Id"', '"CreditStatus"', '"DateCreated"', '"Description"', '"InvoiceTotal"', '"InvoiceType"', '"JobId"', '"TotalDue"', '"TotalPaid"');
        $returnFieldsStr = implode(', ', $returnFields);
        //$action_details = '{"table":"ContactAction","limit":"1000","page":0,"fields":['.$returnFieldsStr.'],"query":{"ContactId":"'.$ContactId.'","ActionType":"Other"}}';
        $action_details = '{"table":"Invoice","limit":"1000","page":0,"fields":[' . $returnFieldsStr . '],"query":{"ContactId":"' . $ContactId . '"}}';
        $Invoice = applyFn('rucksack_request',$action, $action_details);
        return $Invoice->message;
    }
}

/*Get Contact by email and password
  this is use in logging in users.
*/
if (!function_exists('infusionsoft_get_contact_by_email_password')) {
    function infusionsoft_get_contact_by_email_password($Email, $Password)
    {
        /*GET CONTACT NOTES*/
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI =& get_instance();
        $CI->load->helper('macanta_helper');
        $action = "query_is";
        $returnFields = array(
            '"Birthday"',
            '"City"',
            '"Company"',
            '"CompanyID"',
            '"Country"',
            '"Email"',
            '"FirstName"',
            '"Groups"',
            '"Id"',
            '"JobTitle"',
            '"LastName"',
            '"Password"',
            '"Phone1"',
            '"Phone2"',
            '"PostalCode"',
            '"State"',
            '"StreetAddress1"',
            '"StreetAddress2"',
            '"TimeZone"'
        );
        $returnFieldsStr = implode(', ', $returnFields);
        $action_details = '{"table":"Contact","limit":"1000","page":0,"fields":[' . $returnFieldsStr . '],"query":{"Email":"' . $Email . '", "Password":"' . $Password . '"}}';
        $Contact = applyFn('rucksack_request',$action, $action_details);
        return $Contact;
    }
}
if (!function_exists('infusionsoft_apply_tag')) {
    function infusionsoft_apply_tag($ConId, $TagId)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $action = "apply_tag";
        $action_details = '{"tag":' . $TagId . ',"id":' . $ConId . '}';
        $Tags = applyFn('rucksack_request',$action, $action_details);
        return $Tags->message;
    }
}
if (!function_exists('infusionsoft_remove_tag')) {
    function infusionsoft_remove_tag($ConId, $TagId)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $action = "removeTag";
        $action_details = '{"tag":' . $TagId . ',"id":' . $ConId . '}';
        $Tags = applyFn('rucksack_request',$action, $action_details);
        return $Tags->message;
    }
}
if (!function_exists('infusionsoft_get_tags')) {
    function infusionsoft_get_tags($TagId = '', $Force = false)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');

        $Tags = manual_cache_loader('infusionsoft_get_tags');
        $Tags = $Force == true ? false:json_decode($Tags);
        if($Tags == false) {
            $action = "query_is";
            $returnFields = array('"GroupCategoryId"', '"GroupDescription"', '"GroupName"', '"Id"');
            $returnFieldsStr = implode(', ', $returnFields);

            $tempResults = 1000;
            $Tags = [];
            $page = 0;
            while ($tempResults >= 1000) {
                $action_details = '{"table":"ContactGroup","limit":"1000","page":' . $page . ',"fields":[' . $returnFieldsStr . '],"query":{"Id":"%"}}';
                $temp = applyFn('rucksack_request',$action, $action_details);
                $tempResults = sizeof($temp->message);
                $Tags = array_merge($Tags, $temp->message);
                $page++;
            }
            manual_cache_writer('infusionsoft_get_tags', json_encode($Tags), 0);
        }
        if($TagId == ''){
            return $Tags;
        }else{
            $key = array_search($TagId, array_column($Tags, 'Id'));
            $theTag[0] = $Tags[$key];
            return $theTag;
        }

    }
}
if (!function_exists('infusionsoft_get_tags_category')) {
    function infusionsoft_get_tags_category($CatId = '')
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $action = "query_is";
        $query = $CatId != '' ? '{"Id":"' . $CatId . '"}' : '{"Id":"%"}';
        $returnFields = array('"CategoryDescription"', '"CategoryName"', '"Id"');
        $returnFieldsStr = implode(', ', $returnFields);
        $action_details = '{"table":"ContactGroupCategory","limit":"1000","page":0,"fields":[' . $returnFieldsStr . '],"query":' . $query . ',"orderby":"CategoryName"}';
        $Tags = applyFn('rucksack_request',$action, $action_details);
        return $Tags->message;
    }
}
/*Create Infusionsoft Tag*/
if (!function_exists('infusionsoft_create_tag')) {
    function infusionsoft_create_tag($tagParams)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $GroupCategoryId = $tagParams['GroupCategoryId'];
        $GroupName = $tagParams['GroupName'];
        $GroupDescription = $tagParams['GroupDescription'];
        /*Save Notes*/
        $action = "create_is";
        $action_details = '{"table":"ContactGroup","fields":{"GroupCategoryId":' . $GroupCategoryId . ', "GroupName":"' . $GroupName . '", "GroupDescription":"' . $GroupDescription . '"}}';
        $result = applyFn('rucksack_request',$action, $action_details);
        infusionsoft_get_tags('', true);
        return $result;
    }
}
if (!function_exists('infusionsoft_get_contact_action_turbo_dial_header_id')) {
    function infusionsoft_get_contact_action_turbo_dial_header_id($Force = false)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');


        $HeaderId = manual_cache_loader('infusionsoft_get_contact_action_turbo_dial_header_id');
        $HeaderId = $Force == true ? false:$HeaderId;
        if($HeaderId == false) {
            $queryAction = "query_is";
            $returnFields = array('"Id"', '"TabName"', '"FormId"');
            $returnFieldsStr = implode(', ', $returnFields);
            $action_details = '{"table":"DataFormTab","limit":"1000","page":0,"fields":[' . $returnFieldsStr . '],"query":{"FormId":-5,"TabName":"Call Data"}}';
            $Results = applyFn('rucksack_request',$queryAction, $action_details);
            //print_r($Results);
            $Results = $Results->message;
            if (isset($Results[0]->Id)) {
                $TabId = $Results[0]->Id;
                $returnFields = array('"Id"', '"Name"', '"TabId"');
                $returnFieldsStr = implode(', ', $returnFields);
                $action_details = '{"table":"DataFormGroup","limit":"1000","page":0,"fields":[' . $returnFieldsStr . '],"query":{"TabId":' . $TabId . ',"Name":"Call Data"}}';
                $Results2 = applyFn('rucksack_request',$queryAction, $action_details);
                //print_r($Results2);
                $Results2 = $Results2->message;
                if (isset($Results2[0]->Id)) {
                    $HeaderId = $Results2[0]->Id;
                }

            }else{
                $HeaderId = "";
            }
            manual_cache_writer('infusionsoft_get_contact_action_turbo_dial_header_id', $HeaderId, 0);
        }
        return $HeaderId == "" ? false:$HeaderId;




    }
}
if (!function_exists('infusionsoft_get_contact_action_header_id')) {
    function infusionsoft_get_contact_action_header_id($FormId = -5)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $queryAction = "query_is";
        $returnFields = array('"Id"', '"TabName"', '"FormId"');
        $returnFieldsStr = implode(', ', $returnFields);
        $action_details = '{"table":"DataFormTab","limit":"1000","page":0,"fields":[' . $returnFieldsStr . '],"query":{"FormId":'.$FormId.',"TabName":"macanta"}}';
        $Results = applyFn('rucksack_request',$queryAction, $action_details);
        //print_r($Results);
        $Results = $Results->message;
        if (isset($Results[0]->Id)) {
            $TabId = $Results[0]->Id;
            $returnFields = array('"Id"', '"Name"', '"TabId"');
            $returnFieldsStr = implode(', ', $returnFields);
            $action_details = '{"table":"DataFormGroup","limit":"1000","page":0,"fields":[' . $returnFieldsStr . '],"query":{"TabId":' . $TabId . ',"Name":"metadata"}}';
            $Results2 = applyFn('rucksack_request',$queryAction, $action_details);
            //print_r($Results2);
            $Results2 = $Results2->message;
            if (isset($Results2[0]->Id)) {
                $HeaderId = $Results2[0]->Id;
                return $HeaderId;
            }

        }
        return false;
    }
}
if (!function_exists('infusionsoft_get_custom_fields_by_header_id')) {
    function infusionsoft_get_custom_fields_by_header_id($HeaderId, $Label = "")
    {
        $Label = $Label == "" ? false : $Label;
        return infusionsoft_get_custom_fields("%",false, false, $HeaderId,$Label);
    }
}
if (!function_exists('infusionsoft_get_custom_fields_by_label')) {
    function infusionsoft_get_custom_fields_by_label($FormId, $Label)
    {
        return infusionsoft_get_custom_fields("%",false, $FormId, false,$Label);
    }
}
if (!function_exists('infusionsoft_opps_custom_fields')) {
    function infusionsoft_opps_custom_fields()
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $CustomFieldGroupArr = [];
        $DataTypesArr = array(
            1 => "Phone Number",
            2 => "Social Security Number",
            3 => "Currency",
            4 => "Percent",
            5 => "State",
            6 => "Yes/No",
            7 => "Year",
            8 => "Month",
            9 => "Day of Week",
            10 => "Name",
            11 => "Decimal Number",
            12 => "Whole Number",
            13 => "Date",
            14 => "Date/Time",
            15 => "Text",
            16 => "Text Area",
            17 => "List Box",
            18 => "Website",
            19 => "Email",
            20 => "Radio",
            21 => "Dropdown",
            22 => "User",
            23 => "Drilldown",
        );
        $CustomFieldsArr = [];
        $CustomFieldGroups = infusionsoft_get_custom_field_groups();
        foreach ($CustomFieldGroups as $CustomFieldGroup) {
            $CustomFieldGroupArr[$CustomFieldGroup->Id] = ["Name" => $CustomFieldGroup->Name, "TabId" => $CustomFieldGroup->TabId];

        }
        $opps_custom_fields = infusionsoft_get_custom_fields("%",false, -4, false,false);
        foreach ($opps_custom_fields as $CustomField) {
            $CustomFieldsArr[$CustomFieldGroupArr[$CustomField->GroupId]["Name"]][] = $CustomField;
        }
        ksort($CustomFieldsArr);

        $NewCustomFieldsArr = [];
        foreach ($CustomFieldsArr as $GroupName => $items) {
            $NewItems = [];
            foreach ($items as $item) {
                $item->DataTypeName = isset($DataTypesArr[$item->DataType]) ? $DataTypesArr[$item->DataType]:'Text' ;
                $NewItems[ucfirst($item->Label)] = $item;
            }
            ksort($NewItems, SORT_FLAG_CASE);

            $NewCustomFieldsArr[$GroupName] = $NewItems;
        }
        return $NewCustomFieldsArr;
    }
}
if (!function_exists('infusionsoft_contact_custom_fields')) {
    function infusionsoft_contact_custom_fields()
    {
        $CustomFields = array();
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $CustomFieldGroupArr = [];
        $DataTypesArr = array(
            1 => "Phone Number",
            2 => "Social Security Number",
            3 => "Currency",
            4 => "Percent",
            5 => "State",
            6 => "Yes/No",
            7 => "Year",
            8 => "Month",
            9 => "Day of Week",
            10 => "Name",
            11 => "Decimal Number",
            12 => "Whole Number",
            13 => "Date",
            14 => "Date/Time",
            15 => "Text",
            16 => "Text Area",
            17 => "List Box",
            18 => "Website",
            19 => "Email",
            20 => "Radio",
            21 => "Dropdown",
            22 => "User",
            23 => "Drilldown",
        );
        $CustomFieldsArr = [];
        $CustomFieldGroups = infusionsoft_get_custom_field_groups();
        foreach ($CustomFieldGroups as $CustomFieldGroup) {
            $CustomFieldGroupArr[$CustomFieldGroup->Id] = ["Name" => $CustomFieldGroup->Name, "TabId" => $CustomFieldGroup->TabId];

        }
        $CustomFields = infusionsoft_get_custom_fields("%",false, -1, false,false);;

        foreach ($CustomFields as $CustomField) {
            $CustomField->DataType = $DataTypesArr[$CustomField->DataType];
            $CustomField->GroupName = $CustomFieldGroupArr[$CustomField->GroupId]["Name"];
            $CustomFieldsArr[$CustomField->GroupName][] = $CustomField;

        }
        ksort($CustomFieldsArr);

        $NewCustomFieldsArr = [];
        foreach ($CustomFieldsArr as $GroupName => $items) {
            $NewItems = [];
            foreach ($items as $item) {
                $NewItems[ucfirst($item->Name)] = $item;
            }
            ksort($NewItems, SORT_FLAG_CASE);
            $NewCustomFieldsArr[$GroupName] = $NewItems;
        }
        return $NewCustomFieldsArr;
    }
}

if (!function_exists('infusionsoft_generate_contact_action_custom_fields')) {
    function infusionsoft_generate_contact_action_custom_fields($NoteCustomFields = array(),$FormId = -5)
    {
        $customFieldType = [
            -5 => 'ContactAction',
            -1 => 'Contact',
            -6 => 'Company',
            -4 => 'Opportunity'
        ];
        $CustomFields = array();
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $queryAction = "query_is";
        $createAction = "addCustomField";
        $HeaderId = infusionsoft_get_contact_action_header_id($FormId);
        if ($HeaderId == false) return false;
        foreach ($NoteCustomFields as $NoteCustomField) {
            $queryAction_details = '{"table":"DataFormField","limit":"1000","page":0,"fields":["Id","FormId","GroupId","DataType","Label","Name"],"query":{"FormId":'.$FormId.',"Label":"' . $NoteCustomField['Label'] . '"}}';
            $queryAction_results = applyFn('rucksack_request',$queryAction, $queryAction_details);
            if (sizeof($queryAction_results->message) == 0) {

                $createAction_details = '{"customFieldType":"'.$customFieldType[$FormId].'","displayName":"' . $NoteCustomField['Label'] . '","dataType":"' . $NoteCustomField['dType'] . '","headerID":' . $HeaderId . '}';
                $Result = applyFn('rucksack_request',$createAction, $createAction_details);
                $CustomFieldId = $Result->message;
                $queryAction2_details = '{"table":"DataFormField","limit":"1000","page":0,"fields":["Id","FormId","GroupId","DataType","Label","Name"],"query":{"Id":' . $CustomFieldId . '}}';
                $Result2 = applyFn('rucksack_request',$queryAction, $queryAction2_details);
                $CustomFields[$Result2->message[0]->Label] = $Result2->message[0]->Name;
            } else {
                $CustomFields[$queryAction_results->message[0]->Label] = $queryAction_results->message[0]->Name;
            }
        }
        return $CustomFields;
    }
}

if (!function_exists('infusionsoft_get_tags_by_catId')) {
    function infusionsoft_get_tags_by_catId($CatId, $orderBy = '', $Force = false)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');

        $Tags = manual_cache_loader('infusionsoft_get_tags_by_catId-'.$CatId);
        $Tags = $Force == true ? false:json_decode($Tags);
        if($Tags == false) {
            $action = "query_is";
            $returnFields = array('"GroupCategoryId"', '"GroupDescription"', '"GroupName"', '"Id"');
            $returnFieldsStr = implode(', ', $returnFields);
            $tempResults = 1000;
            $Tags = [];
            $page = 0;
            while ($tempResults >= 1000) {
                $action_details = '{"table":"ContactGroup","limit":"1000","page":' . $page . ',"fields":[' . $returnFieldsStr . '],"query":{"GroupCategoryId":' . $CatId . '}}';
                if ($orderBy != '') {
                    $action_details = '{"table":"ContactGroup","limit":"1000","page":' . $page . ',"fields":[' . $returnFieldsStr . '],"query":{"GroupCategoryId":' . $CatId . '},"orderby":"' . $orderBy . '"}';
                }
                $temp = applyFn('rucksack_request',$action, $action_details);
                $tempResults = sizeof($temp->message);
                $Tags = array_merge($Tags, $temp->message);
                $page++;
            }
            manual_cache_writer('infusionsoft_get_tags_by_catId-'.$CatId, json_encode($Tags), 30);
        }
        return $Tags;
    }
}
if (!function_exists('infusionsoft_refresh_get_tags_by_catId')) {
    function infusionsoft_refresh_get_tags_by_catId()
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $CachePath = APPPATH . "cache/";
        $FolderContent = scandir($CachePath);
        $FileNamePart     = 'infusionsoft_get_tags_by_catId';
        foreach($FolderContent as $Item){
            if(strpos($Item, $FileNamePart) !== false){
                $FileNameParts = explode("-", $Item);
                $CatId = $FileNameParts[1];
                infusionsoft_get_tags_by_catId($CatId,'', true);
            }
        }
    }
}
if (!function_exists('infusionsoft_get_tags_by_groupname_and_catId')) {
    function infusionsoft_get_tags_by_groupname_and_catId($Name, $CatId)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $action = "query_is";
        $returnFields = array('"GroupCategoryId"', '"GroupDescription"', '"GroupName"', '"Id"');
        $returnFieldsStr = implode(', ', $returnFields);
        //$action_details = '{"table":"ContactGroup","limit":"1000","page":0,"fields":['.$returnFieldsStr.'],"query":{"GroupName":"'.$Name.'","GroupCategoryId":'.$CatId.'}}';
        $action_details = '{"table":"ContactGroup","limit":"1000","page":0,"fields":[' . $returnFieldsStr . '],"query":{"GroupCategoryId":' . $CatId . '}}';
        $Tags = applyFn('rucksack_request',$action, $action_details);
        $return = [];
        //replace reserved words
        $search = ['>','<','–'];
        $replace = ['&gt;','&lt;','&ndash;'];
        $Name_encoded = str_replace($search, $replace, $Name);

        foreach ($Tags->message as $Tag) {
            if ($Tag->GroupName === $Name) {
                $return[0] = $Tag;
                break;
            }
        }
        if (!isset($return[0])) {
            foreach ($Tags->message as $Tag) {
                if ($Tag->GroupName === $Name_encoded) {
                    $return[0] = $Tag;
                    break;
                }
            }
        }

        return $return;
    }
}
if (!function_exists('infusionsoft_get_tags_by_groupname')) {
    function infusionsoft_get_tags_by_groupname($Name)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $action = "query_is";
        $returnFields = array('"GroupCategoryId"', '"GroupDescription"', '"GroupName"', '"Id"');
        $returnFieldsStr = implode(', ', $returnFields);
        $action_details = '{"table":"ContactGroup","limit":"1000","page":0,"fields":[' . $returnFieldsStr . '],"query":{"GroupName":"' . $Name . '"}}';
        $Tags = applyFn('rucksack_request',$action, $action_details);
        return $Tags->message[0];
    }
}
if (!function_exists('infusionsoft_refresh_custom_fields')) {
    function infusionsoft_refresh_custom_fields()
    {
        infusionsoft_get_custom_fields("%", true);
        infusionsoft_get_custom_field_groups(true);
    }
}
if (!function_exists('infusionsoft_get_custom_field_groups')) {
    function infusionsoft_get_custom_field_groups($Force = false)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $CustomFieldGroups = manual_cache_loader('infusionsoft_get_custom_field_groups');
        $CustomFieldGroups = $Force == true ? false:json_decode($CustomFieldGroups);
        if($CustomFieldGroups == false) {
            $action = "query_is";
            $action_details = '{"table":"DataFormGroup","limit":"1000","page":0,"fields":["Id","Name","TabId"],"query":{"Id":"%"}}';
            $CustomFieldGroups = applyFn('rucksack_request',$action, $action_details);
            manual_cache_writer('infusionsoft_get_custom_field_groups', json_encode($CustomFieldGroups), 0);
        }
        return $CustomFieldGroups->message;
    }
}
if (!function_exists('infusionsoft_get_custom_fields')) {
    function infusionsoft_get_custom_fields($Id = "%", $Force = false, $FormId = false, $GroupId = false, $Label = false)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $CustomFields = manual_cache_loader('infusionsoft_get_custom_fields');
        $CustomFields = $Force == true ? false:json_decode($CustomFields);
        if($CustomFields == false) {
            $action = "query_is";
            $returnFields = array('"DataType"', '"DefaultValue"', '"FormId"', '"GroupId"', '"Id"', '"Label"', '"ListRows"', '"Name"', '"Values"');
            $returnFieldsStr = implode(', ', $returnFields);
            $action_details = '{"table":"DataFormField","limit":"1000","page":0,"fields":[' . $returnFieldsStr . '],"query":{"Id":"' . $Id . '"}}';
            $CustomFields = applyFn('rucksack_request',$action, $action_details);
            manual_cache_writer('infusionsoft_get_custom_fields', json_encode($CustomFields), 0);
        }
        if($FormId == false && $GroupId == false && $Label == false){
            return $CustomFields->message;
        }else{
            $NewCustomFields = [];
            if($FormId !== false){
                foreach ($CustomFields->message as $CustomField){
                    if($CustomField->FormId == $FormId) $NewCustomFields[] = $CustomField;
                }
                $CustomFields->message = $NewCustomFields;
            }

            $NewCustomFields = [];
            if($GroupId !== false){
                foreach ($CustomFields->message as $CustomField){
                    if($CustomField->GroupId == $GroupId) $NewCustomFields[] = $CustomField;
                }
                $CustomFields->message = $NewCustomFields;
            }

            $NewCustomFields = [];
            if($Label !== false){
                foreach ($CustomFields->message as $CustomField){
                    if(strtolower(trim($CustomField->Label ))  == strtolower(trim($Label))) $NewCustomFields[] = $CustomField;
                }
                $CustomFields->message = $NewCustomFields;
            }
            return $CustomFields->message;
        }

    }
}
if (!function_exists('infusionsoft_get_tags_by_cat_id')) {
    function infusionsoft_get_tags_by_cat_id($CatId)
    {
        return infusionsoft_get_tags_by_catId($CatId);
    }
}
if (!function_exists('infusionsoft_get_webform_map')) {
    function infusionsoft_get_webform_map($Force = false)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $WebformMap = manual_cache_loader('infusionsoft_get_webform_map');
        $WebformMap = $Force == true ? false:json_decode($WebformMap);
        if($WebformMap == false) {
            $action = "webform_map_is";
            $action_details = '{}';
            $WebformMap = applyFn('rucksack_request',$action, $action_details);
            manual_cache_writer('infusionsoft_get_webform_map', json_encode($WebformMap), 0);
        }
        return $WebformMap->message;
    }
}
if (!function_exists('infusionsoft_refresh_settings')) {
    function infusionsoft_refresh_settings()
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $CachePath = APPPATH . "cache/";

        $FolderContent = scandir($CachePath);
        $FileNamePart     = 'infusionsoft_get_settings';
        foreach($FolderContent as $Item){
            if(strpos(strtolower($Item), $FileNamePart) !== false){
                $FileNameParts = explode("-", $Item);
                $Module = $FileNameParts[1];
                $Setting = $FileNameParts[2];
                infusionsoft_get_settings($Module, $Setting, true);
            }
        }
    }
}
if (!function_exists('infusionsoft_get_tag_categories_menu')) {
    function infusionsoft_get_tag_categories_menu( $Force = false ){
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $theMenu = manual_cache_loader('infusionsoft_get_tag_categories_menu');
        $theMenu = $Force == true ? false:json_decode($theMenu);
        if($theMenu == false) {
            $TagCategories = infusionsoft_get_tags_category();
            //addTagCatMenu($Title,$FullTitle, $Key)
            $Objs = [];

            $TagCategoriesArr = array();
            foreach($TagCategories as $TagCategory){
                $TagCategoriesArr[$TagCategory->Id] = $TagCategory->CategoryName;
                $val = html_entity_decode($TagCategory->CategoryName);
                $toolTip = $val;
                $valShorten =  strlen($val) > 30 ? substr($val, 0, 30)."...":$val;
                $val = json_encode($valShorten);
                if(!$val) $val = "'$toolTip'";
                $Objs[] = infusionsoft_addTagCatMenu($val,$toolTip, $TagCategory->Id);
            }
            manual_cache_writer('TagCategories', $TagCategoriesArr,0);
            $theMenu = implode(',',$Objs);
            manual_cache_writer('infusionsoft_get_tag_categories_menu', json_encode($theMenu), 0);
        }
        return $theMenu;
    }
}
if (!function_exists('infusionsoft_addWebFormMenu')) {
    function infusionsoft_addTagCatMenu($Title,$FullTitle, $Key){
        if(strlen($FullTitle) > 30 ){
            $Objs = '{text:'.$Title.',tooltip:'.json_encode($FullTitle).',onclick: function() {editor.insertContent(\'[TagCat id='.$Key.' readonly=no]\');}}';
        }else{
            $Objs = '{text:'.$Title.',onclick: function() {editor.insertContent(\'[TagCat id='.$Key.'  readonly=no]\');}}';
        }
        return $Objs;
    }
}
if (!function_exists('infusionsoft_generate_webform_menu')) {
    function infusionsoft_generate_webform_menu( $Force = false )
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $WebformMenu = manual_cache_loader('infusionsoft_generate_webform_menu');
        $WebformMenu = $Force == true ? false:json_decode($WebformMenu);
        if($WebformMenu == false) {
            $ValidExternalForm = manual_cache_loader('ValidISwebForms');
            $ValidExternalForm = $ValidExternalForm ? $ValidExternalForm:[];
            $Objs = [];
            $WebFormPairs = infusionsoft_get_webform_map();
            foreach($WebFormPairs as $key => $val){
                $val = str_replace("\n", "", $val);
                $val = str_replace("~br~", " ", $val);
                $val = html_entity_decode($val);
                $toolTip = $val;
                $valShorten =  strlen($val) > 30 ? substr($val, 0, 30)."...":$val;
                $val = json_encode($valShorten);
                if(!$val) $val = "'$toolTip'";
                if(!array_key_exists($key,$ValidExternalForm)){
                    $ValidExternalForm[$key]['Title'] = $val;
                    $theForm = infusionsoft_get_webform_html($key);
                    if (strpos($theForm, '<form') !== false) {
                        $ValidExternalForm[$key]['Valid'] = 1;
                        $Objs[] = infusionsoft_addWebFormMenu($val,$toolTip, $key);
                        continue;
                    }else{
                        $ValidExternalForm[$key]['Valid'] = 0;
                        continue;
                    }
                }elseif($ValidExternalForm[$key]['Valid']){
                    $Objs[] = infusionsoft_addWebFormMenu($val,$toolTip, $key);
                    continue;
                }

            }
            manual_cache_writer('ValidISwebForms', $ValidExternalForm,0);
            $theMenu = implode(',',$Objs);
            $WebformMenu = $theMenu;
            manual_cache_writer('infusionsoft_generate_webform_menu', json_encode($WebformMenu), 0);
        }
        return $WebformMenu;
    }
}
if (!function_exists('infusionsoft_addWebFormMenu')) {
    function infusionsoft_addWebFormMenu($Title,$FullTitle, $Key){
        if(strlen($FullTitle) > 30 ){
            $Objs = '{text:'.mb_convert_encoding($Title, "HTML-ENTITIES", "UTF-8").',tooltip:'.json_encode($FullTitle).',onclick: function() {editor.insertContent(\'[ISwebform formid='.$Key.' readonly=false createnote=no assign_note_to_contact_owner=no submitted_by_customfield=""]\');}}';
        }else{
            $Objs = '{text:'.mb_convert_encoding($Title, "HTML-ENTITIES", "UTF-8").',onclick: function() {editor.insertContent(\'[ISwebform formid='.$Key.' readonly=false createnote=no assign_note_to_contact_owner=no submitted_by_customfield=""]\');}}';
        }
        return $Objs;
    }
}
if (!function_exists('infusionsoft_refresh_webform_html')) {
    function infusionsoft_refresh_webform_html()
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $CachePath = APPPATH . "cache/";

        $FolderContent = scandir($CachePath);
        $FileNamePart     = 'infusionsoft_get_webform_html';
        foreach($FolderContent as $Item){
            if(strpos(strtolower($Item), $FileNamePart) !== false){
                $FileNameParts = explode("-", $Item);
                $FormId = $FileNameParts[1];
                infusionsoft_get_webform_html($FormId, true);
            }
        }
    }
}
if (!function_exists('infusionsoft_get_webform_html')) {
    function infusionsoft_get_webform_html($FormId, $Force = false)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');

        $WebformHTML = manual_cache_loader('infusionsoft_get_webform_html-'.$FormId);
        $WebformHTML = $Force == true ? false:json_decode($WebformHTML);
        if($WebformHTML == false) {
            $action = "webform_html_is";
            $action_details = '{"formid":"' . $FormId . '"}';
            $WebformHTML = applyFn('rucksack_request',$action, $action_details);
            manual_cache_writer('infusionsoft_get_webform_html-'.$FormId, json_encode($WebformHTML), 0);
        }
        return $WebformHTML->message;
    }
}
function manual_cache_delete($File){
    $APPPATH = APPPATH;
    $CachePath = $APPPATH . "cache/".$File;
    return file_exists($CachePath) ? unlink($CachePath) : FALSE;
}
function manual_cache_writer($File,$Content,$ttl=0){
    $APPPATH = APPPATH;
    $CachePath = $APPPATH . "cache/".$File;
    $jsonContent = [
        'time'=>time(),
        'last_access'=>time(),
        'ttl'=>$ttl,
        'data'=> $Content
        ];
    $jsonContent = serialize($jsonContent);
    $saved = @file_put_contents($CachePath, $jsonContent);
    return $saved;
}
function manual_cache_loader($File){
    $APPPATH = APPPATH;
    $CachePath = $APPPATH . "cache/".$File;
    $CacheContent = @file_get_contents($CachePath);
    if ($CacheContent) {
        $CacheContent = unserialize($CacheContent);
        $ttl = $CacheContent['ttl'];
        $time = $CacheContent['time'];
        $theTime = time();
        $lapse = $theTime - $time;
        if($ttl == 0){
            return $CacheContent['data'];
        }
        if ($lapse < $ttl) {
            $CacheContent['last_access'] = time();
            $jsonContent = serialize($CacheContent);
            @file_put_contents($CachePath, $jsonContent);
            return $CacheContent['data'];
        } else {
            unlink($CachePath);
            return false;
        }

    }else{
        return false;
    }
}
if (!function_exists('infusionsoft_get_settings')) {
    function infusionsoft_get_settings($Module, $Setting, $Force = false)
    {
        $Settings = [];
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $Settings = manual_cache_loader('infusionsoft_get_settings-'.$Module.'-'.$Setting);
        $Settings = $Force == true ? false : $Settings;
        if($Settings == false) {
            $action = "setting_is";
            $action_details = '{"module":"' . $Module . '","setting":"' . $Setting . '"}';
            $Settings = applyFn('rucksack_request',$action, $action_details);
            manual_cache_writer('infusionsoft_get_settings-'.$Module.'-'.$Setting, $Settings,0); // 1 week
        }
        return $Settings;
    }
}
if (!function_exists('infusionsoft_get_app_account_profile')) {
    function infusionsoft_get_app_account_profile( $Force = false)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $CampanyProfile = manual_cache_loader('infusionsoft_get_app_account_profile');
        $CampanyProfile = $Force == true ? false : $CampanyProfile;
        if($CampanyProfile == false) {
            $action = "restRetrieveCampanyProfile";
            $action_details = '{}';
            $CampanyProfile = applyFn('rucksack_request',$action, $action_details);
            manual_cache_writer('infusionsoft_get_app_account_profile', $CampanyProfile,0);
        }
        return $CampanyProfile;
    }
}
if (!function_exists('infusionsoft_parse_call_notes')) {
    function infusionsoft_parse_call_notes($notes, $noteType = 'call_notes', $text = array())
    {
        $return = [];
        foreach ($notes as $key => $value) {
            $theVal = base64_decode($value) != false ? base64_decode($value) : $value;
            $Identity = $noteType == 'call_notes' ? '<i class="fa fa-volume-control-phone" aria-hidden="true"></i>' : '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>';
            $rows = '<div class="col-md-12"><span class="noteLabel"><small><span class="show-editor   btn-link">' . $Identity . ' ' . $text['text_edit_note'] . '</span> <span class="close-editing  btn-link"><i class="fa fa-floppy-o" aria-hidden="true"></i> ' . $text['text_save_notes'] . '</span><span class="cancel-editing  btn-link"><i class="fa fa-stop-circle-o" aria-hidden="true"></i> ' . $text['text_cancel'] . '</span></small></span><div id="noteTextRich' . rand() . '" class="' . $theClass . ' noteTextRich" data-tinymce="true">' . $theVal . '</div></div>';
            $return['rows'] = $rows;
        }
        return $return;
    }
}


if (!function_exists('infusionsoft_get_contact_by_contact_group')) {
    function infusionsoft_get_contact_by_contact_group($GroupId, $Fields = array(), $Force = false)
    {
        /*GET CONTACT NOTES*/
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');

        $ValidContacts = manual_cache_loader('infusionsoft_get_contact_by_contact_group');
        $ValidContacts = $Force == true ? false:json_decode($ValidContacts);

        if($ValidContacts == false) {

            $action = "query_is";
            if (!in_array('all', $Fields)) {
                $returnFields = $Fields;
            } else {
                $returnFields = json_decode($CI->config->item('contact_infusionsoft_return_fields'), true);
            }
            $returnFieldsStr = implode(', ', $returnFields);
            $tempResults = 1000;
            $Contacts = [];
            $page = 0;
            while ($tempResults >= 1000) {
                $action_details = '{"table":"Contact","limit":"1000","page":' . $page . ',"fields":[' . $returnFieldsStr . '],"query":{"Groups":"%' . $GroupId . '%"}}';
                $temp = applyFn('rucksack_request',$action, $action_details);
                $tempResults = sizeof($temp->message);
                $Contacts = array_merge($Contacts, $temp->message);
                $page++;
            }
            $ValidContacts = [];
            foreach ($Contacts as $Contact) {
                $ContactGroupArr = explode(',', $Contact->Groups);
                if (in_array($GroupId, $ContactGroupArr)) $ValidContacts[$Contact->Id] = $Contact;
            }
            manual_cache_writer('infusionsoft_get_contact_by_contact_group', json_encode($ValidContacts), 0);
        }
        return $ValidContacts;

    }
}
if (!function_exists('infusionsoft_get_all_saved_search')) {
    function infusionsoft_get_all_saved_search($Force = false)
    {
        /*GET CONTACT NOTES*/
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');

        $AllSavedSearch = manual_cache_loader('infusionsoft_get_all_saved_search');
        $AllSavedSearch = $Force == true ? false:json_decode($AllSavedSearch);

        if($AllSavedSearch == false) {

            $action = "query_is";
            $returnFields = array(
                '"FilterName"',
                '"Id"',
                '"ReportStoredName"',
                '"UserId"'
            );
            $AllSavedSearch = [];
            $returnFieldsStr = implode(', ', $returnFields);
            $tempResults = 1000;
            $count = $page= 0;
            while ($tempResults >= 1000) {
                $action_details = '{"table":"SavedFilter","limit":"1000","page":' . $page . ',"fields":[' . $returnFieldsStr . '],"query":{"UserId":"~<>~-1"}}';
                $temp = applyFn('rucksack_request',$action, $action_details);
                $tempResults = sizeof($temp->message);
                $AllSavedSearch = array_merge($AllSavedSearch, $temp->message);
                $page++;
                $count++;
            }
            manual_cache_writer('infusionsoft_get_all_saved_search', json_encode($AllSavedSearch), 10);
        }
        return $AllSavedSearch;
    }
}
if (!function_exists('infusionsoft_get_saved_search_columns')) {
    function infusionsoft_get_saved_search_columns($savedSearchID, $userID)
    {
        /*GET CONTACT Searched*/
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $action = "saved_search_columns_is";

        $action_details = '{"savedSearchID":' . $savedSearchID . ',"userID":' . $userID . ',"pageNumber":0}';
        $ContactsArr = applyFn('rucksack_request',$action, $action_details);
        $Columns = $ContactsArr->message;
        return $Columns;
    }
}
if (!function_exists('infusionsoft_get_all_saved_search_contact_report')) {
    function infusionsoft_get_all_saved_search_contact_report($savedSearchID, $userID, $Group, $pageNumber = 0)
    {
        /*GET CONTACT Searched*/
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $action = "saved_search_report_is";
        $Continue = true;
        $pageNumber = 0;
        $Contacts = [];
        while ($Continue){
            $action_details = '{"savedSearchID":' . $savedSearchID . ',"userID":' . $userID . ',"pageNumber":'.$pageNumber.'}';
            $ContactsArr = applyFn('rucksack_request',$action, $action_details);
            $Contacts = array_merge($Contacts,$ContactsArr->message);
            $pageNumber++;
            if(sizeof($ContactsArr->message) < 1000) $Continue = false;
        }
        $Contacts = mapSavedSearchResults($Contacts, $Group, $savedSearchID, $userID);
        return $Contacts;
    }
}
if (!function_exists('infusionsoft_get_all_saved_search_contact')) {
    function infusionsoft_get_all_saved_search_contact($savedSearchID, $userID, $pageNumber = 0)
    {
        /*GET CONTACT Searched*/
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $action = "query_is";
        $returnFields = array(
            '"FilterName"',
            '"Id"',
            '"ReportStoredName"',
            '"UserId"'
        );

        $returnFieldsStr = implode(', ', $returnFields);
        $action_details = '{"table":"SavedFilter","limit":"1000","page":0,"fields":[' . $returnFieldsStr . '],"query":{"Id":"%"}}';
        $ContactArr = applyFn('rucksack_request',$action, $action_details);
        $Contact = $ContactArr->message;
        return $Contact;
    }
}
if (!function_exists('infusionsoft_get_contact_CF_by_id')) {
    function infusionsoft_get_contact_CF_by_id($Id, $Fields = array())
    {
        /*GET CONTACT NOTES*/
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $action = "query_is";
        if (!in_array('all', $Fields)) {
            $returnFields = $Fields;
        } else {
            $returnFields = json_decode($CI->config->item('contact_infusionsoft_return_fields'), true);
        }

        $returnFieldsStr = implode(', ', $returnFields);
        $action_details = '{"table":"Contact","limit":"1000","page":0,"fields":[' . $returnFieldsStr . '],"query":{"Id":"' . $Id . '"}}';
        $ContactArr = applyFn('rucksack_request',$action, $action_details);
        $Contact = $ContactArr->message[0];

        return $Contact;
    }
}
if (!function_exists('infusionsoft_get_contact_by_id_arr')) {
    function infusionsoft_get_contact_by_id_arr($Id = array(), $Fields = array())
    {
        $RSquery = array();
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $action = "query_is";
        if (!in_array('all', $Fields)) {
            $returnFields = $Fields;
        } else {
            $returnFields = json_decode($CI->config->item('contact_infusionsoft_return_fields'), true);
        }
        $returnFields[] = '"TimeZone"';
        $returnFields[] = '"ContactNotes"';
        $returnFields[] = '"EmailAddress2"';
        $returnFields[] = '"EmailAddress3"';

        //Lets Add Custom Fields
        $CFObjs = infusionsoft_get_custom_fields( "%", false, -1);
        foreach ($CFObjs as $CFObj){
            $returnFields[] = '"_'.$CFObj->Name.'"';
        }
        $returnFieldsStr = implode(', ', $returnFields);
        $Contact = array();
        foreach ($Id as $ContactId) {
            $action_details = '{"table":"Contact","limit":"1000","page":0,"fields":[' . $returnFieldsStr . '],"query":{"Id":' . $ContactId . '}}';
            $ContactArr = applyFn('rucksack_request',$action, $action_details, "sherpa", false);
            $Contact = uniquifyBy($Contact, $ContactArr->message);
        }


        //Use search cache

        return $Contact;
    }
}
if (!function_exists('infusionsoft_get_contact_by_id_simple')) {
    function infusionsoft_get_contact_by_id_simple($Id, $Fields = array('"FirstName"','"LastName"','"Email"'))
    {
        /*GET CONTACT NOTES*/
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $action = "query_is";
        $Fields = implode(', ', $Fields);
        $action_details = '{"table":"Contact","limit":"1000","page":0,"fields":[' . $Fields . '],"query":{"Id":"' . $Id . '"}}';
        $ContactArr = applyFn('rucksack_request',$action, $action_details);
        $Contact = $ContactArr->message;
        return $Contact;
    }
}
if (!function_exists('infusionsoft_get_contact_by_id')) {
    function infusionsoft_get_contact_by_id($Id, $Fields = array(), $Force = false, $session_name = '')
    {
        /*GET CONTACT NOTES*/
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $user_seession_data = macanta_get_user_seession_data($session_name);
        $session_data = $user_seession_data == false ? []:unserialize($user_seession_data->session_data);
        $action = "query_is";

        $Contact = manual_cache_loader('infusionsoft_get_contact_by_id_'.$Id);
        $Contact = $Force == true ? false:json_decode($Contact);
        if($Contact == false) {
            if (!in_array('all', $Fields)) {
                $returnFields = $Fields;
            }
            else {
                $returnFields = json_decode($CI->config->item('contact_infusionsoft_return_fields'), true);
            }
            $returnFields[] = '"TimeZone"';
            $returnFields[] = '"ContactNotes"';
            $returnFields[] = '"EmailAddress2"';
            $returnFields[] = '"EmailAddress3"';

            $returnFields[] = '"Address3Street1"';
            $returnFields[] = '"Address3Street2"';
            $returnFields[] = '"Address3Type"';
            $returnFields[] = '"City3"';
            $returnFields[] = '"State3"';
            $returnFields[] = '"Country3"';
            $returnFields[] = '"PostalCode3"';

            //Use search cache
            $searched_cache = manual_cache_loader('searched_cache' . $session_data['InfusionsoftID']);
            $selected_searched_cache = manual_cache_loader('selected_searched_cache' . $session_data['InfusionsoftID']);
            // Due to contact field discrepancy i will disable this cache features
            // Or Enable when the search came from normal search, when $selected_searched_cache is not existing
            if ($searched_cache && !$selected_searched_cache && macanta_has_user_last_action($session_name, 'WebFormSubmitted') == false) {
                $searched_contacts = json_decode($searched_cache);
                foreach ($searched_contacts as $searched_contact) {
                    if ($searched_contact->Id == $Id) {
                        $Contact = $searched_contact;
                        break;
                    }
                }
            }else {
                macanta_remove_user_last_action($session_name, 'WebFormSubmitted');
                //Lets Add Custom Fields
                $CFObjs = infusionsoft_get_custom_fields( "%", true, -1);
                foreach ($CFObjs as $CFObj){
                    $returnFields[] = '"_'.$CFObj->Name.'"';
                }
                $returnFieldsStr = implode(', ', $returnFields);
                $action_details = '{"table":"Contact","limit":"1000","page":0,"fields":[' . $returnFieldsStr . '],"query":{"Id":"' . $Id . '"}}';

                $ContactArr = applyFn('rucksack_request',$action, $action_details);
                $Contact = $ContactArr->message[0];
            }
            if ($Contact === false || (!isset($Contact->Email) && !isset($Contact->FirstName))) {
                //Lets Add Custom Fields
                $CFObjs = infusionsoft_get_custom_fields( "%", true, -1);
                foreach ($CFObjs as $CFObj){
                    $returnFields[] = '"_'.$CFObj->Name.'"';
                }
                $returnFieldsStr = implode(', ', $returnFields);
                $action_details = '{"table":"Contact","limit":"1000","page":0,"fields":[' . $returnFieldsStr . '],"query":{"Id":"' . $Id . '"}}';

                $ContactArr = applyFn('rucksack_request',$action, $action_details);
                $Contact = $ContactArr->message[0];
            }
            //$ContactArr = applyFn('rucksack_request',$action, $action_details );
            //$Contact = $ContactArr->message[0];
            if (isset($Contact->Country)) {
                $d_code = getCountryCode($Contact->Country);
            }
            else {
                $d_code = $CI->config->item('country_code');
            }
            if (isset($Contact->Phone1)) {
                $Phone1Info = SanitizePhone($Contact->Phone1);
                $Thecode = $Phone1Info["Code"] != '' ? $Phone1Info["Code"] : $d_code;
                $Contact->Phone1Twilio = $Thecode . $Phone1Info["Phone"];
            }
            if (isset($Contact->Phone2)) {
                $Phone2Info = SanitizePhone($Contact->Phone2);
                $Thecode = $Phone2Info["Code"] != '' ? $Phone2Info["Code"] : $d_code;
                $Contact->Phone2Twilio = $Thecode . $Phone2Info["Phone"];;
            }

            manual_cache_writer('infusionsoft_get_contact_by_id_'.$Id, json_encode($Contact), 15);
        }
        return $Contact;
    }
}

if (!function_exists('infusionsoft_get_user')) {
    function infusionsoft_get_user($Id = '%', $Force = false)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $Users = manual_cache_loader('infusionsoft_get_user');
        $Users = $Force == true ? false:json_decode($Users);
        if($Users == false) {
            $action = "query_is";
            $returnFields = [];
            $returnFields[] = '"Id"';
            $returnFields[] = '"FirstName"';
            $returnFields[] = '"LastName"';
            $returnFields[] = '"Email"';
            $returnFieldsStr = implode(', ', $returnFields);
            $action_details = '{"table":"User","limit":"1000","page":0,"fields":[' . $returnFieldsStr . '],"query":{"Id":"' . $Id . '"}}';
            $Users = applyFn('rucksack_request', $action, $action_details);
            manual_cache_writer('infusionsoft_get_user', json_encode($Users), 0);
        }
        if($Id == '%'){
            return $Users->message;
        }else{
            foreach ($Users->message as $User){
                if($User->Id == $Id ) return $User;
            }
        }
        return false;
    }
}
if (!function_exists('infusionsoft_get_macanta_user')) {
    function infusionsoft_get_macanta_user($Force = false)
    {
        /*GET CONTACT NOTES*/
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $FilteredContacts = [];
        $access_level = json_decode($CI->config->item('access_level'), true);
        $ContactTag = $access_level['staff'];

        $finalArray = manual_cache_loader('infusionsoft_get_macanta_user');
        $finalArray = $Force == true ? false:json_decode($finalArray);
        if($finalArray == false) {
            $action = "query_is";
            $returnFields = [];
            $returnFields[] = '"Id"';
            $returnFields[] = '"FirstName"';
            $returnFields[] = '"LastName"';
            $returnFields[] = '"Email"';
            $returnFields[] = '"Groups"';
            $returnFieldsStr = implode(', ', $returnFields);
            $action_details = '{"table":"Contact","limit":"1000","page":0,"fields":[' . $returnFieldsStr . '],"query":{"Groups":"%' . $ContactTag . '%"}}';
            $Users = applyFn('rucksack_request',$action, $action_details);
            foreach ($Users->message as $Contact){
                $ContactGroups = explode(',', $Contact->Groups);
                if(in_array($ContactTag, $ContactGroups)){
                    $FilteredContacts[]=$Contact;
                }
            }
            $finalArray = sortMultiArray($FilteredContacts, 'FirstName');
                manual_cache_writer('infusionsoft_get_macanta_user', json_encode($finalArray), 15);
            }
        return $finalArray;
    }
}
function get_opp_stages($Id = '%', $Force = false)
{
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('macanta_helper');
    $Stage = manual_cache_loader('get_opp_stages');
    $Stage = $Force == true ? false:json_decode($Stage);
    if($Stage == false) {
        $action = "query_is";
        $returnFields[] = '"Id"';
        $returnFields[] = '"StageName"';
        $returnFields[] = '"StageOrder"';
        $returnFields[] = '"TargetNumDays"';
        $returnFieldsStr = implode(', ', $returnFields);
        $action_details = '{"table":"Stage","limit":"1000","page":0,"fields":[' . $returnFieldsStr . '],"query":{"Id":"' . $Id . '"}, "orderby":"StageOrder"}';
        $StageArr = applyFn('rucksack_request',$action, $action_details);
        $Stage = $StageArr->message;
        manual_cache_writer('get_opp_stages', json_encode($Stage), 0);
    }
    return $Stage;
}

function get_opportunities_by_id($Id = '%')
{
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('macanta_helper');
    $action = "query_is";
    $returnFields[] = '"CreatedBy"';
    $returnFields[] = '"DateCreated"';
    $returnFields[] = '"DateInStage"';
    $returnFields[] = '"EstimatedCloseDate"';
    $returnFields[] = '"Id"';
    $returnFields[] = '"Leadsource"';
    $returnFields[] = '"NextActionDate"';
    $returnFields[] = '"NextActionNotes"';
    $returnFields[] = '"OpportunityNotes"';
    $returnFields[] = '"OpportunityTitle"';
    $returnFields[] = '"StageID"';
    $returnFields[] = '"UserID"';
    $OppsCustomFields =  infusionsoft_opps_custom_fields();
    foreach ($OppsCustomFields as $GroupName => $OppsCustomFieldObj){
        foreach ($OppsCustomFieldObj as $OppsCustomFieldName =>$OppsCustomFieldDetails){
            $returnFields[] = '"_' . $OppsCustomFieldDetails->Name . '"';
        }
    }
    $returnFieldsStr = implode(', ', $returnFields);
    $action_details = '{"table":"Lead","limit":"1000","page":0,"fields":[' . $returnFieldsStr . '],"query":{"Id":"' . $Id . '"}}';
    $OppsArr = applyFn('rucksack_request',$action, $action_details);
    $Opps = $OppsArr->message;
    return $Opps;
}

function get_opportunities($ContactId, $UserOnly, $Force = false,$session_data=[])
{

    $UserOnly = strtolower($UserOnly);
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('macanta_helper');
    $File = 'get_opportunities_'.$ContactId."_".$UserOnly;
    $Opps = manual_cache_loader($File);
    $Opps = $Force == true ? false:json_decode($Opps);
    if($Opps == false) {
        $action = "query_is";
        $UserKeyValuePairs = [];
        $returnFields[] = '"CreatedBy"';
        $returnFields[] = '"DateCreated"';
        $returnFields[] = '"DateInStage"';
        $returnFields[] = '"EstimatedCloseDate"';
        $returnFields[] = '"Id"';
        $returnFields[] = '"Leadsource"';
        $returnFields[] = '"NextActionDate"';
        $returnFields[] = '"NextActionNotes"';
        $returnFields[] = '"OpportunityNotes"';
        $returnFields[] = '"OpportunityTitle"';
        $returnFields[] = '"StageID"';
        $returnFields[] = '"UserID"';
        $CustomFieldAssignedTo = "_";
        $OppCustomFieldForMacanta = applyFn('infusionsoft_get_custom_fields_by_label',-4, 'Assigned To');

        if (sizeof($OppCustomFieldForMacanta) > 0) {
            $CustomFieldAssignedTo = $OppCustomFieldForMacanta[0]->Name;
            $returnFields[] = '"_' . $CustomFieldAssignedTo . '"';
        }
        $OppsCustomFields =  applyFn('infusionsoft_opps_custom_fields');
        foreach ($OppsCustomFields as $GroupName => $OppsCustomFieldObj){
            foreach ($OppsCustomFieldObj as $OppsCustomFieldName =>$OppsCustomFieldDetails){
                $returnFields[] = '"_' . $OppsCustomFieldDetails->Name . '"';
            }
        }
        $returnFieldsStr = implode(', ', $returnFields);
        $action_details = '{"table":"Lead","limit":"1000","page":0,"fields":[' . $returnFieldsStr . '],"query":{"ContactID":"' . $ContactId . '"}}';
        $OppsArr = applyFn('rucksack_request',$action, $action_details);
        $Opps = $OppsArr->message;
        if ($UserOnly !== 'no' && $UserOnly === 'yes') {
            foreach ($Opps as $OppKey => $Opp) {
                if (!array_key_exists($Opp->UserID, $UserKeyValuePairs)) {
                    $User = applyFn('infusionsoft_get_user',$Opp->UserID);
                    $UserKeyValuePairs[$Opp->UserID] = $User->Email;

                }
                $UserEmail = $UserKeyValuePairs[$Opp->UserID];
                if ($session_data['email'] !== $UserEmail) {
                    if (isset($Opp->$CustomFieldAssignedTo) && $Opp->$CustomFieldAssignedTo == $session_data['email']) {
                        // looged in user is allowed to view Opps
                    } else {
                        unset($Opps[$OppKey]);
                    }

                }

            }

        }
        manual_cache_writer($File, json_encode($Opps), 15);

    }
    return $Opps;
}
if (!function_exists('infusionsoft_refresh_get_opportunities')) {
    function infusionsoft_refresh_get_opportunities($ContactID = false,$session_data)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $CachePath = APPPATH . "cache/";

        $FolderContent = scandir($CachePath);
        $FileNamePart     = 'get_opportunities_';
        foreach($FolderContent as $Item){
            if(strpos(strtolower($Item), $FileNamePart) !== false){
                $FileNameParts = explode("_", $Item);
                $ContactId = $FileNameParts[2];
                $UserOnly = $FileNameParts[3];
                if($ContactID !== false) {
                    if($ContactId!=$ContactID) continue;
                }
                get_opportunities($ContactId, $UserOnly, true,$session_data);
            }
        }
    }
}
function ifPhone($searchKey)
{
    $Search =[' ','-','(',')'];
    $searchKey = str_replace($Search, '', $searchKey);
    if (is_numeric($searchKey)) {
        return true;
    } else {
        return false;
    }
}

function uniquifyBy($arrayA, $arrayB, $field = 'Id')
{
    set_time_limit(86400);
    ini_set("memory_limit", "1024M");
    $duplicate = false;
    $tempArray = array();
    $searchLimit = 50;
    if (sizeof($arrayA) == 0) return $arrayB;
    if (sizeof($arrayB) == 0) return $arrayA;
    foreach ($arrayA as $fieldsA) {
        $tempArray[] = $fieldsA;
        foreach ($arrayB as $fieldsB) {
            if ($fieldsA->$field != $fieldsB->$field) {
                $tempArray[] = $fieldsB;
            }
        }
        $tempArray = array_map('unserialize', array_unique(array_map('serialize', $tempArray)));
        /*if(sizeof($tempArray) >= $searchLimit):
            echo '<!--';
            print_r($tempArray);
            echo ' -->';
            die('
        Error: Too many results (more than '.sizeof($tempArray).'), Please narrow down your search
        <script>jQuery("#loading").fadeOut();</script>

        ');

        endif;*/
    }

    return $tempArray;
}

function sortMultiArray($arrayVal, $field)
{
    $tmp = array();
    foreach ($arrayVal as &$ma)
        $tmp[] = $ma->$field;
    array_multisort($tmp, $arrayVal);
    return $arrayVal;
}

function mapSavedSearchResults($Results, $Group, $savedSearchID, $userID)
{
    $NewResults = array();
    $map = infusionsoft_get_saved_search_columns($savedSearchID, $userID);
    /* if($Group == 'AdvContactSearch'){
        $map = array(
            "Id" => "Id",
            "ContactName" => "Name",
            "ContactName.firstName" => "First name",
            "ContactName.lastName" => "Last name",
            "SpouseName" => "Spouse name",
            "Birthday" => "Birthday",
            "BirthdayMonth" => "Birthday month",
            "BirthdayYear" => "Birthday year",
            "CompanyInfo" => "Company",
            "PhoneWithExtension1" => "Phone 1",
            "PhoneWithExtension2" => "Phone 2",
            "PhoneWithExtension3" => "Phone 3",
            "Email" => "Email",
            "EmailAddress2" => "Email address 2",
            "EmailAddress3" => "Email address 3",
            "FaxWithType1" => "Fax 1",
            "FaxWithType2" => "Fax 2",
            "StreetAddress1" => "Street address 1",
            "StreetAddress2" => "Street address 2",
            "City" => "City",
            "State" => "State",
            "PostalCodePlusZipFour1" => "Postal code",
            "Country" => "Country",
            "Address2Street1" => "Street address 1 (shipping)",
            "Address2Street2" => "Street address 2 (shipping)",
            "City2" => "City (shipping)",
            "State2" => "State (shipping)",
            "PostalCodePlusZipFour2" => "Postal code (shipping)",
            "Country2" => "Country (shipping)",
            "TwitterSocialAccountName" => "Twitter",
            "FacebookSocialAccountName" => "Facebook",
            "LinkedInSocialAccountName" => "Linkedin",
            "Address3Street1" => "Street address 1 (optional)",
            "Address3Street2" => "Street address 2 (optional)",
            "City3" => "City (optional)",
            "State3" => "State (optional)",
            "PostalCodePlusZipFour3" => "Postal code (optional)",
            "Country3" => "Country (optional)",
            "PhoneWithExtension4" => "Phone 4",
            "PhoneWithExtension5" => "Phone 5",
            "Tags" => "Tag ids",
            "TagNames" => "Tags",
            "ContactType" => "Person type",
            "JobTitle" => "Job title",
            "Website" => "Website",
            "LastSSN" => "Last 4 ssn",
            "MiddleName" => "Middle name",
            "Nickname" => "Nickname",
            "Username" => "Username",
            "Password" => "Password",
            "AssistantName" => "Assistant name",
            "AssistantPhone" => "Assistant phone",
            "Title" => "Title",
            "Suffix" => "Suffix",
            "Anniversary" => "Anniversary",
            "AnniversaryMonth" => "Anniversary month",
            "AnniversaryYear" => "Anniversary year",
            "CreatedBy" => "Created by",
            "DateCreated" => "Date created",
            "LastUpdated" => "Last updated",
            "OwnerID" => "Ownerid",
            "ContactNotes" => "Notes",
            "Owner" => "Owner",
            "LeadSourceIdOverride" => "Lead Source",
            "LeadSourceCategoryIdOverride" => "Lead source category",
            "ReferralCode" => "Referral code",
            "TimeZone" => "Time zone",
            "Language" => "Language",
            "Custom_macantaInstallHash" => "Macanta install hash",
            "Custom_macantaURL" => "Macanta url",
            "Custom_macantaUserTag" => "Macanta user tag",
            "Custom_macantaAdminTag" => "Macanta admin tag",
            "ScoreId1" => "Lead score"
        );

    }elseif($Group == 'OpportunitySearch'){
        $map = array(
            "Id" => "Id",
            "ContactId" => "Contactid",
            "OpportunityId" => "Opportunityid",
            "OpportunityTitle" => "Opportunity",
            "linkedContact" => "Contact name",
            "linkedContact.linkedEntity.phoneWithExtension1" => "Phone 1",
            "BatchEmailAddress" => "Batch email address",
            "linkedContact.linkedEntity.contactName.firstName" => "First name",
            "linkedContact.linkedEntity.contactName.lastName" => "Last name",
            "linkedContact.linkedEntity.streetAddress1" => "Street address 1",
            "linkedContact.linkedEntity.city" => "City",
            "linkedContact.linkedEntity.state" => "State",
            "linkedContact.linkedEntity.postalCodePlusZipFour1" => "Postal code",
            "linkedContact.linkedEntity.leadSourceIdOverride" => "Lead Source",
            "UserID" => "Owner",
            "OpportunityStage.stageID" => "Stage",
            "NextActionDate" => "Next action date",
            "NextActionNotes" => "Next action notes",
            "OpportunityNotes" => "Opportunity notes",
            "#Camp" => "Campaigns",
            "Budget" => "Budget",
            "ProjectedRevenueHigh" => "Projected revenue high",
            "ProjectedRevenueLow" => "Projected revenue low",
            "ProductInterestId" => "Product interest id",
            "ProductInterest" => "Product interest",
            "MvDate" => "Move date",
            "TargetMoveDate" => "Target move date",
            "OD" => "Od",
            "DateCreated" => "Date created",
            "LastUpdated" => "Last updated",
            "EstimatedCloseDateWithForecast.estimatedCloseDate" => "Estimated close date",
            "EstimatedCloseDateWithForecast.includeInForecast" => "Include in forecast",
            "FreeTrialDays" => "Free trial days",
            "PayPlanId" => "Pay plan id",
            "OfferExpires" => "Offer expires",
            "OrderRevenue" => "Order revenue",
            "MonthlyRevenue" => "Monthly revenue",
            "CloseDate" => "Close date",
            "OpportunityStage.winReason" => "Win reason",
            "OpportunityStage.lossReason" => "Loss reason",
            "Tags" => "Tag ids",
            "ScoreId1" => "Lead score"
        );
    }elseif($Group == 'FunnelGoalAchieved'){
        $map = array(
            "Id" => "Id",
            "ContactId" => "Contact Id",
            "ContactName" => "Name",
            "ContactName.firstName" => "First name",
            "ContactName.lastName" => "Last name",
            "SpouseName" => "Spouse name",
            "Birthday" => "Birthday",
            "BirthdayMonth" => "Birthday month",
            "BirthdayYear" => "Birthday year",
            "CompanyInfo" => "Company",
            "PhoneWithExtension1" => "Phone 1",
            "PhoneWithExtension2" => "Phone 2",
            "PhoneWithExtension3" => "Phone 3",
            "Email" => "Email",
            "EmailAddress2" => "Email address 2",
            "EmailAddress3" => "Email address 3",
            "FaxWithType1" => "Fax 1",
            "FaxWithType2" => "Fax 2",
            "StreetAddress1" => "Street address 1",
            "StreetAddress2" => "Street address 2",
            "City" => "City",
            "State" => "State",
            "PostalCodePlusZipFour1" => "Postal code",
            "Country" => "Country",
            "Address2Street1" => "Street address 1 (shipping)",
            "Address2Street2" => "Street address 2 (shipping)",
            "City2" => "City (shipping)",
            "State2" => "State (shipping)",
            "PostalCodePlusZipFour2" => "Postal code (shipping)",
            "Country2" => "Country (shipping)",
            "Address3Street1" => "Street address 1 (optional)",
            "Address3Street2" => "Street address 2 (optional)",
            "City3" => "City (optional)",
            "State3" => "State (optional)",
            "PostalCodePlusZipFour3" => "Postal code (optional)",
            "Country3" => "Country (optional)",
            "PhoneWithExtension4" => "Phone 4",
            "PhoneWithExtension5" => "Phone 5",
            "Tags" => "Tag ids",
            "ContactType" => "Person type",
            "JobTitle" => "Job title",
            "Website" => "Website",
            "LastSSN" => "Last 4 ssn",
            "MiddleName" => "Middle name",
            "Nickname" => "Nickname",
            "Username" => "Username",
            "Password" => "Password",
            "AssistantName" => "Assistant name",
            "AssistantPhone" => "Assistant phone",
            "Title" => "Title",
            "Suffix" => "Suffix",
            "Anniversary" => "Anniversary",
            "AnniversaryMonth" => "Anniversary month",
            "AnniversaryYear" => "Anniversary year",
            "CreatedBy" => "Created by",
            "DateCreated" => "Date created",
            "LastUpdated" => "Last updated",
            "OwnerID" => "Ownerid",
            "ContactNotes" => "Notes",
            "LeadSourceIdOverride" => "Lead Source",
            "LeadSourceCategoryIdOverride" => "Lead source category",
            "ReferralCode" => "Referral code",
            "ScoreId1" => "Lead score",
            "Custom_macantaInstallHash" => "Macanta install hash",
            "Custom_macantaURL" => "Macanta url",
            "Custom_macantaUserTag" => "Macanta user tag",
            "Custom_macantaAdminTag" => "Macanta admin tag",
            "CampaignBuilderId" => "Funnel.goalachieved.funnel.campaignbuilderid",
            "funnel.goalachieved.funnel.name" => "Campaign Name",
            "funnel.goalachieved.goal.name" => "Goal Name",
            "funnel.goalachieved.date.achieved" => "Goal Completion Date"
        );
    }*/

    $TableColumn = array();
    foreach ($Results as $intKey => $Result) {
        foreach ($Result as $origKey => $theValue) {
            $theValue = is_object($theValue) ? parseObjectField($origKey, $theValue) : $theValue;
            $newKey = $map->$origKey;
            $NewResults[$intKey][$newKey] = $theValue;
            $TableColumn[$newKey] = true;
        }
    }

    $return['NewResults'] = $NewResults;
    $return['TableColumn'] = $TableColumn;
    return $return;
}

function parseObjectField($Field, $Value)
{
    $newValue = '';
    if (isset($Value->date)) {
        $newValue = date("d M Y", strtotime($Value->date));
    } else {
        $newValue = json_encode($Value->date);
    }

    return $newValue;

}

function SanitizePhone($Phone, $d_code = '')
{
    $Phone = trim($Phone);
    if (substr($Phone, 0, 1) == '0') {
        $Phone = substr($Phone, 1);
    }
    $UncantedChars = array(" ", "(", ")", "-");
    $Phone = str_replace($UncantedChars, "", $Phone);
    $PhoneInfo = removeCountryCode($Phone);
    $Phone = $PhoneInfo['Phone'];
    $TheCode = $PhoneInfo['Code'];
    $Phone = str_replace('+', "", $Phone);

    $PhoneInfo = array("Code" => $TheCode, "Phone" => $Phone);
    return $PhoneInfo;
}

function getUserRestrictionTagsByCatId($UserGroups = '', $DBKey = '')
{
    $CI =& get_instance();
    $CategoryId = $CI->config->item($DBKey);
    $CategoryTags = $CategoryId ? infusionsoft_get_tags_by_cat_id((int)$CategoryId) : array();
    $UserTags = $UserGroups != '' ? explode(',', $UserGroups) : array();
    $CommonTags = array();
    foreach ($CategoryTags as $Tags) {
        if (in_array($Tags->Id, $UserTags)) $CommonTags[] = $Tags->Id;
    }
    return $CommonTags;

}

function hasSavedSearchTag($SavedFilterId,$groups)
{
    $CI =& get_instance();
    $TagJson = $CI->config->item('saved_search_restriction');
    $UserTagStr = $groups;
    $UserTagArr = $UserTagStr != '' ? explode(',', $UserTagStr) : array();
    if ($TagJson) {
        $TagObj = json_decode($TagJson, true);
        if ($TagObj != null) {
            if (isset($TagObj[$SavedFilterId])) {
                $SavedFilterTag = $TagObj[$SavedFilterId];
                if (in_array($SavedFilterTag, $UserTagArr)) {
                    return true;
                } else {
                    return false;
                }

            } else {
                return false;
            }
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function handleMergedFields($ContactId, $Content,$session_name){

    $Contact = infusionsoft_get_contact_by_id($ContactId, array(), false, $session_name);
    $SearchArr = [];
    $ValuehArr = [];
    foreach ($Contact as $Key => $Value){
        $SearchArr[]='~Contact.'.$Key.'~';
        $ValuehArr[]=$Value;
    }
    $Content = str_replace($SearchArr,$ValuehArr,$Content);
    $Content = preg_replace("/~Contact\.(.+?)~/", "", $Content);
    return $Content;
}
function handleShortcodes($content, $shortcodes,$session_data,$ContactInfo)
{
    //Loop through all shortcodes
    foreach ($shortcodes as $key => $function) {
        $dat = array();
        preg_match_all("/\[" . $key . "(.+?)\]/", $content, $dat);
        //if(count($dat) > 0 && $dat[0] != array() && isset($dat[1])){
        if (count($dat) > 0 && $dat[0] != array()) {
            $i = 0;
            $actual_string = $dat[0];
            foreach ($dat[1] as $temp) {
                $temp = explode(" ", $temp);
                $params = array();
                foreach ($temp as $d) {
                    @list($opt, $val) = explode("=", $d);
                    $params[$opt] = trim($val, '"');
                }
                $content = str_replace($actual_string[$i], $function($params,$session_data,$ContactInfo), $content);
                $i++;
            }
        }
    }
    return $content;
}

function generateRandomString($length = 10)
{
    return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
}

function GetBetween($content, $start, $end)
{
    $r = explode($start, $content);
    if (isset($r[1])) {
        $r = explode($end, $r[1]);
        return $r[0];
    }
    return '';
}

function checkContactRestriction($ContactGroupsStr,$session_data)
{
    $ContactGroupsArr = explode(',', $ContactGroupsStr);
    //if (sizeof($session_data['UserContactRestrictionTags']) > 0 && $session_data['userlevel'] != 'administrator') {
    if (sizeof($session_data['UserContactRestrictionTags']) > 0) {
        foreach ($session_data['UserContactRestrictionTags'] as $UserRestrictedTag) {
            if (in_array($UserRestrictedTag, $ContactGroupsArr)) return false;
        }
        return true;
    } else {
        return false;
    }
}

function filterContacts($finalArray = array(),$session_data)
{
    $CI =& get_instance();
    $NewArray = array();
    foreach ($finalArray as $Contact) {
        //remove searchec company
        if (isset($Contact->CompanyID)) {
            if ($Contact->CompanyID == $Contact->Id) continue; // skip this contact
        }
        if (checkContactRestriction($Contact->Groups,$session_data)) continue; // skip this contact when restricted
        $NewArray[] = $Contact;

    }
    return $NewArray;
}

if (!function_exists('infusionsoft_get_contact_by_note_tags')) {
    function infusionsoft_get_contact_by_note_tags($str,$session_data)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $tagsArr = explode(',', $str);
        $ContactIdArrTemp = $ContactIdArr = array();
        $finalArray = array();
        foreach ($tagsArr as $tag_item) {
            $CI->db->like('tag_slugs', $tag_item);
            $query = $CI->db->get('note_tags');
            foreach ($query->result() as $row) {
                if(!is_numeric($row->contact_id)) continue;
                $ContactIdArrTemp[$row->contact_id] = true;
            }
        }

        foreach ($ContactIdArrTemp as $keyId => $bool) {
            $ContactIdArr[] = (int) $keyId;
        }

        $ContactCache = manual_cache_loader('tagged_selected_contact_cache');
        $ContactCache = false;
        if ($ContactCache) {
            $SelectedContactCache = json_decode($ContactCache, true);
            foreach ($ContactIdArr as $ContactId) {
                if (array_key_exists($ContactId, $SelectedContactCache)) {
                    // Get this contact from cache
                    $finalArray[] = $SelectedContactCache[$ContactId];
                } else {
                    if (!$ContactId) continue;
                    $results = (array)infusionsoft_get_contact_by_id($ContactId, array('all'));
                    if (!$results['Id']) continue;
                    $finalArray[] = $results;
                }
            }



        } else {
            $finalArray = infusionsoft_get_contact_by_id_arr($ContactIdArr, array('all'));
            $finalArray = sortMultiArray($finalArray, 'FirstName');
        }


        // Filter contacts that has zuora contacts only, but the function modify to accept non zuora account
        $finalArray = filterContacts($finalArray,$session_data);
        return $finalArray;
    }
}
if (!function_exists('infusionsoft_get_contact_by_search_string')) {
    function infusionsoft_get_contact_by_search_string($str,$session_data)
    {
        set_time_limit(86400);
        ini_set("memory_limit", "1024M");
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $searchKey = trim(str_replace('default:', '', $str));
        $tempArray = array();
        $page = 0;
        $toSort = true;
        $action = "query_is";
        //$returnFields = json_decode($CI->config->item('search_infusionsoft_return_fields'), true);
        // get broader fields and use for contact data
        $returnFields = json_decode($CI->config->item('contact_infusionsoft_return_fields'), true);
        $returnFields[] = '"TimeZone"';
        $returnFields[] = '"ContactNotes"';
        $returnFields[] = '"EmailAddress2"';
        $returnFields[] = '"EmailAddress3"';
        $returnFields[] = '"Address3Street1"';
        $returnFields[] = '"Address3Street2"';
        $returnFields[] = '"Address3Type"';
        $returnFields[] = '"City3"';
        $returnFields[] = '"State3"';
        $returnFields[] = '"Country3"';
        $returnFields[] = '"PostalCode3"';
        //Lets Add Custom Fields
        $CFObjs = infusionsoft_get_custom_fields( "%", true, -1);
        foreach ($CFObjs as $CFObj){
            $returnFields[] = '"_'.$CFObj->Name.'"';
        }
        $returnFieldsStr = implode(', ', $returnFields);
        if (ifPhone($searchKey)) {
            $Search =['+',' ','-','(',')'];
            //$searchKey = str_replace($Search, '', $searchKey);
            $searchKey = str_replace($Search, '', $searchKey);
            $searchKeyTemp = str_split($searchKey);
            $searchKey = implode('%',$searchKeyTemp);
            $action_details = '{"table":"Contact","limit":"1000","page":' . $page . ',"fields":[' . $returnFieldsStr . '],"query":{"FirstName":"' . '%' . $searchKey . '%' . '"}}';
            $temp = applyFn('rucksack_request',$action, $action_details);
            $tempArray = uniquifyBy($tempArray, $temp->message);

            $action_details = '{"table":"Contact","limit":"1000","page":' . $page . ',"fields":[' . $returnFieldsStr . '],"query":{"Phone1":"' . '%' . $searchKey . '%' . '"}}';
            $temp = applyFn('rucksack_request',$action, $action_details);
            $tempArray = uniquifyBy($tempArray, $temp->message);

            $action_details = '{"table":"Contact","limit":"1000","page":' . $page . ',"fields":[' . $returnFieldsStr . '],"query":{"Phone2":"' . '%' . $searchKey . '%' . '"}}';
            $temp = applyFn('rucksack_request',$action, $action_details);
            $tempArray = uniquifyBy($tempArray, $temp->message);

            $action_details = '{"table":"Contact","limit":"1000","page":' . $page . ',"fields":[' . $returnFieldsStr . '],"query":{"Phone3":"' . '%' . $searchKey . '%' . '"}}';
            $temp = applyFn('rucksack_request',$action, $action_details);
            $tempArray = uniquifyBy($tempArray, $temp->message);

            $action_details = '{"table":"Contact","limit":"1000","page":' . $page . ',"fields":[' . $returnFieldsStr . '],"query":{"Groups":"' . '%' . $searchKey . '%' . '"}}';
            $temp = applyFn('rucksack_request',$action, $action_details);
            $tempArray = uniquifyBy($tempArray, $temp->message);
            $finalArray = $tempArray;
        }
        else {
            $searchKey = str_replace('*', '%', $searchKey);
            if (strpos($searchKey, 'connected:') !== false) {
                $searchKeyArr = explode(':', $searchKey);
                $theKey = trim($searchKeyArr[1]);
                $searchKeyArrKey = array_merge([$theKey], explode(' ', $theKey));
                $conencted = macanta_search_connected_info([$theKey]);
                return $conencted;

            }
            elseif (
                strpos($searchKey, 'email:') !== false ||
                strpos($searchKey, 'company:') !== false ||
                strpos($searchKey, 'address:') !== false)
            {
                $searchKeyArr = explode(':', $searchKey);
                // check it search string has a format e.g email:gmail or company:abc
                $Key = ucfirst($searchKeyArr[0]);
                if ($Key == "Address") {
                    set_time_limit(900);
                    ini_set("memory_limit", "1024M");
                    $toSort = false;
                    //try {
                    $TheSearchStrArr = explode(' ', $searchKeyArr[1]);
                    $AddressFields = ['StreetAddress1','PostalCode','City','State','Country','Address2Street1'];
                    while(sizeof($TheSearchStrArr) >= 1){
                        $TheSearchStrArrWildCard = implode("%",$TheSearchStrArr);
                        foreach ($AddressFields as $AddressField){
                            $action_details = '{"table":"Contact","limit":"1000","page":' . $page . ',"fields":[' . $returnFieldsStr . '],"query":{"'.$AddressField.'":"%'.$TheSearchStrArrWildCard.'%","FirstName":"~<>~"}}';
                            $temp = applyFn('rucksack_request',$action, $action_details);
                            $tempArray = uniquifyBy($tempArray, $temp->message);
                        }
                        $LastWord = array_pop($TheSearchStrArr);
                        if(sizeof($tempArray) > 0) break;
                    }


                    /*if(sizeof($tempArray) == 0){
                        foreach ($TheSearchStrArr as $TheSearchStr){
                            if(strlen($TheSearchStr) <= 2) continue;
                            foreach ($AddressFields as $AddressField){
                                $action_details = '{"table":"Contact","limit":"1000","page":' . $page . ',"fields":[' . $returnFieldsStr . '],"query":{"'.$AddressField.'":"%'.$TheSearchStr.'%","FirstName":"~<>~"}}';
                                $temp = applyFn('rucksack_request',$action, $action_details);
                                $tempArray = uniquifyBy($tempArray, $temp->message);
                                //$tempArray = array_merge($tempArray, $temp->message);
                            }
                        }
                    }*/

                   /* $AddressFieldKey = 0;
                    $tempResults = 1000;
                    $count = 0;
                    while ($tempResults >= 1000) {
                        $action_details = '{"table":"Contact","limit":"1000","page":' . $page . ',"fields":[' . $returnFieldsStr . '],"query":{"FirstName":"~<>~"}}';
                        $temp = applyFn('rucksack_request',$action, $action_details);
                        $tempResults = sizeof($temp->message);
                        //$tempArray = uniquifyBy($tempArray, $temp->message); // this make the script sooo slow
                        $tempArray = array_merge($tempArray, $temp->message);
                        $page++;
                        $count++;
                    }*/
                    //file_put_contents(dirname(__FILE__).'/results.txt', json_encode($tempArray));
                    //$EncodedDecoded = json_decode(json_encode($tempArray), true);
                    //require_once(APPPATH . "libraries/Fuzzysearch.php");
                    //$FieldsArr = ['StreetAddress1', 'StreetAddress2', 'Address2Street1', 'Address2Street2', "City", "City2", 'State', 'State2', 'Country', 'Country2'];
                    //$FieldsArr = ['StreetAddress1', 'StreetAddress2','Address2Street1','Address2Street2'];
                    //$sfs = new SimpleFuzzySearch($EncodedDecoded, $FieldsArr, trim($searchKeyArr[1]));
                    //$results = $sfs->search();
                    //$tempArray = [];
                    //$Ids = [];
                    /*foreach ($results as $result) {
                        if (isset($Ids[$result[0]['Id']])) continue;
                        $Ids[$result[0]['Id']] = true;
                        $tempArray[] = $result[0];
                        file_put_contents(dirname(__FILE__) . '/results.txt', $result[0]['Email'] . " " . $result[1] . " " . $result[2] . " " . $result[3] . "\n");
                    }*/

                    /*} catch (Exception $e) {
                            $tempArray = [];
                            echo 'Caught exception: ',  $e->getMessage(), "\n";
                        }*/


                } else {
                    $SearchParts = explode(" ", trim($searchKeyArr[1]));
                    foreach ($SearchParts as $SearchPart) {
                        $action_details = '{"table":"Contact","limit":"1000","page":' . $page . ',"fields":[' . $returnFieldsStr . '],"query":{"' . $Key . '":"%' . $SearchPart . '%"}}';
                        $temp = applyFn('rucksack_request',$action, $action_details);
                        $tempArray = uniquifyBy($tempArray, $temp->message);
                        if($Key == "Email"){
                            $action_details = '{"table":"Contact","limit":"1000","page":' . $page . ',"fields":[' . $returnFieldsStr . '],"query":{"EmailAddress2":"%' . $SearchPart . '%"}}';
                            $temp = applyFn('rucksack_request',$action, $action_details);
                            $tempArray = uniquifyBy($tempArray, $temp->message);

                            $action_details = '{"table":"Contact","limit":"1000","page":' . $page . ',"fields":[' . $returnFieldsStr . '],"query":{"EmailAddress3":"%' . $SearchPart . '%"}}';
                            $temp = applyFn('rucksack_request',$action, $action_details);
                            $tempArray = uniquifyBy($tempArray, $temp->message);
                        }
                        //file_put_contents(dirname(__FILE__).'/results.txt', json_encode($tempArray), FILE_APPEND);
                    }
                }


            }
            else {
                // check if email
                if (filter_var($searchKey, FILTER_VALIDATE_EMAIL)) {
                    $action_details = '{"table":"Contact","limit":"1000","page":' . $page . ',"fields":[' . $returnFieldsStr . '],"query":{"Email":"' . trim($searchKey) . '"}}';
                    $temp = applyFn('rucksack_request',$action, $action_details);
                    $tempArray = uniquifyBy($tempArray, $temp->message);

                    $action_details = '{"table":"Contact","limit":"1000","page":' . $page . ',"fields":[' . $returnFieldsStr . '],"query":{"EmailAddress2":"%' . trim($searchKey) . '%"}}';
                    $temp = applyFn('rucksack_request',$action, $action_details);
                    $tempArray = uniquifyBy($tempArray, $temp->message);

                    $action_details = '{"table":"Contact","limit":"1000","page":' . $page . ',"fields":[' . $returnFieldsStr . '],"query":{"EmailAddress3":"%' . trim($searchKey) . '%"}}';
                    $temp = applyFn('rucksack_request',$action, $action_details);
                    $tempArray = uniquifyBy($tempArray, $temp->message);

                }
                else {
                    $searchKeyArray = explode(" ", trim($searchKey));
                    if (sizeof($searchKeyArray) == 1) {
                        $action_details = '{"table":"Contact","limit":"1000","page":' . $page . ',"fields":[' . $returnFieldsStr . '],"query":{"FirstName":"%' . $searchKeyArray[0] . '%"}}';
                        $temp = applyFn('rucksack_request',$action, $action_details);
                        $tempArray = uniquifyBy($tempArray, $temp->message);
                        $action_details = '{"table":"Contact","limit":"1000","page":' . $page . ',"fields":[' . $returnFieldsStr . '],"query":{"LastName":"%' . $searchKeyArray[0] . '%"}}';
                        $temp = applyFn('rucksack_request',$action, $action_details);
                        $tempArray = uniquifyBy($tempArray, $temp->message);
                    } else {
                        $action_details = '{"table":"Contact","limit":"1000","page":' . $page . ',"fields":[' . $returnFieldsStr . '],"query":{"FirstName":"%' . $searchKeyArray[0] . '%", "LastName":"%' . $searchKeyArray[1] . '%"}}';
                        $temp = applyFn('rucksack_request',$action, $action_details);
                        $tempArray = uniquifyBy($tempArray, $temp->message);
                    }
                }
            }
        }
        $finalArray = $tempArray;
        if ($toSort === true)
            $finalArray = sortMultiArray($finalArray, 'FirstName');
        // Filter contacts that has zuora contacts only, but the function modify to accept non zuora account
        $finalArray = filterContacts($finalArray,$session_data);
        return $finalArray;
    }
}
/*Get IS web form*/
if (!function_exists('infusionsoft_get_web_form')) {
    function infusionsoft_get_web_form($app, $formId)
    {
        $HTML = file_get_contents('https://' . $app . '.infusionsoft.com/app/form/' . $formId);
        return $HTML;
    }
}
/*Send IS Email*/
if (!function_exists('infusionsoft_send_email')) {
    function infusionsoft_send_email($subject, $htmlBody, $contactList, $fromAddress, $toAddress, $bccAddresses, $contentType, $ccAddresses, $textBody, $templateID = 0)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $action = "email_is";
        $action_details = '{"templateID":"' . $templateID . '","textBody":"' . $textBody . '","ccAddresses":"' . $ccAddresses . '","contentType":"' . $contentType . '","subject":"' . $subject . '","htmlBody":"' . $htmlBody . '","contactList":' . $contactList . ',"fromAddress":"' . $fromAddress . '","toAddress":"' . $toAddress . '","bccAddresses":"' . $bccAddresses . '"}';
        $results = applyFn('rucksack_request',$action, $action_details);
        $Templates = $results->message;
        return $Templates;
    }
}
/*Get IS Email Template*/
if (!function_exists('infusionsoft_get_email_template')) {
    function infusionsoft_get_email_template()
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        /*Rucksack Request To add IS contact*/
        //$action = "emailtpl_is";
        //$action_details = '{}';
        //return applyFn('rucksack_request',$action, $action_details);
        /*GET CONTACT NOTES*/
        $action = "query_is";
        $action_details = '{"table":"Template","limit":"1000","page":0,"fields":["Categories","Id","PieceTitle","PieceType"],"query":{"PieceType":"Email","PieceTitle":"~<>~null","Categories":"%"}}';
        $results = applyFn('rucksack_request',$action, $action_details);
        $Templates = $results->message;
        return $Templates;


    }
}
/*Get Get_email_history*/
if (!function_exists('infusionsoft_get_email_history')) {
    function infusionsoft_get_email_history($ContactId,$limit=1000,$offset=0)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $action = "restEmailHistory";
        $action_details = '{"ContactId":' . $ContactId . ',"limit":' . $limit . ',"offset":' . $offset . '}';
        $EmailHistory = applyFn('rucksack_request',$action, $action_details);
        return isset($EmailHistory->message) ? $EmailHistory->message:"";


    }
}
/*Get Get_email_history*/
if (!function_exists('infusionsoft_get_email_item')) {
    function infusionsoft_get_email_item($Id)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $action = "restEmailItem";
        $action_details = '{"Id":' . $Id . '}';
        $EmailItem = applyFn('rucksack_request',$action, $action_details);
        return isset($EmailItem->message) ? $EmailItem->message:"";


    }
}
/*Get FileBox*/
if (!function_exists('infusionsoft_get_files')) {
    function infusionsoft_get_files($ContactId)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $action = "query_is";
        $returnFields = [];
        $returnFields[] = '"Id"';
        $returnFields[] = '"ContactId"';
        $returnFields[] = '"Extension"';
        $returnFields[] = '"FileName"';
        $returnFields[] = '"FileSize"';
        $returnFields[] = '"Public"';
        $returnFieldsStr = implode(', ', $returnFields);
        $action_details = '{"table":"FileBox","limit":"1000","page":0,"fields":[' . $returnFieldsStr . '],"query":{"ContactId":"' . $ContactId . '"}}';
        $StageArr = applyFn('rucksack_request',$action, $action_details);
        $Files = $StageArr->message;
        return $Files;


    }
}

/*Get IS Email Template*/
if (!function_exists('infusionsoft_get_email_template_content')) {
    function infusionsoft_get_email_template_content($Id, $Force = false)
    {

        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $Templates = manual_cache_loader('infusionsoft_get_email_template_content-'.$Id);
        $Templates = $Force == true ? false:json_decode($Templates);
        if($Templates == false) {
            $action = "email_template_is";
            $action_details = '{"Id":"' . (string)$Id . '"}';
            $results = applyFn('rucksack_request',$action, $action_details);
            $Templates = $results->message;
            manual_cache_writer('infusionsoft_get_email_template_content-'.$Id, json_encode($Templates), 0);
        }
        return $Templates;

    }
}
/*Add Infusionsoft Contact*/
if (!function_exists('infusionsoft_add_contact')) {
    function infusionsoft_add_contact($data)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $values = $data;
        $fieldsArr = array();
        $optin = 'No';
        foreach ($values as $isField) {
            $$isField['name'] = $isField['value'];
            if ($isField['name'] == 'marketing') {
                $optin = 'Yes';
                continue;
            }
            if($isField['name'] == 'AddressBillingAddLoqate'){
                continue;
            }
            if($isField['name'] == 'AddressShippingAddLoqate'){
                continue;
            }
            if($isField['name'] == 'AddressBillingAdd'){
                continue;
            }
            if($isField['name'] == 'AddressShippingAdd'){
                continue;
            }
            if($isField['name'] == 'AddressShippingAddLoqate'){
                continue;
            }
            if($isField['name'] == 'AddressBillingAddLoqate'){
                continue;
            }
            if($isField['name'] == 'SelectedAddressItem'){
                continue;
            }
            if ($isField['value'] != "")
                $fieldsArr[] = '"' . $isField['name'] . '":"' . $isField['value'] . '"';
        }
        /*Rucksack Request To add IS contact*/
        $fields = '{' . implode(',', $fieldsArr) . '}';
        $action = "addcon_is";
        $action_details = '{"fields":' . $fields . '}';

        if ($optin == 'Yes') $action_details = '{"fields":' . $fields . ',"optin":1}';

        return applyFn('rucksack_request',$action, $action_details);
    }
}
/*Update Infusionsoft Contact*/
if (!function_exists('infusionsoft_update_contact')) {
    function infusionsoft_update_contact($data, $Id, $optin = '',$session_data=[])
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $values = $data;
        $fieldsArr = array();
        foreach ($values as $isField) {
            //$$isField['name'] = $isField['value'];
            $fieldsArr[] = '"' . $isField['name'] . '":' . json_encode($isField['value']);
        }
        /*Rucksack Request To update IS contact*/
        $fields = '{' . implode(',', $fieldsArr) . '}';
        $action = "update_is";
        $action_details = '{"table":"Contact","id":"' . $Id . '","fields":' . $fields . '}';

        if ($optin == 'Yes') $action_details = '{"table":"Contact","id":"' . $Id . '","fields":' . $fields . ',"optin":1}';
        $Result = applyFn('rucksack_request',$action, $action_details);
        if($session_data != [])
            $UpdateSearchedCache = UpdateSearchedCache($values, $Id,$session_data);
        $updateTaggedSelectedContact = updateTaggedSelectedContact($values, $Id);
        return json_encode($Result)."\nAction Details: ".$action_details;
        //return 'Action Details for ' . $action . ' : ' . $UpdateSearchedCache . ", $updateTaggedSelectedContact";
    }
}
if (!function_exists('UpdateCache')) {
    function UpdateSearchedCache($values, $userId,$session_data=[])
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $searched_cache = manual_cache_loader('searched_cache' . $session_data['InfusionsoftID']);
        $TempContact = array();
        $DateFields = ["Anniversary", "Birthday", "Validated", "DateCreated", "LastUpdated"];
        foreach ($values as $isField) {
            if (in_array($isField['name'], $DateFields)) {
                $isField['value'] = array('date' => $isField['value']);
            }


            foreach (infusionsoft_get_custom_fields() as $ISCustomField) {
                if ($ISCustomField->Name == $isField['name']) {
                    if ($ISCustomField->DataType == 13 || $ISCustomField->DataType == 14) {
                        $isField['value'] = array('date' => $isField['value']);
                    }
                }
            }

            $TempContact[$isField['name']] = $isField['value'];
        }

        if ($searched_cache) {
            $searched_cache = json_decode($searched_cache, true);
            foreach ($searched_cache as $key => $Contact) {
                if ($Contact['Id'] == $userId) {
                    //$OldContact = $Contact;

                    $Contact = array_merge($Contact, $TempContact);
                    $searched_cache[$key] = $Contact;
                    break;
                }
            }
            manual_cache_writer('searched_cache' . $session_data['InfusionsoftID'], json_encode($searched_cache), 86400);
        }

        return 'Search Cache Updated';

    }
}
if (!function_exists('updateTaggedSelectedContact')) {
    function updateTaggedSelectedContact($data, $id)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $ContactCache = manual_cache_loader('tagged_selected_contact_cache');
        $TempContact = array();
        foreach ($data as $isField) {
            $TempContact[$isField['name']] = $isField['value'];
        }
        if ($ContactCache) {
            $SelectedContactCache = json_decode($ContactCache, true);
            if (array_key_exists($id, $SelectedContactCache)) {
                $LastName = isset($TempContact['LastName']) ? $TempContact['LastName']:'';
                $Email = isset($TempContact['Email']) ? $TempContact['Email']:'';
                $Company = isset($TempContact['Company']) ? $TempContact['Company']:'';
                $FirstName = isset($TempContact['FirstName']) ? $TempContact['FirstName']:'';
                $SelectedContactCache[$id] = array(
                    'Company' => $Company,
                    'Email' => $Email,
                    'FirstName' => $FirstName,
                    'Id' => $id,
                    'LastName' => $LastName,

                );
                manual_cache_writer('tagged_selected_contact_cache', json_encode($SelectedContactCache), 0);
                return 'Tagged Selected Cache Updated';
            } else {
                return 'Contact Not Existing in Tagged Selected Cache';
            }

        } else {
            return 'No Tagged Selected Cache';
        }

    }
}
if (!function_exists('storeSelectedContact')) {
    function storeTaggedSelectedContact($data)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $NoteId = $data['note_id'];
        $tagsStr = $data['tags'];
        $ContactToBeStored = array();
        $contactId = $data['conId'];

        /*$theCache =  file_get_contents(APPPATH."cache/tagged_selected_contact_cache");
        if($theCache){

        }*/
        $ContactCache = manual_cache_loader('tagged_selected_contact_cache');
        if ($ContactCache) {
            $SelectedContactCache = json_decode($ContactCache, true);
            if (!array_key_exists($contactId, $SelectedContactCache)) {
                $SelectedContactCache[$contactId] = infusionsoft_get_contact_by_id($contactId, array('all'));
                manual_cache_writer('tagged_selected_contact_cache', json_encode($SelectedContactCache), 0);
                return 'Contact Added to Tagged Selected Cache';
            } else {
                return 'Contact Existing to Tagged Selected Cache';
            }

        } else {
            //insert first contact
            $ContactToBeStored[$contactId] = infusionsoft_get_contact_by_id($contactId, array('all'));
            manual_cache_writer('tagged_selected_contact_cache', json_encode($ContactToBeStored), 0);
            return 'Contact Added to Tagged Selected Cache';
        }


    }
}
/*Update Infusionsoft and Zuora Contact*/
if (!function_exists('zuora_infusionsoft_update_contact')) {
    function zuora_infusionsoft_update_contact($data)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $LoggedInUser = json_decode($CI->session->userdata('details'));
        $searched_contact = json_decode(manual_cache_loader('searched_contact' . $LoggedInUser->Id));
        $userId = $data['userid'];
        $zuoraId = $data['zuoraid'];
        $DataArrKey = $data['DataArrKey'];
        $values = $data['values'];
        $fieldsArr = array();
        $FirstName = $LastName = $Email = $Phone1 = $StreetAddress1 = $StreetAddress2 = $City = $State = $PostalCode = $Company = '';
        foreach ($values as $isField) {
            $$isField['name'] = $isField['value'];
            $fieldsArr[] = '"' . $isField['name'] . '":"' . $isField['value'] . '"';
            $searched_contact[$DataArrKey]->$isField['name'] = $isField['value'];
        }
        /*Rucksack Request To update IS contact*/
        $fields = '{' . implode(',', $fieldsArr) . '}';
        $action = "update_is";
        $action_details = '{"table":"Contact","id":"' . $userId . '","fields":' . $fields . '}';
        applyFn('rucksack_request',$action, $action_details);
        manual_cache_writer('searched_contact' . $LoggedInUser->Id, json_encode($searched_contact), 86400);

        /*Rucksack Request To get Zuora contact Id by AccountId*/
        $action = "zuora_query";
        $action_details = '{"table":"Contact","search_by":{"field_name":"AccountId","field_value":"' . $zuoraId . '"},"fields":["Id"]}';
        $zContactId = applyFn('rucksack_request',$action, $action_details);

        /*Rucksack Request To update Zuora contact*/
        $fieldsArr = array();
        $fieldsArr[] = '"FirstName":"' . $FirstName . '"';
        $fieldsArr[] = '"LastName":"' . $LastName . '"';
        $fieldsArr[] = '"WorkEmail":"' . $Email . '"';
        $fieldsArr[] = '"MobilePhone":"' . $Phone1 . '"';
        $fieldsArr[] = '"Address1":"' . $StreetAddress1 . '"';
        $fieldsArr[] = '"Address2":"' . $StreetAddress2 . '"';
        $fieldsArr[] = '"City":"' . $City . '"';
        $fieldsArr[] = '"State":"' . $State . '"';
        $fieldsArr[] = '"PostalCode":"' . $PostalCode . '"';
        $fields = '{' . implode(',', $fieldsArr) . '}';

        $action = "zuora_update";
        $action_details = '{"table":"Contact","id":"' . $zContactId[0]->Id . '","fields":' . $fields . '}';
        applyFn('rucksack_request',$action, $action_details);

        $action = "zuora_update";
        $action_details = '{"table":"Account","id":"' . $zuoraId . '","fields":{"Name":"' . $Company . '"}}';
        applyFn('rucksack_request',$action, $action_details);

        return 'Action Details for ' . $action . ' : ' . $action_details;
    }
}
if (!function_exists('infusionsoft_update_phone')) {
    function infusionsoft_update_phone($PostData)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $Id = $PostData['Id'];
        $Phone1 = $PostData['Phone1'];
        $Phone2 = $PostData['Phone2'];
        $action = "update_is";
        $action_details = '{"table":"Contact","id":"' . $Id . '","fields":{"Phone1":"' . $Phone1 . '","Phone2":"' . $Phone2 . '"}}';
        $result = applyFn('rucksack_request',$action, $action_details);
        return $result;
    }
}
if (!function_exists('infuDate')) {
    function infuDate($dateStr)
    {
        $dArray = date_parse($dateStr);
        if ($dArray['error_count'] < 1) {
            $tStamp =
                mktime($dArray['hour'], $dArray['minute'], $dArray['second'], $dArray['month'],
                    $dArray['day'], $dArray['year']);
            return date('Ymd\TH:i:s', $tStamp);//1981-05-24 00:00:00.000000
        } else {
            foreach ($dArray['errors'] as $err) {
                echo "ERROR: " . $err . "<br />";
            }
            die("The above errors prevented the application from executing properly.");
        }
    }
}
if (!function_exists('infuDateCustom')) {
    function infuDateCustom($dateStr)
    {
        $dArray = date_parse($dateStr);
        if ($dArray['error_count'] < 1) {
            $tStamp =
                mktime($dArray['hour'], $dArray['minute'], $dArray['second'], $dArray['month'],
                    $dArray['day'], $dArray['year']);
            return date('Y-m-d H:i:s.u', $tStamp);//1981-05-24 00:00:00.000000
        } else {
            foreach ($dArray['errors'] as $err) {
                echo "ERROR: " . $err . "<br />";
            }
            die("The above errors prevented the application from executing properly.");
        }
    }
}
/* Convert Infusionsoft time to unix time*/
if (!function_exists('infusionsoft_timeToTime')) {
    function infusionsoft_timeToTime($str)
    {
        $year = substr($str, 0, 4);
        $month = substr($str, 4, 2);
        $day = substr($str, 6, 2);
        $hour = substr($str, 9, 2);
        $min = substr($str, 12, 2);
        $sec = substr($str, 15, 2);
        $Time = $year . "-" . $month . "-" . $day . ' ' . $hour . ":" . $min . ":" . $sec;
        return strtotime($Time);
    }
}
/* Convert Infusionsoft time to unix time*/
if (!function_exists('infusionsoft_DateToDate')) {
    function infusionsoft_DateToDate($str)
    {
        $year = substr($str, 0, 4);
        $month = substr($str, 4, 2);
        $day = substr($str, 6, 2);
        $hour = substr($str, 9, 2);
        $min = substr($str, 12, 2);
        $sec = substr($str, 15, 2);
        $Time = $year . "-" . $month . "-" . $day . ' ' . $hour . ":" . $min . ":" . $sec;
        return strtotime($Time);
    }
}
if (!function_exists('isJson')) {
    function isJson($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}
if (!function_exists('getCountryCode')) {
    function getCountryCode($Country = false, $Code = false, $CountryCode=false)
    {
        $countries = array();
        $countries[] = array("code" => "AF", "name" => "Afghanistan", "d_code" => "+93");
        $countries[] = array("code" => "AL", "name" => "Albania", "d_code" => "+355");
        $countries[] = array("code" => "DZ", "name" => "Algeria", "d_code" => "+213");
        $countries[] = array("code" => "AS", "name" => "American Samoa", "d_code" => "+1");
        $countries[] = array("code" => "AD", "name" => "Andorra", "d_code" => "+376");
        $countries[] = array("code" => "AO", "name" => "Angola", "d_code" => "+244");
        $countries[] = array("code" => "AI", "name" => "Anguilla", "d_code" => "+1");
        $countries[] = array("code" => "AG", "name" => "Antigua", "d_code" => "+1");
        $countries[] = array("code" => "AR", "name" => "Argentina", "d_code" => "+54");
        $countries[] = array("code" => "AM", "name" => "Armenia", "d_code" => "+374");
        $countries[] = array("code" => "AW", "name" => "Aruba", "d_code" => "+297");
        $countries[] = array("code" => "AU", "name" => "Australia", "d_code" => "+61");
        $countries[] = array("code" => "AT", "name" => "Austria", "d_code" => "+43");
        $countries[] = array("code" => "AZ", "name" => "Azerbaijan", "d_code" => "+994");
        $countries[] = array("code" => "BH", "name" => "Bahrain", "d_code" => "+973");
        $countries[] = array("code" => "BD", "name" => "Bangladesh", "d_code" => "+880");
        $countries[] = array("code" => "BB", "name" => "Barbados", "d_code" => "+1");
        $countries[] = array("code" => "BY", "name" => "Belarus", "d_code" => "+375");
        $countries[] = array("code" => "BE", "name" => "Belgium", "d_code" => "+32");
        $countries[] = array("code" => "BZ", "name" => "Belize", "d_code" => "+501");
        $countries[] = array("code" => "BJ", "name" => "Benin", "d_code" => "+229");
        $countries[] = array("code" => "BM", "name" => "Bermuda", "d_code" => "+1");
        $countries[] = array("code" => "BT", "name" => "Bhutan", "d_code" => "+975");
        $countries[] = array("code" => "BO", "name" => "Bolivia", "d_code" => "+591");
        $countries[] = array("code" => "BA", "name" => "Bosnia and Herzegovina", "d_code" => "+387");
        $countries[] = array("code" => "BW", "name" => "Botswana", "d_code" => "+267");
        $countries[] = array("code" => "BR", "name" => "Brazil", "d_code" => "+55");
        $countries[] = array("code" => "IO", "name" => "British Indian Ocean Territory", "d_code" => "+246");
        $countries[] = array("code" => "VG", "name" => "British Virgin Islands", "d_code" => "+1");
        $countries[] = array("code" => "BN", "name" => "Brunei", "d_code" => "+673");
        $countries[] = array("code" => "BG", "name" => "Bulgaria", "d_code" => "+359");
        $countries[] = array("code" => "BF", "name" => "Burkina Faso", "d_code" => "+226");
        $countries[] = array("code" => "MM", "name" => "Burma Myanmar", "d_code" => "+95");
        $countries[] = array("code" => "BI", "name" => "Burundi", "d_code" => "+257");
        $countries[] = array("code" => "KH", "name" => "Cambodia", "d_code" => "+855");
        $countries[] = array("code" => "CM", "name" => "Cameroon", "d_code" => "+237");
        $countries[] = array("code" => "CA", "name" => "Canada", "d_code" => "+1");
        $countries[] = array("code" => "CV", "name" => "Cape Verde", "d_code" => "+238");
        $countries[] = array("code" => "KY", "name" => "Cayman Islands", "d_code" => "+1");
        $countries[] = array("code" => "CF", "name" => "Central African Republic", "d_code" => "+236");
        $countries[] = array("code" => "TD", "name" => "Chad", "d_code" => "+235");
        $countries[] = array("code" => "CL", "name" => "Chile", "d_code" => "+56");
        $countries[] = array("code" => "CN", "name" => "China", "d_code" => "+86");
        $countries[] = array("code" => "CO", "name" => "Colombia", "d_code" => "+57");
        $countries[] = array("code" => "KM", "name" => "Comoros", "d_code" => "+269");
        $countries[] = array("code" => "CK", "name" => "Cook Islands", "d_code" => "+682");
        $countries[] = array("code" => "CR", "name" => "Costa Rica", "d_code" => "+506");
        $countries[] = array("code" => "CI", "name" => "Côte d'Ivoire", "d_code" => "+225");
        $countries[] = array("code" => "HR", "name" => "Croatia", "d_code" => "+385");
        $countries[] = array("code" => "CU", "name" => "Cuba", "d_code" => "+53");
        $countries[] = array("code" => "CY", "name" => "Cyprus", "d_code" => "+357");
        $countries[] = array("code" => "CZ", "name" => "Czech Republic", "d_code" => "+420");
        $countries[] = array("code" => "CD", "name" => "Democratic Republic of Congo", "d_code" => "+243");
        $countries[] = array("code" => "DK", "name" => "Denmark", "d_code" => "+45");
        $countries[] = array("code" => "DJ", "name" => "Djibouti", "d_code" => "+253");
        $countries[] = array("code" => "DM", "name" => "Dominica", "d_code" => "+1");
        $countries[] = array("code" => "DO", "name" => "Dominican Republic", "d_code" => "+1");
        $countries[] = array("code" => "EC", "name" => "Ecuador", "d_code" => "+593");
        $countries[] = array("code" => "EG", "name" => "Egypt", "d_code" => "+20");
        $countries[] = array("code" => "SV", "name" => "El Salvador", "d_code" => "+503");
        $countries[] = array("code" => "GQ", "name" => "Equatorial Guinea", "d_code" => "+240");
        $countries[] = array("code" => "ER", "name" => "Eritrea", "d_code" => "+291");
        $countries[] = array("code" => "EE", "name" => "Estonia", "d_code" => "+372");
        $countries[] = array("code" => "ET", "name" => "Ethiopia", "d_code" => "+251");
        $countries[] = array("code" => "FK", "name" => "Falkland Islands", "d_code" => "+500");
        $countries[] = array("code" => "FO", "name" => "Faroe Islands", "d_code" => "+298");
        $countries[] = array("code" => "FM", "name" => "Federated States of Micronesia", "d_code" => "+691");
        $countries[] = array("code" => "FJ", "name" => "Fiji", "d_code" => "+679");
        $countries[] = array("code" => "FI", "name" => "Finland", "d_code" => "+358");
        $countries[] = array("code" => "FR", "name" => "France", "d_code" => "+33");
        $countries[] = array("code" => "GF", "name" => "French Guiana", "d_code" => "+594");
        $countries[] = array("code" => "PF", "name" => "French Polynesia", "d_code" => "+689");
        $countries[] = array("code" => "GA", "name" => "Gabon", "d_code" => "+241");
        $countries[] = array("code" => "GE", "name" => "Georgia", "d_code" => "+995");
        $countries[] = array("code" => "DE", "name" => "Germany", "d_code" => "+49");
        $countries[] = array("code" => "GH", "name" => "Ghana", "d_code" => "+233");
        $countries[] = array("code" => "GI", "name" => "Gibraltar", "d_code" => "+350");
        $countries[] = array("code" => "GR", "name" => "Greece", "d_code" => "+30");
        $countries[] = array("code" => "GL", "name" => "Greenland", "d_code" => "+299");
        $countries[] = array("code" => "GD", "name" => "Grenada", "d_code" => "+1");
        $countries[] = array("code" => "GP", "name" => "Guadeloupe", "d_code" => "+590");
        $countries[] = array("code" => "GU", "name" => "Guam", "d_code" => "+1");
        $countries[] = array("code" => "GT", "name" => "Guatemala", "d_code" => "+502");
        $countries[] = array("code" => "GN", "name" => "Guinea", "d_code" => "+224");
        $countries[] = array("code" => "GW", "name" => "Guinea-Bissau", "d_code" => "+245");
        $countries[] = array("code" => "GY", "name" => "Guyana", "d_code" => "+592");
        $countries[] = array("code" => "HT", "name" => "Haiti", "d_code" => "+509");
        $countries[] = array("code" => "HN", "name" => "Honduras", "d_code" => "+504");
        $countries[] = array("code" => "HK", "name" => "Hong Kong", "d_code" => "+852");
        $countries[] = array("code" => "HU", "name" => "Hungary", "d_code" => "+36");
        $countries[] = array("code" => "IS", "name" => "Iceland", "d_code" => "+354");
        $countries[] = array("code" => "IN", "name" => "India", "d_code" => "+91");
        $countries[] = array("code" => "ID", "name" => "Indonesia", "d_code" => "+62");
        $countries[] = array("code" => "IR", "name" => "Iran", "d_code" => "+98");
        $countries[] = array("code" => "IQ", "name" => "Iraq", "d_code" => "+964");
        $countries[] = array("code" => "IE", "name" => "Ireland", "d_code" => "+353");
        $countries[] = array("code" => "IL", "name" => "Israel", "d_code" => "+972");
        $countries[] = array("code" => "IT", "name" => "Italy", "d_code" => "+39");
        $countries[] = array("code" => "JM", "name" => "Jamaica", "d_code" => "+1");
        $countries[] = array("code" => "JP", "name" => "Japan", "d_code" => "+81");
        $countries[] = array("code" => "JO", "name" => "Jordan", "d_code" => "+962");
        $countries[] = array("code" => "KZ", "name" => "Kazakhstan", "d_code" => "+7");
        $countries[] = array("code" => "KE", "name" => "Kenya", "d_code" => "+254");
        $countries[] = array("code" => "KI", "name" => "Kiribati", "d_code" => "+686");
        $countries[] = array("code" => "XK", "name" => "Kosovo", "d_code" => "+381");
        $countries[] = array("code" => "KW", "name" => "Kuwait", "d_code" => "+965");
        $countries[] = array("code" => "KG", "name" => "Kyrgyzstan", "d_code" => "+996");
        $countries[] = array("code" => "LA", "name" => "Laos", "d_code" => "+856");
        $countries[] = array("code" => "LV", "name" => "Latvia", "d_code" => "+371");
        $countries[] = array("code" => "LB", "name" => "Lebanon", "d_code" => "+961");
        $countries[] = array("code" => "LS", "name" => "Lesotho", "d_code" => "+266");
        $countries[] = array("code" => "LR", "name" => "Liberia", "d_code" => "+231");
        $countries[] = array("code" => "LY", "name" => "Libya", "d_code" => "+218");
        $countries[] = array("code" => "LI", "name" => "Liechtenstein", "d_code" => "+423");
        $countries[] = array("code" => "LT", "name" => "Lithuania", "d_code" => "+370");
        $countries[] = array("code" => "LU", "name" => "Luxembourg", "d_code" => "+352");
        $countries[] = array("code" => "MO", "name" => "Macau", "d_code" => "+853");
        $countries[] = array("code" => "MK", "name" => "Macedonia", "d_code" => "+389");
        $countries[] = array("code" => "MG", "name" => "Madagascar", "d_code" => "+261");
        $countries[] = array("code" => "MW", "name" => "Malawi", "d_code" => "+265");
        $countries[] = array("code" => "MY", "name" => "Malaysia", "d_code" => "+60");
        $countries[] = array("code" => "MV", "name" => "Maldives", "d_code" => "+960");
        $countries[] = array("code" => "ML", "name" => "Mali", "d_code" => "+223");
        $countries[] = array("code" => "MT", "name" => "Malta", "d_code" => "+356");
        $countries[] = array("code" => "MH", "name" => "Marshall Islands", "d_code" => "+692");
        $countries[] = array("code" => "MQ", "name" => "Martinique", "d_code" => "+596");
        $countries[] = array("code" => "MR", "name" => "Mauritania", "d_code" => "+222");
        $countries[] = array("code" => "MU", "name" => "Mauritius", "d_code" => "+230");
        $countries[] = array("code" => "YT", "name" => "Mayotte", "d_code" => "+262");
        $countries[] = array("code" => "MX", "name" => "Mexico", "d_code" => "+52");
        $countries[] = array("code" => "MD", "name" => "Moldova", "d_code" => "+373");
        $countries[] = array("code" => "MC", "name" => "Monaco", "d_code" => "+377");
        $countries[] = array("code" => "MN", "name" => "Mongolia", "d_code" => "+976");
        $countries[] = array("code" => "ME", "name" => "Montenegro", "d_code" => "+382");
        $countries[] = array("code" => "MS", "name" => "Montserrat", "d_code" => "+1");
        $countries[] = array("code" => "MA", "name" => "Morocco", "d_code" => "+212");
        $countries[] = array("code" => "MZ", "name" => "Mozambique", "d_code" => "+258");
        $countries[] = array("code" => "NA", "name" => "Namibia", "d_code" => "+264");
        $countries[] = array("code" => "NR", "name" => "Nauru", "d_code" => "+674");
        $countries[] = array("code" => "NP", "name" => "Nepal", "d_code" => "+977");
        $countries[] = array("code" => "NL", "name" => "Netherlands", "d_code" => "+31");
        $countries[] = array("code" => "AN", "name" => "Netherlands Antilles", "d_code" => "+599");
        $countries[] = array("code" => "NC", "name" => "New Caledonia", "d_code" => "+687");
        $countries[] = array("code" => "NZ", "name" => "New Zealand", "d_code" => "+64");
        $countries[] = array("code" => "NI", "name" => "Nicaragua", "d_code" => "+505");
        $countries[] = array("code" => "NE", "name" => "Niger", "d_code" => "+227");
        $countries[] = array("code" => "NG", "name" => "Nigeria", "d_code" => "+234");
        $countries[] = array("code" => "NU", "name" => "Niue", "d_code" => "+683");
        $countries[] = array("code" => "NF", "name" => "Norfolk Island", "d_code" => "+672");
        $countries[] = array("code" => "KP", "name" => "North Korea", "d_code" => "+850");
        $countries[] = array("code" => "MP", "name" => "Northern Mariana Islands", "d_code" => "+1");
        $countries[] = array("code" => "NO", "name" => "Norway", "d_code" => "+47");
        $countries[] = array("code" => "OM", "name" => "Oman", "d_code" => "+968");
        $countries[] = array("code" => "PK", "name" => "Pakistan", "d_code" => "+92");
        $countries[] = array("code" => "PW", "name" => "Palau", "d_code" => "+680");
        $countries[] = array("code" => "PS", "name" => "Palestine", "d_code" => "+970");
        $countries[] = array("code" => "PA", "name" => "Panama", "d_code" => "+507");
        $countries[] = array("code" => "PG", "name" => "Papua New Guinea", "d_code" => "+675");
        $countries[] = array("code" => "PY", "name" => "Paraguay", "d_code" => "+595");
        $countries[] = array("code" => "PE", "name" => "Peru", "d_code" => "+51");
        $countries[] = array("code" => "PH", "name" => "Philippines", "d_code" => "+63");
        $countries[] = array("code" => "PL", "name" => "Poland", "d_code" => "+48");
        $countries[] = array("code" => "PT", "name" => "Portugal", "d_code" => "+351");
        $countries[] = array("code" => "PR", "name" => "Puerto Rico", "d_code" => "+1");
        $countries[] = array("code" => "QA", "name" => "Qatar", "d_code" => "+974");
        $countries[] = array("code" => "CG", "name" => "Republic of the Congo", "d_code" => "+242");
        $countries[] = array("code" => "RE", "name" => "Réunion", "d_code" => "+262");
        $countries[] = array("code" => "RO", "name" => "Romania", "d_code" => "+40");
        $countries[] = array("code" => "RU", "name" => "Russia", "d_code" => "+7");
        $countries[] = array("code" => "RW", "name" => "Rwanda", "d_code" => "+250");
        $countries[] = array("code" => "BL", "name" => "Saint Barthélemy", "d_code" => "+590");
        $countries[] = array("code" => "SH", "name" => "Saint Helena", "d_code" => "+290");
        $countries[] = array("code" => "KN", "name" => "Saint Kitts and Nevis", "d_code" => "+1");
        $countries[] = array("code" => "MF", "name" => "Saint Martin", "d_code" => "+590");
        $countries[] = array("code" => "PM", "name" => "Saint Pierre and Miquelon", "d_code" => "+508");
        $countries[] = array("code" => "VC", "name" => "Saint Vincent and the Grenadines", "d_code" => "+1");
        $countries[] = array("code" => "WS", "name" => "Samoa", "d_code" => "+685");
        $countries[] = array("code" => "SM", "name" => "San Marino", "d_code" => "+378");
        $countries[] = array("code" => "ST", "name" => "São Tomé and Príncipe", "d_code" => "+239");
        $countries[] = array("code" => "SA", "name" => "Saudi Arabia", "d_code" => "+966");
        $countries[] = array("code" => "SN", "name" => "Senegal", "d_code" => "+221");
        $countries[] = array("code" => "RS", "name" => "Serbia", "d_code" => "+381");
        $countries[] = array("code" => "SC", "name" => "Seychelles", "d_code" => "+248");
        $countries[] = array("code" => "SL", "name" => "Sierra Leone", "d_code" => "+232");
        $countries[] = array("code" => "SG", "name" => "Singapore", "d_code" => "+65");
        $countries[] = array("code" => "SK", "name" => "Slovakia", "d_code" => "+421");
        $countries[] = array("code" => "SI", "name" => "Slovenia", "d_code" => "+386");
        $countries[] = array("code" => "SB", "name" => "Solomon Islands", "d_code" => "+677");
        $countries[] = array("code" => "SO", "name" => "Somalia", "d_code" => "+252");
        $countries[] = array("code" => "ZA", "name" => "South Africa", "d_code" => "+27");
        $countries[] = array("code" => "KR", "name" => "South Korea", "d_code" => "+82");
        $countries[] = array("code" => "ES", "name" => "Spain", "d_code" => "+34");
        $countries[] = array("code" => "LK", "name" => "Sri Lanka", "d_code" => "+94");
        $countries[] = array("code" => "LC", "name" => "St. Lucia", "d_code" => "+1");
        $countries[] = array("code" => "SD", "name" => "Sudan", "d_code" => "+249");
        $countries[] = array("code" => "SR", "name" => "Suriname", "d_code" => "+597");
        $countries[] = array("code" => "SZ", "name" => "Swaziland", "d_code" => "+268");
        $countries[] = array("code" => "SE", "name" => "Sweden", "d_code" => "+46");
        $countries[] = array("code" => "CH", "name" => "Switzerland", "d_code" => "+41");
        $countries[] = array("code" => "SY", "name" => "Syria", "d_code" => "+963");
        $countries[] = array("code" => "TW", "name" => "Taiwan", "d_code" => "+886");
        $countries[] = array("code" => "TJ", "name" => "Tajikistan", "d_code" => "+992");
        $countries[] = array("code" => "TZ", "name" => "Tanzania", "d_code" => "+255");
        $countries[] = array("code" => "TH", "name" => "Thailand", "d_code" => "+66");
        $countries[] = array("code" => "BS", "name" => "The Bahamas", "d_code" => "+1");
        $countries[] = array("code" => "GM", "name" => "The Gambia", "d_code" => "+220");
        $countries[] = array("code" => "TL", "name" => "Timor-Leste", "d_code" => "+670");
        $countries[] = array("code" => "TG", "name" => "Togo", "d_code" => "+228");
        $countries[] = array("code" => "TK", "name" => "Tokelau", "d_code" => "+690");
        $countries[] = array("code" => "TO", "name" => "Tonga", "d_code" => "+676");
        $countries[] = array("code" => "TT", "name" => "Trinidad and Tobago", "d_code" => "+1");
        $countries[] = array("code" => "TN", "name" => "Tunisia", "d_code" => "+216");
        $countries[] = array("code" => "TR", "name" => "Turkey", "d_code" => "+90");
        $countries[] = array("code" => "TM", "name" => "Turkmenistan", "d_code" => "+993");
        $countries[] = array("code" => "TC", "name" => "Turks and Caicos Islands", "d_code" => "+1");
        $countries[] = array("code" => "TV", "name" => "Tuvalu", "d_code" => "+688");
        $countries[] = array("code" => "UG", "name" => "Uganda", "d_code" => "+256");
        $countries[] = array("code" => "UA", "name" => "Ukraine", "d_code" => "+380");
        $countries[] = array("code" => "AE", "name" => "United Arab Emirates", "d_code" => "+971");
        $countries[] = array("code" => "GB", "name" => "United Kingdom", "d_code" => "+44");
        $countries[] = array("code" => "US", "name" => "United States", "d_code" => "+1");
        $countries[] = array("code" => "UY", "name" => "Uruguay", "d_code" => "+598");
        $countries[] = array("code" => "UZ", "name" => "Uzbekistan", "d_code" => "+998");
        $countries[] = array("code" => "VU", "name" => "Vanuatu", "d_code" => "+678");
        $countries[] = array("code" => "VA", "name" => "Vatican City", "d_code" => "+39");
        $countries[] = array("code" => "VE", "name" => "Venezuela", "d_code" => "+58");
        $countries[] = array("code" => "VN", "name" => "Vietnam", "d_code" => "+84");
        $countries[] = array("code" => "WF", "name" => "Wallis and Futuna", "d_code" => "+681");
        $countries[] = array("code" => "YE", "name" => "Yemen", "d_code" => "+967");
        $countries[] = array("code" => "ZM", "name" => "Zambia", "d_code" => "+260");
        $countries[] = array("code" => "ZW", "name" => "Zimbabwe", "d_code" => "+263");
        if ($Country == false) {
            return $countries;
        }
        if ($Code == true) {
            foreach ($countries as $theCountry) {
                if ($theCountry['d_code'] == $Country) {
                    return $theCountry['name'];
                }
            }
        } elseif($CountryCode == true) {
            foreach ($countries as $theCountry) {
                if ($theCountry['code'] == $Country) {
                    return $theCountry['d_code'];
                }
            }
        }else{
            foreach ($countries as $theCountry) {
                if ($theCountry['name'] == $Country) {
                    return $theCountry['d_code'];
                }
            }
        }

        return false;
    }
}
if (!function_exists('removeCountryCode')) {
    function removeCountryCode($Phone)
    {
        $countries = array();
        $countries[] = array("code" => "AF", "name" => "Afghanistan", "d_code" => "+93");
        $countries[] = array("code" => "AL", "name" => "Albania", "d_code" => "+355");
        $countries[] = array("code" => "DZ", "name" => "Algeria", "d_code" => "+213");
        $countries[] = array("code" => "AS", "name" => "American Samoa", "d_code" => "+1");
        $countries[] = array("code" => "AD", "name" => "Andorra", "d_code" => "+376");
        $countries[] = array("code" => "AO", "name" => "Angola", "d_code" => "+244");
        $countries[] = array("code" => "AI", "name" => "Anguilla", "d_code" => "+1");
        $countries[] = array("code" => "AG", "name" => "Antigua", "d_code" => "+1");
        $countries[] = array("code" => "AR", "name" => "Argentina", "d_code" => "+54");
        $countries[] = array("code" => "AM", "name" => "Armenia", "d_code" => "+374");
        $countries[] = array("code" => "AW", "name" => "Aruba", "d_code" => "+297");
        $countries[] = array("code" => "AU", "name" => "Australia", "d_code" => "+61");
        $countries[] = array("code" => "AT", "name" => "Austria", "d_code" => "+43");
        $countries[] = array("code" => "AZ", "name" => "Azerbaijan", "d_code" => "+994");
        $countries[] = array("code" => "BH", "name" => "Bahrain", "d_code" => "+973");
        $countries[] = array("code" => "BD", "name" => "Bangladesh", "d_code" => "+880");
        $countries[] = array("code" => "BB", "name" => "Barbados", "d_code" => "+1");
        $countries[] = array("code" => "BY", "name" => "Belarus", "d_code" => "+375");
        $countries[] = array("code" => "BE", "name" => "Belgium", "d_code" => "+32");
        $countries[] = array("code" => "BZ", "name" => "Belize", "d_code" => "+501");
        $countries[] = array("code" => "BJ", "name" => "Benin", "d_code" => "+229");
        $countries[] = array("code" => "BM", "name" => "Bermuda", "d_code" => "+1");
        $countries[] = array("code" => "BT", "name" => "Bhutan", "d_code" => "+975");
        $countries[] = array("code" => "BO", "name" => "Bolivia", "d_code" => "+591");
        $countries[] = array("code" => "BA", "name" => "Bosnia and Herzegovina", "d_code" => "+387");
        $countries[] = array("code" => "BW", "name" => "Botswana", "d_code" => "+267");
        $countries[] = array("code" => "BR", "name" => "Brazil", "d_code" => "+55");
        $countries[] = array("code" => "IO", "name" => "British Indian Ocean Territory", "d_code" => "+246");
        $countries[] = array("code" => "VG", "name" => "British Virgin Islands", "d_code" => "+1");
        $countries[] = array("code" => "BN", "name" => "Brunei", "d_code" => "+673");
        $countries[] = array("code" => "BG", "name" => "Bulgaria", "d_code" => "+359");
        $countries[] = array("code" => "BF", "name" => "Burkina Faso", "d_code" => "+226");
        $countries[] = array("code" => "MM", "name" => "Burma Myanmar", "d_code" => "+95");
        $countries[] = array("code" => "BI", "name" => "Burundi", "d_code" => "+257");
        $countries[] = array("code" => "KH", "name" => "Cambodia", "d_code" => "+855");
        $countries[] = array("code" => "CM", "name" => "Cameroon", "d_code" => "+237");
        $countries[] = array("code" => "CA", "name" => "Canada", "d_code" => "+1");
        $countries[] = array("code" => "CV", "name" => "Cape Verde", "d_code" => "+238");
        $countries[] = array("code" => "KY", "name" => "Cayman Islands", "d_code" => "+1");
        $countries[] = array("code" => "CF", "name" => "Central African Republic", "d_code" => "+236");
        $countries[] = array("code" => "TD", "name" => "Chad", "d_code" => "+235");
        $countries[] = array("code" => "CL", "name" => "Chile", "d_code" => "+56");
        $countries[] = array("code" => "CN", "name" => "China", "d_code" => "+86");
        $countries[] = array("code" => "CO", "name" => "Colombia", "d_code" => "+57");
        $countries[] = array("code" => "KM", "name" => "Comoros", "d_code" => "+269");
        $countries[] = array("code" => "CK", "name" => "Cook Islands", "d_code" => "+682");
        $countries[] = array("code" => "CR", "name" => "Costa Rica", "d_code" => "+506");
        $countries[] = array("code" => "CI", "name" => "Côte d'Ivoire", "d_code" => "+225");
        $countries[] = array("code" => "HR", "name" => "Croatia", "d_code" => "+385");
        $countries[] = array("code" => "CU", "name" => "Cuba", "d_code" => "+53");
        $countries[] = array("code" => "CY", "name" => "Cyprus", "d_code" => "+357");
        $countries[] = array("code" => "CZ", "name" => "Czech Republic", "d_code" => "+420");
        $countries[] = array("code" => "CD", "name" => "Democratic Republic of Congo", "d_code" => "+243");
        $countries[] = array("code" => "DK", "name" => "Denmark", "d_code" => "+45");
        $countries[] = array("code" => "DJ", "name" => "Djibouti", "d_code" => "+253");
        $countries[] = array("code" => "DM", "name" => "Dominica", "d_code" => "+1");
        $countries[] = array("code" => "DO", "name" => "Dominican Republic", "d_code" => "+1");
        $countries[] = array("code" => "EC", "name" => "Ecuador", "d_code" => "+593");
        $countries[] = array("code" => "EG", "name" => "Egypt", "d_code" => "+20");
        $countries[] = array("code" => "SV", "name" => "El Salvador", "d_code" => "+503");
        $countries[] = array("code" => "GQ", "name" => "Equatorial Guinea", "d_code" => "+240");
        $countries[] = array("code" => "ER", "name" => "Eritrea", "d_code" => "+291");
        $countries[] = array("code" => "EE", "name" => "Estonia", "d_code" => "+372");
        $countries[] = array("code" => "ET", "name" => "Ethiopia", "d_code" => "+251");
        $countries[] = array("code" => "FK", "name" => "Falkland Islands", "d_code" => "+500");
        $countries[] = array("code" => "FO", "name" => "Faroe Islands", "d_code" => "+298");
        $countries[] = array("code" => "FM", "name" => "Federated States of Micronesia", "d_code" => "+691");
        $countries[] = array("code" => "FJ", "name" => "Fiji", "d_code" => "+679");
        $countries[] = array("code" => "FI", "name" => "Finland", "d_code" => "+358");
        $countries[] = array("code" => "FR", "name" => "France", "d_code" => "+33");
        $countries[] = array("code" => "GF", "name" => "French Guiana", "d_code" => "+594");
        $countries[] = array("code" => "PF", "name" => "French Polynesia", "d_code" => "+689");
        $countries[] = array("code" => "GA", "name" => "Gabon", "d_code" => "+241");
        $countries[] = array("code" => "GE", "name" => "Georgia", "d_code" => "+995");
        $countries[] = array("code" => "DE", "name" => "Germany", "d_code" => "+49");
        $countries[] = array("code" => "GH", "name" => "Ghana", "d_code" => "+233");
        $countries[] = array("code" => "GI", "name" => "Gibraltar", "d_code" => "+350");
        $countries[] = array("code" => "GR", "name" => "Greece", "d_code" => "+30");
        $countries[] = array("code" => "GL", "name" => "Greenland", "d_code" => "+299");
        $countries[] = array("code" => "GD", "name" => "Grenada", "d_code" => "+1");
        $countries[] = array("code" => "GP", "name" => "Guadeloupe", "d_code" => "+590");
        $countries[] = array("code" => "GU", "name" => "Guam", "d_code" => "+1");
        $countries[] = array("code" => "GT", "name" => "Guatemala", "d_code" => "+502");
        $countries[] = array("code" => "GN", "name" => "Guinea", "d_code" => "+224");
        $countries[] = array("code" => "GW", "name" => "Guinea-Bissau", "d_code" => "+245");
        $countries[] = array("code" => "GY", "name" => "Guyana", "d_code" => "+592");
        $countries[] = array("code" => "HT", "name" => "Haiti", "d_code" => "+509");
        $countries[] = array("code" => "HN", "name" => "Honduras", "d_code" => "+504");
        $countries[] = array("code" => "HK", "name" => "Hong Kong", "d_code" => "+852");
        $countries[] = array("code" => "HU", "name" => "Hungary", "d_code" => "+36");
        $countries[] = array("code" => "IS", "name" => "Iceland", "d_code" => "+354");
        $countries[] = array("code" => "IN", "name" => "India", "d_code" => "+91");
        $countries[] = array("code" => "ID", "name" => "Indonesia", "d_code" => "+62");
        $countries[] = array("code" => "IR", "name" => "Iran", "d_code" => "+98");
        $countries[] = array("code" => "IQ", "name" => "Iraq", "d_code" => "+964");
        $countries[] = array("code" => "IE", "name" => "Ireland", "d_code" => "+353");
        $countries[] = array("code" => "IL", "name" => "Israel", "d_code" => "+972");
        $countries[] = array("code" => "IT", "name" => "Italy", "d_code" => "+39");
        $countries[] = array("code" => "JM", "name" => "Jamaica", "d_code" => "+1");
        $countries[] = array("code" => "JP", "name" => "Japan", "d_code" => "+81");
        $countries[] = array("code" => "JO", "name" => "Jordan", "d_code" => "+962");
        $countries[] = array("code" => "KZ", "name" => "Kazakhstan", "d_code" => "+7");
        $countries[] = array("code" => "KE", "name" => "Kenya", "d_code" => "+254");
        $countries[] = array("code" => "KI", "name" => "Kiribati", "d_code" => "+686");
        $countries[] = array("code" => "XK", "name" => "Kosovo", "d_code" => "+381");
        $countries[] = array("code" => "KW", "name" => "Kuwait", "d_code" => "+965");
        $countries[] = array("code" => "KG", "name" => "Kyrgyzstan", "d_code" => "+996");
        $countries[] = array("code" => "LA", "name" => "Laos", "d_code" => "+856");
        $countries[] = array("code" => "LV", "name" => "Latvia", "d_code" => "+371");
        $countries[] = array("code" => "LB", "name" => "Lebanon", "d_code" => "+961");
        $countries[] = array("code" => "LS", "name" => "Lesotho", "d_code" => "+266");
        $countries[] = array("code" => "LR", "name" => "Liberia", "d_code" => "+231");
        $countries[] = array("code" => "LY", "name" => "Libya", "d_code" => "+218");
        $countries[] = array("code" => "LI", "name" => "Liechtenstein", "d_code" => "+423");
        $countries[] = array("code" => "LT", "name" => "Lithuania", "d_code" => "+370");
        $countries[] = array("code" => "LU", "name" => "Luxembourg", "d_code" => "+352");
        $countries[] = array("code" => "MO", "name" => "Macau", "d_code" => "+853");
        $countries[] = array("code" => "MK", "name" => "Macedonia", "d_code" => "+389");
        $countries[] = array("code" => "MG", "name" => "Madagascar", "d_code" => "+261");
        $countries[] = array("code" => "MW", "name" => "Malawi", "d_code" => "+265");
        $countries[] = array("code" => "MY", "name" => "Malaysia", "d_code" => "+60");
        $countries[] = array("code" => "MV", "name" => "Maldives", "d_code" => "+960");
        $countries[] = array("code" => "ML", "name" => "Mali", "d_code" => "+223");
        $countries[] = array("code" => "MT", "name" => "Malta", "d_code" => "+356");
        $countries[] = array("code" => "MH", "name" => "Marshall Islands", "d_code" => "+692");
        $countries[] = array("code" => "MQ", "name" => "Martinique", "d_code" => "+596");
        $countries[] = array("code" => "MR", "name" => "Mauritania", "d_code" => "+222");
        $countries[] = array("code" => "MU", "name" => "Mauritius", "d_code" => "+230");
        $countries[] = array("code" => "YT", "name" => "Mayotte", "d_code" => "+262");
        $countries[] = array("code" => "MX", "name" => "Mexico", "d_code" => "+52");
        $countries[] = array("code" => "MD", "name" => "Moldova", "d_code" => "+373");
        $countries[] = array("code" => "MC", "name" => "Monaco", "d_code" => "+377");
        $countries[] = array("code" => "MN", "name" => "Mongolia", "d_code" => "+976");
        $countries[] = array("code" => "ME", "name" => "Montenegro", "d_code" => "+382");
        $countries[] = array("code" => "MS", "name" => "Montserrat", "d_code" => "+1");
        $countries[] = array("code" => "MA", "name" => "Morocco", "d_code" => "+212");
        $countries[] = array("code" => "MZ", "name" => "Mozambique", "d_code" => "+258");
        $countries[] = array("code" => "NA", "name" => "Namibia", "d_code" => "+264");
        $countries[] = array("code" => "NR", "name" => "Nauru", "d_code" => "+674");
        $countries[] = array("code" => "NP", "name" => "Nepal", "d_code" => "+977");
        $countries[] = array("code" => "NL", "name" => "Netherlands", "d_code" => "+31");
        $countries[] = array("code" => "AN", "name" => "Netherlands Antilles", "d_code" => "+599");
        $countries[] = array("code" => "NC", "name" => "New Caledonia", "d_code" => "+687");
        $countries[] = array("code" => "NZ", "name" => "New Zealand", "d_code" => "+64");
        $countries[] = array("code" => "NI", "name" => "Nicaragua", "d_code" => "+505");
        $countries[] = array("code" => "NE", "name" => "Niger", "d_code" => "+227");
        $countries[] = array("code" => "NG", "name" => "Nigeria", "d_code" => "+234");
        $countries[] = array("code" => "NU", "name" => "Niue", "d_code" => "+683");
        $countries[] = array("code" => "NF", "name" => "Norfolk Island", "d_code" => "+672");
        $countries[] = array("code" => "KP", "name" => "North Korea", "d_code" => "+850");
        $countries[] = array("code" => "MP", "name" => "Northern Mariana Islands", "d_code" => "+1");
        $countries[] = array("code" => "NO", "name" => "Norway", "d_code" => "+47");
        $countries[] = array("code" => "OM", "name" => "Oman", "d_code" => "+968");
        $countries[] = array("code" => "PK", "name" => "Pakistan", "d_code" => "+92");
        $countries[] = array("code" => "PW", "name" => "Palau", "d_code" => "+680");
        $countries[] = array("code" => "PS", "name" => "Palestine", "d_code" => "+970");
        $countries[] = array("code" => "PA", "name" => "Panama", "d_code" => "+507");
        $countries[] = array("code" => "PG", "name" => "Papua New Guinea", "d_code" => "+675");
        $countries[] = array("code" => "PY", "name" => "Paraguay", "d_code" => "+595");
        $countries[] = array("code" => "PE", "name" => "Peru", "d_code" => "+51");
        $countries[] = array("code" => "PH", "name" => "Philippines", "d_code" => "+63");
        $countries[] = array("code" => "PL", "name" => "Poland", "d_code" => "+48");
        $countries[] = array("code" => "PT", "name" => "Portugal", "d_code" => "+351");
        $countries[] = array("code" => "PR", "name" => "Puerto Rico", "d_code" => "+1");
        $countries[] = array("code" => "QA", "name" => "Qatar", "d_code" => "+974");
        $countries[] = array("code" => "CG", "name" => "Republic of the Congo", "d_code" => "+242");
        $countries[] = array("code" => "RE", "name" => "Réunion", "d_code" => "+262");
        $countries[] = array("code" => "RO", "name" => "Romania", "d_code" => "+40");
        $countries[] = array("code" => "RU", "name" => "Russia", "d_code" => "+7");
        $countries[] = array("code" => "RW", "name" => "Rwanda", "d_code" => "+250");
        $countries[] = array("code" => "BL", "name" => "Saint Barthélemy", "d_code" => "+590");
        $countries[] = array("code" => "SH", "name" => "Saint Helena", "d_code" => "+290");
        $countries[] = array("code" => "KN", "name" => "Saint Kitts and Nevis", "d_code" => "+1");
        $countries[] = array("code" => "MF", "name" => "Saint Martin", "d_code" => "+590");
        $countries[] = array("code" => "PM", "name" => "Saint Pierre and Miquelon", "d_code" => "+508");
        $countries[] = array("code" => "VC", "name" => "Saint Vincent and the Grenadines", "d_code" => "+1");
        $countries[] = array("code" => "WS", "name" => "Samoa", "d_code" => "+685");
        $countries[] = array("code" => "SM", "name" => "San Marino", "d_code" => "+378");
        $countries[] = array("code" => "ST", "name" => "São Tomé and Príncipe", "d_code" => "+239");
        $countries[] = array("code" => "SA", "name" => "Saudi Arabia", "d_code" => "+966");
        $countries[] = array("code" => "SN", "name" => "Senegal", "d_code" => "+221");
        $countries[] = array("code" => "RS", "name" => "Serbia", "d_code" => "+381");
        $countries[] = array("code" => "SC", "name" => "Seychelles", "d_code" => "+248");
        $countries[] = array("code" => "SL", "name" => "Sierra Leone", "d_code" => "+232");
        $countries[] = array("code" => "SG", "name" => "Singapore", "d_code" => "+65");
        $countries[] = array("code" => "SK", "name" => "Slovakia", "d_code" => "+421");
        $countries[] = array("code" => "SI", "name" => "Slovenia", "d_code" => "+386");
        $countries[] = array("code" => "SB", "name" => "Solomon Islands", "d_code" => "+677");
        $countries[] = array("code" => "SO", "name" => "Somalia", "d_code" => "+252");
        $countries[] = array("code" => "ZA", "name" => "South Africa", "d_code" => "+27");
        $countries[] = array("code" => "KR", "name" => "South Korea", "d_code" => "+82");
        $countries[] = array("code" => "ES", "name" => "Spain", "d_code" => "+34");
        $countries[] = array("code" => "LK", "name" => "Sri Lanka", "d_code" => "+94");
        $countries[] = array("code" => "LC", "name" => "St. Lucia", "d_code" => "+1");
        $countries[] = array("code" => "SD", "name" => "Sudan", "d_code" => "+249");
        $countries[] = array("code" => "SR", "name" => "Suriname", "d_code" => "+597");
        $countries[] = array("code" => "SZ", "name" => "Swaziland", "d_code" => "+268");
        $countries[] = array("code" => "SE", "name" => "Sweden", "d_code" => "+46");
        $countries[] = array("code" => "CH", "name" => "Switzerland", "d_code" => "+41");
        $countries[] = array("code" => "SY", "name" => "Syria", "d_code" => "+963");
        $countries[] = array("code" => "TW", "name" => "Taiwan", "d_code" => "+886");
        $countries[] = array("code" => "TJ", "name" => "Tajikistan", "d_code" => "+992");
        $countries[] = array("code" => "TZ", "name" => "Tanzania", "d_code" => "+255");
        $countries[] = array("code" => "TH", "name" => "Thailand", "d_code" => "+66");
        $countries[] = array("code" => "BS", "name" => "The Bahamas", "d_code" => "+1");
        $countries[] = array("code" => "GM", "name" => "The Gambia", "d_code" => "+220");
        $countries[] = array("code" => "TL", "name" => "Timor-Leste", "d_code" => "+670");
        $countries[] = array("code" => "TG", "name" => "Togo", "d_code" => "+228");
        $countries[] = array("code" => "TK", "name" => "Tokelau", "d_code" => "+690");
        $countries[] = array("code" => "TO", "name" => "Tonga", "d_code" => "+676");
        $countries[] = array("code" => "TT", "name" => "Trinidad and Tobago", "d_code" => "+1");
        $countries[] = array("code" => "TN", "name" => "Tunisia", "d_code" => "+216");
        $countries[] = array("code" => "TR", "name" => "Turkey", "d_code" => "+90");
        $countries[] = array("code" => "TM", "name" => "Turkmenistan", "d_code" => "+993");
        $countries[] = array("code" => "TC", "name" => "Turks and Caicos Islands", "d_code" => "+1");
        $countries[] = array("code" => "TV", "name" => "Tuvalu", "d_code" => "+688");
        $countries[] = array("code" => "UG", "name" => "Uganda", "d_code" => "+256");
        $countries[] = array("code" => "UA", "name" => "Ukraine", "d_code" => "+380");
        $countries[] = array("code" => "AE", "name" => "United Arab Emirates", "d_code" => "+971");
        $countries[] = array("code" => "GB", "name" => "United Kingdom", "d_code" => "+44");
        $countries[] = array("code" => "US", "name" => "United States", "d_code" => "+1");
        $countries[] = array("code" => "UY", "name" => "Uruguay", "d_code" => "+598");
        $countries[] = array("code" => "UZ", "name" => "Uzbekistan", "d_code" => "+998");
        $countries[] = array("code" => "VU", "name" => "Vanuatu", "d_code" => "+678");
        $countries[] = array("code" => "VA", "name" => "Vatican City", "d_code" => "+39");
        $countries[] = array("code" => "VE", "name" => "Venezuela", "d_code" => "+58");
        $countries[] = array("code" => "VN", "name" => "Vietnam", "d_code" => "+84");
        $countries[] = array("code" => "WF", "name" => "Wallis and Futuna", "d_code" => "+681");
        $countries[] = array("code" => "YE", "name" => "Yemen", "d_code" => "+967");
        $countries[] = array("code" => "ZM", "name" => "Zambia", "d_code" => "+260");
        $countries[] = array("code" => "ZW", "name" => "Zimbabwe", "d_code" => "+263");
        $Code = '';
        foreach ($countries as $theCountry) {
            $codeLength = strlen($theCountry['d_code']);
            if (substr($Phone, 0, $codeLength) == $theCountry['d_code']) {
                $Code = $theCountry['d_code'];
                $Phone = substr($Phone, $codeLength);
                break;
            }
        }
        $PhoneInfo = array("Code" => $Code, "Phone" => $Phone);
        return $PhoneInfo;
    }
}
function generateAdjNounPhrase()
{
    $AjectiveJson = '
    {"data":[
            {"adjective":"different"},
            {"adjective":"used"},
            {"adjective":"important"},
            {"adjective":"every"},
            {"adjective":"large"},
            {"adjective":"available"},
            {"adjective":"popular"},
            {"adjective":"able"},
            {"adjective":"basic"},
            {"adjective":"known"},
            {"adjective":"various"},
            {"adjective":"difficult"},
            {"adjective":"several"},
            {"adjective":"united"},
            {"adjective":"hot"},
            {"adjective":"useful"},
            {"adjective":"mental"},
            {"adjective":"scared"},
            {"adjective":"emotional"},
            {"adjective":"old"},
            {"adjective":"political"},
            {"adjective":"similar"},
            {"adjective":"healthy"},
            {"adjective":"financial"},
            {"adjective":"medical"},
            {"adjective":"federal"},
            {"adjective":"entire"},
            {"adjective":"strong"},
            {"adjective":"actual"},
            {"adjective":"expensive"},
            {"adjective":"pregnant"},
            {"adjective":"poor"},
            {"adjective":"happy"},
            {"adjective":"cute"},
            {"adjective":"helpful"},
            {"adjective":"recent"},
            {"adjective":"willing"},
            {"adjective":"nice"},
            {"adjective":"wonderful"},
            {"adjective":"serious"},
            {"adjective":"huge"},
            {"adjective":"rare"},
            {"adjective":"technical"},
            {"adjective":"typical"},
            {"adjective":"critical"},
            {"adjective":"immediate"},
            {"adjective":"aware"},
            {"adjective":"global"},
            {"adjective":"legal"},
            {"adjective":"relevant"},
            {"adjective":"accurate"},
            {"adjective":"capable"},
            {"adjective":"dangerous"},
            {"adjective":"dramatic"},
            {"adjective":"efficient"},
            {"adjective":"powerful"},
            {"adjective":"foreign"},
            {"adjective":"hungry"},
            {"adjective":"practical"},
            {"adjective":"severe"},
            {"adjective":"suitable"},
            {"adjective":"numerous"},
            {"adjective":"unusual"},
            {"adjective":"cultural"},
            {"adjective":"existing"},
            {"adjective":"famous"},
            {"adjective":"pure"},
            {"adjective":"afraid"},
            {"adjective":"obvious"},
            {"adjective":"careful"},
            {"adjective":"latter"},
            {"adjective":"obviously"},
            {"adjective":"unhappy"},
            {"adjective":"boring"},
            {"adjective":"distinct"},
            {"adjective":"eastern"},
            {"adjective":"logical"},
            {"adjective":"strict"},
            {"adjective":"automatic"},
            {"adjective":"civil"},
            {"adjective":"former"},
            {"adjective":"massive"},
            {"adjective":"southern"},
            {"adjective":"unfair"},
            {"adjective":"visible"},
            {"adjective":"alive"},
            {"adjective":"angry"},
            {"adjective":"desperate"},
            {"adjective":"exciting"},
            {"adjective":"friendly"},
            {"adjective":"lucky"},
            {"adjective":"realistic"},
            {"adjective":"sorry"},
            {"adjective":"ugly"},
            {"adjective":"unlikely"},
            {"adjective":"anxious"},
            {"adjective":"curious"},
            {"adjective":"informal"},
            {"adjective":"inner"},
            {"adjective":"pleasant"},
            {"adjective":"sexual"},
            {"adjective":"sudden"},
            {"adjective":"terrible"},
            {"adjective":"unable"},
            {"adjective":"weak"},
            {"adjective":"wooden"},
            {"adjective":"asleep"},
            {"adjective":"confident"},
            {"adjective":"conscious"},
            {"adjective":"decent"},
            {"adjective":"guilty"},
            {"adjective":"lonely"},
            {"adjective":"mad"},
            {"adjective":"nervous"},
            {"adjective":"odd"},
            {"adjective":"tall"},
            {"adjective":"tiny"}
            ]}';
    $NounJson = '
    {"data":[
            {"noun":"people"},
            {"noun":"history"},
            {"noun":"way"},
            {"noun":"art"},
            {"noun":"world"},
            {"noun":"map"},
            {"noun":"two"},
            {"noun":"family"},
            {"noun":"government"},
            {"noun":"health"},
            {"noun":"system"},
            {"noun":"computer"},
            {"noun":"meat"},
            {"noun":"year"},
            {"noun":"thanks"},
            {"noun":"music"},
            {"noun":"person"},
            {"noun":"reading"},
            {"noun":"method"},
            {"noun":"data"},
            {"noun":"food"},
            {"noun":"theory"},
            {"noun":"law"},
            {"noun":"bird"},
            {"noun":"literature"},
            {"noun":"problem"},
            {"noun":"software"},
            {"noun":"control"},
            {"noun":"knowledge"},
            {"noun":"power"},
            {"noun":"ability"},
            {"noun":"economics"},
            {"noun":"love"},
            {"noun":"internet"},
            {"noun":"television"},
            {"noun":"science"},
            {"noun":"library"},
            {"noun":"nature"},
            {"noun":"fact"},
            {"noun":"product"},
            {"noun":"idea"},
            {"noun":"investment"},
            {"noun":"area"},
            {"noun":"society"},
            {"noun":"activity"},
            {"noun":"story"},
            {"noun":"industry"},
            {"noun":"media"},
            {"noun":"thing"},
            {"noun":"oven"},
            {"noun":"community"},
            {"noun":"definition"},
            {"noun":"safety"},
            {"noun":"quality"},
            {"noun":"language"},
            {"noun":"management"},
            {"noun":"player"},
            {"noun":"variety"},
            {"noun":"video"},
            {"noun":"week"},
            {"noun":"security"},
            {"noun":"country"},
            {"noun":"exam"},
            {"noun":"movie"},
            {"noun":"equipment"},
            {"noun":"physics"},
            {"noun":"analysis"},
            {"noun":"policy"},
            {"noun":"series"},
            {"noun":"thought"},
            {"noun":"basis"},
            {"noun":"boyfriend"},
            {"noun":"direction"},
            {"noun":"strategy"},
            {"noun":"technology"},
            {"noun":"army"},
            {"noun":"camera"},
            {"noun":"freedom"},
            {"noun":"paper"},
            {"noun":"child"},
            {"noun":"instance"},
            {"noun":"month"},
            {"noun":"truth"},
            {"noun":"marketing"},
            {"noun":"university"},
            {"noun":"writing"},
            {"noun":"article"},
            {"noun":"department"},
            {"noun":"difference"},
            {"noun":"goal"},
            {"noun":"news"},
            {"noun":"audience"},
            {"noun":"fishing"},
            {"noun":"growth"},
            {"noun":"income"},
            {"noun":"marriage"},
            {"noun":"user"},
            {"noun":"failure"},
            {"noun":"meaning"},
            {"noun":"medicine"},
            {"noun":"philosophy"},
            {"noun":"teacher"},
            {"noun":"night"},
            {"noun":"chemistry"},
            {"noun":"disease"},
            {"noun":"disk"},
            {"noun":"energy"},
            {"noun":"nation"},
            {"noun":"road"},
            {"noun":"role"},
            {"noun":"soup"},
            {"noun":"location"},
            {"noun":"success"},
            {"noun":"addition"},
            {"noun":"apartment"},
            {"noun":"education"},
            {"noun":"math"},
            {"noun":"moment"},
            {"noun":"painting"},
            {"noun":"politics"},
            {"noun":"attention"},
            {"noun":"decision"},
            {"noun":"event"},
            {"noun":"property"},
            {"noun":"shopping"},
            {"noun":"student"},
            {"noun":"wood"},
            {"noun":"office"},
            {"noun":"population"},
            {"noun":"president"},
            {"noun":"unit"},
            {"noun":"category"},
            {"noun":"cigarette"},
            {"noun":"context"},
            {"noun":"driver"},
            {"noun":"flight"},
            {"noun":"length"},
            {"noun":"magazine"},
            {"noun":"newspaper"},
            {"noun":"teaching"},
            {"noun":"cell"},
            {"noun":"dealer"},
            {"noun":"finding"},
            {"noun":"lake"},
            {"noun":"member"},
            {"noun":"message"},
            {"noun":"phone"},
            {"noun":"scene"},
            {"noun":"appearance"},
            {"noun":"concept"},
            {"noun":"customer"},
            {"noun":"death"},
            {"noun":"discussion"},
            {"noun":"housing"},
            {"noun":"inflation"},
            {"noun":"insurance"},
            {"noun":"mood"},
            {"noun":"woman"},
            {"noun":"advice"},
            {"noun":"blood"},
            {"noun":"effort"},
            {"noun":"expression"},
            {"noun":"importance"},
            {"noun":"opinion"},
            {"noun":"payment"},
            {"noun":"reality"},
            {"noun":"situation"},
            {"noun":"skill"},
            {"noun":"statement"},
            {"noun":"wealth"},
            {"noun":"city"},
            {"noun":"county"},
            {"noun":"depth"},
            {"noun":"estate"},
            {"noun":"foundation"},
            {"noun":"heart"},
            {"noun":"photo"},
            {"noun":"recipe"},
            {"noun":"studio"},
            {"noun":"topic"},
            {"noun":"collection"},
            {"noun":"depression"},
            {"noun":"passion"},
            {"noun":"percentage"},
            {"noun":"resource"},
            {"noun":"setting"},
            {"noun":"ad"},
            {"noun":"agency"},
            {"noun":"college"},
            {"noun":"connection"},
            {"noun":"criticism"},
            {"noun":"debt"},
            {"noun":"memory"},
            {"noun":"patience"},
            {"noun":"secretary"},
            {"noun":"solution"},
            {"noun":"aspect"},
            {"noun":"attitude"},
            {"noun":"director"},
            {"noun":"psychology"},
            {"noun":"response"},
            {"noun":"selection"},
            {"noun":"storage"},
            {"noun":"version"},
            {"noun":"alcohol"},
            {"noun":"argument"},
            {"noun":"complaint"},
            {"noun":"contract"},
            {"noun":"emphasis"},
            {"noun":"highway"},
            {"noun":"loss"},
            {"noun":"membership"},
            {"noun":"possession"},
            {"noun":"steak"},
            {"noun":"union"},
            {"noun":"agreement"},
            {"noun":"cancer"},
            {"noun":"currency"},
            {"noun":"employment"},
            {"noun":"entry"},
            {"noun":"mixture"},
            {"noun":"preference"},
            {"noun":"region"},
            {"noun":"republic"},
            {"noun":"tradition"},
            {"noun":"virus"},
            {"noun":"actor"},
            {"noun":"classroom"},
            {"noun":"delivery"},
            {"noun":"device"},
            {"noun":"difficulty"},
            {"noun":"drama"},
            {"noun":"election"},
            {"noun":"engine"},
            {"noun":"football"},
            {"noun":"guidance"},
            {"noun":"hotel"},
            {"noun":"owner"},
            {"noun":"priority"},
            {"noun":"protection"},
            {"noun":"suggestion"},
            {"noun":"tension"},
            {"noun":"variation"},
            {"noun":"anxiety"},
            {"noun":"atmosphere"},
            {"noun":"awareness"},
            {"noun":"bath"},
            {"noun":"bread"},
            {"noun":"candidate"},
            {"noun":"climate"},
            {"noun":"comparison"},
            {"noun":"confusion"},
            {"noun":"elevator"},
            {"noun":"emotion"},
            {"noun":"employee"},
            {"noun":"employer"},
            {"noun":"guest"},
            {"noun":"height"},
            {"noun":"leadership"},
            {"noun":"mall"},
            {"noun":"manager"},
            {"noun":"operation"},
            {"noun":"recording"},
            {"noun":"sample"},
            {"noun":"charity"},
            {"noun":"cousin"},
            {"noun":"disaster"},
            {"noun":"editor"},
            {"noun":"efficiency"},
            {"noun":"excitement"},
            {"noun":"extent"},
            {"noun":"feedback"},
            {"noun":"guitar"},
            {"noun":"homework"},
            {"noun":"leader"},
            {"noun":"mom"},
            {"noun":"outcome"},
            {"noun":"permission"},
            {"noun":"promotion"},
            {"noun":"reflection"},
            {"noun":"resolution"},
            {"noun":"revenue"},
            {"noun":"session"},
            {"noun":"singer"},
            {"noun":"tennis"},
            {"noun":"basket"},
            {"noun":"bonus"},
            {"noun":"cabinet"},
            {"noun":"childhood"},
            {"noun":"church"},
            {"noun":"clothes"},
            {"noun":"coffee"},
            {"noun":"dinner"},
            {"noun":"drawing"},
            {"noun":"hair"},
            {"noun":"hearing"},
            {"noun":"initiative"},
            {"noun":"judgment"},
            {"noun":"lab"},
            {"noun":"mode"},
            {"noun":"mud"},
            {"noun":"orange"},
            {"noun":"poetry"},
            {"noun":"police"},
            {"noun":"procedure"},
            {"noun":"queen"},
            {"noun":"ratio"},
            {"noun":"relation"},
            {"noun":"restaurant"},
            {"noun":"sector"},
            {"noun":"signature"},
            {"noun":"song"},
            {"noun":"tooth"},
            {"noun":"town"},
            {"noun":"vehicle"},
            {"noun":"volume"},
            {"noun":"wife"},
            {"noun":"accident"},
            {"noun":"airport"},
            {"noun":"arrival"},
            {"noun":"assumption"},
            {"noun":"baseball"},
            {"noun":"chapter"},
            {"noun":"committee"},
            {"noun":"database"},
            {"noun":"enthusiasm"},
            {"noun":"error"},
            {"noun":"farmer"},
            {"noun":"gate"},
            {"noun":"girl"},
            {"noun":"hall"},
            {"noun":"historian"},
            {"noun":"hospital"},
            {"noun":"injury"},
            {"noun":"meal"},
            {"noun":"perception"},
            {"noun":"pie"},
            {"noun":"poem"},
            {"noun":"presence"},
            {"noun":"proposal"},
            {"noun":"reception"},
            {"noun":"revolution"},
            {"noun":"river"},
            {"noun":"son"},
            {"noun":"speech"},
            {"noun":"tea"},
            {"noun":"village"},
            {"noun":"warning"},
            {"noun":"winner"},
            {"noun":"worker"},
            {"noun":"writer"},
            {"noun":"assistance"},
            {"noun":"breath"},
            {"noun":"buyer"},
            {"noun":"chest"},
            {"noun":"chocolate"},
            {"noun":"conclusion"},
            {"noun":"cookie"},
            {"noun":"courage"},
            {"noun":"dad"},
            {"noun":"desk"},
            {"noun":"drawer"},
            {"noun":"garbage"},
            {"noun":"grocery"},
            {"noun":"honey"},
            {"noun":"impression"},
            {"noun":"insect"},
            {"noun":"inspection"},
            {"noun":"inspector"},
            {"noun":"king"},
            {"noun":"ladder"},
            {"noun":"menu"},
            {"noun":"penalty"},
            {"noun":"piano"},
            {"noun":"potato"},
            {"noun":"profession"},
            {"noun":"professor"},
            {"noun":"quantity"},
            {"noun":"reaction"},
            {"noun":"salad"},
            {"noun":"sister"},
            {"noun":"tongue"},
            {"noun":"weakness"},
            {"noun":"wedding"},
            {"noun":"affair"},
            {"noun":"ambition"},
            {"noun":"analyst"},
            {"noun":"apple"},
            {"noun":"assignment"},
            {"noun":"assistant"},
            {"noun":"bathroom"},
            {"noun":"bedroom"},
            {"noun":"beer"},
            {"noun":"birthday"},
            {"noun":"cheek"},
            {"noun":"client"},
            {"noun":"departure"},
            {"noun":"diamond"},
            {"noun":"dirt"},
            {"noun":"ear"},
            {"noun":"fortune"},
            {"noun":"friendship"},
            {"noun":"funeral"},
            {"noun":"gene"},
            {"noun":"girlfriend"},
            {"noun":"hat"},
            {"noun":"indication"},
            {"noun":"intention"},
            {"noun":"lady"},
            {"noun":"midnight"},
            {"noun":"obligation"},
            {"noun":"passenger"},
            {"noun":"pizza"},
            {"noun":"platform"},
            {"noun":"poet"},
            {"noun":"pollution"},
            {"noun":"reputation"},
            {"noun":"shirt"},
            {"noun":"sir"},
            {"noun":"speaker"},
            {"noun":"stranger"},
            {"noun":"surgery"},
            {"noun":"sympathy"},
            {"noun":"tale"},
            {"noun":"throat"},
            {"noun":"trainer"},
            {"noun":"uncle"},
            {"noun":"youth"}
            ]}';

    $AjectiveObj = json_decode($AjectiveJson, true);
    $NounObj = json_decode($NounJson, true);
    $Ajective = $AjectiveObj['data'][rand(0, count($AjectiveObj['data']) - 1)];
    $Noun = $NounObj['data'][rand(0, count($NounObj['data']) - 1)];
    return $Ajective['adjective'] . '-' . $Noun['noun'];
}

if (!function_exists('infusionsoft_connected_data_process')) {
    function infusionsoft_connected_data_process($Data)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $result = [];
        $Data = base64_decode($Data);
        $Data = json_decode($Data, true);
        $Process = "infusionsoft_" . $Data['request'];
        $Parameters = $Data['data'];
        if (function_exists($Process)) {
            $output = $Process($Parameters);
            if($output == false){
                return ["Error"=>"Could not process ConnectedData With Given json Data"];
            }
            $result[] = $output;
        } else {
            $result[] = "No Existing Function: {$Process}";
        }
        return $result;
    }
}
function infusionsoft_set_next_sequence($Parameters)
{
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('macanta_helper');
    $contactId = trim($Parameters['contactId']);
    $currentSequence = trim($Parameters['currentSequence']);
    $nextSequence = trim($Parameters['nextSequence']);
    $cd_guid = isset($Parameters['cd_guid']) ? $Parameters['cd_guid'] : "";
    $moveAfter = isset($Parameters['moveAfter']) ? trim($Parameters['moveAfter']):"5 sec";
    $moveWhen = isset($Parameters['moveWhen']) ? trim($Parameters['moveWhen']):false;
    unset($Parameters['contactId']);
    unset($Parameters['currentSequence']);
    unset($Parameters['nextSequence']);
    unset($Parameters['moveAfter']);
    unset($Parameters['moveWhen']);
    unset($Parameters['api_key']); // just in case there has api_key
    unset($Parameters['access_key']);
    unset($Parameters['cd_guid']);

    if($cd_guid != ""){
        $CI->db->where('id', $cd_guid);
    }else{
        $CI->db->like('connected_contact', '"' . $contactId . '":{');
    }
    $query = $CI->db->get('connected_data');
    $ContactIdArr = [];
    foreach ($query->result() as $row) {
        $Id = $row->id;
        $ConnectedContacts = json_decode($row->connected_contact, true);
        $ConnectedData = json_decode($row->value, true);
        $NameIdentifier ='';
        foreach ($ConnectedData as $fieldName=>$fieldValue){
            $NameIdentifier = $fieldValue;
            break;
        }
        $toSaved = $ConnectedContacts;
        if(isset($ConnectedContacts[$contactId]['Sequence'][$currentSequence]) && $ConnectedContacts[$contactId]['Sequence'][$currentSequence]['Status'] == 'running'){

            if(sizeof($Parameters) > 0){
                $Parsed = infusionsoft_parse_criteria($Parameters);
                $toSaved[$contactId]['Sequence'][$currentSequence]['Parsed'] = $Parsed;
            }
            $toSaved[$contactId]['Sequence'][$currentSequence]['Status'] = 'end';
            $toSaved[$contactId]['Sequence'][$currentSequence]['NextSequence'] = $nextSequence;
            $toSaved[$contactId]['Sequence'][$currentSequence]['MoveAfter'] = strtotime($moveAfter);
            if($moveWhen !== false && $moveWhen !== '') $toSaved[$contactId]['Sequence'][$currentSequence]['MoveWhen'] = strtolower($moveWhen);
            if(strtolower($toSaved[$contactId]['Sequence'][$currentSequence]['NextSequence'] ) == "none"){
                unset($toSaved[$contactId]['Sequence'][$currentSequence]['NextSequence']);
                unset($toSaved[$contactId]['Sequence'][$currentSequence]['MoveAfter']);
                unset($toSaved[$contactId]['Sequence'][$currentSequence]['MoveWhen']);
                $ContactIdArr[$contactId] = ['Data'=>$NameIdentifier, 'To:'=>$nextSequence];
                $log = ['Campaign Ended For:'=>[$ContactIdArr]];

            }else{
                $ContactIdArr[$contactId] = ['Data'=>$NameIdentifier, 'DataId'=>$Id, 'To:'=>$nextSequence,'On:'=>date('Y-m-d H:i:s',strtotime($moveAfter))];
                $log = ['Set Next Sequence Details:'=>[$ContactIdArr]];
            }
            $DBdata = [];
            $DBdata['connected_contact'] = json_encode($toSaved);
            $CI->db->where('id', $Id);
            $CI->db->update('connected_data', $DBdata);
            unset($query);
            unset($Parsed);
            unset($toSaved);
            unset($DBdata);
            unset($ConnectedContacts);
            unset($ConnectedData);
            return $log;

        }
    }

}
function infusionsoft_trigger_move_to_next_sequence(){
    $verboseLog = false;
    $Logs = [];
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('macanta_helper');
    $CI->db->like('connected_contact', 'NextSequence');
    $query = $CI->db->get('connected_data');
    $GroupFieldsMap = macanta_get_connected_info_group_fields_map();
    $OriginalData = [];
    $ToCampaign = [];
    $ConnectedInfosByGroupName = [];
    foreach ($query->result() as $row) {
        $ItemId = $row->id;
        $GroupId = $row->group;
        $ConnectedContacts = json_decode($row->connected_contact, true);
        $ConnectedData = json_decode($row->value, true);
        $OriginalData[$ItemId] = $ConnectedContacts;
        foreach ($ConnectedContacts as $ContactId => $ContactDetails){
            if(isset($ContactDetails['Sequence'])){
                foreach ($ContactDetails['Sequence'] as $CurrentSequence=>$SequenceDetails){
                    if(isset($SequenceDetails['MoveAfter']) && isset($SequenceDetails['NextSequence']) && $SequenceDetails['MoveAfter'] < time()){
                        $NextSequence = $SequenceDetails['NextSequence'];
                        $InSequence = macanta_check_contact_sequence($ContactId,$NextSequence);
                        if($InSequence == true) continue; // Dont let a contact to move on a sequence while same contact is running on it.

                        //Check moveWhen Parameters
                        $Passed = true;
                        $Parameters = [];
                        if(isset($SequenceDetails['MoveWhen'])){
                            $ConnectedInfosByGroupName[$GroupId]  = isset($ConnectedInfosByGroupName[$GroupId]) ? $ConnectedInfosByGroupName[$GroupId]: macanta_get_connected_info_by_groupname('','', $GroupId);
                            $theCriteriaArr = explode(' is ',$SequenceDetails['MoveWhen']);
                            $theCriteriaArr = array_map('trim', $theCriteriaArr);
                            $FieldName = $theCriteriaArr[0];
                            $Criteria = @$theCriteriaArr[1];
                            $Parameters[$FieldName] = $Criteria;
                            $Parsed = infusionsoft_parse_criteria($Parameters);

                            foreach ($ConnectedInfosByGroupName[$GroupId] as $ConnectedInfo => $ItemDetails) {
                                $Fields = $ItemDetails[$ItemId]['fields'];
                                //Validate Fields values
                                $Passed = infusionsoft_evaluate_parsed_criteria($Parsed, $Fields,'Trigger MoveWhen', $ItemId);
                            }
                        }
                        if($Passed === false) continue; // if moveWhen is set and did not meet the criteria, skip

                        // Parsed is Given
                        if(isset($SequenceDetails['Parsed'])){
                            $ConnectedInfos = macanta_get_connected_info_by_groupname('','',$GroupId,$ItemId);
                            foreach ($ConnectedInfos as $connected_data => $Item) {
                                foreach ($Item as $ItemId=>$ItemDetails){
                                    $Fields = $ItemDetails['fields'];
                                    //Validate Fields values
                                    $Passed = infusionsoft_evaluate_parsed_criteria($SequenceDetails['Parsed'],$Fields,'Start Campaign', $ItemId);

                                }
                            }
                        }
                        if($Passed === false) continue; // if Parsed produces false result
                        // This will make contacts unique in sequences
                        $ToCampaign[$ContactId][$NextSequence]=['RestartAfter'=>$SequenceDetails['RestartAfter'],'PreviousSequence'=>$CurrentSequence,'GroupId' => $GroupId, 'ItemId' => $ItemId, 'ConnectedData' => $ConnectedData];
                    }
                }
            }
        }
    }
    foreach ($ToCampaign as $ContactId => $Sequences){

            foreach ($Sequences as $Sequence=>$theData){
                $NextSequence = $Sequence;
                $prefixLength = strlen($NextSequence);
                $CampaignSequenceIdArr = explode('.',$NextSequence);
                $ItemId = $theData['ItemId'];
                $GroupId = $theData['GroupId'];
                $PreviousSequence = $theData['PreviousSequence'];
                $restart_after = $theData['RestartAfter'];
                $ConnectedData = $theData['ConnectedData'];

                $data = [];
                $ItemIdentifier = '';
                foreach ($GroupFieldsMap as $GroupName => $GroupDetails) {
                    if($GroupDetails['id'] == $GroupId){
                        foreach ($GroupDetails['fields'] as $FieldId=>$FieldDetails){
                            if ($FieldDetails['custom-field'] != ""){
                                $ConnectedData[$FieldId] = trim($ConnectedData[$FieldId]);
                                if(is_array($ConnectedData[$FieldId])){
                                    $ConnectedData[$FieldId] = isset($ConnectedData[$FieldId]["id_" . (string)$ContactId]) ? $ConnectedData[$FieldId]["id_" . (string)$ContactId]:'';
                                }
                                if(strpos($ConnectedData[$FieldId],'|') !== false){
                                    $ConnectedData[$FieldId] = str_replace('|',',',$ConnectedData[$FieldId]);
                                }
                                $data[] = ['name' => "_" . $FieldDetails['custom-field'], 'value' => $ConnectedData[$FieldId]];
                                $ItemIdentifier = $ItemIdentifier == '' ? $ConnectedData[$FieldId]:$ItemIdentifier;
                            }
                        }
                        break;
                    }
                }
                if(sizeof($data) > 0){
                    $Logs[] = "[".date('Y-m-d H:i:s')."]"." Updating Contact Record #$ContactId with $ItemIdentifier";
                    infusionsoft_update_contact($data, $ContactId);
                }else{
                    if($verboseLog)
                        $Logs[] =  "[".date('Y-m-d H:i:s')."]"." Nothing To Update For Contact Record #$ContactId (check custom field assigned for each connected data fields )";
                }
                $CampaignDetails = infusionsoft_get_campaign_by_id($CampaignSequenceIdArr[0]);
                $FoundSequence = false;
                if (isset($CampaignDetails->message->sequences)) {
                    foreach ($CampaignDetails->message->sequences as $sequence) {
                        if (substr( $sequence->name, 0, $prefixLength ) === $NextSequence) {
                            $FoundSequence = true;
                            $sequenceId = $sequence->id;
                            $Logs[] =  "[".date('Y-m-d H:i:s')."]"." Moving Contact #$ContactId to Sequence Id: $sequenceId ( $NextSequence )";
                            infusionsoft_multiple_add_to_sequence($CampaignSequenceIdArr[0], $sequenceId, $ContactId);
                            $UpdatedConnectedContacts = $OriginalData[$ItemId];
                            $UpdatedConnectedContacts[$ContactId]['Sequence'][$NextSequence] = ["Status"=>'running',"RestartAfter"=>$restart_after];
                            if(isset($UpdatedConnectedContacts[$ContactId]['Sequence'][$PreviousSequence]['MoveAfter']))
                                unset($UpdatedConnectedContacts[$ContactId]['Sequence'][$PreviousSequence]['MoveAfter']);
                            if(isset($UpdatedConnectedContacts[$ContactId]['Sequence'][$PreviousSequence]['MoveWhen']))
                                unset($UpdatedConnectedContacts[$ContactId]['Sequence'][$PreviousSequence]['MoveWhen']);
                            $OriginalData[$ItemId] = $UpdatedConnectedContacts;
                            $Logs[] =  "[".date('Y-m-d H:i:s')."]"." Updating Database Connected Contact #$ContactId of Item $ItemId with Sequence ".json_encode($UpdatedConnectedContacts[$ContactId]['Sequence']);
                            $DBdata = [];
                            $DBdata['connected_contact'] = json_encode($UpdatedConnectedContacts);
                            $CI->db->where('id', $ItemId);
                            $CI->db->update('connected_data', $DBdata);
                            break;
                        }
                    }
                }
                if($FoundSequence == false){
                    if($verboseLog)
                        $Logs[] =  "[".date('Y-m-d H:i:s')."]"." No $NextSequence Sequence Found, Please check your Infusionsoft Sequences starting with $NextSequence";
                }
            }
    }
    if(sizeof($Logs) > 0){
        $LogsStr = implode("\n", $Logs)."\n======================================\n\n";
        echo $LogsStr;
    }
    //Add contact to the First Sequence in the Campaign with the its queued connected data
    // clear memories
    unset($Logs);
    unset($DBdata);
    unset($ConnectedInfosByGroupName);
    unset($GroupFieldsMap);
    unset($ConnectedContacts);
    unset($OriginalData);
    unset($ConnectedData);
    unset($ConnectedInfos);
    unset($ToCampaign);
    unset($CampaignDetails);
    unset($query);
    unset($Parsed);
    unset($NextSequence);
    unset($ContactDetails);
    unset($row);
    infusionsoft_restart_campaign();
}
function infusionsoft_restart_campaign()
{
    // Revision: Run this in the background
    // Check for Contact that existing in first sequence
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('macanta_helper');
    $ContactArr = [];
    $CI->db->where('key', 'StartCampaignPost');
    $query = $CI->db->get('config_data');
    $row = $query->row();
    if (isset($row) && $row->value != '{}' && $row->value != '' && $row->value != '[]') {
        $StartCampaignPost = json_decode($row->value, true);
        $SavedStartCampaignPost = $StartCampaignPost;
        foreach ($StartCampaignPost as $CampaignFirstSequence=>$SequenceDetails){
            $Parameters = $SequenceDetails["Parameters"];
            $restart_after = isset($Parameters['restart_after']) ? strtolower($Parameters['restart_after']) : "never";
            $Contacts = $SequenceDetails["Contacts"];
            $campaignId = $Parameters['campaignId'];
            $ToCampaign['CampaignId'] = $campaignId;
            $CurrentSequence = $CampaignFirstSequence;

            /*Update Contacts*/
            foreach ($Contacts as $ContactId => $Queue) {
                //$NextContacts[$ContactId][] = ['ItemId' => $ItemId, 'Fields' => $Fields];
                $inProcess = macanta_check_contact_sequence($ContactId,$CampaignFirstSequence);
                if($inProcess == true) continue; // skip this contact

                foreach ($Queue as $Key=>$ConDetails){
                    $data = [];
                    $ContactArr[] = $ContactId;
                    $ItemIdentifier = '';
                    $ToCampaign['Contacts'][$ContactId]=['ItemId' => $ConDetails['ItemId'], 'Fields' => $ConDetails['Fields']];
                    foreach ($ConDetails['Fields'] as $MacantaFieldName => $MacantaFieldDetails) {
                        if ($MacantaFieldDetails['custom-field'] != ""){
                            $MacantaFieldDetails['value'] = trim($MacantaFieldDetails['value']);
                            if(is_array( $MacantaFieldDetails['value'])){
                                $MacantaFieldDetails['value'] = isset( $MacantaFieldDetails['value'][$ContactId]) ?  $MacantaFieldDetails['value'][$ContactId]:'';
                            }
                            if(strpos($MacantaFieldDetails['value'],'|') !== false){
                                $MacantaFieldDetails['value'] = str_replace('|',',',$MacantaFieldDetails['value']);
                            }
                            $data[] = ['name' => "_" . $MacantaFieldDetails['custom-field'], 'value' => trim($MacantaFieldDetails['value'])];
                            $ItemIdentifier = $ItemIdentifier == '' ? $MacantaFieldDetails['value']:$ItemIdentifier;
                        }
                    }
                    echo "[".date('Y-m-d H:i:s')."]"." Updating Contact Record #$ContactId with $ItemIdentifier \n";
                    infusionsoft_update_contact($data, $ContactId);
                    $CampaignDetails = infusionsoft_get_campaign_by_id($campaignId);
                    if (isset($CampaignDetails->message->sequences)) {
                        foreach ($CampaignDetails->message->sequences as $sequence) {
                            if (strpos($sequence->name, $CurrentSequence) !== false) {
                                $sequenceId = $sequence->id;
                                unset($SavedStartCampaignPost[$CampaignFirstSequence]["Contacts"][$ContactId][$Key]);
                                if(sizeof($SavedStartCampaignPost[$CampaignFirstSequence]["Contacts"][$ContactId]) == 0)
                                    unset($SavedStartCampaignPost[$CampaignFirstSequence]["Contacts"][$ContactId]);
                                if(sizeof($SavedStartCampaignPost[$CampaignFirstSequence]["Contacts"]) == 0)
                                    unset($SavedStartCampaignPost[$CampaignFirstSequence]);
                                //Update Saved StartCampaignPost
                                echo "[".date('Y-m-d H:i:s')."]"." Updating Saved Connected Data Queue to count: ".sizeof($SavedStartCampaignPost)." \n";
                                if(sizeof($SavedStartCampaignPost) > 0){
                                    $DBdata = [];
                                    $DBdata['value'] = json_encode($SavedStartCampaignPost);
                                    $CI->db->where('key', 'StartCampaignPost');
                                    $CI->db->update('config_data', $DBdata);
                                }else{
                                    $CI->db->where('key', 'StartCampaignPost');
                                    $CI->db->delete('config_data');
                                }
                                $CI->db->where('id', $ConDetails['ItemId']);
                                $query = $CI->db->get('connected_data');
                                $row = $query->row();
                                if (isset($row)) {
                                    $ConnectedContacts = json_decode($row->connected_contact, true);
                                    $ConnectedContacts[$ContactId]['Sequence'][$CurrentSequence] = ["Status"=>'running',"StartedOn"=>time(),"RestartAfter"=>$restart_after];
                                    $DBdata = [];
                                    $DBdata['connected_contact'] = json_encode($ConnectedContacts);
                                    $CI->db->where('id', $ConDetails['ItemId']);
                                    $CI->db->update('connected_data', $DBdata);
                                    echo "[".date('Y-m-d H:i:s')."]"." Updating Connected Contact #$ContactId with Sequence ".json_encode($ConnectedContacts[$ContactId]['Sequence'])." \n";

                                }
                                infusionsoft_multiple_add_to_sequence($campaignId, $sequenceId, $ContactId);
                                echo "[".date('Y-m-d H:i:s')."]"." Moved Contact #$ContactId to Sequence Id: $sequenceId ( $CurrentSequence )\n";
                                break;
                            }
                        }
                    }
                    echo "======================================\n";
                    // Get only one item in the queue so break it here
                    break;
                }
            }
        }
    }
    unset($CampaignDetails);
    unset($Queue);
    unset($Contacts);
    unset($SavedStartCampaignPost);
    unset($StartCampaignPost);
    unset($row);
    unset($query);
}
function infusionsoft_start_campaign($Parameters)
{
    header("Content-Type: text/plain");
    ini_set('display_errors', 1);
    error_reporting(E_ERROR);
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('macanta_helper');
    $NextContacts = [];
    $campaignId = $Parameters['campaignId'];
    $CurrentSequence = $campaignId . ".1";
    $StartCampaignSavedParameters = $Parameters;
    $connected_data = strtolower($Parameters['connected_data']);
    $relationship = strtolower($Parameters['relationship']);
    $restart_after = isset($Parameters['restart_after']) ? strtolower($Parameters['restart_after']) : "never";
    $cd_guid = isset($Parameters['cd_guid']) ? $Parameters['cd_guid'] : "";
    //post_purchase_sequence: For single type campaign, a sequence after successful payment, used in plugins
    $post_purchase_sequence = isset($Parameters['post_purchase_sequence']) ? strtolower($Parameters['post_purchase_sequence']) : false;
    $campaign_type = isset($Parameters['campaign_type']) ? strtolower($Parameters['campaign_type']) : false;

    $StartCampaignDetails = [];
    $ConnectedInfos = macanta_get_connected_info_by_groupname($connected_data, '', '', $cd_guid);
    unset($Parameters['cd_guid']);
    unset($Parameters['campaignId']);
    unset($Parameters['api_key']);
    unset($Parameters['restart_after']);
    unset($Parameters['connected_data']);
    unset($Parameters['relationship']);
    unset($Parameters['campaign_type']);
    unset($Parameters['post_purchase_sequence']);

    $Parsed = sizeof($Parameters) == 0 ? []:infusionsoft_parse_criteria($Parameters);
    //file_put_contents(dirname(__FILE__)."/Parameters.txt",$campaignId ." Parameters: ".json_encode($Parameters)."\n", FILE_APPEND);
    $ToCampaign = [];
    $ToCampaign['CampaignId'] = $campaignId;
    $ToCampaign['Contacts'] = [];
    $AllItems = [];
    $PassedArr = [];
    $AllContacts = [];
    foreach ($ConnectedInfos[$connected_data] as $ItemId => $ItemDetails) {
        $Fields = $ItemDetails['fields'];
        $ConnectedContact = $ItemDetails['connected_contact'];
        $Meta = $ItemDetails['meta'];
        if(isset($Meta['can_start_in_campaign'])){
            if(strtolower($Meta['can_start_in_campaign']) == 'no' || strtolower($Meta['can_start_in_campaign']) == 'false'){
                continue; // skip this connected data from campaign
            }
        }
        //Validate Fields values
        $Passed = sizeof($Parsed) == 0 ? true : infusionsoft_evaluate_parsed_criteria($Parsed,$Fields,'Start Campaign', $ItemId);
        //Validate Contact Relationships
        if ($Passed === true) {

            foreach ($ConnectedContact as $ContactId => $ContactDetails) {
                if(isset($ContactDetails['Sequence'][$CurrentSequence]['Status'])){
                    if($ContactDetails['Sequence'][$CurrentSequence]['Status'] == 'end'){
                        if(isset($ContactDetails['Sequence'][$CurrentSequence]['RestartAfter'])){
                            if(isValidTimeString($ContactDetails['Sequence'][$CurrentSequence]['RestartAfter'])){
                                $StartedOn = isset($ContactDetails['Sequence'][$CurrentSequence]['StartedOn']) ? date('Y-m-d H:i:s', $ContactDetails['Sequence'][$CurrentSequence]['StartedOn']):date('Y-m-d H:i:s');
                                $RestartAfter = $ContactDetails['Sequence'][$CurrentSequence]['RestartAfter'];
                                $CanReStartOn = strtotime($StartedOn ." ".$RestartAfter);
                                if($CanReStartOn >= time()){
                                    continue;
                                }
                            }elseif($ContactDetails['Sequence'][$CurrentSequence]['RestartAfter'] == 'never'){
                                continue;
                            }else{
                                continue;
                            }

                        }else{
                            continue;
                        }
                    }else{
                        continue;
                    }
                }
                if($relationship === 'any'){

                    if(!in_array($ItemId,$AllItems)) $AllItems[] = $ItemId;
                    if(isset($ToCampaign['Contacts'][$ContactId])){
                        $PassedArr[$ItemId] = 'queued';
                        $NextContacts[$ContactId][] = ['ItemId' => $ItemId, 'Fields' => $Fields];
                    }else{
                        $PassedArr[$ItemId] = 'running';
                        $ToCampaign['Contacts'][$ContactId] = ['ItemId' => $ItemId, 'Fields' => $Fields];
                    }
                }
                elseif (strpos($relationship, " or ") !== false) {
                    $relationshipArr = explode(" or " , $relationship);
                    foreach ($relationshipArr as $relationshipItem) {
                        if (in_array(trim($relationshipItem), $ContactDetails['relationships'])) {
                            $PassedArr[] = $ItemId;
                            if(!in_array($ItemId,$AllItems)) $AllItems[] = $ItemId;
                            if(isset($ToCampaign['Contacts'][$ContactId])){
                                $PassedArr[$ItemId] = 'queued';
                                $NextContacts[$ContactId][] = ['ItemId' => $ItemId, 'Fields' => $Fields];
                            }else{
                                $PassedArr[$ItemId] = 'running';
                                $ToCampaign['Contacts'][$ContactId] = ['ItemId' => $ItemId, 'Fields' => $Fields];
                            }
                            break;
                        }
                    }
                }
                elseif (strpos($relationship, " and ") !== false) {
                    $relationshipArr = explode(" and " , $relationship);
                    $toAdd = true;
                    foreach ($relationshipArr as $relationshipItem) {
                        if (!in_array(trim($relationshipItem), $ContactDetails['relationships'])) {
                            $toAdd = false;
                            break;
                        }
                    }
                    if($toAdd === true){
                        if(!in_array($ItemId,$AllItems)) $AllItems[] = $ItemId;
                        if(isset($ToCampaign['Contacts'][$ContactId])){
                            $PassedArr[$ItemId] = 'queued';
                            $NextContacts[$ContactId][] = ['ItemId' => $ItemId, 'Fields' => $Fields];
                        }else{
                            $PassedArr[$ItemId] = 'running';
                            $ToCampaign['Contacts'][$ContactId] = ['ItemId' => $ItemId, 'Fields' => $Fields];
                        }
                    }
                }else{
                    if (in_array($relationship, $ContactDetails['relationships'])) {
                        if(!in_array($ItemId,$AllItems)) $AllItems[] = $ItemId;
                        if(isset($ToCampaign['Contacts'][$ContactId])){
                            $PassedArr[$ItemId] = 'queued';
                            $NextContacts[$ContactId][] = ['ItemId' => $ItemId, 'Fields' => $Fields];
                        }else{
                            $PassedArr[$ItemId] = 'running';
                            $ToCampaign['Contacts'][$ContactId] = ['ItemId' => $ItemId, 'Fields' => $Fields];
                        }
                    }
                }
            }
        }
    }


    /*Update Contacts*/
    $UniqueId = macanta_generate_key('ref');
    foreach ($ToCampaign['Contacts'] as $ContactId => $ConDetails) {
        $AllContacts[]=$ContactId;
        $data = [];
        $ItemIdentifier = '';
        foreach ($ConDetails['Fields'] as $MacantaFieldName => $MacantaFieldDetails) {
            if ($MacantaFieldDetails['custom-field'] != ""){

                // If json, field is dynamic, get value for contact specific
                if(is_array($MacantaFieldDetails['value'])){

                    $MacantaFieldDetails['value'] = isset($MacantaFieldDetails['value']["id_" . (string)$ContactId]) ? $MacantaFieldDetails['value']["id_" . (string)$ContactId]:'';
                }else{
                    $MacantaFieldDetails['value'] = trim($MacantaFieldDetails['value']);
                    if(strpos($MacantaFieldDetails['value'],'|') !== false){
                        $MacantaFieldDetails['value'] = str_replace('|',',',$MacantaFieldDetails['value']);
                    }
                }
                $data[] = ['name' => "_" . $MacantaFieldDetails['custom-field'], 'value' => trim($MacantaFieldDetails['value'])];
                $ItemIdentifier = $ItemIdentifier == '' ? $MacantaFieldDetails['value']:$ItemIdentifier;
            }
        }
        $StartCampaignDetails[$ContactId] = ['Sequence' => $CurrentSequence,'ConnectedData' => $ItemIdentifier];

        if($campaign_type === 'single'){
            $CampaignRefCustomFields = $CI->config->item('CampaignRefCustomFields');
            if($CampaignRefCustomFields){
                $CampaignRefCustomFields = json_decode($CampaignRefCustomFields,true);
                $CampaignRefCustomFields = '_'.$CampaignRefCustomFields['Campaign Reference Id'];
                $data[] = ['name' => $CampaignRefCustomFields, 'value' => $UniqueId];
            }
        }
        $Update_result = infusionsoft_update_contact($data, $ContactId);
        //file_put_contents(dirname(__FILE__)."/update_contact_$ContactId.txt",json_encode($data)."\nResult:".$Update_result."\n\n", FILE_APPEND);
        $CampaignDetails = infusionsoft_get_campaign_by_id($campaignId);
        if (isset($CampaignDetails->message->sequences)) {
            foreach ($CampaignDetails->message->sequences as $sequence) {
                if (strpos($sequence->name, $CurrentSequence) !== false) {
                    $sequenceId = $sequence->id;
                    infusionsoft_multiple_add_to_sequence($campaignId, $sequenceId, $ContactId);
                    $DBdata = [];
                    $CI->db->where('id', $ConDetails['ItemId']);
                    $query = $CI->db->get('connected_data');
                    $row = $query->row();
                    if (isset($row)) {
                        $ConnectedContacts = json_decode($row->connected_contact, true);
                        $ConnectedContacts[$ContactId]['Sequence'][$CurrentSequence] = ["Status"=>'running',"StartedOn"=>time(),"RestartAfter"=>$restart_after];
                        $DBdata = [];
                        $DBdata['connected_contact'] = json_encode($ConnectedContacts);
                        $CI->db->where('id', $ConDetails['ItemId']);
                        $CI->db->update('connected_data', $DBdata);
                    }
                    break;
                }
            }
        }
    }

    if($campaign_type === 'single'){
        $NextContacts = []; // remove all queued if single
        $campaign_reference = [
            'items'=>$AllItems,
            'paid_items'=>[],
            'campaign' =>['id'=>$campaignId,'post_purchase_sequence'=>$post_purchase_sequence]
        ];
        $DBdata = [
            'user_id' => 0,
            'meta_key' => $UniqueId,
            'meta_value' => json_encode($campaign_reference)
        ];
        $CI->db->insert('users_meta', $DBdata);
    }

    if(sizeof($NextContacts) > 0){
        $DBdata = [];
        $CI->db->where('key', 'StartCampaignPost');
        $query = $CI->db->get('config_data');
        $row = $query->row();
        if (isset($row)) {
            $SavedStartCampaignPost = json_decode($row->value, true);
            $SavedStartCampaignPost[$CurrentSequence] = ["Parameters"=>$StartCampaignSavedParameters,"Contacts"=>$NextContacts,"Status"=>'running'];
            $DBdata['value'] = json_encode($SavedStartCampaignPost);
            $CI->db->where('key', 'StartCampaignPost');
            $CI->db->update('config_data', $DBdata);
        }else{
            $SavedStartCampaignPost[$CurrentSequence] = ["Parameters"=>$StartCampaignSavedParameters,"Contacts"=>$NextContacts,"Status"=>'running'];
            $DBdata = array(
                'key' => 'StartCampaignPost',
                'value' => json_encode($SavedStartCampaignPost)
            );
            $CI->db->insert('config_data', $DBdata);
        }
    }
    unset($ToCampaign);
    unset($ConnectedInfos);
    unset($Parsed);
    unset($ConnectedContact);
    return ["Start Campaign Details" => $StartCampaignDetails, "Connected Data Passed: "=>$AllItems, "Connected Data Queued"=>sizeof($NextContacts), "Passed"=>$PassedArr];

}
function infusionsoft_evaluate_parsed_criteria($Parsed,$Fields, $FromNote = 'Start Campaign', $ItemId){
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('macanta_helper');
    $Passed = false;
    $Logging = false;
    $Log = '';
    $Log .= "Parsed: ".json_encode($Parsed)."\n";
    $Log .= "Fields: ".json_encode($Fields)."\n";
    $Log .= "--Evaluating $FromNote of $ItemId-- \n";
    if($Logging) file_put_contents(dirname(__FILE__)."/Expression_".$CI->config->item('IS_App_Name').".txt",$Log, FILE_APPEND);
    foreach ($Parsed as $FieldName => $Expression) {
        $FieldName = strtolower(str_replace('_',' ',$FieldName));
        $field_value = strtolower(trim($Fields[$FieldName]['value']));
        if($Fields[$FieldName]['field-type'] == 'Checkbox'){
            if($field_value == ''){
                $Log .= '$field_value:' ." $field_value\n".$Expression."\n";
                if($Logging) file_put_contents(dirname(__FILE__)."/Expression_".$CI->config->item('IS_App_Name').".txt",$Log, FILE_APPEND);
                if (eval('return ' . $Expression . ";")) {
                    $Passed = true;
                }
            }else{
                $field_valueArr = explode('|', $field_value);
                foreach ($field_valueArr as $field_value){
                    $field_value = trim($field_value);
                    $field_value = isValidTimeString(trim($field_value)) ? strtotime($field_value):$field_value;
                    $Log .= '$field_value:' ." $field_value\n".$Expression."\n";
                    if($Logging) file_put_contents(dirname(__FILE__)."/Expression_".$CI->config->item('IS_App_Name').".txt",$Log, FILE_APPEND);
                    if (eval('return ' . $Expression . ";")) {
                        $Passed = true;
                        break;
                    }
                }
            }

        }
        else{
            if($Fields[$FieldName]['field-type'] == 'Date'){
                if($field_value != ""){
                    $field_value = strtotime($field_value);
                    $Log = '$field_value:' ." $field_value\n".$Expression."\n";
                    if($Logging) file_put_contents(dirname(__FILE__)."/Expression_".$CI->config->item('IS_App_Name').".txt",$Log, FILE_APPEND);
                    if (eval('return ' . $Expression . ";")) {
                        $Passed = true;
                    } else {
                        $Passed = false;
                        break;
                    }
                }else{
                    $Log = '$field_value: ' .json_encode($field_value)." or -- Blank --\n".$Expression."\n";
                    if($Logging) file_put_contents(dirname(__FILE__)."/Expression_".$CI->config->item('IS_App_Name').".txt",$Log, FILE_APPEND);
                    $Passed = false;
                    break;
                }
            }else{
                if (eval('return ' . $Expression . ";")) {
                    $Passed = true;
                } else {
                    $Passed = false;
                    break;
                }
                $Log = '$field_value:' ." $field_value\n".$Expression."\n";
                if($Logging) file_put_contents(dirname(__FILE__)."/Expression_".$CI->config->item('IS_App_Name').".txt",$Log, FILE_APPEND);
            }


        }

    }
    $Result = $Passed ? "True":"False";
    $Log = 'Result: ' .$Result."\n\n";
    if($Logging) file_put_contents(dirname(__FILE__)."/Expression_".$CI->config->item('IS_App_Name').".txt",$Log, FILE_APPEND);
    return $Passed;
}
function infusionsoft_parse_criteria($FieldAndCriteria)
{
    $Parsed = [];

    //Explode to fields field condition pairs by `and`
    foreach ($FieldAndCriteria as $FieldName => $Value) {
        $Parsed[$FieldName] = create_logic($Value);
    }
    return $Parsed;

}
function create_logic($Criteria, $FieldType = '')
{
    $Criteria = strtolower($Criteria);
    $Operators = [
        ">" => ["greater than", "more than", "larger than", "higher than", "exceeds", "over", "older than", "bigger than"],
        "<" => ["less than", "fewer than", "lower than", "under", "younger than"],
        "within" => ["within"],
        "between" => ["between"]
    ];
    $expression = '';
    foreach ($Operators as $OpSymbol => $OpSynonyms) {
        foreach ($OpSynonyms as $OpSynonym) {
            if (strpos($Criteria, $OpSynonym) !== false) {
                switch ($OpSymbol) {
                    case ">" :
                        $expression = trim(str_replace($OpSynonym, '', $Criteria));
                        if (isValidTimeString($expression)) {
                            $expression = '$field_value > strtotime(date("Y-m-d",strtotime("' . $expression . '")))';
                        }elseif (empty($expression)){
                            $expression = false;
                        } else {
                            $expression = '$field_value > ' . $expression;
                        }
                        return $expression;

                    case "<":
                        $expression = trim(str_replace($OpSynonym, '', $Criteria));
                        if (isValidTimeString($expression)) {
                            $expression = '$field_value < strtotime(date("Y-m-d",strtotime("' . $expression . '")))';
                        }elseif (empty($expression)){
                            $expression = false;
                        } else {
                            $expression = '$field_value < ' . $expression;
                        }
                        return $expression;

                    case "within":
                        $expression = trim(str_replace($OpSynonym, '', $Criteria));
                        if (isValidTimeString($expression)) {
                            $expression = '$field_value >= strtotime(date("Y-m-d")) && $field_value <= strtotime(date("Y-m-d",strtotime("' . $expression . '")))';
                        }elseif (empty($expression)){
                            $expression = false;
                        } else {
                            $expression = '"Invalid Time String"';
                        }
                        return $expression;
                    case "between":
                        $CriteriaStr = trim(str_replace($OpSynonym, '', $Criteria));
                        $CriteriaArr = explode('and', $CriteriaStr);
                        if (isValidTimeString($CriteriaArr[0])) {
                            $expression = '$field_value >= strtotime(date("Y-m-d",strtotime("' . $CriteriaArr[0] . '"))) && $field_value <= strtotime(date("Y-m-d",strtotime("' . $CriteriaArr[1] . '")))';
                        } else {
                            $expression = '$field_value >=  ' . $CriteriaArr[0] . ' && $field_value <= ' . $CriteriaArr[1];
                        }
                        return $expression;
                }

            }

        }

    }
    if (preg_match_all('/\(([A-Za-z0-9\-_ ]+?)\)/', $Criteria, $groups)) {
        $CriteriaRemaining = $Criteria;
        $expression = [];
        foreach ($groups[1] as $group) {
            $CriteriaRemaining = trim(str_replace('(' . $group . ')', '', $CriteriaRemaining));
            if (strpos($group, ' or ') !== false) {
                $CriteriaArr = explode(' or ', $group);
                $expressionArr = [];
                foreach ($CriteriaArr as $CriteriaStr) {
                    $CriteriaStr = strtolower(trim($CriteriaStr));
                    //$CriteriaStr = is_numeric($CriteriaStr) ? $CriteriaStr : '"' . $CriteriaStr . '"';
                    $CriteriaStr = '"' . $CriteriaStr . '"';
                    $expressionArr[] = 'strtolower($field_value) == ' . $CriteriaStr;
                }
                $expression[] = '(' . implode(' || ', $expressionArr) . ')';

            } elseif (strpos($group, " and ") !== false) {
                $CriteriaArr = explode(' and ', $group);
                $expressionArr = [];
                foreach ($CriteriaArr as $CriteriaStr) {
                    $CriteriaStr = strtolower(trim($CriteriaStr));
                    //$CriteriaStr = is_numeric($CriteriaStr) ? $CriteriaStr : '"' . $CriteriaStr . '"';
                    $CriteriaStr = '"' . $CriteriaStr . '"';
                    $expressionArr[] = 'strtolower($field_value) == ' . $CriteriaStr;
                }
                $expression[] = '(' . implode(' && ', $expressionArr) . ')';
            } else {
                $group = strtolower(trim($group));
                //$group = is_numeric($group) ? $group : '"' . $group . '"';
                $group = '"' . $group . '"';
                $expression[] = '(' . 'strtolower($field_value) ==  ' . trim($group) . ')';
            }
        }

        if (strpos($CriteriaRemaining, " or ") !== false) {
            $expression = implode(' || ', $expression);

        } elseif (strpos($CriteriaRemaining, " and ") !== false) {
            $expression = implode(' && ', $expression);
        }

    }
    elseif (strpos($Criteria, " or ") !== false) {
        $CriteriaArr = explode(' or ', $Criteria);
        $CriteriaArr = array_map('trim',$CriteriaArr);
        $expressionArr = [];
        foreach ($CriteriaArr as $CriteriaStr) {


            if (isValidTimeString($CriteriaStr)) {
                $CriteriaStr = '$field_value == strtotime(date("Y-m-d",strtotime("' . $CriteriaStr . '")))';

                $expressionArr[] = $CriteriaStr;
            }else{
                $CriteriaStr = $CriteriaStr == "is null" ? '""':'"' . strtolower($CriteriaStr) . '"';
                $expressionArr[] = 'strtolower($field_value) == ' . $CriteriaStr;
            }


        }
        $expression = implode(' || ', $expressionArr);

    }
    elseif (strpos($Criteria, " and ") !== false) {
        $CriteriaArr = explode(' and ', $Criteria);
        $CriteriaArr = array_map('trim',$CriteriaArr);
        $expressionArr = [];
        foreach ($CriteriaArr as $CriteriaStr) {
            //$CriteriaStr = is_numeric($CriteriaStr) ? $CriteriaStr : '"' . $CriteriaStr . '"';
            //$CriteriaStr = '"' . $CriteriaStr . '"';
            if (isValidTimeString($CriteriaStr)) {
                $CriteriaStr = '$field_value == strtotime(date("Y-m-d",strtotime("' . $CriteriaStr . '")))';
                $expressionArr[] = $CriteriaStr;
            }else{
                $CriteriaStr = $CriteriaStr == "is null" ? '""':'"' . strtolower($CriteriaStr) . '"';
                $expressionArr[] = 'strtolower($field_value) == ' . $CriteriaStr;
            }
        }

        $expression = implode(' && ', $expressionArr);
    }
    elseif(trim($Criteria) == 'is null') {
        $expression = '$field_value ==  ""';
    }elseif(trim($Criteria) == 'not null') {
        $expression = '$field_value !=  ""';
    }elseif(preg_match('/%.*?%/',$Criteria) === 1){
        $Criteria = str_replace('%','', $Criteria);
        $expression = 'strpos($field_value, "'.$Criteria.'") !== false';
    }  else {
        $Criteria = trim($Criteria);
        if (isValidTimeString($Criteria)) {
            $expression = '$field_value == strtotime(date("Y-m-d",strtotime("' . $Criteria . '")))';
        }else{
            $Criteria = '"' . strtolower($Criteria) . '"';
            $expression = 'strtolower($field_value) == ' . $Criteria ;
        }

    }
    return $expression;
}

function isUSDate($date){
    //mm/dd/yyyy
    $patternUS = '/(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d/';
    return 1 === preg_match($patternUS,$date);
}
function isGBDate($date){
    //dd/mm/yyyy
    $patternGB = '/^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/m';
    return 1 === preg_match( $patternGB,$date);

}
function isValidTimeString(&$string)
{
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('macanta_helper');
    //$string = isGBDate($string) ? str_replace("/","-",$string):$string;
    //It's important to note the different interpretation of - and / in the date.
    // If you use a - php will determine it to be DD-MM,
    // if you use a / php will determine it to be MM-DD.
    //8\/11\/2018 // GB formats
    $ToConvertApps = ['zg400'];
    if(in_array($CI->config->item('IS_App_Name'), $ToConvertApps)){
        $string = str_replace("/","-",$string); // some app uses US even the country is GB
    }
    $theDate = date("Y-m-d", strtotime($string));
    if ("1970-01-01" !== $theDate) {
        $string = $theDate;
        return true;
    }

    return false;
}
function infusionsoft_multiple_add_to_sequence($campaignId, $sequenceId, $ContactStr)
{
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('macanta_helper');
    $action = "restAddMultipleToCampaignSequence";
    $action_details = '{"CampaignId":' . $campaignId . ',"SequenceId":' . $sequenceId . ',"Contacts":{"ids":[' . $ContactStr . ']}}';
    $AddResult = applyFn('rucksack_request',$action, $action_details);
    return $AddResult;
}
function infusionsoft_get_campaign_by_id($Id)
{
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('macanta_helper');
    $action = "restRetrieveCampaign";
    $action_details = '{"CampaignId":' . $Id . '}';
    $Campaign = applyFn('rucksack_request',$action, $action_details);
    return $Campaign;
}
function infusionsoft_get_file_by_id($Id)
{
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('macanta_helper');
    $action = "restRetrieveFile";
    $action_details = '{"FileId":' . $Id . '}';
    $Campaign = applyFn('rucksack_request',$action, $action_details);
    return $Campaign;
}
function infusionsoft_get_file_by_id_lite($Id)
{
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('macanta_helper');
    $action = "restRetrieveFileLite";
    $action_details = '{"FileId":' . $Id . '}';
    $Campaign = applyFn('rucksack_request',$action, $action_details);
    return $Campaign;
}
function infusionsoft_delete_file_by_id($Id)
{
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('macanta_helper');
    $action = "restDeleteFile";
    $action_details = '{"FileId":' . $Id . '}';
    $Campaign = applyFn('rucksack_request',$action, $action_details);
    return $Campaign;
}
if (!function_exists('infusionsoft_get_hook_subscriptions')) {
    function infusionsoft_get_hook_subscriptions()
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $action = "restListHookSubscriptions";
        $action_details = '{}';
        $HookSubscriptions = applyFn('rucksack_request',$action, $action_details);
        return $HookSubscriptions;
    }
}
if (!function_exists('infusionsoft_delete_hook_subscription')) {
    function infusionsoft_delete_hook_subscription($key)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $action = "restDeleteHookSubscription";
        $action_details = '{"key":"'.$key.'"}';
        $HookSubscriptions = applyFn('rucksack_request',$action, $action_details);
        return $HookSubscriptions;
    }
}
if (!function_exists('infusionsoft_create_hook_subscriptions')) {
    function infusionsoft_create_hook_subscriptions($eventKey,$hookUrl)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $action = "restCreateHookSubscription";
        $action_details = '{"eventKey":"' . $eventKey . '","hookUrl":"' . $hookUrl . '"}';
        $CreateResult = applyFn('rucksack_request',$action, $action_details);
        return $CreateResult;
    }
}