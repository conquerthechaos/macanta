<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');
class Reconnect extends MY_Controller
{
    public $OpenCalls = array(
        'login',
        'frontpage',
        'dashboard'
    );
    public $LogFile;
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->LogFile = FCPATH."token_log_".$this->config->item('IS_App_Name').".txt";
    }
    public function index(){
        // LETS START WITH BLANK SCREEN FOR DYNAMIC CONTENT!

        // Get all controller assets
        $AllTabs = json_decode($this->config->item('macanta_tabs'),true);
        $this->MacantaIgniter =& get_instance();
        foreach ($AllTabs as $Tab => $Params){
            foreach($Params['controllers'] as $Controller => $Methods ){
                $loadedController = _loadMacantaController($this->MacantaIgniter, $Controller);
                if(isset($this->MacantaIgniter->$loadedController)){
                    //if(method_exists($this->MacantaIgniter->$loadedController, 'theCSS'))
                    //print_r($this->MacantaIgniter->$loadedController) ;

                    //if(method_exists($this->$loadedController, 'theJS'))
                    //$this->local_javascripts = $this->$loadedController->theJS;
                }
            }
        }
        $language = $this->config->item('macanta_lang');
        if ($language === NULL)
        {
            $language = 'english';
        }
        $this->config->load('version', FALSE, TRUE);
        // Load the language file
        $this->lang->load('macanta', $language);

        if ($this->agent->is_browser())
        {
            $agent = $this->agent->browser().' '.$this->agent->version();
        }
        elseif ($this->agent->is_robot())
        {
            $agent = $this->agent->robot();
        }
        elseif ($this->agent->is_mobile())
        {
            $agent = $this->agent->mobile();
        }
        else
        {
            $agent = 'Unidentified User Agent';
        }
        $data['browser']['agent'] = $agent;
        $data['browser']['platform'] = $this->agent->platform();

        $data['FrontPage'] = '';
        $this->content[] = $this->load->view('frontpage-reconnect', $data, true); // return the view
        $this->render($this->content); // render the data
    }

    public function token($hash){
        session_start();
        $URL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        if(!isset($_GET['code'])){
            $passwordFile = dirname(APPPATH).DIRECTORY_SEPARATOR."phpliteadmin.txt";
            $passwordArr = json_decode(file_get_contents($passwordFile),true);
            $URL = str_replace("/".$hash,"",$URL);
            if($hash != $passwordArr['password']) exit("Forbidden!");
            header("location: http://rucksack.macanta.org/generate/generate_macanta_token.php?url=$URL");
            exit();
        }else{
            $Message = "Regenerating Infusionsoft Token";
            $ToLog = "\n[".gmdate('Y-m-d H:i:s')."] ".$Message."\n";
            file_put_contents($this->LogFile, $ToLog, FILE_APPEND);
            $Credential = json_decode(base64_decode($hash), true);
            $URL= trim($URL."/");
            $URLArr = explode('?',$URL);
            $infusionsoft = new \Infusionsoft\Infusionsoft(array(
                'clientId'     => $Credential['key'],
                'clientSecret' => $Credential['secret'],
                'redirectUri'  => $URLArr[0]
            ));
            $infusionsoft->setHttpClient(new \Infusionsoft\Http\CurlClient());
            $infusionsoft->requestAccessToken($_GET['code']);
            $token = serialize($infusionsoft->getToken());
            $DBdata = [];
            $DBdata['value'] = $token;
            $this->db->where('key','InfusionsoftRSToken');
            $this->db->update('config_data', $DBdata);
            $Message = "New Infusionsoft Token Saved: $token";
            $ToLog = "[".gmdate('Y-m-d H:i:s')."] ".$Message."\n";
            file_put_contents($this->LogFile, $ToLog, FILE_APPEND);
            $_SESSION['token_generated'] = 1;
            header("location: http://$_SERVER[HTTP_HOST]");
            exit();
        }
    }

}
