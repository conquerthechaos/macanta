<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 09/03/16
 * Time: 2:28 PM
 */
/*Zuora API*/
ini_set("soap.wsdl_cache_enabled", 0);
function check_zuora_credentials(){
    $CI =& get_instance();
    if($CI->config->item('ZuoraUsername') && $CI->config->item('ZuoraPassword')) return true;
    return false;
}
function zuora_instance(){

    require_once (APPPATH."libraries/zuora/lib/API.php");
    libxml_disable_entity_loader(false);// Fix for SOAP-ERROR: Parsing WSDL: Couldn't load
    $CI =& get_instance();
    $ZuoraLogin = [
        'PRODUCTION' => array(
            'username' => $CI->config->item('ZuoraUsername'),
            'password' => $CI->config->item('ZuoraPassword'),
            'endpoint' => 'https://www.zuora.com/apps/services/a/72.0',
        ),
        'SANDBOX' => array(
            'username' => $CI->config->item('ZuoraUsername_Sandbox'),
            'password' => $CI->config->item('ZuoraPassword_Sandbox'),
            'endpoint' => 'https://apisandbox.zuora.com/apps/services/a/72.0'
        )
    ];
    $Mode = $CI->config->item('ZuoraMode');
    $Mode = $Mode == "production" ? "PRODUCTION":"SANDBOX";
    $LOGIN = $ZuoraLogin[$Mode];// USE SANDBOX
    $ZuoraConfig = new stdClass();

    $wsdlFile =  $CI->config->item('base_url').'zuora.a.90.0.wsdl';
    $ZuoraConfig->wsdl = $wsdlFile;
    try{
        $ZuoraInstance = new stdClass();
        $ZuoraInstance = Zuora_API::getInstance($ZuoraConfig);
        $ZuoraInstance->setQueryOptions(1000);
        $ZuoraInstance->setLocation($LOGIN['endpoint']);
        $ZuoraInstance->login($LOGIN['username'], $LOGIN['password']);

    } catch (Exception $e) {
        $ZuoraInstance = new stdClass();
        $ZuoraInstance->ERROR  = 'Zuora exception: '.  $e->getMessage();
    }

    return $ZuoraInstance;
}
if (!function_exists('zuora_query'))
{
    function zuora_query($Query,$Table,$Where, $Action = "SELECT"){
        $records = [];
        $ZuoraTableFields = [
            "Account"=>[
                "AccountNumber",
                "AllowInvoiceEdit",
                "Id",
                "AutoPay",
                "InvoiceDeliveryPrefsPrint",
                "UpdatedDate",
                "CreditBalance",
                "BillCycleDay",
                "BcdSettingOption",
                "PaymentTerm",
                "Status",
                "MRR",// Not In the WSDL but working
                "TotalInvoiceBalance",
                "UpdatedById",
                "Batch",
                "CreatedById",
                "InvoiceDeliveryPrefsEmail",
                "Name",
                "SoldToId",
                "Notes",
                "Balance",
                "InvoiceTemplateId",
                "CrmId",
                "BillToId",
                "CreatedDate",
                "DefaultPaymentMethodId",
                "Currency",
                "LastInvoiceDate",
                "PurchaseOrderNumber"
            ],
            "Contact" => [
                "Address1",
                "Address2",
                "City",
                "Country",
                "County",
                "Description",
                "Fax",
                "FirstName",
                "HomePhone",
                "LastName",
                "MobilePhone",
                "NickName",
                "OtherPhone",
                "OtherPhoneType",
                "PersonalEmail",
                "PostalCode",
                "State",
                "TaxRegion",
                "WorkEmail",
                "WorkPhone"
            ],
            "Invoice" =>[
                "AccountId",
                "AdjustmentAmount",
                "Amount",
                "AmountWithoutTax",
                "Balance",
                //"Body", // working but too large
                "Comments",
                "CreatedById",
                "CreatedDate",
                "CreditBalanceAdjustmentAmount",
                "DueDate",
                "IncludesOneTime",
                "IncludesRecurring",
                "InvoiceDate",
                "IncludesUsage",
                "InvoiceNumber",
                "PaymentAmount",
                "LastEmailSentDate",
                "PostedBy",
                "PostedDate",
                "RefundAmount",
                "Status",
                "TargetDate",
                "TaxAmount",
                "TaxExemptAmount",
                "TransferredToAccounting",
                "UpdatedById",
                "UpdatedDate"
            ],
            "InvoiceItem"=>[
                "AccountingCode",
                "ChargeAmount",
                "ChargeDate",
                "ChargeDescription",
                "ChargeId",
                "ChargeName",
                "ChargeNumber",
                "ChargeType",
                "CreatedDate",
                "InvoiceId",
                "ProductId",
                "ProductName",
                //"ProductRatePlanChargeId", //gives error
                "Quantity",
                "RatePlanChargeId",
                "ServiceEndDate",
                "ServiceStartDate",
                "SKU",
                "SubscriptionId",
                "SubscriptionNumber"
            ],
            "AccountingCode"=>[
                "Category",
                "CreatedById",
                "CreatedDate",
                "GLAccountName",
                "GLAccountNumber",
                "Name",
                "Notes",
                "Status",
                "Type",
                "UpdatedById",
                "UpdatedDate"
            ],
            "InvoicePayment" => [
                "Amount",
                "CreatedDate",
                "InvoiceId",
                "PaymentId",
                "RefundAmount"
            ],
            "Payment" =>[
                "AccountId",
                "Amount",
                "AppliedCreditBalanceAmount",
                //"AppliedInvoiceAmount",//ERROR
                "Comment",
                "CreatedDate",
                "EffectiveDate",
                //"InvoiceId", //ERROR
                //"InvoiceNumber",//ERROR
                //"InvoicePaymentData",//ERROR
                "PaymentNumber",
                "ReferenceId",
                "RefundAmount",
                "Source",
                "SourceName",
                "Status",
                "Type"
            ],
            "RatePlanCharge" => [
                "AccountingCode",
                "ChargedThroughDate",
                "ChargeModel",
                "ChargeNumber",
                "ChargeType",
                "BillCycleDay",
                "BillCycleType",
                "BillingPeriod",
                "BillingPeriodAlignment",
                "BillingTiming",
                "IsLastSegment",
                "Description",
                //"DiscountAmount", //ERROR
                //"DiscountPercentage", //ERROR
                "EffectiveEndDate",
                "EffectiveStartDate",
                "EndDateCondition",
                "MRR",
                "Name",
                "NumberOfPeriods",
                "Price",
                "Quantity",
                "RatePlanId",
                "SpecificEndDate",
                "TriggerDate"
            ],
            "RatePlan" =>[
                "CreatedDate",
                "Name",
                "ProductRatePlanId",
                "SubscriptionId"
            ],
            "Subscription" =>[
                "CancelledDate",
                "ContractAcceptanceDate",
                "ContractEffectiveDate",
                //"CurrentTerm",
                //"CurrentTermPeriodType",
                "InitialTerm",
                "Name",
                "Notes",
                "RenewalSetting",
                "RenewalTerm",
                //"RenewalTermPeriodType",
                "ServiceActivationDate",
                "Status",
                "SubscriptionEndDate",
                "SubscriptionStartDate",
                "TermEndDate",
                "TermStartDate",
                "TermType"
            ]
        ];
        $Fields = implode(", ", $ZuoraTableFields[$Table]);
        $ZuoraInstance = zuora_instance();
        if(!isset($ZuoraInstance->ERROR)){
            switch ($Action){
                case "SELECT":
                    $query = sprintf($Query, $Fields, $Table, $Where);
                    $records = queryAll($ZuoraInstance, $query);
                    break;
            }
        }else{
            $records['Error'] = $ZuoraInstance->ERROR;
        }
        return $records;
    }
}

if (!function_exists('zuora_queryData'))
{
    function zuora_queryData($By = '', $Value = '', $Table){
        $Where = "";
        if($By != '')
            $Where = "WHERE $By = '$Value'";
        $Query = "SELECT %s FROM %s %s";
        return zuora_query($Query,$Table,$Where);
    }
}

/*Get Zuora membership subscriptions */
if (!function_exists('zuora_get_membership_subscriptions'))
{
    function zuora_get_membership_subscriptions($zuoraAccountId){
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $action = "zuora_get_membership_subscriptions";
        $action_details = '{"AccountID":"'.$zuoraAccountId.'"}';
        $subscription = rucksack_request($action, $action_details );
        if(isset($subscription->message) || $subscription == '' || sizeof($subscription) == 0){
            $subscription = 'Empty';
        }
        return $subscription;
    }
}

function queryAll($instance, $query){

    $moreCount = 0;
    $recordsArray = array();
    $totalStart = time();

    $start = time();
    $result = $instance->query($query);
    $end = time();
    $elapsed = $end - $start;

    $done = $result->result->done;
    $size = $result->result->size;
    $records = $result->result->records;

    if ($size == 0){
    } else if ($size == 1){
        array_push($recordsArray, $records);
    } else {

        $locator = $result->result->queryLocator;
        $newRecords = $result->result->records;
        $recordsArray = array_merge($recordsArray, $newRecords);

        while (!$done && $locator && $moreCount == 0){

            $start = time();
            $result = $instance->queryMore($locator);
            $end = time();
            $elapsed = $end - $start;

            $done = $result->result->done;
            $size = $result->result->size;
            $locator = $result->result->queryLocator;
            $newRecords = $result->result->records;
            $count = count($newRecords);
            if ($count == 1){
                array_push($recordsArray, $newRecords);
            } else {
                $recordsArray = array_merge($recordsArray, $newRecords);
            }

        }
    }

    $totalEnd = time();
    $totalElapsed = $totalEnd - $totalStart;

    return $recordsArray;

}

function getPostValue($name,$default=null){
    $v = $_POST[$name];
    if(!isset($v)){
        $v = $default;
    }
    return $v;
}
function isEmpty($var){
    return !isset($var) or trim($var)=='';
}

function makeAccount($name,$currency,$status){
    $zAccount = new Zuora_Account();
    $zAccount->AllowInvoiceEdit = 1;
    $zAccount->AutoPay = 0;
    $zAccount->Batch = 'Batch1';
    $zAccount->BillCycleDay = 1;
    $zAccount->Currency = $currency;
    $zAccount->Name = $name;
    $zAccount->PaymentTerm = 'Due Upon Receipt';
    $zAccount->Status = $status;

    //$zAccount->CrmId = 'SFDC-1223471249003';
    //$zAccount->PurchaseOrderNumber = 'PO-1223471249003';
    return $zAccount;
}
# method to create an active account. requires that you have:
#
#   1.) a gateway setup,
#   2.) gateway configured to not verify new credit cards
#
# if you want to verify a new credit card, make sure the card info
# you specify is correct.
function createActiveAccount($instance, $name){

    # Create Draft Account
    $zAccount = makeAccount($name,'USD','Draft');

    $result = $instance->create(array($zAccount));
    $accountId = $result->result->Id;

    # Create Contact on Account
    $zBillToContact = makeContact('Robert','Smith','4901 Morena Blvd',null,'San Diego','Virginia','United States','92117','robert@smith.com',null,$accountId);

    $result = $instance->create(array($zBillToContact));
    $contactId = $result->result->Id;

    # Create Payment Method on Account
    $zPaymentMethod = makePaymentMethod('Firstly Lastly', '52 Vexford Lane',null, 'Anaheim', 'California', 'United States','22042', 'Visa', '5105105105105100', '12', '2012',$accountId);

    $result = $instance->create(array($zPaymentMethod));
    $paymentMethodId = $result->result->Id;

    # Update Account w/ Bill To/Sold To; also specify as active
    $zAccount = new Zuora_Account();
    $zAccount->Id = $accountId;
    $zAccount->Status = 'Active';
    $zAccount->BillToId = $contactId;
    $zAccount->SoldToId = $contactId;
    $zAccount->DefaultPaymentMethodId = $paymentMethodId;
    $result = $instance->update(array($zAccount));

    return $accountId;

}

# get ProductRatePlanCharge by AccountingCode.
function getChargeByAccountingCode($instance, $accountingCode){

    $result = $instance->query("select Id, ProductRatePlanId from ProductRatePlanCharge where AccountingCode = '$accountingCode'");
    if ($result->result->size != 1){
        print "No Product Rate Plan Charge found with AccountingCode = '$accountingCode'";
        exit();
    }

    $ProductRatePlanCharge = $result->result->records;
    return $ProductRatePlanCharge;

}
function subscribe($instance, $ProductRatePlan,$GeneratePayments=true,$GenerateInvoice=true,$accountName=null, $subscriptionName=null){

    $ProductRatePlanId = $ProductRatePlan->Id;

    # SUBSCRIBE
    $zAccount =  makeAccount((isset($accountName)? $accountName : 'Robert Smith') ,'USD','Active');

    $zPaymentMethod = makePaymentMethod('Firstly Lastly', '52 Vexford Lane',null, 'Anaheim', 'California', 'United States','22042', 'Visa', '5105105105105100', '12', '2012');

    $zBillToContact = makeContact('Robert','Smith','4901 Morena Blvd',null,'San Diego','Virginia','United States','92117','robert@smith.com',null);


    $zSubscription = makeSubscription((isset($subscriptionName)?$subscriptionName:"Name".time()),null);

    $zSubscriptionData = makeSubscriptionData($zSubscription,array(),array(),$ProductRatePlanId);

    $zSubscribeOptions = new Zuora_SubscribeOptions($GenerateInvoice,$GeneratePayments);

    $result = $instance->subscribe($zAccount, $zSubscriptionData,$zBillToContact, $zPaymentMethod, $zSubscribeOptions);

    return $result;

}

# make subscribe w/ existing call
function subscribeWithExistingAccount($instance, $ProductRatePlan, $accountId,$GeneratePayments=true,$GenerateInvoice=true, $subscriptionName=null){

    $ProductRatePlanId = $ProductRatePlan->Id;

    # SUBSCRIBE
    $zAccount = new Zuora_Account();
    $zAccount->Id = $accountId;

    $zSubscription = makeSubscription((isset($subscriptionName)?$subscriptionName:"Name".time()),null);

    $zSubscriptionData = makeSubscriptionData($zSubscription,array(),array(),$ProductRatePlanId);


    $zSubscribeOptions = new Zuora_SubscribeOptions($GenerateInvoice, $GeneratePayments);

    $result = $instance->subscribeWithExistingAccount($zAccount, $zSubscriptionData, $zSubscribeOptions);

    return $result;

}
# upload some hard code sample usage info
function uploadUsages($instance,$usages){

    $zUsages = array();

    foreach($usages as $usage){
        $zUsage = new Zuora_Usage();
        $zUsage->AccountId = $usage['AccountId'];
        $zUsage->Quantity  = $usage['Quantity'];
        $zUsage->StartDateTime = $usage['StartDateTime'];
        $zUsage->UOM = $usage['UOM'];
        $zUsages[] = $zUsage;
    }
    // please Iterate through the list of zUsages, 50 at a time
    $result = $instance->create($zUsages);

    return $result;
}

# generate an invoice
function generateInvoice($instance,$accountId,$invoiceDate,$targetDate){
    $zInvoices = array();

    $invoice = new Zuora_Invoice();
    $invoice->AccountId = $accountId;
    $invoice->InvoiceDate = $invoiceDate;
    $invoice->TargetDate = $targetDate;

    $zInvoices[] = $invoice;

    $result = $instance->generate($zInvoices);
    return $result;
}

# post invoice
function postInvoice($instance,$invoiceId){
    $invoice = new Zuora_Invoice();
    $invoice->Id = $invoiceId;
    $invoice->Status = 'Posted';

    $result = $instance->update(array($invoice));
    return $result;
}

function createAndApplyPayment($instance,$accountId){
    # QUERY PaymentMethod
    $query = "SELECT Id,Type FROM PaymentMethod WHERE AccountId = '".$accountId."'";
    $records = queryAll($instance, $query);
    $paymentMethodId=$records[0]->Id;
    print "\nPaymentMethod Queried ($query): " . $records[0]->Id . "  " . $records[0]->Type;

    # QUERY Invoice Balance
    $query = "select Id,Balance from Invoice where AccountId = '".$accountId."' and Balance>0";
    $records = queryAll($instance, $query);
    $amount = $records[0]->Balance;
    $invoiceId = $records[0]->Id;
    print "\nInvoice Balance Queried ($query): " . $records[0]->Id . "  " . $records[0]->Balance;

    $payment = new Zuora_Payment();
    $payment->AccountId = $accountId;
    $payment->Amount = $amount;
    $payment->EffectiveDate = date('Y-m-d\TH:i:s');
    $payment->PaymentMethodId = $paymentMethodId;
    $payment->Type = 'Electronic';
    $payment->Status = 'Draft';

    $result = $instance->create(array($payment));
    $paymentId = $result->result->Id;

    $success1 = $result->result->Success;
    $msg = "Payment: ".($success1 ? "Success" : $result->result->errors->Code . " (" . $result->result->errors->Message.")");

    $invoicePayment = new Zuora_InvoicePayment();
    $invoicePayment->Amount = $amount;
    $invoicePayment->InvoiceId = $invoiceId;
    $invoicePayment->PaymentId = $paymentId;
    $result = $instance->create(array($invoicePayment));

    $success2 = $result->result->Success;
    $msg .=" -> InvoicePayment: ". ($success2 ?  "Success" : $result->result->errors->Code . " (" . $result->result->errors->Message.")");

    $payment = new Zuora_Payment();
    $payment->Id = $paymentId;
    $payment->Status = 'Processed';
    $result=$instance->update(array($payment));
    $success3 = $result->result->Success;
    $msg .=" -> Payment Processed:". ($success3 ?  "Success" : $result->result->errors->Code . " (" . $result->result->errors->Message.")");

    print "\nCreate and Apply Payment: " . $msg;
}

function getProductRatePlan($instance,$productName){
    $result = $instance->query("select Id from Product where Name='$productName'");
    if ($result->result->size != 1){
        print "No Product found with Name = '$productName'";
        exit();
    }
    $productId = $result->result->records->Id;

    $result = $instance->query("select Id,Name from ProductRatePlan where ProductId = '$productId'");
    if ($result->result->size == 0){
        print "No Product Rate Plan found with ProductId = '$productId'(Product Name = '$productName')";
        exit();
    }

    if(is_array($result->result->records)){
        $ProductRatePlan = $result->result->records[0];
    }else{
        $ProductRatePlan = $result->result->records;
    }

    return $ProductRatePlan;
}

function getProductRatePlanCharges($instance,$productRatePlanId){
    $result = $instance->query("select Id from ProductRatePlanCharge where ProductRatePlanId='$productRatePlanId'");
    if ($result->result->size == 0){
        print "No Product RatePlan Charge found with ProductRatePlanId = '$productRatePlanId'";
        exit();
    }

    $ProductRatePlanCharges = array();
    if(is_array($result->result->records)){
        $ProductRatePlanCharges = $result->result->records;
    }else{
        $ProductRatePlanCharges[] = $result->result->records;
    }

    return $ProductRatePlanCharges;
}
function newProductAmendment($instance,$newProductName,$subscriptionId){
    $date = date('Y-m-d\TH:i:s');

    $amendment = new Zuora_Amendment();
    $amendment->EffectiveDate = $date;
    $amendment->Name = 'addproduct' . time();
    $amendment->Status = 'Draft';
    $amendment->SubscriptionId = $subscriptionId;
    $amendment->Type = 'NewProduct';

    $result = $instance->create(array($amendment));

    $amendmentId = $result -> result -> Id;

    $rateplan = new Zuora_RatePlan();
    $rateplan->AmendmentId = $amendmentId;
    $rateplan->AmendmentType = 'NewProduct';

    $ProductRatePlan = getProductRatePlan($instance,$newProductName);
    $rateplan->ProductRatePlanId = $ProductRatePlan->Id;

    $instance->create(array($rateplan));

    $amendment = new Zuora_Amendment();
    $amendment->Id = $amendmentId;
    $amendment->ContractEffectiveDate = $date;

    $amendment->Status = 'Completed';

    $result = $instance->update(array($amendment));
    return $result;
}

function removeProductAmendment($instance,$ratePlanId,$subscriptionId){
    $date = date('Y-m-d\TH:i:s');

    $amendment = new Zuora_Amendment();
    $amendment->EffectiveDate = $date;
    $amendment->Name = 'removeproduct' . time();
    $amendment->Status = 'Draft';
    $amendment->SubscriptionId = $subscriptionId;
    $amendment->Type = 'RemoveProduct';

    $result = $instance->create(array($amendment));

    $amendmentId = $result -> result -> Id;

    $rateplan = new Zuora_RatePlan();
    $rateplan->AmendmentId = $amendmentId;
    $rateplan->AmendmentType = 'RemoveProduct';

    $rateplan->AmendmentSubscriptionRatePlanId  = $ratePlanId;

    $instance->create(array($rateplan));

    $amendment = new Zuora_Amendment();
    $amendment->Id = $amendmentId;
    $amendment->ContractEffectiveDate = $date;

    $amendment->Status = 'Completed';

    $result = $instance->update(array($amendment));
    return $result;
}

function cancelSubscriptionAmendment($instance,$subscriptionId){
    $date = date('Y-m-d\TH:i:s');

    $amendment = new Zuora_Amendment();
    $amendment->EffectiveDate = $date;
    $amendment->Name = 'cancel' . time();
    $amendment->Status = 'Draft';
    $amendment->SubscriptionId = $subscriptionId;
    $amendment->Type = 'Cancellation';

    $result = $instance->create(array($amendment));

    $amendmentId = $result -> result -> Id;

    $amendment = new Zuora_Amendment();
    $amendment->Id = $amendmentId;
    $amendment->ContractEffectiveDate = $date;

    $amendment->Status = 'Completed';

    $result = $instance->update(array($amendment));
    return $result;

}
function makeContact($firstName,$lastName,$address1,$address2,$city,$state,$country,$postalCode,$workMail,$workPhone,$accountId=null){

    $zBillToContact = new Zuora_Contact();

    $zBillToContact->FirstName = $firstName;
    $zBillToContact->LastName = $lastName;
    $zBillToContact->Address1 = $address1;
    $zBillToContact->Address2 = $address2;
    $zBillToContact->City = $city;
    $zBillToContact->State = $state;
    $zBillToContact->Country = $country;
    $zBillToContact->PostalCode = $postalCode;
    $zBillToContact->WorkEmail = $workMail;
    $zBillToContact->WorkPhone = $workPhone;
    $zBillToContact->AccountId = $accountId;

    return  $zBillToContact;
}
function makePaymentMethod($creditCardHolderName, $address1,$address2, $city, $state, $country, $postalCode, $creditCardType, $creditCardNumber, $creditCardExpirationMonth, $creditCardExpirationYear,$accountId=null){
    $zPaymentMethod = new Zuora_PaymentMethod();
    $zPaymentMethod->AccountId = $accountId;

    $zPaymentMethod->CreditCardAddress1 = $address1;
    $zPaymentMethod->CreditCardAddress2 = $address2;
    $zPaymentMethod->CreditCardCity = $city;
    $zPaymentMethod->CreditCardCountry = $country;
    $zPaymentMethod->CreditCardExpirationMonth = $creditCardExpirationMonth;
    $zPaymentMethod->CreditCardExpirationYear = $creditCardExpirationYear;
    $zPaymentMethod->CreditCardHolderName = $creditCardHolderName;
    $zPaymentMethod->CreditCardNumber = $creditCardNumber;
    $zPaymentMethod->CreditCardPostalCode = $postalCode;
    $zPaymentMethod->CreditCardState = $state;
    $zPaymentMethod->CreditCardType = $creditCardType;

    $zPaymentMethod->Type = 'CreditCard';
    return $zPaymentMethod;
}
function makeSubscription($subscriptionName, $subscriptionNotes){
    $date = date('Y-m-d\TH:i:s',time());

    $zSubscription = new Zuora_Subscription();

    $zSubscription->Name = $subscriptionName;
    $zSubscription->Notes = $subscriptionNotes;

    $zSubscription->ContractAcceptanceDate = $date;
    $zSubscription->ContractEffectiveDate = $date;

    $zSubscription->InitialTerm = 12;
    $zSubscription->RenewalTerm = 12;
    $zSubscription->ServiceActivationDate = $date;

    $zSubscription->TermStartDate=$date;
    $zSubscription->Status = 'Active';
    $zSubscription->Currency = 'USD';
    $zSubscription->AutoRenew = 1;

    return 	$zSubscription;
}
function setRatePlanData($zSubscriptionData,$chargeIds,$rateplancharges,$productRatePlanId){
    $zRatePlan = new Zuora_RatePlan();
    $zRatePlan->AmendmentType = 'NewProduct';

    $zRatePlan->ProductRatePlanId = $productRatePlanId;
    $zRatePlanData = new Zuora_RatePlanData($zRatePlan);

    foreach($chargeIds as $cid){
        foreach($rateplancharges as $rc){
            if($rc->Id == $cid){
                $rpc = new Zuora_RatePlanCharge();
                $rpc->ProductRatePlanChargeId = $cid;
                if($rc->DefaultQuantity>0){
                    $rpc->Quantity =  1;
                }
                $rpc->TriggerEvent ="ServiceActivation";

                $zPlanChargeData = new Zuora_RatePlanChargeData($rpc);


                $zRatePlanData->addRatePlanChargeData($zPlanChargeData);
            }
        }
    }

    $zSubscriptionData->addRatePlanData($zRatePlanData);

}

function makeSubscriptionData($subscription,$chargeIds,$rateplancharges,$rateplanId){
    $zSubscriptionData = new Zuora_SubscriptionData($subscription);
    setRatePlanData($zSubscriptionData,$chargeIds,$rateplancharges,$rateplanId);
    return $zSubscriptionData;
}