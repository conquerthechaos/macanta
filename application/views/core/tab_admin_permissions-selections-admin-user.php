
<h3>Admin Tag: </h3>
<select class="admintagpicker macanta-admin-tag"  title="Choose Tag For Macanta Admin">
    <?php
     $DBPermissionTag = array();
     $DBPermissionTag[$DBPermissionTags->administrator] = true;
    foreach($OtherTags as $Tag){
        $selected = isset($DBPermissionTag[$Tag->Id]) ? 'selected':'';
        echo "<option value='".$Tag->Id."' $selected>".ucfirst($Tag->GroupName)."</option>";
    }
    ?>
</select>
<?php
$DBPermissionTag = array();
$DBPermissionTag[$DBPermissionTags->staff] = true;
?>
<h3>User Tag: </h3>
<select class="admintagpicker macanta-user-tag"  title="Choose Tag For Macanta User">
    <?php
    foreach($OtherTags as $Tag){
        $selected = isset($DBPermissionTag[$Tag->Id]) ? 'selected':'';
        echo "<option value='".$Tag->Id."' $selected>".ucfirst($Tag->GroupName)."</option>";
    }

    ?>
</select>