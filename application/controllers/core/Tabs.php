<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 07/03/16
 * Time: 3:00 PM
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . '/libraries/Inflect.php';

class Tabs extends MY_Controller
{
    //protected $local_stylesheets = array('mystylesheet.css');
    public $content = array();
    public $ajaxResults = array(
        "status" => 0, // true or false
        "message" => "",// any massage
        "data" => array(), // returned data
        "script" => '' // javascript need to execute // returned data
    );
    public $TabMenuName;
    private $MacantaIgniter;

    public function __construct()
    {
        parent::__construct();
        $this->TabMenuName = array(
            'Call' => $this->lang->line('text_call'),
            'Note' => $this->lang->line('text_note')
        );

    }

    public function index($Data)
    {
        $CurrentDir = dirname(__FILE__) . "/";
        $TimeStarted = time();
        $ToRecord = 'Time Started: ' . date('Y-m-d H:i:s', $TimeStarted) . "\n";
        $allowedTabs = $Data['Items'];
        $session_name = $Data['session_name'];
        $user_seession_data = macanta_get_user_seession_data($session_name);
        $session_data = unserialize($user_seession_data->session_data);
        $Prams['Id'] = $Data['Id'];
        //Get Contact Details to be available for entire tabs!!
        $ContactInfo = infusionsoft_get_contact_by_id($Data['Id'], array('all'));
        //Make a javascript variable of each contact info
        $ContactJSVars = '';
        foreach ($ContactInfo as $FieldName => $FieldValue) {
            $ContactJSVars .= "ContactInfo['{$FieldName}'] = " . json_encode($FieldValue) . ";";
        }
        $TabMenu = array();
        $TabContent = array();
        $AllTabs = json_decode($this->config->item('macanta_tabs'), true);
        $this->MacantaIgniter =& get_instance();
        foreach ($AllTabs as $Tab => $Params) {
            if (!in_array('all', $allowedTabs)) {
                if (!in_array($Tab, $allowedTabs)) continue;
            }
            $TabName = isset($this->TabMenuName[$Tab]) ? $this->TabMenuName[$Tab] : $Tab;
            $TabMenu[$TabName] = array('icon' => $Params['tab_icon'], 'active' => $Params['active']);
            foreach ($Params['controllers'] as $Controller => $Methods) {
                $loadedController = _loadMacantaController($this->MacantaIgniter, $Controller, $ContactInfo);
                if (isset($this->$loadedController)) {
                    foreach ($Methods as $Method => $Items) {
                        if (method_exists($this->$loadedController, $Method)) {
                            $TabContent[$TabName][] = $this->$loadedController->$Method($Items, $session_name);
                        } else {
                            $TabContent[$TabName][] = 'No "' . $Method . '" Method Existing in ' . $loadedController . "(" . $Controller . ") Controller";
                        }
                    }
                } else {
                    $TabContent[$TabName][] = $loadedController;
                }

            }
        }
        $TimeEnded = time();
        $TimeLapse = $TimeEnded - $TimeStarted;
        $ToRecord .= "ContactNotes: " . $TimeLapse . "\n";
        /*For connector tab contents*/
        $TimeStarted = time();
        $ConnectorTabsEncoded = $this->config->item('connected_info');
        if ($ConnectorTabsEncoded) {
            $ConnectorTabs = json_decode($ConnectorTabsEncoded, true);
            $UserConnectedInfo = macanta_get_connected_info($Data['Id']);
            foreach ($ConnectorTabs as $key => $ConnectorTab) {

                if (isset($ConnectorTab['visibility'])) {
                    if ($ConnectorTab['visibility'] !== "ShowCDToAll") {
                        $icon = 'fa fa-info-circle';
                        if ($session_data['userlevel'] !== "administrator") {
                            $tagsArr = explode(',', $session_data['groups']);
                            if ($ConnectorTab['visibility'] === "ShowCDToAllUserSpecific") {
                                if (!in_array($ConnectorTab['permission_tag'], $tagsArr)) continue;
                            }
                        }

                    } else {
                        $icon = 'fa fa-sitemap';
                    }
                } else {
                    $icon = 'fa fa-sitemap';
                }

                $TabName = $ConnectorTab['title'];
                $TabMenu[$TabName] = array('icon' => $icon, 'active' => false);
                $ParamsTemp['title'] = $TabName;
                $ParamsTemp['titleSingular'] = Inflect::singularize($TabName);
                $ParamsTemp['ConnectorTab'] = $ConnectorTab;
                $ParamsTemp['GUID'] = $ConnectorTab['id'];
                $ParamsTemp['ContactInfo'] = $ContactInfo;
                $ParamsTemp['session_data'] = $session_data;
                $ParamsTemp['UserValue'] = isset($UserConnectedInfo[$ConnectorTab['id']]) ? $UserConnectedInfo[$ConnectorTab['id']] : [];
                $TabContent[$TabName][] = $this->load->view('core/tab_connected_info', $ParamsTemp, true);
            }
        }
        $TimeEnded = time();
        $TimeLapse = $TimeEnded - $TimeStarted;
        $ToRecord .= "ConnectorTab: " . $TimeLapse . "\n";
        //==============================//

        /*For custom tab contents*/
        $TimeStarted = time();
        $CustomTabsEncoded = $this->config->item('custom_tabs');
        $CustomTabsOffTagId = $this->config->item('custom_tabs_tag');
        $CustomTabOnTagId = $this->config->item('custom_tabs_on_tag');
        $CustomTabOnTagId = $CustomTabOnTagId ? trim($CustomTabOnTagId) : "";
        if (isset($ContactInfo->Groups) && trim($ContactInfo->Groups) != '') {
            $tagsArrSearchedContact = explode(',', $ContactInfo->Groups);

        } else {
            $tagsArrSearchedContact = [];
        }

        if ($CustomTabsEncoded) {
            $CustomTabs = json_decode($CustomTabsEncoded, true);
            if (sizeof($CustomTabs) > 0) {
                foreach ($CustomTabs as $CustomTab) {

                    if ($CustomTabsOffTagId) {
                        if (in_array($CustomTabsOffTagId, $tagsArrSearchedContact) && $session_data['userlevel'] != "administrator") {
                            if ($CustomTabOnTagId != '') {
                                $CustomTabOnTagIdArr = explode(',', $CustomTabOnTagId);
                                if (in_array($CustomTab['permission_tag'], $CustomTabOnTagIdArr)) {
                                    if (!in_array($CustomTab['permission_tag'], $tagsArrSearchedContact)) continue;
                                }
                            } else {
                                continue;
                            }

                        }
                    }
                    if ($CustomTab['permission'] == "1" && $session_data['userlevel'] != "administrator") {
                        $tagsArr = explode(',', $session_data['groups']);


                        if (!in_array($CustomTab['permission_tag'], $tagsArr)) continue;

                        if ($CustomTab['permission_all'] == "0") {
                            if (!in_array($CustomTab['permission_tag'], $tagsArrSearchedContact)) continue;
                        }
                    }
                    if ($CustomTab['permission'] !== "ShowToAll" && $session_data['userlevel'] !== "administrator") {

                        $tagsArr = explode(',', $session_data['groups']);
                        $tagsArrSearchedContact = explode(',', $ContactInfo->Groups);

                        if ($CustomTab['permission'] === "ShowToAll_UserSpecific") {
                            if (!in_array($CustomTab['permission_tag'], $tagsArr)) continue;
                        }
                        if ($CustomTab['permission'] === "ContactUserSpecific") {
                            if (!in_array($CustomTab['permission_tag'], $tagsArr)) continue;
                            if (!in_array($CustomTab['permission_tag'], $tagsArrSearchedContact)) continue;
                        }
                        if ($CustomTab['permission'] === "ContactHaveTag") {
                            if (in_array($CustomTab['permission_tag'], $tagsArrSearchedContact)) continue;
                        }
                    }
                    $TabName = $CustomTab['title'];
                    $icon = 'fa fa-user-circle';
                    if (isset($CustomTab['global']) && $CustomTab['global'] == 'yes') $icon = 'fa fa-globe';
                    $TabMenu[$TabName] = array('icon' => $icon, 'active' => false);
                    $ParamsTemp['title'] = $TabName;
                    $ParamsTemp['contentid'] = $CustomTab['id'];

                    /*// DISABLE FOR LAZY LOADING
                    $TempContent = str_replace('<p></p>','',base64_decode($CustomTab['content']));
                    $TempContent = handleMergedFields($ContactInfo->Id, $TempContent,$session_name);
                    $ParamsTemp['content'] = handleShortcodes($TempContent, $shortcodes,$session_data,$ContactInfo);*/

                    $TabContent[$TabName][] = $this->load->view('core/tab_custom_lazy', $ParamsTemp, true);
                }
            }
        }
        $TimeEnded = time();
        $TimeLapse = $TimeEnded - $TimeStarted;
        $ToRecord .= "CustomTab: " . $TimeLapse . "\n";
        //==============================//

        //========== ADMIN AREA =========//
        $TimeStarted = time();
        $ActivateAdminTabScript = '';
        if (isset($session_data['userlevel']) && $session_data['userlevel'] == 'administrator') {
            $ActivateAdminTabScript = "if(ToAdmin()) $('.nav-tabs a[href=#Admin]').tab('show'); ";
            $TabMenu['Admin'] = array('icon' => 'glyphicon glyphicon-wrench', 'active' => false);
            $ParamsTemp['title'] = 'Admin';
            $ParamsTemp['contentid'] = 'macanta-admin';
            $TabContent['Admin'][] = $this->load->view('core/tab_admin_lazy', $ParamsTemp, true);


            /*// DISABLE FOR LAZY LOADING
            $AdminController = _loadMacantaController($this->MacantaIgniter, "core/tabs/admin", $ContactInfo);
            $AdminMethods = array("index"=>"all");
            foreach($AdminMethods as $Method => $Items){
                if(method_exists($this->$AdminController, $Method)){
                    $TabContent['Admin'][] = $this->$AdminController->$Method($Items);
                }else{
                    $TabContent['Admin'][] = 'No "'.$Method .'" Method Existing in '. $AdminController . "(core/tabs/admin) Controller";
                }
            }*/
        }
        $TimeEnded = time();
        $TimeLapse = $TimeEnded - $TimeStarted;
        $ToRecord .= "AdminTab: " . $TimeLapse . "\n";
        file_put_contents($CurrentDir . "_TabsTimeLapse_".$this->config->item('IS_App_Name').".txt", $ToRecord . "\n", FILE_APPEND);
        //==============================//
        $ConnectorRelationship = $this->config->item('ConnectorRelationship') ? json_decode($this->config->item('ConnectorRelationship')) : [];
        $ConnectorFields = $this->config->item('connected_info') ? json_decode($this->config->item('connected_info'), true) : [];
        $RelationshipRules = [];
        foreach ($ConnectorFields as $FieldGroups) {
            $RelationshipRules[$FieldGroups['id']] = $FieldGroups['relationships'];
        }
        $RelationshipRules = json_encode($RelationshipRules);
        $availableRelationships = [];
        foreach ($ConnectorRelationship as $RelationshipItem) {
            $availableRelationships[] = $RelationshipItem->RelationshipName;
        }

        $availableRelationships = json_encode($availableRelationships);
        $Prams['menu'] = $TabMenu;
        $Prams['content'] = $TabContent;
        $Tabs = $this->load->view('core/tab_index', $Prams, true);
        $availableTags = $this->notetags_get();
        $this->ajaxResults['data'] = $Tabs;
        $this->ajaxResults['message'] = "Tabs Displayed";
        $this->ajaxResults['script'] = 'noteCollapsible();' . $ActivateAdminTabScript . 'availableTags=' . $availableTags . ';relationshipRules=' . $RelationshipRules . ';availableRelationships=' . $availableRelationships . ';Macanta("Tab index");' . $ContactJSVars . '; toggleThisTaskInside();';
        $this->ajaxResults['status'] = 1;
        return $this->ajaxResults;
    }

    public function notetags_get()
    {
        $tags = array();
        $tagsStr = array();
        $query = $this->db->get('tags');
        foreach ($query->result() as $row) {
            $tagsStr[] = $row->tag_name;
            $tags[] = array(
                'id' => $row->tag_slug,
                'label' => $row->tag_name,
                'value' => $row->tag_name
            );
        }
        //echo  json_encode($tags);
        return json_encode($tagsStr);
    }

    public function tab_admin($Data)
    {
        $session_name = $Data['session_name'];
        $this->MacantaIgniter =& get_instance();
        $ContactInfo = infusionsoft_get_contact_by_id($Data['ContactId'], array('all'));
        $AdminController = _loadMacantaController($this->MacantaIgniter, "core/tabs/admin", $ContactInfo);
        $AdminMethods = array("index" => "all");
        $Content = '';
        foreach ($AdminMethods as $Method => $Items) {
            if (method_exists($this->$AdminController, $Method)) {
                $Content .= $this->$AdminController->$Method($Items);
            } else {
                $Content .= 'No "' . $Method . '" Method Existing in ' . $AdminController . "(core/tabs/admin) Controller";
            }
        }

        $this->ajaxResults['data'] = $Content;
        $this->ajaxResults['message'] = $Data['title'];
        $this->ajaxResults['script'] = '';
        $this->ajaxResults['status'] = 1;
        return $this->ajaxResults;
    }

    public function tab_custom($Data)
    {
        $session_name = $Data['session_name'];
        $user_seession_data = macanta_get_user_seession_data($session_name);
        $session_data = unserialize($user_seession_data->session_data);
        $CustomTabsEncoded = $this->config->item('custom_tabs');
        $CustomTabs = json_decode($CustomTabsEncoded, true);
        $ContactInfo = infusionsoft_get_contact_by_id($Data['ContactId'], array('all'));
        $Content = '';
        $shortcodes = array(
            "ISwebform" => function ($data, $session_data, $ContactInfo) {
                $content = "";
                //Calculate the age
                $randStr = generateRandomString(5);
                if (!isset($data["readonly"])) {
                    $data["readonly"] = 'false';
                }
                if (!isset($data["createnote"])) {
                    $data["createnote"] = 'no';
                }
                if (!isset($data["assign_note_to_contact_owner"])) {
                    $data["assign_note_to_contact_owner"] = 'no';
                }
                if (!isset($data["submitted_by_customfield"])) {
                    $data["submitted_by_customfield"] = '';
                }
                $readonly = trim($data["readonly"]);
                $createnote = trim($data["createnote"]);
                $assign_note_to_contact_owner = trim($data["assign_note_to_contact_owner"]);
                $submitted_by_customfield = trim($data["submitted_by_customfield"]);
                if (isset($data["formid"])) {
                    //process form here
                    $formid = trim($data["formid"]);
                    $class = 'webform' . $randStr;
                    $appname = $this->config->item('IS_App_Name');
                    $content = '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 web-form-panel">
                    <div class="panel panel-default container' . $class . '">
                      <div class="panel-heading webformPanelTitle"><h3 class="panel-title ' . $class . '"></h3></div>
                      <div class="webformPanelBody panel-body ' . $class . '">
                      </div>
                    </div><script>
                    _getWebform("' . $class . '", "' . $appname . '", "' . $formid . '", "' . $readonly . '", "' . $createnote . '", "' . $assign_note_to_contact_owner . '", "' . $submitted_by_customfield . '");
                    </script>
                    </div>';
                } else {
                    $content = 'ISwebform Shortcode Error: Invalid or missing parameters';
                }
                return $content;
            },
            "EmailForm" => function ($data, $session_data, $ContactInfo) {
                $randStr = generateRandomString(5);
                $class = 'EmailForm' . $randStr;
                if (!isset($data["createnote"])) {
                    $data["createnote"] = 'no';
                }
                $createnote = trim($data["createnote"]);
                $content = '<div class="col-md-12">
                    <div class="panel panel-default container' . $class . '">
                      <div class="panel-heading EmailFormPanelTitle"><h3 class="panel-title ' . $class . '">Compose Email</h3></div>
                      <div class="EmailFormPanelBody panel-body ' . $class . '">
                      <div class="form-group  col-md-6">
                        <label for="subject">Subject:</label>
                        <input type="text" class="form-control subject" name="subject">
                      </div>
                      <div class="form-group  col-md-6">
                        <label for="subject">BCC:</label>
                        <input type="text" class="form-control bcc" name="bcc" value="' . $session_data['email'] . '">
                      </div>
                       <div class="form-group col-md-12">
                        <label for="message">Message:</label>
                        <textarea id="' . $class . '" name="' . $class . '" class="form-control EmailForm TextEditor" placeholder="Text Here"></textarea>
                       </div>
                       <div class="form-group col-md-12">
                       <button  type="button" value="Send" class="btn btn-default" onclick="sendISEmail(\'' . $class . '\',\'' . $createnote . '\');">Send Email</button>
                       </div>
                      </div>
                    </div>
                     <script>
                        initTinymceById("' . $class . '");
                        </script>
                    </div>
                    ';

                return $content;
            },
            "EmailHistory" => function ($data, $session_data, $ContactInfo) {
                $randStr = generateRandomString(5);
                $GetMore = true;
                $TotalHistory = [];
                $Offset = 0;
                $Limit = 1000;
                while($GetMore == true ){
                    $EmailHistory = infusionsoft_get_email_history($ContactInfo->Id,$Limit,$Offset);
                    if(sizeof($EmailHistory->emails)<1000) $GetMore = false;
                    $TotalHistory = array_merge($TotalHistory,$EmailHistory->emails);
                    if(isset($EmailHistory->next)){
                        $parts = parse_url($EmailHistory->next);
                        parse_str($parts['query'], $query);
                        if(isset($query['offset'])){
                            $Offset = $query['offset'];
                        }
                    }
                }
                $class = 'EmailHistory' . $randStr;
                $tbody = '';
                foreach ($TotalHistory as $Email) {
                    if ($Email->sent_to_address == 'geover@gmail.com' && strpos($Email->subject, 'Your Conquer The Chaos Ltd receipt') !== false) continue;
                    $opened_date = $Email->opened_date ? date('d M Y', strtotime($Email->opened_date)):"";
                    $tbody .= '
                            <tr data-historyid="' . $Email->id . '">
                            <td>' . $Email->subject . '</td>
                            <td>' . $Email->sent_to_address . '</td>
                            <td>' . $Email->sent_from_address . '</td>
                            <td data-original="'.$Email->sent_date.'">' . date('d M Y', strtotime($Email->sent_date)) . '</td>
                            <td data-original="'.@$Email->opened_date.'">' . $opened_date . '</td>
                            </tr>
                            ';

                }
                $content = '
                <div class="panel panel-primary right-LookAndFeel">
                    <div class="panel-heading">
                        <h3 class="panel-title "><i class="fa fa-envelope"></i> Email History </h3>
                    </div>
                    <div class="panel-body admin-panelBody">
                    <div class="table-responsive">
                    	<table id="EmailHistoryTable" class="table table-hover table-striped table-bordered" cellspacing="0" width="98%">
                       <thead>
                       <tr>
                          <th>Subject</th>
                          <th>To:</th>
                          <th>From:</th>
                          <th>Date Sent</th>
                          <th>Date Opened</th>
                       </tr>
                       </thead>
                       <tbody>' . $tbody . '</tbody>
                   </table>
                    </div>
                    </div>
                    </div>
                    
                    
                   <script>
                    renderEmailHistory();
                        </script>
                    ';

                return $content;
            },
            "EmailTemplate" => function ($data, $session_data, $ContactInfo) {
                $randStr = generateRandomString(5);
                $template = applyFn('infusionsoft_get_email_template_content', $data["id"]);;
                if (!isset($data["createnote"])) {
                    $data["createnote"] = 'no';
                }
                $createnote = trim($data["createnote"]);
                $class = 'EmailForm' . $randStr;
                if (!isset($template->pieceTitle)) {
                    $Alert = '<div class="col-md-12"><span class="col-md-12 alert alert-danger">Template [' . $data["id"] . '] Did not found or set private</span></div>';
                } else {
                    $Alert = '';
                }
                $content = $Alert . '<div class="col-md-12">
                    <div class="panel panel-default container' . $class . '">
                      <div class="panel-heading EmailFormPanelTitle"><h3 class="panel-title ' . $class . '">' . $template->pieceTitle . '</h3></div>
                      <div class="EmailFormPanelBody panel-body ' . $class . '">
                      <div class="form-group col-md-6">
                        <label for="subject">Subject:</label>
                        <input type="text" class="form-control subject"  name="subject" value="' . $template->subject . '">
                      </div>
                      <div class="form-group  col-md-6">
                        <label for="subject">BCC:</label>
                        <input type="text" class="form-control bcc" name="bcc" value="' . $session_data['email'] . '">
                      </div>
                       <div class="form-group col-md-12">
                        <label for="message">Message:</label>
                        <textarea id="' . $class . '" name="' . $class . '" class="form-control TextEditor" placeholder="Text Here">
                        ' . $template->htmlBody . '
                        </textarea>
                       </div>
                       <div class="form-group col-md-12">
                       <button  type="button" value="Send" class="btn btn-default" onclick="sendISEmail(\'' . $class . '\',\'' . $createnote . '\');">Send Email</button>
                       </div>
                      </div>
                    </div>
                     <script>
                        initTinymceById("' . $class . '");
                        </script>
                    </div>
                    ';
                return $content;
            },
            "TagCat" => function ($data, $session_data, $ContactInfo) {
                $ReadOnly = isset($data["readonly"]) ? $data["readonly"] : 'no';
                $Disabled = $ReadOnly === 'no' ? "" : "disabled";
                $randStr = generateRandomString(5);
                $Tags = infusionsoft_get_tags_by_catId($data["id"], 'GroupName');
                $TagCategories = manual_cache_loader('TagCategories');
                //if(!isset($ContactInfo['Groups'])) $ContactInfo['Groups'] = '';
                $AppliedTags = explode(',', $ContactInfo->Groups);
                $class = 'Tags' . $randStr;
                $content = '<div class="col-md-3">
                    <div class="panel panel-default container' . $class . ' ContacTagsPanelTitle">
                      <div class="panel-heading ">
                         <h3 class="panel-title ' . $class . '">' . $TagCategories[$data["id"]] . '</h3>
                      </div>
                      <div class="panel-body "><select  multiple="multiple"  class="ContactTags ' . $class . '" id="' . $class . '" name="ContactTags[]" ' . $Disabled . '> ';
                foreach ($Tags as $Tag) {
                    $selected = in_array($Tag->Id, $AppliedTags) ? 'selected' : '';
                    $content .= ' <option value="' . $Tag->Id . '" ' . $selected . '>' . $Tag->GroupName . '</option>';
                }
                $content .= ' </select></div></div>
                        <script>
                        initMultiSelect("' . $class . '");
                        </script>
                         </div>';
                return $content;
            },
            "ISopportunities" => function ($data, $session_data, $ContactInfo) {
                if (!isset($data["Only_Show_Opps_Assigned_To_Logged_In_User"])) {
                    $data["Only_Show_Opps_Assigned_To_Logged_In_User"] = 'no';
                }
                if (isset($data["only_show_opps_assigned_to_logged_in_user"])) {
                    $data["Only_Show_Opps_Assigned_To_Logged_In_User"] = $data["only_show_opps_assigned_to_logged_in_user"];
                }
                if (!isset($data["pipelines"])) {
                    $data["pipelines"] = 'all';
                }
                $UserOnly = $data["Only_Show_Opps_Assigned_To_Logged_In_User"];
                $Params['Opportunities'] = get_opportunities($ContactInfo->Id, $UserOnly, false, $session_data);
                $Params['Stages'] = get_opp_stages();
                $Params['AllowedPipeline'] = strtoupper($data["pipelines"]);
                $Params['ShortCodeId'] = strtolower(random_string('alpha', 5));
                $Tabs = $this->load->view('core/tab_opportunities', $Params, true);
                return $Tabs;
            },
            "FileBox" => function ($data, $session_data, $ContactInfo) {
                $randStr = generateRandomString(5);
                $class = 'filebox' . $randStr;
                $Files = infusionsoft_get_files($ContactInfo->Id);
                $Table = '
                <table class="table table-hover FileBox ' . $class . '">
                	<thead>
                		<tr>
                		<th>Id</th>
                		<th>Name</th>
                		<!--<th>Size</th>-->
                		<th>Download</th>
                		<!--<th>Date Uploaded</th>
                		<th>Date Updated</th>-->
                		</tr>
                	</thead>
                	<tbody>';
                foreach ($Files as $File) {
                    //$TheFile = infusionsoft_get_file_by_id_lite($File->Id)->message;
                    //$FileDetails = $TheFile->file_descriptor;
                    //$FileDataEncoded = $TheFile->file_data;
                    //$FileDataDecoded = base64_decode($FileDataEncoded);
                    //mkdir(FCPATH.'tmp/'.$ContactInfo->Id, 0755, true);
                    //$Path = FCPATH.'tmp/'.$ContactInfo->Id.'/'.$File->FileName;
                    //file_put_contents($Path, $FileDataDecoded);
                    /*
                     stdClass Object (  [id] => 1054
                                        [category] => Documents
                                        [date_created] => 2017-11-17T02:28:59.000+0000
                                        [last_updated] => 2017-11-17T02:28:59.000+0000
                                        [file_name] => MyAvatar.jpg
                                        [file_size] => 698463
                                        [created_by] => 1
                                        [public] =>
                                        [contact_id] => 364
                                        [remote_file_key] =>
                                        [file_box_type] => Application
                                        [download_url] => https://qj311.infusionsoft.com/Download?Id=1054 )
                    */
                    $Table .= '<tr>';
                    $Table .= '<td>' . $File->Id . '</td>';
                    $Table .= '<td>' . $File->FileName . '</td>';
                    /*$Table .= '<td>'.formatBytes($File->FileSize).'</td>';*/
                    $Table .= '<td><a target="macanta" data-fileid="' . $File->Id . '" href="' . $this->config->item('base_url') . 'filebox/fileId/' . $File->Id . '">Download</a></td>';
                    //$Table .= '<td>'.date('d M Y',strtotime($FileDetails->date_created)).'</td>';
                    //$Table .= '<td>'.date('d M Y',strtotime($FileDetails->last_updated)).'</td>';
                    $Table .= '</tr>';
                }
                $Table .= '</tbody></table><script>makeDataTable(".' . $class . '")</script>';
                return $Table;
            },
            "Zuora" => function ($data, $session_data, $ContactInfo) {
                $ZuoraData['ZuoraContacts'] = [];
                if (check_zuora_credentials()) {
                    $ZuoraData['ZuoraContacts'] = zuora_queryData('WorkEmail', $ContactInfo->Email, "Contact");
                }
                return $this->load->view('core/tab_zuora', $ZuoraData, true);
            },

        );
        foreach ($CustomTabs as $CustomTab) {
            if ($Data['title'] == $CustomTab['title']) {
                $TempContent = str_replace('<p></p>', '', base64_decode($CustomTab['content']));
                $TempContent = handleMergedFields($ContactInfo->Id, $TempContent, $session_name);
                $Content = handleShortcodes($TempContent, $shortcodes, $session_data, $ContactInfo);
            }

        }
        $this->ajaxResults['data'] = $Content;
        $this->ajaxResults['message'] = $Data['title'];
        $this->ajaxResults['script'] = '';
        $this->ajaxResults['other'] = $ContactInfo;
        $this->ajaxResults['status'] = 1;
        return $this->ajaxResults;
    }

    public function connector_csv_generate_download()
    {
        if($_POST['CurrentConnectorDownLoadType']!='')
        {
            $CurrentConnectorDownLoadData=json_decode(base64_decode($_POST['CurrentConnectorDownLoadType']));
        }

        $ConnectedInfosEncoded = $this->config->item('connected_info');
        if (!$ConnectedInfosEncoded) $ConnectedInfosEncoded = [];
        $ConnectedInfos = json_decode($ConnectedInfosEncoded, true);

        $FieldList=array();
        $FieldList['ContactId']='Contact Id';
        $FieldList['FirstName']='First Name';
        $FieldList['LastName']='Last Name';
        $FieldList['Email']='Email';
        foreach($ConnectedInfos as $Pkey=>$ConnectorData)
        {
            foreach($ConnectorData['fields'] as $ConnectedFields)
            {
                $FieldList[$ConnectedFields['fieldId']]=$ConnectedFields['fieldLabel'];
            }
        }
        $FieldList['ConnectorRelationship']='Connector Relationship';

        $ConnectorRelationship = json_decode($this->config->item('ConnectorRelationship'));
        $ConnectorRelationshipData=array();

        foreach($ConnectorRelationship as $tmpData)
        {
            $ConnectorRelationshipData[$tmpData->Id]=$tmpData;
        }

        $GroupsRelation=array();
        $RelationWithGroup=array();
        foreach($ConnectedInfos as $Pkey=>$ConnectorData)
        {
            $ConnectedQuery=$this->db->query("select * from connected_data where `group`='$Pkey'");
            foreach($ConnectedQuery->result_array() as $tempData)
            {
                $ConsiderFlag=1;
                if(count($CurrentConnectorDownLoadData))
                {
                    if(!in_array($tempData['id'],$CurrentConnectorDownLoadData))
                    {
                        $ConsiderFlag=0;
                    }
                }

                if($ConsiderFlag) {
                    $connected_contact=json_decode($tempData['connected_contact']);
                    $connected_value=json_decode($tempData['value']);

                    foreach($connected_contact as $tempContact)
                    {
                        $tmpContactData=array();
                        $tmpContactData['ContactId']=$tempContact->ContactId;
                        $tmpContactData['FirstName']=$tempContact->FirstName;
                        $tmpContactData['LastName']=$tempContact->LastName;
                        $tmpContactData['Email']=$tempContact->Email;

                        foreach($ConnectorData['fields'] as $tmpFields)
                        {
                            $FieldId=$tmpFields['fieldId'];
                            if(is_object($connected_value->$FieldId))
                            {
                                $tmpData=array();
                                foreach($connected_value->$FieldId as $tmpObjKey=>$tmpVal)
                                {
                                    $tmpObjArr=explode('_',$tmpObjKey);

                                    if($tmpObjArr['1']==$tempContact->ContactId)
                                    {
                                        $tmpContactData[$FieldId]=$tmpVal;
                                    }
                                }

                            }
                            else
                            {
                                $tmpContactData[$FieldId]=$connected_value->$FieldId;
                            }
                        }

                        foreach($tempContact->relationships as $relationId)
                        {
                            $tmpRelationName=array();
                            if($RelationWithGroup[$tempContact->ContactId]['ConnectorRelationship'])
                            {
                                $tmpRelationName=explode(',',$RelationWithGroup[$tempContact->ContactId]['ConnectorRelationship']);
                            }
                            $tmpRelationName[]=$ConnectorRelationshipData[$relationId]->RelationshipName;
                            $tmpContactData['ConnectorRelationship']=implode(',',$tmpRelationName);
                            if($tempContact->ContactId!='undefined')
                            {
                                $RelationWithGroup[$tempContact->ContactId]=$tmpContactData;
                            }
                        }
                    }
                }
            }
            foreach($ConnectorData['relationships'] as $ConnectedFields)
            {
                $GroupsRelation[$Pkey][]=$ConnectedFields['Id'];
            }
        }

        $FieldNameData=array();
        foreach($RelationWithGroup as $Key=>$tmpData)
        {
            foreach($tmpData as $tmpContact)
            {
                foreach($tmpContact as $CKey=>$CVal)
                {
                    $FieldNameData[$Key][$CKey]=$CKey;
                }
            }
        }

        $CSVData=array();
        foreach($_POST['CSVHeader'] as $FieldKey=>$FieldsIds)
        {
            foreach($RelationWithGroup as $ContactData)
            {
                $tmpData=array();
                if(count($CSVData)==0)
                {
                    $tmpHeader=array();
                    foreach($FieldsIds as $FieldIdVal)
                    {
                        $tmpHeader[]=$FieldList[$FieldIdVal];
                    }
                    $CSVData[]=$tmpHeader;
                }

                foreach($FieldsIds as $FieldName)
                {
                    if($FieldName=='ConnectorRelationship')
                    {
                        $ConnectorRelationship=explode(',',$ContactData[$FieldName]);
                        $ConnectorRelationship=array_unique($ConnectorRelationship);
                        $ContactData[$FieldName]=implode(',',$ConnectorRelationship);
                    }
                    $tmpData[$FieldName]=$ContactData[$FieldName];
                }
                $CSVData[]=$tmpData;
            }
        }
        /*
        echo "<pre>";
        print_r($CSVData);
        echo "</pre>";
        die;
        */

        $delimiter=',';
        $filename='Macanta_Connector_Contacts.csv';
        $f = fopen('php://memory', 'w');
        foreach ($CSVData as $line)
        {
            fputcsv($f, $line, $delimiter);
        }
        fseek($f, 0);
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="'.$filename.'";');
        fpassthru($f);

        die;
        /*
        echo "<pre>";
        print_r($_POST);
        echo "</pre>";

        echo "<pre>";
        print_r($CSVData);
        echo "</pre>";

        echo "<pre>";
        print_r($RelationWithGroup);
        echo "</pre>";
        */

    }

    public function connector_csv_download()
    {

        $ConnectedInfosEncoded = $this->config->item('connected_info');
        if (!$ConnectedInfosEncoded) $ConnectedInfosEncoded = [];
        $ConnectedInfos = json_decode($ConnectedInfosEncoded, true);

        $ConnectorRelationship = json_decode($this->config->item('ConnectorRelationship'));
        $ConnectorRelationshipData=array();

        foreach($ConnectorRelationship as $tmpData)
        {
            $ConnectorRelationshipData[$tmpData->Id]=$tmpData;
        }

        $Current_Element=$_POST['Current_Element'];
        $GroupsRelation=array();
        $RelationWithGroup=array();
        $ConnectorFieldsData=array();
        foreach($ConnectedInfos as $Pkey=>$ConnectorData)
        {
            if($Current_Element==$Pkey)
            {
                $ConnectedQuery=$this->db->query("select * from connected_data where `group`='$Pkey'");
                foreach($ConnectedQuery->result_array() as $tempData)
                {
                    $connected_contact=json_decode($tempData['connected_contact']);
                    $connected_value=json_decode($tempData['value']);

                    foreach($connected_contact as $tempContact)
                    {
                        $tmpContactData=array();
                        $tmpContactData['ContactId']=$tempContact->ContactId;
                        $tmpContactData['FirstName']=$tempContact->FirstName;
                        $tmpContactData['LastName']=$tempContact->LastName;
                        $tmpContactData['Email']=$tempContact->Email;

                        foreach($ConnectorData['fields'] as $tmpFields)
                        {
                            $FieldId=$tmpFields['fieldId'];
                            if(is_object($connected_value->$FieldId))
                            {
                                $tmpData=array();
                                foreach($connected_value->$FieldId as $tmpVal)
                                {
                                    $tmpData[]=$tmpVal;
                                }
                                $tmpContactData[$FieldId]=implode(',',$tmpData);
                            }
                            else
                            {
                                $tmpContactData[$FieldId]=$connected_value->$FieldId;
                            }
                        }

                        foreach($tempContact->relationships as $relationId)
                        {
                            $RelationWithGroup[$relationId][]=$tmpContactData;
                        }
                    }
                }
                foreach($ConnectorData['relationships'] as $ConnectedFields)
                {
                    $GroupsRelation[$Pkey][]=$ConnectedFields['Id'];
                }
            }
        }

        $FieldNameData=array();
        foreach($RelationWithGroup as $Key=>$tmpData)
        {
            foreach($tmpData as $tmpContact)
            {
                foreach($tmpContact as $CKey=>$CVal)
                {
                    $FieldNameData[$Key][$CKey]=$CKey;
                }
            }
        }

        /*
        echo "<pre>";
        print_r($FieldNameData);
        echo "</pre>";

        echo "<pre>";
        print_r($RelationWithGroup);
        echo "</pre>";
        */

        $FieldList=array();
        $FieldList['ContactId']='Contact Id';
        $FieldList['FirstName']='First Name';
        $FieldList['LastName']='Last Name';
        $FieldList['Email']='Email';
        foreach($ConnectedInfos as $Pkey=>$ConnectorData)
        {
            foreach($ConnectorData['fields'] as $ConnectedFields)
            {
                $FieldList[$ConnectedFields['fieldId']]=$ConnectedFields['fieldLabel'];
            }
        }

        /*
        echo "<pre>";
        print_r($ConnectorFieldsData);
        echo "</pre>";

        echo "<pre>";
        print_r($FieldNameData);
        echo "</pre>";
        */

        if(count($FieldNameData))
        {
            foreach($FieldNameData as $GroupKey=>$tmpFields)
            {
                $objRelation=$ConnectorRelationshipData[$GroupKey];
                ?>
                <form class="connector_csv_generate_download" action="core/tabs/connector_csv_generate_download" method="post" onsubmit="$(this).slideUp();$(this).slideUp();$('#CSVFieldsSelection').modal('toggle');">

                    <input type="hidden" name="CurrentConnectorDownLoadType" value="<?php echo $_POST['CurrentConnectorDownLoadType'];?>"/>

                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title"><label>Plase select column to download</label></h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <ul class="col-xs-12 col-sm-12 col-md-12 col-lg-12 CDcolumnList no-pad-left no-pad-right">


                                <?php foreach($tmpFields as $FieldName) { ?>

                                        <li class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="font-size:14px;">
                                            <input type="checkbox" style="width:30px; height:15px;" checked name="CSVHeader[<?php echo $GroupKey;?>][]" value="<?php echo $FieldName;?>"/> <?php echo $FieldList[$FieldName];?>

                                        </li>

                                <?php } ?>

                                    <li class="col-xs-12 col-sm-12 col-md-6 col-lg-6"  style="font-size:14px;">
                                        <input type="checkbox" checked style="width:30px; height:15px;" name="CSVHeader[<?php echo $GroupKey;?>][]" value="ConnectorRelationship"/> Connector Relationship

                                    </li>


                            </ul>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <button class="btn btn-default col-xs-12 col-sm-12 col-md-6 col-lg-6 no-pad-right no-pad-left"  style="float: right;" >
                    <i class="fa fa-download" aria-hidden="true"></i> <span>Download CSV</span>
                    </button>
                </form>
                <?php
                break;
            }
        }
        else
        {
            ?>
            <div class="box">
                <div class="box-body">
                    <table class="table table-bordered">
                        <tr><td align="center" style="height:50px; padding:20px;"> Record not found!</td></tr>
                    </table>
                </div>
            </div>
            <?php
        }

    }
}
