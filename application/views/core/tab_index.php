<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 31/05/16
 * Time: 1:37 PM
 */
$replaceArr = array(' ','#','!','@','$','%','&','*','(',')','?',',','.','/','\\',':',';','"',"'",'[',']','-','=','+','|','{','}','<','>');

?>
<div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#macanta-tabs" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand">
        <?php
        foreach($menu as $MenuName => $MenuSetting) {
            $NiceMenu = $MenuName;
            $MenuName = str_replace($replaceArr, '_', $MenuName);
            if ($MenuSetting['active']) echo '<i class="' . $MenuSetting['icon'] . '"></i>' . $NiceMenu;
        }
        ?>
    </a>
</div>
<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 tab-menu collapse navbar-collapse"  id="macanta-tabs"> <!-- required for floating -->
    <!-- Nav tabs -->
    <ul class="nav nav-tabs tabs-left">
        <?php
        foreach($menu as $MenuName => $MenuSetting){
            $NiceMenu = $MenuName;
            $MenuName = str_replace($replaceArr,'_',$MenuName);
            ?>

            <li class="tab<?php echo $MenuName;?> <?php echo $MenuSetting['active'] ? "active":"" ;?>"><a href="#<?php echo $MenuName;?>" data-toggle="tab"><i class="<?php echo $MenuSetting['icon'];?>"></i> <?php echo $NiceMenu;?></a></li>
        <?php }
        ?>

    </ul>
</div>
<div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 tab-body">
    <!-- Tab panes -->
    <div class="tab-content">
        <?php
        foreach($menu as $MenuName => $MenuSetting){
        $NiceMenu = $MenuName;
        $MenuName = str_replace($replaceArr,'_',$MenuName);
        ?>
            <div class="tab-pane  <?php echo $MenuSetting['active'] ? "active":"" ;?>"" id="<?php echo $MenuName;?>"> <?php echo implode("\r\n",$content[$NiceMenu]);?></div>        <?php }
        ?>

    </div>
</div>