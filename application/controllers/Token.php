<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');
class Token extends MY_Controller
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }
    public function index(){
        // create a test query to rucksack lib to testquickly token expire
    }

    public function test($Id="%"){
        require_once (APPPATH."libraries/rucksack/Rucksack.php");
        $userFn = 'readRecord';
        $returnFields = array(
            '"Company"',
            '"CompanyID"',
            '"Country"',
            '"Email"',
            '"FirstName"',
            '"Id"',
            '"LastName"',
        );
        $returnFieldsStr = implode(', ',$returnFields);
        $action_details = '{"table":"Contact","limit":"1","page":0,"fields":['.$returnFieldsStr.'],"query":{"Id":"'.$Id.'"}}';
        $test = new Rucksack();
        $test->callMethod($userFn,$action_details) ;
        header("Content-Type: text/plain");
        echo  "Data: ". $test->MasterResults['Data']."\n";
        echo  "Message: ". json_encode($test->MasterResults['Message'])."\n";
        echo  "Errors: ". json_encode($test->MasterResults['Errors'])."\n";

    }

}
