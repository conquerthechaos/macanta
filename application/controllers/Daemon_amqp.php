<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . '/libraries/Threads.php';
class Daemon_amqp extends CI_Controller
{
    public $BUILD;
    public $IDEN;
    public $SERVER_ID;
    public $SERVICE_NAME;
    public $SERVER_LOG;
    public $AppName;
    public $PROCS = [];
    public $MessageTable = 'campaign_requests';
    private static $instance;
    public function __construct(){
        parent::__construct();
        $CI =& get_instance();
        foreach($CI->Siteconfig->get_all()->result() as $site_config)
        {
            $CI->config->set_item($site_config->key,$site_config->value);
        }
        $this->load->database();
        $this->load->dbforge();

        /*Crate Table If not exist*/
        $fields = array(
            'message' => array(
                'type' => 'TEXT',
                'null' => TRUE,
            ),
            'date_received' => array(
                'type' => 'DATETIME',
                'null' => TRUE,
            ),
            'status' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'default' => 'ready',
            ),
        );
        $this->dbforge->add_field("id");
        $this->dbforge->add_field($fields);
        $this->dbforge->create_table($this->MessageTable, true);

        $this->config->load('version', FALSE, TRUE);
        self::$instance =& $this;

        $this->BUILD = $this->config->item('macanta_verson');
        $this->IDEN = "macanta";
        $this->SERVER_ID = $this->config->item('IS_App_Name');;
        $this->SERVICE_NAME = 'consumer';
        $this->SERVER_LOG = SERVICEPATH.'apps';
    }
    public static function &get_instance()
    {
        return self::$instance;
    }

    public function start_campaign(){
        $request = "start-campaign";
        $this->producer($request);
    }
    public function set_next_sequence(){
        $request = "set-next-sequence";
        $this->producer($request);
    }
    public function producer($request){
        header("Content-Type: text/plain");
        error_reporting(E_ALL);
        set_time_limit(0);

        $RequiredParam = [
            "start-campaign"=>
                [
                    "campaignId",
                    "connected_data",
                    "relationship"
                ],
            "set-next-sequence"=>
                [
                    "contactId",
                    "currentSequence",
                    "nextSequence"
                ]
        ];

        $MessageStr = $this->input->raw_input_stream;
        $MessageArr = [];

        $MessageArrTemp = [];
        if($this->isJson($MessageStr)){
            $MessageArr = json_decode($MessageStr, true);
        }else{
            $MessageStr = str_replace('.','_dot_',$MessageStr);
            parse_str($MessageStr,$MessageArr);
        }
        foreach ($MessageArr as $theKey => $theValue){
            $theKey =  str_replace('_dot_','.',$theKey);
            $theValue =  str_replace('_dot_','.',$theValue);
            $MessageArrTemp[$theKey] = $theValue;
        }
        $MessageArr=$MessageArrTemp;
        $RequiredItems = $RequiredParam[$request];
        $Error = [];
        foreach ($RequiredItems as $RequiredItem){
            if(!array_key_exists($RequiredItem,$MessageArr)) {
                $Error[] = "Bad Request: missing '$RequiredItem'";
            }
        }
        if(sizeof($Error ) == 0){
            $Work = ['request'=>str_replace('-','_',$request),'data'=>$MessageArr];
            $message = json_encode($Work);
            /*Store Messages*/
            $DBData = [
                "message" => $message,
                "date_received" => date("Y-m-d H:i:s"),
                "status" => "ready"
            ];
            $this->db->insert($this->MessageTable, $DBData);
            $this->output->set_status_header(200); // ok;
            $this->output->set_output($message);
        }else{
            $Error = implode("\n", $Error);
            $this->output->set_status_header(400); // ok;
            $this->output->set_output($Error."\nPassed Data: \n". print_r($MessageArr, true));
        }

        //$this->output->_display();
    }
    private function consumer_init($config){
        set_time_limit(0);
        //Set to true to run as system service
        $Message = '['.gmdate('Y-m-d H:i:s').'] >> Script Version: '.$this->BUILD."\n";
        $this->stdout_log($Message);

        $this->as_service = true;
        $this->last_db_log = 0;
        $this->verbose	=	(isset($config['verbose']))?$config['verbose']:false;
        if(is_array($_SERVER['argv']) AND in_array('--verbose', $_SERVER['argv']) ) $this->verbose = true;

        $Message='['.gmdate('Y-m-d H:i:s').'] >> Service Started in '.ENVIRONMENT.' PID: '.getmypid()."\n";
        $this->stdout_log($Message);
        $this->logit_init(true);

        $this->logit('SERVICE', '-------------------------------------------------');
        $this->logit('SERVICE', 'Running Macanta Consumer in version: '.$this->BUILD);
        $this->logit('SERVICE', 'Environment '.strtoupper(ENVIRONMENT));
        $this->debug	=	(isset($config['debug']))?$config['debug']:false;
    }
    public function consumer($appname){
        $this->AppName = $appname;
        $config = array();
        $config['name'] = $this->config->item('DAEMON_SERVICE_NAME');
        $config['verbose'] = $this->config->item('DAEMON_SERVICE_VERBOSE');
        $config['IS_App_Name'] = $this->config->item('IS_App_Name');
        $this->consumer_init($config);
        $this->logit('SERVICE', 'Macanta Consumer Service Started sucessfully w PID: '.getmypid());

        while ($this->get_message()) {
            sleep(5);
        }

        $this->logit('SERVICE', '-------------------------------------------------');
        $this->logit('SERVICE', 'Shutting Down Macanta Consumer Service From RabbitMQ Message (quit)');
        $this->logit('SERVICE', '-------------------------------------------------');
    }
    function get_message(){
        global $servicepleasestop;
        if((isset($servicepleasestop) && $servicepleasestop === true)) return false;

        $this->db->where('status','ready');
        $query = $this->db->get($this->MessageTable);
        if (sizeof($query->result()) > 0){
            foreach ($query->result() as $row) {
                $message = $row->message;
                $this->process_message($message);
                // Remove consumed messages
                $this->db->where('id', $row->id);
                $this->db->delete ($this->MessageTable);
            }
        }
        return true;
    }
    public function sequence_trigger(){
        $Message =  "[".date('Y-m-d H:i:s')."]".' >> Run: infusionsoft_trigger_move_to_next_sequence'."\n";
        $this->trigger_log($Message);
        $pidfile = fopen($this->SERVER_LOG.'/'.$this->SERVER_ID.'/trigger.pid', 'w');
        fwrite($pidfile, getmypid());
        fclose($pidfile);
        while (true){
            ob_start();
            infusionsoft_trigger_move_to_next_sequence();
            $Message = ob_get_contents();
            $this->trigger_log($Message);
            ob_end_clean();
            sleep(5);
        }
    }
    public function thread($ProcId,$AppName, $Message ){
        $ThreadsManager = new ThreadsManager($ProcId,$AppName);
        $ThreadsManager->addTask( new ProcessTask($Message,$AppName, $ProcId, $this->BUILD) );
        $ThreadsManager->run($AppName);
    }
    public function process_message($message)
    {
        $ProcId = 100001;
        $LastProcIdFile = SERVICEPATH.'apps/' . $this->AppName . '/last.pid';
        if (!file_exists($LastProcIdFile)){
            file_put_contents($LastProcIdFile, $ProcId);
        }else{
            $ProcId = file_get_contents($LastProcIdFile);
            $ProcId++;
            file_put_contents($LastProcIdFile, $ProcId);
        }
        $Message = "[".date("Y-m-d H:i:s")."] >> Process# $ProcId Run In The Background\n";
        $this->stdout_log($Message);
        if($this->as_service){
            $this->logit_init();
            $encodedBody =  base64_encode($message);
            $this->thread($ProcId,$this->AppName, $encodedBody );
        }
    }
    public function logit_init($init = false){
        if($init OR ($this->day != gmdate('Ymd')) ){
            $this->day	=	gmdate('Ymd');
            $dir = $this->SERVER_LOG.'/'.$this->SERVER_ID.'/';
            if(isset($this->logfile)){fclose($this->logfile);}
            $this->logfile = fopen($dir.'consume.log', 'a');
            /*if(!$wasdir){
                //log that new folder wsa created
                $this->logit('SERVICE', 'New Folder Created.('.$dir.')');

                //Remove old current link if exists
                if( file_exists( $this->SERVER_LOG.'/'.$this->SERVER_ID.'/logs/current' ) ){
                    $link_command	=	'unlink '.$this->SERVER_LOG.'/'.$this->SERVER_ID.'/logs/current';
                    exec($link_command, $link_output, $link_result);
                }
                //Link CURRENT folder to most recent folder
                $link_command	=	'ln -s '.$this->SERVER_LOG.'/'.$this->SERVER_ID.'/logs/'. $this->day.'/'.' '.$this->SERVER_LOG.'/'.$this->SERVER_ID.'/logs/current';
                exec($link_command, $link_output, $link_result);
                $this->logit('SERVICE', '+++++++++++++++++++++++++++++++++++++++++++++++++');
                //Log result of the link if directory was created, ie.was not a dir before
                if($link_output){
                    $this->logit('SERVICE', 'Relinked to current directory ('.$dir.')');
                }else{
                    $this->logit('SERVICE', 'FAILED to relink to current directory ('.$dir.')');
                }

                $this->logit('SERVICE', 'Code: '.$this->BUILD.' in '.strtoupper(ENVIRONMENT).' ENV');
                $this->logit('SERVICE', '+++++++++++++++++++++++++++++++++++++++++++++++++');

            }*/

        }

        // This only runs if its the FIRST TIME not every day
        if($init){
            //Write pid file ONLY RAN ON First loop execution
            $pidfile = fopen($this->SERVER_LOG.'/'.$this->SERVER_ID.'/consumer.pid', 'w');
            fwrite($pidfile, getmypid());
            fclose($pidfile);
        }

    }
    public function logit($who, $string){

        if($this->as_service){
            // Log to file
            fwrite($this->logfile, '['.gmdate('Y-m-d H:i:s').'] '.$who.' >> '.$string."\n");

            // If verbose throw to STDOUT too
            if($this->verbose){
                echo '['.gmdate('H:i:s').'] '.$who.' >> '.$string.chr(10);
            }
        }else{
            // Web browser
            echo $who.' >> '.$string.'<br>';
        }

    }
    public function stdout_log($Message){
        file_put_contents($this->SERVER_LOG.'/'.$this->SERVER_ID.'/stdout.log', $Message, FILE_APPEND);
    }
    public function trigger_log($Message){
        file_put_contents($this->SERVER_LOG.'/'.$this->SERVER_ID.'/trigger.log', $Message, FILE_APPEND);
    }
    public function isJson($string) {
        json_decode(trim($string));
        return (json_last_error() == JSON_ERROR_NONE);
    }
}